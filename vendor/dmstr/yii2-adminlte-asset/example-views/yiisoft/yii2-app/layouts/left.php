<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <!--  esta es la imagen por default-->
                <!-- <img src="<?//= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/> -->
                <img src="<?= Yii::$app->user->identity->image ?>" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->firstname.' '.Yii::$app->user->identity->lastname ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
<!--Yii::$app->params['superUserRole'])-->                    
        <?php echo dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu'],
                                        
                    'items' => [                   
                        ['label' => 'Menu List', 'options' => ['class' => 'header']],                        
                        [
                            'label' => 'Creditos',
                            'icon' => 'fa fa-credit-card',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Niveles de Credito', 'icon' => 'fa fa-credit-card', 'url' => ['/level'],],
                                ['label' => 'Montos', 'icon' => 'fa fa-money', 'url' => ['/level-amount'],],
                                ['label' => 'Plazos', 'icon' => 'fa fa-dashboard', 'url' => ['/period'],],
                                ['label' => 'Cuotas', 'icon' => 'fa fa-pencil-square-o', 'url' => ['/quota'],],                                
                            ],
                            'visible'=> Yii::$app->user->identity->id_role == Yii::$app->params['superUserRole'] ? true : false,
                        ],
                        [
                            'label' => 'Sistema',
                            'icon' => 'fa fa-gear',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Preguntas del Sistema', 'icon' => 'fa fa-question', 'url' => ['/question-system'],],
//                                ['label' => 'Mensajes del Sistema', 'icon' => 'fa fa-envelope-o', 'url' => ['/message-default'],
  //                                  'visible'=> Yii::$app->user->identity->id_role == Yii::$app->params['superUserRole'] ? true : false,], 
                                ['label' => 'Estatus del Sistema', 'icon' => 'fa fa-heartbeat', 'url' => ['/status'],
                                'visible'=> Yii::$app->user->identity->id_role == Yii::$app->params['superUserRole'] ? true : false,],                                
                                ['label' => 'Bancos Registrados', 'icon' => 'fa fa-bank', 'url' => ['/banks'],
                                    'visible'=> Yii::$app->user->identity->id_role == Yii::$app->params['superUserRole'] ? true : false,],                                
                                ['label' => 'Invitaciones a Prestatarios',  'icon' => 'fa fa-send-o', 'url' => ['/client-invitation'],],         
                                ['label' => 'Trabajadores del Sistema',  'icon' => 'fa fa-user-secret', 'url' => ['/user'],
                                    'visible'=> Yii::$app->user->identity->id_role == Yii::$app->params['superUserRole'] ? true : false,], 
                                ['label' => 'Roles de Usuarios', 'icon' => 'fa fa-sitemap', 'url' => ['/user-role'],
                                    'visible'=> Yii::$app->user->identity->id_role == Yii::$app->params['superUserRole'] ? true : false,], 
                                ['label' => 'Configuracion del Sistema', 'icon' => 'fa fa-gear', 'url' => ['/config'],
                                    'visible'=> Yii::$app->user->identity->id_role == Yii::$app->params['superUserRole'] ? true : false,], 
                                ['label' => 'Cuentas Bancarias', 'icon' => 'fa fa-home', 'url' => ['/system-bank'],
                                    'visible'=> Yii::$app->user->identity->id_role == Yii::$app->params['superUserRole'] ? true : false,], 

                            ],
                        ],                        
                        [
                            'label' => 'Administracion',
                            'icon' => 'fa fa-server',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Solicitudes de Prestamos', 'icon' => 'fa fa-money', 'url' => ['/site/solicitudes'],],
                                ['label' => 'Clientes Registrados', 'icon' => 'fa fa-users', 'url' => ['/clients/index'],],                                
                                ['label' => 'Liquidaciones de Prestamos', 'icon' => 'fa fa-book', 'url' => ['/site/liquidaciones-prestamos'],],                                
                                ['label' => 'Cobranzas',  'icon' => 'fa fa-bank', 'url' => ['/record-payment/index'],],         
                                ['label' => 'Cartera de Aportes',  'icon' => 'fa fa-credit-card', 'url' => ['/site/cartera-credito'],], 
                              //  ['label' => 'Registros Contables',  'icon' => 'fa fa-bar-chart', 'url' => ['/#'],], 
                              //  ['label' => 'Procesos Diarios',  'icon' => 'fa fa-exchange', 'url' => ['/#'],],                                 
                                ['label' => 'Generacion de Mensajes',  'icon' => 'fa fa-envelope-o', 'url' => ['/message-system/index'],],                                 
                                ['label' => 'Calendario Bancario',  'icon' => 'fa fa-calendar', 'url' => ['/bank-holiday'],], 
                                ['label' => 'Comisiones a Padrinos',  'icon' => 'fa fa-line-chart', 'url' => ['/commission-round/index'],],                                 
                                
                                ['label' => 'Consultas',  'icon' => 'fa fa-search', 'url' => ['/consulta'],],
//                                ['label' => 'Consultas',  'icon' => 'fa fa-search', 'url' => ['/#'],],
                                
                                
                                
                            ],
                        ],         
                        
                    ],
                ]) 
                
        
        ?>

    </section>

</aside>
