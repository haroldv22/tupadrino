<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use frontend\models\Loan;
use frontend\models\LoanQuota;
use frontend\models\Level;
use frontend\models\Period;
use yii\helpers\ArrayHelper;


use beastbytes\wizard\WizardBehavior;

class CreditoController extends Controller
{

  /**
     * @inheritdoc
     */
    public function behaviors()
    {        
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['vigente','historico','view','solicitud','getperiod','complete'],
                        'allow' => true,
                        'roles' => ['@'],  
                    ],
                ],
            ],
            'solicitud'=>[
                'class'=>'beastbytes\wizard\WizardBehavior',
                'steps' => ['Presolicitud','VerificacionFinal'],
                'events' => [
                    WizardBehavior::EVENT_WIZARD_STEP => [$this,'registrationWizardStep'],
                    WizardBehavior::EVENT_AFTER_WIZARD => [$this,'registrationAfterWizard'],
                    WizardBehavior::EVENT_INVALID_STEP => [$this, 'invalidStep']
                ]
            ]

        ];
    }

        public function actionComplete()
    {
        $this->layout = "solicitud";
        return $this->render('complete');
    }

    /**
     * Solicitud de Credito.
     *----------------------
     */
    public function actionSolicitud($step = null){
        //if ($step===null) $this->resetWizard();
        $this->layout = "solicitud";

        $useId= Yii::$app->user->identity->id;
        $modLoan = Loan::find()->where(['id_client'=>$useId, 'id_status'=>[20,41,42,43]]);
        if(!$modLoan->exists())
            return $this->step($step);

        return $this->render('invalid');
        

    }


     public function registrationWizardStep($event)
    {   

        if (empty($event->stepData)) {
            $modelName = 'frontend\\models\\solicitud\\'.($event->step);
            $model = new $modelName();


            //Asigno el valor del interes al modelo
            if($event->step=='Presolicitud'){
                $modLevel = new level();
                $model->rate = $modLevel->getRate(Yii::$app->user->identity->id_level);
            }
        } else {
            $model = $event->stepData;
        }

        $post = Yii::$app->request->post();
        if (isset($post['cancel'])) {
            $event->continue = false;
        } elseif (isset($post['prev'])) {
            $event->nextStep = WizardBehavior::DIRECTION_BACKWARD;
            $event->handled  = true;
        } elseif ($model->load($post) && $model->validate()) {
            $event->data    = $model;
            $event->handled = true;

           // print_r($event->data);
        } else {
            if ($event->step=='VerificacionFinal'){
                $data = $this->read();
            }
            else{
                $data = null;
            }
            $event->data = $this->render($event->step, compact('event', 'model','data'));
        }
    }
    public function registrationAfterWizard($event)
    {   
        $this->layout = "solicitud";
        if ($event->step === null) {
            $event->data = $this->render('cancelled');
        } elseif ($event->step) {//Registro Completo Guardado

            //Guardado en base de datos
            if($this->saveData($event->stepData)==true){
                $event->data = $this->render('complete');
            }

        } else {
            $event->data = $this->render('notStarted');
        }
    }

    public function invalidStep($event)
    {
        $event->data = $this->render('invalidStep', compact('event'));
        $event->continue = false;
    }

        //separo el numero de dias del id
    function Myperiod($data){
        return explode("-", $data);
    }
     private function saveData($data){

            $transaction = Yii::$app->db->beginTransaction();
            $Presolicitud=$data['Presolicitud'][0]->toArray();

            try {
                #Guardado info del credito
                $modelLoan = new Loan();
                $modelLoan->attributes = $Presolicitud;
                $modelLoan->id_client = Yii::$app->user->identity->id;
                $modelLoan->date_add = $modelLoan->newDate(date('j-m-Y'));///lamoalafuncion q verifica la hora
                $modelLoan->id_status = 20;
                $Myperiod = $this->Myperiod($Presolicitud['period']);


                $Asiganacion = Loan::defAsignacion($Presolicitud['quota'],
                                                        $Myperiod[1],
                                                        $Presolicitud['rate'],
                                                        $Presolicitud['amount']);

                $modelLoan->total_interest = $Asiganacion['total_interest'];
                $modelLoan->total_pay = $Asiganacion['total_pay'];
                $modelLoan->cant_quota = $Asiganacion['nQuota'];    
                $modelLoan->period = $Myperiod[1];
                $modelLoan->points = $Myperiod[0];     
                $modelLoan->save();

                        
                #Guardado de cuotas del credito 
                $tablaPago = Loan::TablaPago($Presolicitud['quota'],
                                                $Myperiod[1],
                                                $Presolicitud['rate'],
                                                $Presolicitud['amount']);

                foreach ($tablaPago as $key => $value) {
                    $modelLoanQt = new LoanQuota;
                    $modelLoanQt->attributes = $value;
                    //asigno pending pay
                    $modelLoanQt->pending_pay=$value['quota'];
                    $modelLoanQt->id_loan = $modelLoan->id;
                    $modelLoanQt->id_status = 31;
                    $modelLoanQt->save();
                }

                $transaction->commit();
                $result = true;             
                
                } catch (Exception $e) {
                    $transaction->rollBack();
                    $result =false;
                }
                return $result;


     }
    /*
    ** Fin solicitud de Credito
    **---------------------------
    */

    public function actionVigente()
    {
        $useId= Yii::$app->user->identity->id;
        $modLoan = Loan::find()->where(['id_client'=>$useId]);

        $quest = $modLoan->andwhere(['id_status'=>20])->exists();
        if($quest){
            return $this->render('noVigente',['status'=>'espera']);
        }

        $modLoan = Loan::find()->where(['id_client'=>$useId]);

        $quest = $modLoan->andwhere(['id_status'=>[41,42,43]])->exists();

        if($quest){
            $query = LoanQuota::find()->where(['id_loan'=>$modLoan->one()->id])
                                    ->with(['idStatus','arrears'])
                                    ->orderBy('n_quota ASC');

            $modQuota = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [],
                'sort' => false,
                
            ]);

            return $this->render('vigente',['modLoan'=>$modLoan->one(),
                                                    'modQuota'=>$modQuota
                                                    ]);
        }
        
        return $this->render('noVigente',['status'=>'noposee']);

        

       // 
    }


    public function actionHistorico()
    {
        $useId= Yii::$app->user->identity->id;
        $dataProvider = new ActiveDataProvider([
            'query' => Loan::find()->where(['id_client'=>$useId])
                                    ->with(['idStatus']),
        ]);

        return $this->render('historico', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Loan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {

        $useId= Yii::$app->user->identity->id;
        $modLoan = Loan::find()->where(['id_client'=>$useId, 'id'=>$id]);
        if($modLoan->exists()){

            $query = LoanQuota::find()->where(['id_loan'=>$modLoan->one()->id])
                                    ->with(['idStatus','arrears'])
                                    ->orderBy('n_quota ASC');

            $modQuota = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [],
                'sort' => false,
            ]);

            return $this->render('view',['modLoan'=>$modLoan->one(),
                                        'modQuota'=>$modQuota
                                        ]);
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

   
    /**
     * Finds the Loan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Loan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Loan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

        //Permite obtener los periodos validos para
    //un monto determinado
    public function actionGetperiod(){ 
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $period = Period::find()
                ->innerJoin('period_amount','period_amount.id_period= period.id')
                ->innerJoin('level_amount','level_amount.id=period_amount.id_level_amount')
                ->where(['level_amount.amount'=>$data['key']])
                ->orderBy('level_amount.amount ASC');
            if($period->exists()){
                $period = $period->all();
                $listperiod = ArrayHelper::map($period, 'code','alias');
                $validos='';
                foreach ($listperiod as $key => $value) {
                    $validos.="<option value='$key'>$value</option>";
                }
                
                return $validos;
            }
        }
    }
    

}
