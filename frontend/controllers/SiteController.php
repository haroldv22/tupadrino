<?php
namespace frontend\controllers;

use Yii;
use frontend\models\Loan;
use frontend\models\login\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\ClientInvitation;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\ResetPass;
use  yii\web\Session;



/**
 * Site controller
 */
class SiteController extends Controller
{
    public $layout = "site";
    /**
     * @inheritdoc
     */
  

    public function behaviors()
    {        
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login','password-reset-step1','password-reset-step2'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => [
                                        'logout','index','faq','invitacion','sistema-grafico',
                                        'sistema','pago-cancelaciones','calendario','rapidez-confianza',
                                        'oportunidades','error','contact','captcha'
                                    ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            
        ];
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

/*
    public function beforeAction($action)
    {
        //if(\Yii::$app->user->isGuest)
         //   return $this->redirect(['site/login']);
        

        return true;
        
    }
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex($alert=false)
    {

        return $this->render('index',['alert'=>$alert]);
    }

    public function actionFaq(){
        return $this->render('preguntas');
    }
    public function actionInvitacion(){
        return $this->render('invitacion');
    }
    public function actionSistema(){
        return $this->render('sistema');
    }
    public function actionPagoCancelaciones(){
        return $this->render('pagos');
    }
    public function actionCalendario(){
        return $this->render('calendario');
    }
    public function actionSistemaGrafico(){
        return $this->render('sistemagrafico');
    }
    public function actionRapidezConfianza(){
        return $this->render('rapidez');
    }
    public function actionOportunidades(){
        return $this->render('oportunidades');
    }





    public function actionLogin()
    {
        $this->layout = 'nomain';

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            return $this->redirect(['site/index', 'alert'=>$this->alertVerifid()]);
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }

    }
    
    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionPasswordResetStep1()
    {
        $this->layout = 'resetPass';

        $model = new ResetPass(['scenario'=>'step1']);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $newmodel = new ResetPass(['scenario'=>'step2']);
            $session = new Session;
            $session->open();
            $session['email'] =$model->email;

            return $this->redirect(['site/password-reset-step2']);


        }

        return $this->render('resetStep1', [
            'model' => $model,
        ]);
    }
    public function actionPasswordResetStep2(){
        $this->layout = 'resetPass';
        $model = new ResetPass(['scenario'=>'step2']);
        $model->email = Yii::$app->session['email'];
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                
                if ($model->sendEmail()) {
                    Yii::$app->session->setFlash('success', 'Una nueva contraseña fue enviada a su correo.');
                    return $this->redirect(['site/login']);
                }
                else {
                    Yii::$app->session->setFlash('error', 'Lo sentimos, No pudimos enviarte una nueva contraseña.');
                }
            }

        return $this->render('resetStep2', [
            'model' => $model,
        ]);
    }


    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token){
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    private function alertVerifid(){
       $id=Yii::$app->user->identity->id;
       $exist = Loan::find()->where(['id_client'=>$id, 'id_status'=>[42,43]])->exists();
       return $exist;
    }
    
    public function actionContact()
    {
        $model = new ContactForm();
        $model->name =Yii::$app->user->identity->Shortname();
        $model->email = Yii::$app->user->identity->email;
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

}
