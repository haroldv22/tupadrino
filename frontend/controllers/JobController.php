<?php

namespace frontend\controllers;

use Yii;

#actionGetPayment
use frontend\models\RecordPayment;
use frontend\models\TdcPayment;
$path = Yii::getAlias("@vendor/mercadopago/yii2-lib/mercadopago.php");
require_once($path);

use frontend\models\Loan;
use frontend\models\LoanQuota;
use frontend\models\CommissionRound;
use frontend\models\Commission;
use frontend\models\Arrears;
use frontend\models\ArrearsDetails;
use frontend\models\Config;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CommissionRoundController implements the CRUD actions for CommissionRound model.
 */
class JobController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

public function actionGetPayment(){

  $mp = new \MP ('APP_USR-3163460913815188-092714-b7b4689715672745ba752ac1d8ec27f7__LB_LD__-226054726');
  $modRecordPayments = RecordPayment::findAll(['method'=>'TDC', 'id_status'=>50]);

  foreach ($modRecordPayments as $modRecordPayment) {
      $transaction = Yii::$app->db->beginTransaction();

      try {
        $reference = $modRecordPayment->reference;
        $TdcPayment= $mp->get ("/v1/payments/{$reference}");
        
        if($TdcPayment['status']==200){
          $modTdcPayment = new TdcPayment;
          $idRecordPayment = $modTdcPayment->ProcesarPagoTdc($modRecordPayment,$TdcPayment,$modTdcPayment,false);
          error_log('PAGO-TDC[EXITO]: '.$idRecordPayment);
        }
        else{
          error_log('PAGO-TDC[SINREPUESTA]: '.json_encode($TdcPayment));
        }

        $transaction->commit();
      }
      catch (\Exception $e) {
        error_log('PAGO-TDC[ERROR]: '.$e->getMessage());
        $transaction->rollBack();
      }    
  }
}

/*
//Action que Cambia los Status de las Quotas
public function actionQuotaStatusChange(){
  $this->GeneradoStatus();
  $this->VencidoStatus();
  $this->MoraUltimaCuota();
  $this->MorasDiarias();
}
/*-----------------------------
-------------------------------
* Metodos Principales De Estatus de Cuota *
-------------------------------
-------------------------------*/
  /**
   * Cambia el estatus de las cuotas a generadas
        
   public function GeneradoStatus(){
    

     // Obtiene todos los prestamos sean vigentes, vencidos o en mora
     $models = Loan::find()
                  ->where(['id_status'=>[
                                          Yii::$app->params['status_vigente'],
                                          Yii::$app->params['status_vencido'],
                                          Yii::$app->params['status_mora']]
                                        ])
                  ->all();


      // Recorre las cuotas de ese prestamo    
      foreach ($models as $loan) {

        $loanQUpdated = LoanQuota::updateAll(
                                              ['id_status' => Yii::$app->params['cuota_generada']],
                                              'id_status ='.Yii::$app->params["cuota_noGenerada"].'
                                               AND date_pay <= :date AND id_loan = :id_loan',
                                              ['date'=>date('Y-m-j'),'id_loan'=>$loan->id]
                                            );

        // Si es mayor que cero que le envie un mensaje a este cliente
        /*
        if($loanQUpdated > 0){
          
          $email      = $loan->getIdClient()->one()->email;         // email del cliente
          $clientName = $loan->getIdClient()->one()->Shortname();   // Nombre del cliente           
          
          $this->sentQuoteGenerated($email,$clientName); // envie un mensaje al usuario para 
                                                       // notificarle se han generado sus cuotas
        }       
     }  
   }
  
   /**
    * Cambia el estatus de cuotas a vencido
    *
    public function VencidoStatus(){
      
      // Busca las cuotas generadas con fecha vencida
      $models= LoanQuota::find()
                      ->where(['id_status'=>Yii::$app->params['cuota_generada']])
                      ->andWhere(['<=', 'date_expired', date('Y-m-j')])
                      ->all();

      // Recorre todas las cuotas expiradas para asignarle a su cuota vencida
      foreach ($models as $LoanQuota) {
          
          $LoanQuota->id_status = Yii::$app->params['cuota_vencida'];
          $LoanQuota->update();

          $this->buscarMora($LoanQuota);

      }
    }
    
  /**
    * Cambia el estatus de la "ultima cuota" a vencido
    *
    public function MoraUltimaCuota(){
      $Loans = Loan::find()->where(['id_status'=>[
                                          Yii::$app->params['status_vigente'],
                                          Yii::$app->params['status_vencido'],
                                          Yii::$app->params['status_mora']
                                          ]
                                  ])->all();

      foreach ($Loans as $Loan) {
        $LoanQuota = $Loan->getLoanQuotas()
                                ->where(['id_status'=>Yii::$app->params['cuota_vencida']])
                                ->orderBy('n_quota DESC')->one();
        if($LoanQuota){
          //vuelvo a filtrar 
          $n_quota = $LoanQuota->n_quota + 1;          
          //Busco la cuota siguiente
          $NextQuota = LoanQuota::find()->where([
                                        'id_loan'=>$LoanQuota->id_loan,
                                        'n_quota'=>$n_quota
                                      ])->exists();
          if(!$NextQuota){//Si no Arroja resultado es la ultima sin duda
            $iniDate= $this->CalcFechaGenMora($LoanQuota); //genero la fecha en la que cae en mora
            if(strtotime(date('Y-m-j'))>=strtotime($iniDate)){
              $this->CalcularGuardarMorasBD(date('Y-m-j'),date('Y-m-j'),$LoanQuota,$LoanQuota->pending_pay);
              $this->CommissionStatus($Loan);

              //Actualizo LoanQuota
              $LoanQuota->id_status= Yii::$app->params['cuota_mora'];
              $LoanQuota->have_arrear = true;
              $LoanQuota->update();

              //actualizo Loan
              if($Loan->id_status != Yii::$app->params['status_mora']){                
                $Loan->id_status = Yii::$app->params['status_mora'];
                $Loan->update();
              }
            }
          }
        }
      }
    }

    /**
     * encargado de buscar Moras Diarias  
     * tambien cambia el estatus
     * de morosida a las que se les ha pagado ya la cuota y no se reflejaron 
     * cuango se hizo la conciliacion. 
     *
    public function MorasDiarias(){
        // Busca las cuotas con en mora
        $models= LoanQuota::find()
                        ->where(['id_status'=>Yii::$app->params['cuota_mora']])
                        ->orderBy('id ASC')
                        ->all();
        foreach ($models as $LoanQuota) {
          //Llamo la tabla arrears(Mora)asociadas con la quota
          $modelArrears = $LoanQuota->getArrears()->one();

          if($modelArrears){///verifico si tiene moras ya creadas para sumarle mas
            if(strtotime($modelArrears->upd_date)< strtotime(date('Y-m-j'))){
              $iniDate =date('Y-m-j', strtotime ('+1 day',strtotime($modelArrears->upd_date)));
              $this->CalcularGuardarMorasBD($iniDate,date('Y-m-j'),$LoanQuota,$LoanQuota->pending_pay);
            }else{//en caso de que tenga las moras al dia, paso al siguiente ciclo
              continue;
            }
          }else{///No tiene moras Asociadas en BD

            #Esta seccion nunca se usara solo en caso que falle
            #La confirmacion de Pagos

            //Llamo todas los pagos asociados a la cuota
            $modelQuotaPayments = $LoanQuota->getQuotaPayments()->orderBy('id ASC')->all();

            $QuotaPendindPay = $LoanQuota->pending_pay; //Guardo temporalmente lo que se debe de la cuota
            $iniDate= $this->CalcFechaGenMora($LoanQuota); //Guardo temporalmente la fecha en que se genero la mora
            //Verifico si tiene pagos hechos
            if($modelQuotaPayments){   
              foreach ($modelQuotaPayments as $modelQuotaPayment) {//Recorro Cada uno de los pagos que tenga asociado
                //Llamo al recordpayment asociado 
                $modelRPayment= $modelQuotaPayment->getIdRecordPayment()->one();
                //verifico si es un pago confirmado
                if($modelRPayment->id_status = Yii::$app->params['confirmado']){

                  $QuotaEndDate =date('Y-m-j', strtotime ('-1 day',strtotime($modelRPayment->date_add)));
                  ///Verifico si lo pagado es mayor o igual a lo correspondiente
                  if($modelQuotaPayment->amount>=$LoanQuota->quota){
                    if(strtotime($modelRPayment->date_add)>strtotime($iniDate)){//Si lafecha de pago esmayor a lafecha vencimiento calcu mora
                      $this->CalcularGuardarMorasBD($iniDate,$QuotaEndDate,$LoanQuota,$QuotaPendindPay);
                    }
                    $QuotaPendindPay = 0; //Establesco 0 porque si es mayor o igual no debe tener pago pendiente
                    $this->ActualizarLoanQuota($LoanQuota,$QuotaPendindPay);
                    continue 2;//Continuo hasta la siguiente interaccion de Quotas (No confundir con lainteraccion de pago)
                  }else{//Si el monto pagado es menor
                    if(strtotime($modelRPayment->date_add)>strtotime($iniDate)){//Si lafecha de pago esmayor a lafecha vencimiento calcu mora
                      $this->CalcularGuardarMorasBD($iniDate,$QuotaEndDate,$LoanQuota,$QuotaPendindPay);
                    }
                    $QuotaPendindPay=$QuotaPendindPay-$modelQuotaPayment->amount;
                    $iniDate = $modelRPayment->date_add;
                    $this->ActualizarLoanQuota($LoanQuota,$QuotaPendindPay);
                  }
                }
              }//fin for each depagos decuota
              
            }
            //En caso de que se escape alguna mora del foreach de Pago de mora
            //Tambien para los que no tienen Pagos asociados
            if($QuotaPendindPay>0){
              $this->CalcularGuardarMorasBD($iniDate,date('Y-m-j'),$LoanQuota,$QuotaPendindPay);
              $this->ActualizarLoanQuota($LoanQuota,$QuotaPendindPay);
             #PD:En esta caso tabla Loan no tiene cambios
            }
          }//Fin Else No moras Asociadas en BD
        }//Fin for each

    }
/*
-------------------------------
* FIN De Medotos principales Estatus de Cuota *
-------------------------------

    /*--------------------------------
    ----------------------------------
    Metodos segundarios de Estatus de cuota   
    ---------------------------------
    --------------------------*/

    /**
     * Metodo encargado de buscar las 
     * cuotas o prestamos en mora
     * @param $model modelo LoanQuota 
     *
     protected function buscarMora($model){

        //Obtengo  el porcentaje de la mora
        $modelConfig = Config::find()
                             ->where(['id_option'=> Yii::$app->params['option_mRate']])
                             ->one();
          
          //resto 1 a n_quota para poder traer la cuota anterior
          $n_quota = $model->n_quota - 1;
          
          if ($n_quota!=0) {
              //Busco la cuota para identificar luego si esta vencidad
              $LoanQuota = LoanQuota::findOne([
                                                'id_status'=>Yii::$app->params['cuota_vencida'],
                                                'id_loan'=>$model->id_loan,
                                                'n_quota'=>$n_quota
                                              ]);
              
              if($LoanQuota){ //verifico si existe  
                  //cambio estatus a lacuota a Mora
                  $LoanQuota->id_status= Yii::$app->params['cuota_mora'];
                  $LoanQuota->have_arrear = true;
                  $LoanQuota->update();
                  //cambio e estatus loan a Mora
                  $Loan = $LoanQuota->getIdLoan()->one();
                  
                  if($Loan->id_status != Yii::$app->params['status_mora']){                
                     $Loan->id_status = Yii::$app->params['status_mora'];
                     $Loan->update();
                  }

                  $this->CalcularGuardarMorasBD(date('Y-m-j'),date('Y-m-j'),$LoanQuota,$LoanQuota->pending_pay);

                  $this->CommissionStatus($Loan);
                  //cambio de estatus comision -> en Mora,
                  // coloco otra comision -> en riesgo

              }
          }else{
              //cambio e estatus loan a vencido
              $Loan = $model->getIdLoan()->one();
              if($Loan->id_status!=43){
                 $Loan->id_status = 42;
                 $Loan->update();
              }
    
              //cambio de estatus comision a en riesgo
              $comission = $Loan->getCommissions();
              if($comission->exists()){
                  $comission = $comission->one();
                  if($comission->id_status!=65){
                      $comission->id_status = 66;
                      $comission->update();
    
                      $round = $comission->getIdCommissionRound()->one();
    
                       $round->to_pay = $round->to_pay - $comission->commission;
                      $round->risk_pay = $rount->risk_pay + $comission->commission;
                      $round->update;
                  }
               }
        }
      return;
    }

     /**
      * Metodo encargado de calcular y guardar las moras generadas
      * en unrango de fecha
      * @param $LoanQuota modelo LoanQuota
      * @param $iniDate fecha que comienza generarse mora
      * @param $pending_pay Pago pendiente con en que serealizaran los calculos de mora(porcentaje*pendientecuota)
      * @param $endDate fecha de finalizacion 
      *
    protected function CalcularGuardarMorasBD($iniDate,$endDate,$LoanQuota,$pending_pay){
        //Obtengo  el porcentaje de la mora
        $modelConfig = Config::find()
                             ->where(['id_option'=> Yii::$app->params['option_mRate']])
                             ->one();
        $iniDate=strtotime($iniDate);
        $endDate=strtotime($endDate);
        $moraValue=round($pending_pay*($modelConfig->value/100),2);//calculo valor mora
        $total = 0;
        $days = 0;
        $Loan = $LoanQuota->getIdLoan()->one();

        $modelArrears =Arrears::findOne(['id_loan_quota'=>$LoanQuota->id]);

        if(!$modelArrears){//para saber si tiene arrears y crearlo
          $modelArrears = new Arrears;
          $modelArrears->id_client= $Loan->id_client;
          $modelArrears->id_loan_quota = $LoanQuota->id;
          $modelArrears->delay_days = 0;
          $modelArrears->total_interest= 0;
          $modelArrears->id_status=45;
          $modelArrears->upd_date=date('Y-m-j') ;
          $modelArrears->pending_pay=0;
          $modelArrears->add_date=date('Y-m-j');
          $modelArrears->save();
        }
        //hago un ciclo for desde lafecha de inicio hasta fecha de hoy
        for($i=$iniDate; $i<=$endDate; $i+=86400){
          $total += $moraValue;
          ++$days;
          $modelArDetail= new ArrearsDetails;
          $modelArDetail->id_arrears = $modelArrears->id;
          $modelArDetail->add_date= date('Y-m-j',$i);
          $modelArDetail->interest_rate=$modelConfig->value;
          $modelArDetail->interest_generate= $moraValue;
          $modelArDetail->amount_base=$pending_pay;
          $modelArDetail->save();

        }
        ///Actualizo el registro nuevamente
        $modelArrears->delay_days = $modelArrears->delay_days + $days;
        $modelArrears->total_interest= round($modelArrears->total_interest+$total,2);
        $modelArrears->pending_pay=round($modelArrears->pending_pay+$total,2);
        $modelArrears->upd_date=date('Y-m-j') ;
        $modelArrears->save();

    }

     /** 
      * Metodo encargado de calcular y guardar las moras generadas
      * en unrango de fecha
      * @param $LoanQuota modelo LoanQuota
      * @param $pending_pay Pago pendiente 
      *
      protected function ActualizarLoanQuota($LoanQuota,$pending_pay){
        $status = ($pending_pay > 0) ? Yii::$app->params['cuota_mora'] : Yii::$app->params['cuota_pagada']; 
        $have_arrear = Arrears::find()->where(['id_status'=>45,'id_loan_quota'=>$LoanQuota->id])->exists();//Si la mora esta Impagada
        $LoanQuota->have_arrear = ($have_arrear == true) ? true:false;
        $LoanQuota->pending_pay = $pending_pay;
        $LoanQuota->id_status = $status;
        $LoanQuota->save();

        //Verifico Si hay otras cuotas con mora o vencida para
        //poder cambiarle el estatus al loan
        $validator= LoanQuota::find()
                        ->where(['id_status'=>Yii::$app->params['cuota_mora']])
                        ->exists();
        if(!$validator){
          $validator= LoanQuota::find()
                        ->where(['id_status'=>Yii::$app->params['cuota_vencida']])
                        ->exists();
          if(!$validator){              
            $Loan = $LoanQuota->getIdLoan()->one();
            $Loan->status=Yii::$app->params['status_pagados'];
            $Loan->save();
          }else{
            $Loan = $LoanQuota->getIdLoan()->one();
            $Loan->status=Yii::$app->params['status_vencido'];
            $Loan->save();

          }

        }

      }

    /**
      * Metodo encargado de calcular la fecha en que 
      * se comienza a generar una mora
      * @param $actualQuota modelo LoanQuota 
      *

      protected function CalcFechaGenMora($actualQuota){
          $n_quota = $actualQuota->n_quota + 1;
          //Busco la cuota siguiente
          $NextQuota = LoanQuota::findOne([
                                          'id_loan'=>$actualQuota->id_loan,
                                          'n_quota'=>$n_quota
                                          ]);
          if($NextQuota){
            return $NextQuota->date_expired;
          }else{//en caso de que sea la ultima cuota
            $Loan = $actualQuota->getIdLoan()->one();
            $days = $Loan->quota + Yii::$app->params['expired_days'];
            $date =  $actualQuota->date_pay;
            return date('Y-m-j',$Loan->addDays($days,$date));

          }

      }
      /**
      * Metodo encargado de cambiar los estatus de las comisiones
      * @param $Loan modelo Loan
      *
      protected function CommissionStatus($Loan){
        $comission = $Loan->getCommissions();
          if($comission->exists()){//Continuo siempre y cuando tenga comisiones asociadas a este loan
            $comission = $comission->one();
            //almaceno el status que tiene
            $beforeStatus= $comission->id_status;
            if($beforeStatus != Yii::$app->params['commission_mora']){
                $comission->id_status = Yii::$app->params['commission_mora'];
                $comission->update();
                //traigo la ronda
                $round = $comission->getIdCommissionRound()->one();
    
                //asigno los valores a la ronda en caso que no este en riesgo
                //Observacion: las comisiones por default deben venir en riesgo
                if($beforeStatus!=Yii::$app->params['commission_riesgo']){
                  $round->to_pay = $round->to_pay - $comission->commission;
                  $round->risk_pay = $rount->risk_pay + $comission->commission;
                }
                //traigo las demas comisiones para colocar una randon en riesgo
                $allcomision = Commission::find()
                                          ->where(['id_status'=>[
                                                                Yii::$app->params['commission_puntual'],
                                                                Yii::$app->params['commission_finalizada']
                                                                ],
                                                  'id_commission_round' => $round->id]);
    
                //Verifico que haya comisiones disponibles para colocar el riesgo
                $allcount =  $allcomision->count();
                          
                if($allcount>=1){
                  $rand = rand(0,$allcount-1);///Selecciono aleatoriamente uno sdisponibles
                  $allcomision = $allcomision->all();
                  $allcomision[$rand]->id_status = Yii::$app->params['commission_riesgo'];
                  $allcomision[$rand]->update();//cambio el status  a la comision obtenida por randon
                  //asigno los neuvos valores a la ronda
                  $round->to_pay = $round->to_pay - $allcomision[$rand]->commission;
                  $round->risk_pay = $rount->risk_pay + $allcomision[$rand]->commission;
                }
          
                $round->update();
            }
        }        
      }

       /*-------------------------------
       FIn de metodos segundarios estatus de cuota
       --------------------------------*/


}
