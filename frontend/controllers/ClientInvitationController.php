<?php

namespace frontend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\User;
use frontend\models\Status;
use frontend\models\ClientInvitation;


/**
 * ClientInvitationController implements the CRUD actions for ClientInvitation model.
 */
class ClientInvitationController extends Controller
{
    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','view','create','update','delete'],
                        'allow' => true,
                        'roles' => ['@'],  
                    ],
                ],
            ],

        ];
    }

    /**
     * Lists all ClientInvitation models.
     * @return mixed
     */
    public function actionIndex(){
        $dataProvider = new ActiveDataProvider([
            'query' => ClientInvitation::find()->where([
                        'id_godfather'=>Yii::$app->user->identity->id,
                        'id_status'=>13,
                    ])                        
        ]);
                    
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ClientInvitation model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id){
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ClientInvitation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate(){
        $model = new ClientInvitation();                         
        
        if ($model->load(Yii::$app->request->post())) {
                                
            $model->invitation_code = $model->getInvitationCode();
            $model->id_godfather    = Yii::$app->user->identity->id;
            $model->id_status       = 13;
            $model->date_exp       = '2016-02-02';
            $model->count_invitation = 0;
            if($model->save()){                            
                // se encarga de enviar la invitacion al usuario registrado                 
                $model->sentInvitationUser( Yii::$app->user->identity->email,
                                            $model->email,$model->name,
                                            $model->invitation_code );

                return $this->redirect(['view', 'id' => $model->id]);        
            } 
            
        }else {
                return $this->render('create', [
                    'model'  => $model,
                ]);
        }   
    }

    /**
     * Updates an existing ClientInvitation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id){
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ClientInvitation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id){
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ClientInvitation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ClientInvitation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ClientInvitation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
