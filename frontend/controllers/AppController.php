<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\filters\AccessControl;
use frontend\models\Clients;
use frontend\models\ClientsPwd;
use frontend\models\BankAccount;
use frontend\models\CommissionRound;
use frontend\models\Commission;
use frontend\models\LoanQuota;
use frontend\models\Loan;
use frontend\models\UploadForm;
use frontend\models\Address;

use yii\web\UploadedFile; 

/**
 * App controller
 */
class AppController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {        
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                                        'index','pay','config','emailupdate',
                                        'pwdupdate','bankupdate','godson','imagenupdate','dirupdate'
                                    ],
                        'allow' => true,
                        'roles' => ['@'],  
                    ],
                ],
            ],

        ];
    }

    /**
     * Displays App homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
       //$ronda = $this->addComission(Yii::$app->user->identity->id,'hola');
        $userId= Yii::$app->user->identity->id;
        $models = CommissionRound::find()->where(['id_status' => 60, 'id_client'=> $userId])
                                    ->all();
       
        $commissions = CommissionRound::getLastComision();
        if(!$commissions){
            $commissions = 'false';
        }
        return $this->render('index', [
             'models' => $models,
             'comisionGrafic'=>$commissions,
        ]);
    }
    
    public function actionConfig()
    {
        $userId= Yii::$app->user->identity->id;
        $modClient = $this->findModel(Clients::className(),$userId);
        //$modClientPwd = $this->findModel(ClientsPwd::className(),$modClient->id);
        $modBank = $modClient->getBankAccounts()->One();
        

        return $this->render('config',['modClient'=>$modClient,
                                        //'modClientPwd'=>$modClientPwd,
                                        'modBank'=> $modBank
                                    ]);
    }

    /* Actualizacion de email  Config*/
    public function actionEmailupdate()
    {
        $userId= Yii::$app->user->identity->id;
        $model = Clients::findOne($userId);
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return ['response'=>$this->renderPartial('EmailUpdate',['model'=>$model,
                                                                        'success'=>true
                                                                        ]),
                        'success'=>true
                        ];
            }
            
        }

        return ['response'=>$this->renderPartial('EmailUpdate',['model'=>$model,
                                                                        'success'=>false
                                                                        ]),
                        'success'=>false
                        ];
    }

    /*Actualizacion de direccion y numeros de telefono*/
    public function actionDirupdate(){

        $userId= Yii::$app->user->identity->id;
        $model = Address::findOne(['id_client'=>$userId]);
        if($model){
            $model->cellphoneprefix=substr($model->cellphone,0,3);
            $model->phoneprefix=substr($model->phonenumber,0,4);
            $model->newcellphone =substr($model->cellphone,3);
            $model->newphonenumber=substr($model->phonenumber,4);
        }
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax) {

        $model->upd_date = date('Y-m-j');
            if ($model->load(Yii::$app->request->post())) {
                $model->cellphone = $model->cellphoneprefix.$model->newcellphone;
                $model->phonenumber = $model->phoneprefix.$model->newphonenumber;
                if($model->save()){
                    return ['response'=>$this->renderPartial('DirUpdate',['model'=>$model,
                                                                        'success'=>true
                                                                        ]),
                            'success'=>true
                            ];
                }
            }
        }
        return ['response'=>$this->renderPartial('DirUpdate',['model'=>$model,
                                                                        'success'=>false
                                                                        ]),
                        'success'=>false
                        ];

    }

     /* Actualizacion de contraseña  Config*/
     public function actionPwdupdate()
    {
        $userId= Yii::$app->user->identity->id;
        $model = ClientsPwd::findOne($userId);
        $model->scenario = 'update';
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model->password = '';
        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->password = $model->getPasswordHash($model->password); 
                $model->date_upd = date('Y-m-j');   
                $model->save(false);
                return ['response'=>$this->renderPartial('PwdUpdate',['model'=>$model,
                                                                        'success'=>true
                                                                    ]),
                        'success'=>true
                        ];
            }
            
        }

        return ['response'=>$this->renderPartial('PwdUpdate',['model'=>$model,
                                                                'success'=>false
                                                            ]),
                'success'=>false
                ];
    }

     public function actionBankupdate()
    {
        $userId= Yii::$app->user->identity->id;
        $model = BankAccount::findOne(['id_client'=>$userId]);
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return ['response'=>$this->renderPartial('BankUpdate',['model'=>$model,
                                                                        'success'=>true//es para el php 
                                                                        ]),
                        'success'=>true///es para efecto del js
                        ];
            }
            
        }

        return ['response'=>$this->renderPartial('BankUpdate',['model'=>$model,
                                                                        'success'=>false
                                                                        ]),
                        'success'=>false
                        ];
    }

    public function actionImagenupdate(){

        $userId= Yii::$app->user->identity->id;
        $modClient = $this->findModel(Clients::className(),$userId);
        $model = new UploadForm();
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost && Yii::$app->request->isAjax) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            $oldname = $model->imageFile->name;
            $model->imageFile->name = time().'.'.$model->imageFile->extension;
               
            if(!empty($model->imageFile)){
                $Dir  = '/resources/clients/'.$userId.'/';                                 
                // Verifica que la carpeta a sido creada
                if(!is_dir('.'.$Dir)) {
                    if(!mkdir('.'.$Dir,0777,true)){
                        die('Fallo al subir imagen...');
                    }// Crea la carpeta del usuario                                                                                

                }      

                if($model->upload($Dir,$_POST['x'],$_POST['y'],$_POST['w'],$_POST['h'])){
                    $oldfile = $modClient->image;
                    $modClient->image =  $Dir.$model->imageFile->name;   
                    if($modClient->save()){
                        if($oldfile!= '/img/user.jpg'){
                            unlink('.'.$oldfile);
                            return ['response'=>$this->renderPartial('ImageUpdate',[
                                                                        'success'=>true
                                                                        ]),
                            'success'=>true,
                            'newImage'=>Yii::$app->request->baseUrl.$modClient->image
                        ];      
                        }
                    };                                             

                }      
            }
        }


        return ['response'=>$this->renderPartial('ImageUpdate',['model'=>$model,
                                                                        'actualimg'=>$modClient->image,
                                                                        'success'=>false
                                                                        ]),
                        'success'=>false
                        ];       



    }

    protected function findModel($model, $id)
    {  
        if (($model = $model::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGodson(){
        return $this->render('godson');
    }

}
