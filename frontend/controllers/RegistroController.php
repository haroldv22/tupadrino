<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use frontend\models\registro\SearchIdentity;
use beastbytes\wizard\WizardBehavior;
use frontend\models\registro\DatosPersonales;
use frontend\models\ClientInvitation;
use frontend\models\Level;
//modelos para guardado
use frontend\models\Clients;
use frontend\models\BankAccount;
use frontend\models\Address;
use frontend\models\Question;
use frontend\models\ClientsPwd;
use frontend\models\Loan;
use frontend\models\LoanQuota;
use frontend\models\Period;
use yii\helpers\ArrayHelper;



class RegistroController extends Controller
{
	public function behaviors()
    {   
    	return[
    		'registration'=>[
    			'class'=>'beastbytes\wizard\WizardBehavior',
    			'steps' => ['Presentacion','Presolicitud', 'DatosPersonales','CreacionUsuario','VerificacionFinal'],
                'events' => [
                    WizardBehavior::EVENT_WIZARD_STEP => [$this,'registrationWizardStep'],
                    WizardBehavior::EVENT_AFTER_WIZARD => [$this,'registrationAfterWizard'],
                    WizardBehavior::EVENT_INVALID_STEP => [$this, 'invalidStep']
                ]
    		]
    	];
    }

	public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrusercreationorAction',
            ],

        ];
    }

    public function actionComplete()
    {
        $this->layout = "registro";
        return $this->render('complete');
    }
	  public function actionIndex()
    {
        $this->redirect('registration');
    }


    public function actionRegistration($step = null, $token= null){
        //if ($step===null) $this->resetWizard();

        $this->layout = "registro";
        if($token!= null){
            $model = new ClientInvitation();
            if($model->validateToken($token)){
                $session = Yii::$app->getSession();
                $session['token'] = $token;
                return $this->step($step);
            }
        }
        return $this->render('invalidToken');
    }

 public function registrationWizardStep($event)
    {   

        if (empty($event->stepData)) {
            $modelName = 'frontend\\models\\registro\\'.($event->step);
            $model = new $modelName();
            //Asigno el email al model correspondiente
            if ($event->step=='CreacionUsuario'){
                $session = Yii::$app->getSession();
                $modelClient = new ClientInvitation();
                $model->token= $session['token'];
                $model->email= $modelClient->searchEmail($session['token']);
            }

            //Asigno el valor del interes al modelo
            if($event->step=='Presolicitud'){
                $modLevel = new level();
                $model->rate = $modLevel->getRate(\Yii::$app->params['plevel']);
            }
        } else {
            $model = $event->stepData;
        }

        $post = Yii::$app->request->post();
        if (isset($post['cancel'])) {
            $event->continue = false;
        } elseif (isset($post['prev'])) {
            $event->nextStep = WizardBehavior::DIRECTION_BACKWARD;
            $event->handled  = true;
        } elseif ($model->load($post) && $model->validate()) {
            $event->data    = $model;
            $event->handled = true;
           // print_r($event->data);
        } else {
            if ($event->step=='VerificacionFinal'){
                $data = $this->read();
            }
            else{
                $data = null;
            }
            $event->data = $this->render($event->step, compact('event', 'model','data'));
        }
    }
    public function registrationAfterWizard($event)
    {   
        if ($event->step === null) {
            $event->data = $this->render('cancelled');
        } elseif ($event->step) {//Registro Completo Guardado

            //Guardado en base de datos
            if($this->saveData($event->stepData)==true){
                $event->data = $this->render('complete');
            }

        } else {
            $event->data = $this->render('notStarted');
        }
    }

    public function invalidStep($event)
    {
        $event->data = $this->render('invalidStep', compact('event'));
        $event->continue = false;
    }

    //Busca la cedula en el seniat y cne
    public function actionSearchidentity(){
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
             $valido = Clients::find()->where(['identity'=>$data['cedula']])->exists();           
            if($valido){
                return ['error'=>'El numero de identificación ingresado, ya se encuentra registrado'];
            }

            $search = new SearchIdentity;
            return $search->Identity($data['cedula']);
        }
    }

    //devuelve los municipios,parroquias y ciudades
    public function actionList(){
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();

            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if($data['key']=='state'){
                $rows = DatosPersonales::getMunicipality($data['id']);
                $municipality="<option value>-- Seleccione --</option>";
                foreach ($rows as $key => $value) {
                     $municipality.="<option value='$key'>$value</option>";
                }
                $rows = DatosPersonales::getCity($data['id']);
                $city="<option value>-- Seleccione --</option>";
                foreach ($rows as $key => $value) {
                     $city.="<option value='$key'>$value</option>";
                }
                return ['city'=>$city,'municipality'=>$municipality];
            }

            if($data['key']=='municipality'){
                $rows = DatosPersonales::getParish($data['id']);
                $parish="<option value>-- Seleccione --</option>";
                foreach ($rows as $key => $value) {
                    $parish.="<option value='$key'>$value</option>";
                }
                return $parish;
            }
        }
    }
    //Permite obtener los periodos validos para
    //un monto determinado
    public function actionGetperiod(){ 
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $period = Period::find()
                ->innerJoin('period_amount','period_amount.id_period= period.id')
                ->innerJoin('level_amount','level_amount.id=period_amount.id_level_amount')
                ->where(['level_amount.amount'=>$data['key']])
                ->orderBy('level_amount.amount ASC');
            if($period->exists()){
                $period = $period->all();
                $listperiod = ArrayHelper::map($period, 'code','alias');
                $validos='';
                foreach ($listperiod as $key => $value) {
                    $validos.="<option value='$key'>$value</option>";
                }
                
                return $validos;
            }
        }
    }
    
    //separo el numero de dias del id
function Myperiod($data){
    return explode("-", $data);
}
      /**
      * permite guardar todos los datos del wizzard 
      * en la base de datos      
      */ 
     private function saveData($data){

            $transaction = Yii::$app->db->beginTransaction();

            try {
                $DatosPersonales= $data['DatosPersonales'][0]->toArray();
                $CreacionUsuario = $data['CreacionUsuario'][0]->toArray();
                $Presolicitud=$data['Presolicitud'][0]->toArray();
                #Guardado datos del cliente
                $modelCInvit = new ClientInvitation();
                $modClients = new Clients();
                $modClients->attributes=$DatosPersonales;

                #Guardo correctamente la ocupacion
                if($DatosPersonales['ocupation']=='Otro'){
                    $modClients->ocupation = $DatosPersonales['pocupation'];
                }
                $modClients->id_godfather=$modelCInvit->searchGofather($CreacionUsuario['token']); 
                $modClients->id_level = \Yii::$app->params['plevel'];
                $modClients->email = $CreacionUsuario['email'];
                $modClients->date_add = date('Y-m-j');
                $modClients->image = '/img/user.jpg';
                $modClients->identity = $DatosPersonales['typeid'].$DatosPersonales['identity'];
                $result = false;

                #cambio de estatus a invitacion
                $modelCInvit = ClientInvitation::find()
                            ->where(['invitation_code'=>$CreacionUsuario['token'],
                            'id_status'=>13])
                            ->one(); 
                $modelCInvit->id_status = 12;
                $modelCInvit->save();
                $modClients->id_client_invitation = $modelCInvit->id;
                $modClients->id_user_system = $modelCInvit->id_user_system;

                if($modClients->save()){
                    #Guardado de datos de la cuenta
                    $modelBankAcc = new BankAccount();
                    $modelBankAcc->id_client = $modClients->id;
                    $modelBankAcc->attributes=$DatosPersonales;
                    $modelBankAcc->save();
                    #Guardado direccion
                    $modelAddress = new Address();
                    $modelAddress->id_client = $modClients->id;
                    $modelAddress->attributes = $DatosPersonales;
                    $modelAddress->upd_date = date('Y-m-j');
                    #Guardo los numeros telefonicos correctamente
                    $modelAddress->cellphone = $DatosPersonales['cellphoneprefix'].$DatosPersonales['cellphone'];
                    $modelAddress->phonenumber = $DatosPersonales['phoneprefix'].$DatosPersonales['phonenumber'];
                    $modelAddress->save();
                    #Guardado Preguntas secretas
                    $question = 'questiona';
                    $questionp = 'questionpa';
                    $answer = 'answera';
                    for ($i=0; $i < 2 ; $i++) { 
                        $modelQues = new Question();

                        if($CreacionUsuario[$question]=='specific'){
                            $modelQues->question =$CreacionUsuario[$questionp];
                        }
                        else{
                            $modelQues->question =$CreacionUsuario[$question];
                        }
                        $modelQues->answer =$CreacionUsuario[$answer];
                        $modelQues->id_client = $modClients->id;
                        $modelQues->save();

                        $question = 'questionb';
                        $questionp = 'questionpb';
                        $answer = 'answerb';
                    }
                    #Guardado de Password
                    $modelCPwd = new ClientsPwd();
                    $modelCPwd->id_client =  $modClients->id;
                    $modelCPwd->password   = $modelCPwd->getPasswordHash($CreacionUsuario['password']); 
                    $modelCPwd->date_add = date('Y-m-j');
                    $modelCPwd->date_upd = date('Y-m-j');                    
                    $modelCPwd->save();


                    #Guardado info del credito
                    $modelLoan = new Loan();
                    $modelLoan->attributes = $Presolicitud;
                    $modelLoan->id_client = $modClients->id;
                    $modelLoan->date_add = $modelLoan->newDate(date('j-m-Y'));///lamoalafuncion q verifica la hora
                    $modelLoan->id_status = 20;

                    $Myperiod = $this->Myperiod($Presolicitud['period']);

                    $Asiganacion = Loan::defAsignacion($Presolicitud['quota'],
                                                        $Myperiod[1],
                                                        $Presolicitud['rate'],
                                                        $Presolicitud['amount']);

                    $modelLoan->total_interest = $Asiganacion['total_interest'];
                    $modelLoan->total_pay = $Asiganacion['total_pay'];
                    $modelLoan->cant_quota = $Asiganacion['nQuota']; 
                    $modelLoan->period = $Myperiod[1];
                    $modelLoan->points = $Myperiod[0];       
                    $modelLoan->save();

                        
                    #Guardado de cuotas del credito 
                    $tablaPago = Loan::TablaPago($Presolicitud['quota'],
                                                $Myperiod[1],
                                                $Presolicitud['rate'],
                                                $Presolicitud['amount']);

                    foreach ($tablaPago as $key => $value) {
                        $modelLoanQt = new LoanQuota;
                        $modelLoanQt->attributes = $value;
                        //asigno pending pay
                        $modelLoanQt->pending_pay=$value['quota'];
                        $modelLoanQt->id_loan = $modelLoan->id;
                        $modelLoanQt->id_status = 31;
                        $modelLoanQt->save();
                    }


                    $transaction->commit();
                    $result = true;
                }

            } catch (Exception $e) {
                $transaction->rollBack();
                $result =false;
            }
            return $result;


     }
 
    

}