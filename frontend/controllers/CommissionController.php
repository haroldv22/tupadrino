<?php

namespace frontend\controllers;

use Yii;
use frontend\models\CommissionRound;
use frontend\models\Commission;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CommissionRoundController implements the CRUD actions for CommissionRound model.
 */
class CommissionController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
             'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['activa','historico','details'],
                        'allow' => true,
                        'roles' => ['@'],  
                    ],
                ],
            ],
        ];
    }

    /**
     * Comisisones activas
     * @return mixed
     */
    public function actionActiva()
    {
        $dataProvider = $this->getProvider(['id_status'=>60]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'type'=>'activa'
        ]);
    }

    public function actionHistorico()
    {
        $dataProvider = $this->getProvider(['id_status'=>[61,62]]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'type'=>'historico'
        ]);
    }

    private function getProvider($condition=''){
        return $dataProvider = new ActiveDataProvider([
                 'query' => CommissionRound::find()
                                            ->where(['id_client'=>Yii::$app->user->identity->id])
                                            ->andwhere($condition),
             ]);
    }

    /**
     * Displays a single CommissionRound model.
     * @param integer $id
     * @return mixed
     */
    public function actionDetails($id,$type)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Commission::find()->where(['id_commission_round'=>$id])
                                        ->with(['idClient','idStatus']),
        ]);

        return $this->render('details', [
            'dataProvider' => $dataProvider,
            'id'=>$id,
            'type'=>$type,
        ]);
    }


    

    /**
     * Finds the CommissionRound model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CommissionRound the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CommissionRound::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
