<?php

namespace frontend\controllers;

use Yii;
use frontend\models\RecordPayment;
use frontend\models\QuotaPayment;
use frontend\models\ArrearsPayment;
use frontend\models\Arrears;
use frontend\models\TdcPayment;
use yii\filters\AccessControl;

use frontend\models\LoanQuota;
use frontend\models\Loan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

$path = Yii::getAlias("@vendor/mercadopago/yii2-lib/mercadopago.php");
require_once($path);
/**
 * RecordPaymentController implements the CRUD actions for RecordPayment model.
 */
class RecordPaymentController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                                        'index','view','get-payment','create',
                                        'get-quota-details','get-quota-amount','update',
                                    ],
                        'allow' => true,
                        'roles' => ['@'],  
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all RecordPayment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => RecordPayment::find()->where([
                        'id_client'=>Yii::$app->user->identity->id,
                    ]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RecordPayment model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionGetPayment(){
        $mp = new \MP ("99020246650329", "I9DqKO5z1VkOCPph1Sc0yHabcqEp5Ph4");

        $paymentInfo = $mp->get_payment ("1761601");

        print_r ($paymentInfo);
    }

    /**
     * Creates a new RecordPayment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new RecordPayment;
        
        if ($model->load(Yii::$app->request->post())) {

            if ($model->method!='TDC'){//Si no es un pago con TDC
                $model->scenario= 'record';
                if($model->validate()){
                    $transaction = Yii::$app->db->beginTransaction();

                    try {

                        $model->id_client =  Yii::$app->user->identity->id;
                        $model->date_add = date('Y-m-j');
                        $model->id_status =50;
                        $model->ispayloan=($model->ncuota=='ALL')?true:false;
                        $model->id_loan = $model->IdLoanPayment();
                        if($model->save()){
                            $model->SavePaymentNotAll($model);
                            $transaction->commit();
                            return $this->redirect(['view', 'id' => $model->id]);        

                        }

                    } catch (Exception $e) {
                        $transaction->rollBack();
                    }
                }
            }
            else{//Pago con TDC
                $model->scenario='tdcpay';
                $model->id_loan = $model->IdLoanPayment();

                if($model->validate() && isset($_POST['token'])){

                    $mp = new \MP('APP_USR-3163460913815188-092714-b7b4689715672745ba752ac1d8ec27f7__LB_LD__-226054726');
                    //if($model->validate()){
                        $payment_data = array(
                            "transaction_amount" => floatval($model->amount),
                            "token" => $_POST['token'],
                            "description" => "Pago",
                            "installments" => 1,
                            "payment_method_id" => $_POST['paymentMethodId'],
                            "payer" => array (
                                "email" => Yii::$app->user->identity->email
                            )
                        );
                        $TdcPayment = $mp->post("/v1/payments", $payment_data);
                        
                        if($TdcPayment['status']==201){
                            $modTdcPayment = new TdcPayment;
                            $transaction = Yii::$app->db->beginTransaction();

                            try {
                                $idRecordPayment = $modTdcPayment->ProcesarPagoTdc($model,$TdcPayment,$modTdcPayment);

                                $transaction->commit();
                                return $this->redirect(['view', 'id' => $idRecordPayment]);        

                            }
                            catch (\Exception $e) {
                                Yii::$app->getSession()->setFlash('errorTDC', $e->getMessage());
                                $transaction->rollBack();

                            }
                        }
                       // $paymentInfo = $mp->get_payment ($_POST['token']);
                }
                    //}
            }
        } 
        return $this->render('create', [
            'model' => $model,
        ]);
        
    }

    
    /**
     *Trae los detalles de pago
     *
     */
    public function actionGetQuotaDetails(){

        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            
            if($data['id']=='ALL'){
                $totalpay = $this->totalpay();
                return [
                    'total'=>round(($totalpay['quota']+$totalpay['arrear']),2),
                    'arrear'=>$totalpay['arrear'],
                    'arreardate'=>'-',
                    'quota'=>$totalpay['quota'],
                    'havearrear'=>($totalpay['arrear']>0)?true:false,
                    ]; 
            }
            else{

                $model  = LoanQuota::find()->where(['id'=>$data['id']]);

                if($model->exists()){
                    $model = $model->One();
                    if (Yii::$app->user->identity->id ==  $model->getIdLoan()->one()->getIdClient()->one()->id){
                        $arrear = 0;
                        $arreardate = '00/00/0000';
                        if($model->have_arrear == true){
                            $modArrear = $model->getArrears()->One();
                            $arrear = $modArrear->pending_pay;
                            $arreardate = $modArrear->add_date;
                        }
                       return [
                            'total'=>round(($model->pending_pay+$arrear),2),
                            'arrear'=>$arrear,
                            'arreardate'=>$arreardate,
                            'quota'=>$model->pending_pay,
                            'havearrear'=>$model->have_arrear,
                            ];
                    }


                }
            }
        }
    }
    /**
    *Devuelve el monto a pagar
    */

    public function actionGetQuotaAmount(){

        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $amounts = $this->getAmounts($data['id']);
            switch ($data['idtype']) {
                case 'cuota':
                    if($amounts['amount_quota']>0){
                        return ['amount'=>$amounts['amount_quota']];
                    }
                    break;
                case 'mora':
                    if($amounts['amount_arrear']>0){
                        return ['amount'=>$amounts['amount_arrear']];
                    }
                    break;
                case 'ambos':
                    if($amounts['amount_quota']>0 && $amounts['amount_arrear'] >0){
                        return ['amount'=>round(($amounts['amount_quota']+$amounts['amount_arrear']),2),];
                    }
                    break;
                default:
                    break;
            }
                
            
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Finds the RecordPayment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RecordPayment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RecordPayment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function totalpay(){
        $totaltopay =0;
        $arreartopay =0;

        $Loan= Loan::find()->where(['id_client'=>Yii::$app->user->identity->id, 
                                            'id_status'=>[41,42,43]
                                            ]);
        if($Loan->exists()){
            $Loan= $Loan->One();

            $model = LoanQuota::find()->where('id_loan = :id_loan AND 
                                            (have_arrear=true OR id_status IN(30,31,33,34))',
                                            [':id_loan'=>$Loan->id])
                                        ->All();
            foreach ($model as $LoanQuota) {
                $totaltopay += $LoanQuota->pending_pay;
                if($LoanQuota->have_arrear == true){
                    $modArrear = $LoanQuota->getArrears()->One();
                    $arreartopay += $modArrear->pending_pay;
                }
            }   
        }
        return['quota'=>$totaltopay,'arrear'=>$arreartopay];     
    }
    private function getAmounts($id){
        $amount_arrear = '';
        $amount_quota = '';
        if($id!='ALL'){//si es solo una cuota saco los montos
            $model  = LoanQuota::find()->where(['id'=>$id]);
            if($model->exists()){
                $model = $model->One();
                if (Yii::$app->user->identity->id ==  $model->getIdLoan()->one()->id_client){
                    $amount_quota = $model->pending_pay;//monto pago de la cuota
                    if($model->have_arrear == true){
                        $modArrear = $model->getArrears()->One();
                        $amount_arrear = $modArrear->pending_pay;//monto pago de la mora
                    }

                }
            }
        }else{
           $amount_all = $this->totalpay();//todo el credito
           $amount_quota = $amount_all['quota'];
           $amount_arrear= $amount_all['arrear'];
        }

        return ['amount_quota'=>$amount_quota,'amount_arrear'=>$amount_arrear];


    }
}
