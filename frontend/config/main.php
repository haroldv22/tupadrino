<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'frontend\controllers',
    'language'=>'es',
    'bootstrap' => ['log'],
    'modules' => [],
    'defaultRoute' => 'site/login',

    'components' => [   
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=localhost;port=5432;dbname=microdb',
            'username' => 'hvyv',
            'password' => 'perita01',
            'charset' => 'utf8',
        ],
        'user' => [
            'identityClass' => 'frontend\models\login\Clients',
            'enableAutoLogin' => true,
            'enableSession' => true,
            'authTimeout' => 300,
            'loginUrl' => ['site/login'],  
        ],      
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => array(
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'dateFormat' => 'dd.MM.yyyy',
            //'currencyCode' =>'EUR',
            'nullDisplay' => '', 
        ]
    ],
    'params' => $params,
];
