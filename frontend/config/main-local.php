<?php
return [
    
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'iUrqxfYL9dGxiQG5JbUlKq1B2IpsiFpn',
        ],
    ],
    'modules' => [
            'gii' => [
                'class' => 'yii\gii\Module',
                'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.*'] // adjust this to your needs
    ],
    ],
];
 if (!YII_ENV_TEST) {
 // configuration adjustments for 'dev' environment
 $config['bootstrap'][] = 'debug';
 // $config['modules']['debug'] = 'yiidebugModule';
 $config['modules']['debug'] = [
   'class' => 'yiidebugModule',
   'allowedIPs' => ['127.0.0.1', '::1'],
 ];
}
