<?php

namespace frontend\models;

use Yii;
use yii\helpers\ArrayHelper;
use frontend\models\LoanQuota;
use frontend\models\SystemBankAccount;
use frontend\models\Arrears;
use frontend\models\QuotaPayment;
use frontend\models\ArrearsPayment;
use yii\base\Model;

/**
 * This is the model class for table "record_payment".
 *
 * @property integer $id
 * @property integer $id_client
 * @property string $amount
 * @property integer $reference
 * @property string $date_add
 * @property string $method
 * @property string $type
 * @property integer $id_status
 * @property string $pay_date
 * @property string $code_bank_origin
 * @property string $code_bank_destiny
 * @property string $money_excess
 *
 * @property ArrearsPayment[] $arrearsPayments
 * @property QuotaPayment[] $quotaPayments
 * @property Banks $codeBankOrigin
 * @property Banks $codeBankDestiny
 * @property Clients $idClient
 * @property Status $idStatus
 */
class RecordPayment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $ncuota;

    public static function tableName()
    {
        return 'record_payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_client', 'amount', 'reference', 'date_add', 'method', 'type', 'id_status', 'pay_date',
             'ncuota'], 'required'],
            [['id_client', 'reference', 'id_status'], 'integer'],
            [['amount', 'money_excess'], 'number'],
            ['amount', 'compare', 'compareValue' => 0, 'operator' => '>='],
            [['date_add', 'pay_date'], 'safe'],
            [['method'], 'string', 'max' => 3],
            //[['method'],'validateMethod'],
            [['type'], 'string', 'max' => 20],
            [['code_bank_origin', 'code_bank_destiny'], 'string', 'max' => 4],
            ['type', 'validateType','skipOnEmpty' => false,'skipOnError' => false],

            ['code_bank_origin', 'required', 'when' => function ($model) {
                return $model->method == 'TRF';
            }, 'whenClient' => "function (attribute, value) {
                return $('#recordpayment-method').val() == 'TRF';
            }"],

            ['code_bank_destiny', 'required', 'when' => function ($model) {
                return ($model->method != 'TDC');
            }, 'whenClient' => "function (attribute, value) {
                return ($('#recordpayment-method').val() != 'TDC');
            }"],
            ['amount','ValidarAmountAllPayment'],


        ];
    }

    public function scenarios(){
        $scenarios = parent::scenarios();
        $scenarios['record']=['type','ncuota','amount','method','code_bank_origin','code_bank_destiny','reference','pay_date'];
        $scenarios['tdcpay']=['type','ncuota','amount','method'];
        return $scenarios;
    }
    //valido el metodo de pago
    /*
    public function validateMethod(){
        if($this->method =='TDC')
            $this->addError('method', 'El metodo seleccionado actualmente no se encuentra disponible');

    }*/

    //valido que la cuota pertenezca al usuario
    public function valideQuota(){
        $IdUser = Yii::$app->user->identity->id;

        $modLoanQuota  = LoanQuota::find()->where(['id'=>$this->ncuota]);
            if($model->exists()){
                 if ($IdUser !=  $modLoanQuota->getIdLoan()->one()->id_client){
                    $this->addError('ncuota', 'La cuota seleccionada no le corresponde, por favor no intente burlar al sistema');
                 }
            }else{
                $this->addError('ncuota', 'Ha habido un error con la seleccion de cuotas');
            }
    }

    public function ValidarAmountAllPayment(){
        if($this->ncuota=='ALL'){
            $TotalLoanToPay = $this->totalpay();
            if($this->amount < $TotalLoanToPay['quota']){
                $this->addError('amount', 'El monto debe ser superior al total de las quotas pendientes');
            }
        }
    }

    public function totalpay(){
        $totaltopay =0;
        $arreartopay =0;

        $Loan= Loan::find()->where(['id_client'=>Yii::$app->user->identity->id, 
                                            'id_status'=>[41,42,43]
                                            ]);
        if($Loan->exists()){
            $Loan= $Loan->One();

            $model = LoanQuota::find()->where('id_loan = :id_loan AND 
                                            (have_arrear=true OR id_status IN(30,31,33,34))',
                                            [':id_loan'=>$Loan->id])
                                        ->All();
            foreach ($model as $LoanQuota) {
                $totaltopay += $LoanQuota->pending_pay;
                if($LoanQuota->have_arrear == true){
                    $modArrear = $LoanQuota->getArrears()->One();
                    $arreartopay += $modArrear->pending_pay;
                }
            }   
        }
        return['quota'=>$totaltopay,'arrear'=>$arreartopay];     
    }
    //verificacion cuando type = ambos
    public function validateType($attribute,$params){//"Mejorar"
        if($this->ncuota!='ALL'){
            $modLoanQuota  = LoanQuota::find()->where(['id'=>$this->ncuota]);
            if($modLoanQuota->exists()){
                $modLoanQuota = $modLoanQuota->One();

                //validacion para seleccion ambos
                if($this->type=='ambos'){
                    if($modLoanQuota->have_arrear == true ){
                        if($modLoanQuota->pending_pay<= 0){
                            $this->addError('type', 'La cuota seleccionada solo posee mora, por favor seleccione "mora"');
                        }
                        else{
                            if($this->amount<=$modLoanQuota->pending_pay){
                                $this->addError('type', 'El monto colocado es inferior o igual a la deuda de la cuota, por favor seleccione "cuota"');
                            }
                        }
                    }
                    else{
                        $this->addError('type', 'La cuota seleccionada no posee mora, por favor seleccione "cuota"');
                    }
                }

                //validacion para cuota
                if($this->type=='cuota'){
                    if($modLoanQuota->pending_pay<=0){
                        $this->addError('type', 'La cuota seleccionada ya se encuentra pagada');
                    }
                }
                //validacion para mora
                if($this->type=='mora'){
                    if($modLoanQuota->have_arrear==false){
                        $this->addError('type', 'La cuota seleccionada no posee mora');
                    }else{
                        $modArrears  = Arrears::find()->where(['id_loan_quota'=>$this->ncuota]);
                            if($modArrears->exists()){
                                if($modArrears->One()->pending_pay<=0){
                                    $this->addError('type', 'La cuota seleccionada no posee mora');
                                }
                            }else{
                                $this->addError('type', 'La cuota seleccionada no posee mora');
                            }
                    }

                }
                
            }else{
                $this->addError('type', 'Error, vuelva a intentarlo');
            }
        }else{//mejorar codigo

            $totaltopay =0;
            $arreartopay =0;

            $Loan= Loan::find()->where(['id_client'=>Yii::$app->user->identity->id, 
                                                'id_status'=>[41,42,43]
                                                ]);
            if($Loan->exists()){
                $Loan= $Loan->One();

                $model = LoanQuota::find()->where('id_loan = :id_loan AND 
                                                (have_arrear=true OR id_status IN(30,31,33,34))',
                                                [':id_loan'=>$Loan->id])
                                            ->All();
                foreach ($model as $LoanQuota) {
                    $totaltopay += $LoanQuota->pending_pay;
                    if($LoanQuota->have_arrear == true){
                        $modArrear = $LoanQuota->getArrears()->One();
                        $arreartopay += $modArrear->pending_pay;
                    }
                }
                switch ($this->type) {
                    case 'ambos':
                        if($arreartopay<=0){
                            $this->addError('type', 'No posee moras, por favor seleccione otro tipo de pago');

                        }
                        if($totaltopay<=0){
                            $this->addError('type', 'No posee deudas de cuotas, por favor seleccione otro tipo de pago');
                        }
                        break;
                    case 'cuota':
                        if($totaltopay<=0){
                            $this->addError('type', 'No posee deudas de cuotas, por favor seleccione otro tipo de pago');
                        }                        break;
                    case 'mora':
                        if($arreartopay<=0){
                            $this->addError('type', 'No posee moras, por favor seleccione otro tipo de pago');

                        }
                        break;
                    
                    default:
                        # code...
                        break;
                }
            }

        }
        
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_client' => 'cliente',
            'amount' => 'Monto',
            'reference' => 'Codigo referencia',
            'date_add' => 'Fecha de registro',
            'method' => 'Metodo de Pago',
            'type' => 'Tipo de pago',
            'id_status' => 'Estatus',
            'pay_date' => 'Fecha de pago',
            'code_bank_origin' => 'Banco origen',
            'code_bank_destiny' => 'Banco destino',
            'money_excess' => 'Exceso ',
            'ncuota'=>'No. Cuota',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArrearsPayments()
    {
        return $this->hasMany(ArrearsPayment::className(), ['id_record_payment' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuotaPayments()
    {
        return $this->hasMany(QuotaPayment::className(), ['id_record_payment' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodeBankOrigin()
    {
        return $this->hasOne(Banks::className(), ['code_bank' => 'code_bank_origin']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodeBankDestiny()
    {
        return $this->hasOne(Banks::className(), ['code_bank' => 'code_bank_destiny']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'id_client']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'id_status']);
    }

     //Devuelve un dropdowlist con las cuotas disponibles a pagar 
    public static function GetQuotaList(){
        $model = new QuotaPayment;
        $useId= Yii::$app->user->identity->id;
        $modLoan = Loan::find()->where(['id_client'=>$useId, 'id_status'=>[41,42,43]]);
        if($modLoan->exists()){
            $query = LoanQuota::find()->where('id_loan = :id_loan AND 
                                            (have_arrear=true OR id_status IN(30,33,34))',
                                            [':id_loan'=>$modLoan->One()->id])
                                    ->orderBy('n_quota DESC');

            if($query->exists()){
                return ArrayHelper::map($query->all(), 'id', 'n_quota');
            }
            else{
                return 'noQuota';
            }
            
        }
        else{
            return 'noLoan';
        }
    }

    public static function GetBanks(){
        return ArrayHelper::map(Banks::find()->All(), 'code_bank', 'name');
    }
    
    public static function GetSystemBanks(){
        return ArrayHelper::map(SystemBankAccount::find()->with('codeBank')->all(), 'code_bank', 'codeBank.name');
    }


    public function SavePaymentNotAll($model){
        if($model->ncuota!='ALL'){
            switch ($model->type) {   
                case 'cuota':
                    $modLoanQuota  = LoanQuota::find()->where(['id'=>$model->ncuota])->One();
                    $modQuotaPayment =  new QuotaPayment();
                    $modQuotaPayment->id_loan_quota = $modLoanQuota->id;
                    $modQuotaPayment->id_record_payment = $model->id;

                    //verifico si el monto pagado es mayor al monto esperado
                    if($model->amount > $modLoanQuota->pending_pay){
                        $modQuotaPayment->amount =  $modLoanQuota->pending_pay;
                        $model->money_excess = round(($model->amount  -  $modLoanQuota->pending_pay),2);
                        //Actualizo recordpayment
                        $model->save(false);
                    }
                    else{
                        $modQuotaPayment->amount = $model->amount;
                    }
                    //Guardo en la bd
                    $modQuotaPayment->save();
                    break;
                case 'mora':
                    $modArrears  = Arrears::find()->where(['id_loan_quota'=>$model->ncuota])->One();
                    $modArrearPayment = new  ArrearsPayment();
                    $modArrearPayment->id_arrears = $modArrears->id;
                    $modArrearPayment->id_record_payment = $model->id;

                    //verifico si el monto pagado es mayor al monto esperado
                    if($model->amount > $modArrears->pending_pay){
                        $modArrearPayment->amount = $modArrears->pending_pay;
                        $model->money_excess = round(($model->amount - $modArrears->pending_pay),2);
                        //Actualizo recordpayment
                        $model->save(false);
                    }
                    else{
                        $modArrearPayment->amount = $model->amount;
                    }

                    //guardo en la bd
                    $modArrearPayment->save();

                    break;
                case 'ambos':
                    //Inicializo los modelos
                    $modQuotaPayment =  new QuotaPayment();
                    $modArrearPayment = new  ArrearsPayment();
                    $modArrears  = Arrears::find()->where(['id_loan_quota'=>$model->ncuota])->One();
                    $modLoanQuota  = LoanQuota::find()->where(['id'=>$model->ncuota])->One();

                    //asiono algunos datos sin calculo a los modelos
                    $modQuotaPayment->id_loan_quota = $modLoanQuota->id;
                    $modQuotaPayment->id_record_payment = $model->id;
                    $modArrearPayment->id_arrears = $modArrears->id;
                    $modArrearPayment->id_record_payment = $model->id;

                    //asigno el monto depositado para Quota payment
                    //siempre debe ser el mismo de pendiente
                    $modQuotaPayment->amount =  $modLoanQuota->pending_pay;

                    //calculo cuando queda despues de pagar la cuota
                    $amountrestante = round(($model->amount - $modLoanQuota->pending_pay),2);

                    //verifico si el monto que queda es mayor a la mora
                    if($amountrestante > $modArrears->pending_pay){
                        $modArrearPayment->amount = $modArrears->pending_pay;
                        $model->money_excess = round(($amountrestante - $modArrears->pending_pay),2);

                        //Actualizo recordpayment
                        $model->save(false);

                    }
                    else{
                        $modArrearPayment->amount = $amountrestante;
                    }

                    //Guardo en la bd
                    $modQuotaPayment->save();
                    $modArrearPayment->save();

                default:
                    throw new \Exception('Ningua coincidencia');

            }
        }
    }

    public function IdLoanPayment(){        
        $useId= Yii::$app->user->identity->id;
        $modLoan = Loan::find()->where(['id_client'=>$useId, 'id_status'=>[41,42,43]]);
        if($modLoan->exists()){
            return $modLoan->one()->id;
        }
        else{
            throw new \Exception("Error, definiendo usuario");
                
        }
    }
    public function GetMethodFullName(){

        switch (trim($this->method)) {
            case 'TDC':
                return 'Tarjeta de Credito';
                break;
            case 'DEP':
                return 'Deposito';
                break;
            case 'TRF':
                return 'Transferencia';
                break;
            default:
                return 'Desconocido';
        }
    }


}
