<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "phone_prefix".
 *
 * @property integer $id
 * @property string $prefix
 */
class PhonePrefix extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'phone_prefix';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prefix'], 'required'],
            [['prefix'], 'string', 'max' => 4]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id de la tabla',
            'prefix' => 'prefijo',
        ];
    }
}
