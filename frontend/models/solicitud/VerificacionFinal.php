<?php
namespace frontend\models\solicitud;
use yii\base\Model;
use yii;

class VerificacionFinal extends Model
{
	public $term = 1;//Terminos y condiciones
    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            ['term','safe'],
            ['term','required'],
        ];
    }
    public function attributeLabels()
    {
        return [
            'term' => 'Acepto los terminos & condiciones',
        ];
    }
    

}
