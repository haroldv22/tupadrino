<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "municipality".
 *
 * @property integer $id
 * @property integer $id_state
 * @property string $name
 *
 * @property Address[] $addresses
 * @property State $idState
 * @property Parish[] $parishes
 */
class Municipality extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'municipality';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_state', 'name'], 'required'],
            [['id_state'], 'integer'],
            [['name'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id de la tabla',
            'id_state' => 'id del estado al que pertenece',
            'name' => 'nombre del municipio',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(Address::className(), ['id_municipality' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdState()
    {
        return $this->hasOne(State::className(), ['id' => 'id_state']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParishes()
    {
        return $this->hasMany(Parish::className(), ['id_municipality' => 'id']);
    }
}
