<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "quota".
 *
 * @property integer $days
 * @property integer $id_level
 * @property integer $id
 *
 * @property Level $idLevel
 */
class Quota extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'quota';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['days', 'id_level'], 'integer'],
            [['id_level'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'days' => 'Numero de dias para el pago',
            'id_level' => 'Id Level',
            'id' => 'ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLevel()
    {
        return $this->hasOne(Level::className(), ['id' => 'id_level']);
    }
    public function getText()
    {
        return $this->days.' días';
    }

    //Coloca la clase css a cada estatus
    public function CssSmfclass(){
        switch ($this->id_status) {
            case 34://En Mora
                return 'label-danger';
                break;
            case 33://En Riesgo
                return 'label-warning';
                break;
            case 32://Puntual
                return 'label-success';
                break;
            case 30://Finalizado
                return 'label-success';
                break;  
        }

    }

}
