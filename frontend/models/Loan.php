<?php

namespace frontend\models;

use Yii;
use frontend\models\BankHoliday;

/**
 * This is the model class for table "loan".
 *
 * @property integer $id
 * @property integer $id_client
 * @property string $amount
 * @property integer $rate
 * @property integer $id_status
 * @property integer $quota
 * @property integer $period
 * @property string $date_add
 * @property integer $reference_pay
 * @property string $date_transference
 * @property string $total_interest
 * @property integer $cant_quota
 * @property string $total_pay
 *
 * @property Commission[] $commissions
 * @property Clients $idClient
 * @property Status $idStatus
 * @property LoanQuota[] $loanQuotas
 */
class Loan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'loan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_client', 'amount', 'rate', 'id_status', 'quota', 'period', 'date_add'], 'required'],
            [['id_client', 'rate', 'id_status', 'quota', 'period', 'reference_pay', 'cant_quota'], 'integer'],
            [['amount', 'total_interest', 'total_pay'], 'number'],
            [['date_add', 'date_transference'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_client' => 'Id Client',
            'amount' => 'Monto Prestado',
            'rate' => 'tasa de interes',
            'id_status' => 'Estatus',
            'quota' => 'numeros de dias en el cual se genera una quota',
            'period' => 'Periodo de pago asiganado por el usuario',
            'date_add' => 'Fecha de Solicitud',
            'reference_pay' => 'numero de referencia de la liquidacion',
            'date_transference' => 'fecha de la transferencia del sistema al cliente',
            'total_interest' => 'Intereses total a pagar ',
            'cant_quota' => 'cantidad de cuotas',
            'total_pay' => 'total a pagar',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommissions()
    {
        return $this->hasMany(Commission::className(), ['id_loan' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'id_client']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'id_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoanQuotas()
    {
        return $this->hasMany(LoanQuota::className(), ['id_loan' => 'id']);
    }

     public static function defAsignacion($quota,$period,$rate,$amount){
        $nQuota = round($period/$quota);
        $rate = $rate/100;
        #Formula para calculo de interes
        $P = $amount;
        $i = $rate;
        $n = $nQuota;
        $R = $P*(($i*pow((1+$i),$n))/(pow((1+$i),$n)-1));
        #Fin Formula
        $R= round($R,2);
        $total_pay = round(($R *$nQuota),2);
        $total_interest = round(($total_pay-$P),2);

        return ['nQuota'=>$nQuota, 'total_interest'=>$total_interest,'total_pay'=>$total_pay];
    }
    public static function TablaPago($quota,$period,$rate,$amount){
        $nQuota = round($period/$quota);
        $payDate = self::newDate(date('j-m-Y'));///Llamo a la funcion que verifica la hora

        $result =[];
        $rate = $rate/100;
        #Formula para calculo de interes
        $P = $amount;
        $i = $rate;
        $n = $nQuota;
        $R = $P*(($i*pow((1+$i),$n))/(pow((1+$i),$n)-1));
        #Fin Formula
        $R= round($R,2);
        $balance = $amount;
        for ($i=0; $i < $nQuota ; $i++) { 
            $payDate= self::addDays($quota,$payDate);
            $payDate = date ( 'j-m-Y' , $payDate );

            $expDate = self::addDays(\Yii::$app->params['payExp'],$payDate);
            $expDate = date ( 'j-m-Y' , $expDate );
            
            $interest = round(($rate * $balance),2);
            $amort=round($R-$interest,2);
            $array = array('n_quota' => $i+1,
                            'date_pay'=>$payDate,
                            'date_expired'=>$expDate,
                            'interest'=>$interest,
                            'amortization'=>$amort,
                            'quota'=>$R);
            array_push($result,$array);
            $balance = round($balance -$amort,2);
        }
        return $result;
    }
    //agrega dias a la fecha de pago
    public static function addDays($days, $date){
        $rdate =  strtotime ('+'.$days.' day',strtotime($date));
        return self::validarFecha($rdate);
    }
    //valida si es dia habil
    private static function validarFecha($date){
        if(date('w',$date)==0 || date('w',$date)==6){
            $rdate = strtotime ('+1 day',$date);
            return self::validarFecha($rdate);
        } 
        $isholiday= BankHoliday::find()->where(['holiday'=>date('Y-m-j',$date)]);
        if($isholiday->exists()){
            $rdate = strtotime ('+1 day',$date);
            return self::validarFecha($rdate);
        }
        return $date;
    }

    public static function columTotal($provider, $fieldName){
        $total=0;
        foreach($provider as $item){
          switch ($fieldName) {
              case 'amortization':
                if($item['id_status']==32){
                    $total+=$item[$fieldName];
                }
                break;
                case 'arrears.pending_pay':
               // if($item['id_status']==34){
                    $total+=$item['arrears']['pending_pay'];
                //}
                break;
                case 'arrears.total_interest':
                    $total+=$item['arrears']['total_interest'];
                //}
                break;
              case 'quota':
                $total+=$item[$fieldName];
                break;
              default:
                  # code...
                  break;
          }


        }
        return $total;
    }   

    public static function countQ($provider){
        $pay=0;
        $pending =0;
        foreach($provider as $item){
          
            if($item['id_status']==32){
                $pay++;
            }
            if(in_array($item['id_status'], [30,33,34])){
                $pending++;
            }
        }
        return ['pay'=>$pay,'pending'=>$pending];
    }  

    ///Esta funcion verifica la horay agrega un dia encaso de ser mayor a 3:40pm
    public function newDate($fecha){
        if(date('H')>=15){
            if(date('H')==15){
                if(date('i'<=40)){
                    return $fecha;
                }
            }
            $nuevafecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;
            $nuevafecha = date ( 'j-m-Y' , $nuevafecha );
            return $nuevafecha;
        }
	return $fecha;
    }
}
