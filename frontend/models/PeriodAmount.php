<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "period_amount".
 *
 * @property integer $id
 * @property integer $id_period
 * @property integer $id_level_amount
 *
 * @property LevelAmount $idLevelAmount
 * @property Period $idPeriod
 */
class PeriodAmount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'period_amount';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_period', 'id_level_amount'], 'required'],
            [['id_period', 'id_level_amount'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id de la tabla',
            'id_period' => 'id del periodo al que pertenece',
            'id_level_amount' => 'id del monto asociado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLevelAmount()
    {
        return $this->hasOne(LevelAmount::className(), ['id' => 'id_level_amount']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPeriod()
    {
        return $this->hasOne(Period::className(), ['id' => 'id_period']);
    }
}
