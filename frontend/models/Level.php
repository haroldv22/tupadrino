<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "level".
 *
 * @property integer $id
 * @property string $amount_min
 * @property integer $conditioning
 * @property string $rate
 * @property string $amount_max
 * @property integer $id_level_next
 * @property integer $godson_cant
 *
 * @property Clients[] $clients
 * @property Period[] $periods
 * @property Quota[] $quotas
 */
class Level extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'level';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'amount_min', 'conditioning', 'rate', 'amount_max', 'id_level_next', 'godson_cant'], 'required'],
            [['id', 'conditioning', 'id_level_next', 'godson_cant'], 'integer'],
            [['amount_min', 'rate', 'amount_max'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'amount_min' => 'monto minimo del prestamo',
            'conditioning' => 'Numero de creditos cancelados para aumentar de level',
            'rate' => 'tasa de interes',
            'amount_max' => 'monto maximo de prestamo',
            'id_level_next' => 'id del level precedente',
            'godson_cant' => 'Cantidad de ahijados que tiene que recomendar',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this->hasMany(Clients::className(), ['id_level' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriods()
    {
        return $this->hasMany(Period::className(), ['id_level' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuotas()
    {
        return $this->hasMany(Quota::className(), ['id_level' => 'id']);
    }

    public function getRate($id){ 
        $result = $this::find() ->select('rate')
                                ->where(['id'=>$id])
                                ->one();
        return $result->rate;
    }

}
