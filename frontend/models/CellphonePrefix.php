<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "cellphone_prefix".
 *
 * @property integer $id
 * @property string $prefix
 */
class CellphonePrefix extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cellphone_prefix';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prefix'], 'required'],
            [['prefix'], 'string', 'max' => 3]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id de la tabla',
            'prefix' => 'prefijo',
        ];
    }
}
