<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "arrears_details".
 *
 * @property integer $id
 * @property integer $id_arrears
 * @property string $add_date
 * @property string $interest_rate
 * @property string $interest_generate
 * @property string $amount_base
 *
 * @property Arrears $idArrears
 */
class ArrearsDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'arrears_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_arrears', 'add_date', 'interest_rate', 'interest_generate', 'amount_base'], 'required'],
            [['id_arrears'], 'integer'],
            [['add_date'], 'safe'],
            [['interest_rate', 'interest_generate', 'amount_base'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_arrears' => 'Id Arrears',
            'add_date' => 'Add Date',
            'interest_rate' => 'Interest Rate',
            'interest_generate' => 'Interest Generate',
            'amount_base' => 'Amount Base',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdArrears()
    {
        return $this->hasOne(Arrears::className(), ['id' => 'id_arrears']);
    }
}
