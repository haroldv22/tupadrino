<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "ocupation".
 *
 * @property integer $id
 * @property string $ocupation
 */
class Ocupation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ocupation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ocupation'], 'required'],
            [['ocupation'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id de la tabla',
            'ocupation' => 'ocupacion nombre',
        ];
    }
}
