<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "clients".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $gender
 * @property integer $id_godfather
 * @property integer $id_level
 * @property integer $count_client
 * @property string $birth_date
 * @property string $identity
 * @property string $date_add
 *
 * @property Address[] $addresses
 * @property BankAccount[] $bankAccounts
 * @property Level $idLevel
 * @property ClientsDefaulter[] $clientsDefaulters
 * @property ClientsPwd $clientsPwd
 * @property Loan[] $loans
 * @property Question[] $questions
 * @property RecordPayment[] $recordPayments
 */
class Clients extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'email', 'gender', 'birth_date', 'date_add', 'ocupation'], 'required'],
            [['id_godfather', 'id_level'], 'integer'],
            [['birth_date', 'date_add'], 'safe'],
            [['firstname', 'lastname'], 'string', 'max' => 50],
            [['email'], 'string', 'max' => 75],
            [['gender'], 'string', 'max' => 1],
            [['identity'], 'string', 'max' => 20],
            [['email'],'match','pattern'=>'/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$/'],


        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'firstname',
            'lastname' => 'lastname ',
            'email' => 'email',
            'gender' => 'gender',
            'id_godfather' => 'id godfather ',
            'id_level' => 'id level',
           
            'birth_date' => 'fecha de nacimiento',
            'identity' => 'cedula de identida',
            'date_add' => 'Fecha de Registro',
            'ocupation'=>'ocupacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(Address::className(), ['id_client' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankAccounts()
    {
        return $this->hasMany(BankAccount::className(), ['id_client' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLevel()
    {
        return $this->hasOne(Level::className(), ['id' => 'id_level']);
    }

    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientsPwd()
    {
        return $this->hasOne(ClientsPwd::className(), ['id_client' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoans()
    {
        return $this->hasMany(Loan::className(), ['id_client' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasMany(Question::className(), ['id_client' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecordPayments()
    {
        return $this->hasMany(RecordPayment::className(), ['id_client' => 'id']);
    }


    public function getClientsAdmin()
    {
        return $this->hasOne(ClientsAdmin::className(), ['id_client' => 'id']);
    }

    /**
    *@return \frontend\models\login\LoginForm
    **/
    public static function findByEmail($email){
        return static::findOne(['email' => $email]);
    }

    /**
    *@return \frontend\models\login\LoginForm
    **/
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->getClientsPwds()->password);
    }

    /*
    ** Funciones propias
    **----------------------
    */

    public function Shortname(){
        
        $nombres = $this->firstname;
        $apellidos = $this->lastname;
        
        $firstname = strstr($nombres, ' ',true);
        $lastname = strstr($apellidos, ' ', true);
        
        if(empty($firstname))
            $firstname = $nombres;
        else
            $firstname = $firstname;

        if(empty($lastname))
            $lastname = $apellidos;
        else
            $lastname = $lastname;
        
        return ucwords(strtolower($firstname. ' '. $lastname));    
    
    }

    public function ShortFirstname(){
        $name = $this->firstname;
        $name = strstr($name, ' ', true);
        if($name==''){
            $name= $this->firstname;
        }

        return ucwords(strtolower($name));
    }

    public function creditEstatus(){

        //variables para asignor las clases al semaforo
        $success ="";
        $warning ="";
        $danger ="";

        $status= " Sin credito activo";

        //verifico si tiene credito vigente 
        $model = $this->getLoans()
                    ->where(['id_status'=>[41,42,43]]);
        if($model->exists()){
            $model = $model->one();
            $code = $model->id_status;
            $status = ' Credito '.$model->getIdStatus()->one()->name;

            //asigno la clase ccs
            switch ($model->id_status) {
                case 41:
                    $success = 'bg-green';
                    break;
                case 42:
                    $warning ="bg-orange";
                    break;
                case 43;
                    $danger ="bg-red";
                    break;
            }

        }
        return ['text'    => $status,
                'success' => $success,
                'warning' => $warning,
                'danger'  => $danger
                ];
    }

    public function Nexlevel($loan){
        
        $puntos = $loan->points;

        $cliente = ClientsAdmin::find(['id_client'=>$this->id]);
        if($cliente->exists()){
            $cliente = $cliente->one();
            $cliente->experience_points = $cliente->experience_points + $puntos;
        } else{
            $cliente = new ClientsAdmin;
            $cliente->id_client = $this->id;
            $cliente->experience_points = $puntos; 
        }

        $cliente->save(false);

        $puntosrequeridos = $this->getIdLevel()->one()->points_required;
        if($cliente->experience_points >= $puntosrequeridos){
            $this->id_level = $this->getIdLevel()->one()->id_level_next;
            $this->update();
            $level = level::findOne(['id'=>$this->id_level]);
            $cliente->godson_available = $cliente->godson_available +$level->godson_cant;
            $cliente->update();
        }
    }

}
