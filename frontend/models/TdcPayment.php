<?php

namespace frontend\models;
use frontend\models\RecordPayment;
use frontend\models\LoanQuota;
use frontend\models\Loan;
use frontend\models\Arrears;


use Yii;

/**
 * This is the model class for table "tdc_payment".
 *
 * @property integer $id_record_payment
 * @property string $consortium
 * @property string $cardholder
 * @property string $last_four_digits
 * @property string $exp_date
 *
 * @property RecordPayment $idRecordPayment
 */
class TdcPayment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tdc_payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_record_payment', 'consortium', 'cardholder', 'last_four_digits', 'exp_date'], 'required'],
            [['id_record_payment'], 'integer'],
            [['consortium'], 'string', 'max' => 50],
            [['cardholder'], 'string', 'max' => 200],
            [['last_four_digits'], 'string', 'max' => 4],
            [['exp_date'], 'string', 'max' => 8]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_record_payment' => 'Id Record Payment',
            'consortium' => 'Consortium',
            'cardholder' => 'Cardholder',
            'last_four_digits' => 'Last Four Digits',
            'exp_date' => 'Exp Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRecordPayment()
    {
        return $this->hasOne(RecordPayment::className(), ['id' => 'id_record_payment']);
    }

    //metodos de Pagos TDC
    
    //Metodo para pagos Aprobados
    public function PagoTdcAprobado($model){
        if($model->ncuota == 'ALL'){
            
            $TotalToPay = $model->totalpay();
            $id_loan    = $model->IdLoanPayment();
            $Loan       = Loan::findOne($id_loan);
            $residuo    = $model->amount - round($TotalToPay['quota'],2);

            //Default Values
            $Loan->id_status = 40;
            $UpdateValues     = [
                                'pending_pay' => 0,
                                'id_status'   => 32
                               ];

            if($TotalToPay['arrear'] > 0){
                
                if($residuo >= round($TotalToPay['arrear'],2)){//Pago todo
                    
                    $UpdateValues= array_merge($UpdateValues, ['have_arrear' => false]);

                    $modCLients = Clients::findOne(['id'=>Yii::$app->user->identity->id]);
                    $modCLients->Nexlevel($Loan);
                }
                elseif($residuo > 0){
                    $Loan->id_status = 43;
                    $this->UpdateLoanQuotaAndArrear($id_loan,$residuo);
                    $Loan->update();
                    return 'nothing';
                }
                else{
                    $Loan->id_status = 43;

                }

            }else{
                $modCLients = Clients::findOne(['id'=>Yii::$app->user->identity->id]);
                $modCLients->Nexlevel($Loan);
            }

            LoanQuota::UpdateAll($UpdateValues,
                                 'id_loan=:id_loan',
                                 ['id_loan'=>$id_loan]
                                );
            $Loan->update();

        }else{//pagos de cuotas
            $LoanQuota = LoanQuota::findOne($model->ncuota);
            if($model->type=='cuota' or $model->type=='ambos'){
                $residuo = $LoanQuota->pending_pay - $model->amount;
                if ($residuo<=0){
                    $LoanQuota->pending_pay = 0;
                    $LoanQuota->id_status= 32;
                    if($model->type=='ambos'){
                        $LoanQuota = $this->DescontarMora($LoanQuota,$residuo);
                    }
                }
                else{
                    $LoanQuota->pending_pay= $resiuo;
                }
            }
            if($model->type=='mora'){
                $LoanQuota = $this->DescontarMora($LoanQuota,$model->amount);
            }
            $LoanQuota->update();

        }
    }

    //actualiza todas los loan y loan quota de un solo golpe
    private function UpdateLoanQuotaAndArrear($id_loan,$residuo){

        $LoanQuotas = LoanQuota::findAll(['id_loan'=>$id_loan]);
        foreach ($LoanQuotas as $LoanQuota) {

            $LoanQuota->pending_pay = 0;
            $LoanQuota->id_status   = 32;

            if($residuo > 0){
                $arrear = Arrears::findOne(['id_loan_quota'=>$LoanQuota->id]);

                if($arrear->pending_pay <= $residuo){

                    $arrear->pending_pay    = 0;
                    $arrear->id_status      = 46;
                    $LoanQuota->have_arrear = false;

                    //actualizo residuo
                    $residuo = $residuo - $arrear->pending_pay;

                }else{
                    $arrear->pending_pay = $arrear->pending_pay-$residuo;
                    $residuo = 0;
                }
                $arrear->update();
            }
            $LoanQuota->update();
        }

    }

    private function DescontarMora($LoanQuota,$MontoQuitar){
        $arrear = Arrears::findOne(['id_loan_quota'=>$LoanQuota->id]);
        if($arrear->pending_pay<=$MontoQuitar){
            $arrear->pending_pay = 0;
            $arrear->id_status = 46;
            $LoanQuota->have_arrear=false;
        }
        else{
            $LoanQuota->have_arrear=true;
            $arrear->pending_pay = $arrear->pending_pay-$MontoQuitar;
        }
        $arrear->update();
        return $LoanQuota;
    }

    //Metodo para pagos rechazados
    public function PagoTdcARechazado($model,$TdcPayment,$modTdcPayment, $new){
        $statusCode = 52;
        switch ($TdcPayment['response']['status_detail']) {
            case 'cc_rejected_bad_filled_card_number':
                throw new \Exception('Revisa el número de tarjeta.');
                break;
            case 'cc_rejected_bad_filled_date':
                throw new \Exception('Revisa la fecha de vencimiento.');
                break;

            case 'cc_rejected_bad_filled_other':
                throw new \Exception('Revisa los datos de la tarjeta ingresada.');
                break;

            case 'cc_rejected_bad_filled_security_code':
                throw new \Exception('Revisa el código de seguridad.');
                break;

            default:
                break;

        }
        return $modTdcPayment->SaveTdcPaymentBD($model,$statusCode,$TdcPayment,$modTdcPayment, $new);

    }

    public function SaveTdcPaymentBD($model,$statusCode,$TdcPayment,$modTdcPayment, $new){
        //Guardo Payment Record
        if($new){
            $model->id_client =  Yii::$app->user->identity->id;    
            $model->date_add  =  date('Y-m-j');
            $model->pay_date  = date('Y-m-j',strtotime($TdcPayment['response']['date_created']));
            $model->ispayloan =($model->ncuota=='ALL')?true:false;
            $model->reference = $TdcPayment['response']['id'];
        }

        $model->id_status = $statusCode;
        $model->save($new ? true : false);

        //Guardo TdcPayment
        $modTdcPayment->id_record_payment = $model->id;
        $modTdcPayment->consortium=$TdcPayment['response']['payment_method_id'];
        $cardResponse =$TdcPayment['response']['card']; 
        $modTdcPayment->cardholder= $cardResponse['cardholder']['name'];
        $modTdcPayment->last_four_digits= $cardResponse['last_four_digits'];
        $modTdcPayment->exp_date= $cardResponse['expiration_month'].'-'.$cardResponse['expiration_year'];
        $modTdcPayment->date_update= date('Y-m-j',strtotime($TdcPayment['response']['date_last_updated']));
        $modTdcPayment->description= $TdcPayment['response']['description'];
        $modTdcPayment->status_detail= $TdcPayment['response']['status_detail'];
        $modTdcPayment->save();

        $model->SavePaymentNotAll($model);//detalles del pago (cuota,mora o ambos)

        return $model->id;

    }

    public function ProcesarPagoTdc($model,$TdcPayment,$modTdcPayment, $new = true){
        switch ($TdcPayment['response']['status']) {
            case 'approved':
                $statusCode = 51;
                $result = $this->PagoTdcAprobado($model);
                return $this->SaveTdcPaymentBD($model,$statusCode,$TdcPayment,$modTdcPayment, $new);                
                break;
            case 'in_process':
                $statusCode = 50;
                return $this->SaveTdcPaymentBD($model,$statusCode,$TdcPayment,$modTdcPayment, $new);
                break;
            case 'pending':
                $statusCode = 50;
                return $this->SaveTdcPaymentBD($model,$statusCode,$TdcPayment,$modTdcPayment, $new);
                break;
            case 'rejected':
                $this->PagoTdcARechazado($model,$TdcPayment,$modTdcPayment, $new);
                break;
            default:
                $this->PagoNoProcesado();
                break;
        }
    }

}
