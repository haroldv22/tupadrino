<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "period".
 *
 * @property integer $id
 * @property integer $n_days
 * @property string $alias
 * @property integer $points
 *
 * @property PeriodAmount[] $periodAmounts
 */
class Period extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'period';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['n_days', 'points'], 'required'],
            [['n_days', 'points'], 'integer'],
            [['alias'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'n_days' => 'numero de dias',
            'alias' => 'alias del plazo Ej( 1 mes, 2 meses,..)',
            'points' => 'Puntos por cumplir ese periodo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodAmounts()
    {
        return $this->hasMany(PeriodAmount::className(), ['id_period' => 'id']);
    }

    public function getCode(){
        return $this->points.'-'.$this->n_days;
    }
}
