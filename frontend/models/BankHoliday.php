<?php

namespace frontend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "bank_holiday".
 *
 * @property integer $id
 * @property string $holiday
 * @property string $description
 */
class BankHoliday extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bank_holiday';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['holiday'], 'required'],
            [['holiday'], 'safe'],
            [['description'], 'string', 'max' => 75]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'holiday' => 'fechas no laborables del A&o',
            'description' => 'descripcion del dia festivo',
        ];
    }

    public static function getHolidays($year){
        $result= static::find()
            ->select("holiday,row_number() OVER () as id")
            ->where("TO_CHAR(holiday,'YYYY')='".$year."'")
            ->all();
           
       $result= ArrayHelper::map($result,'id','holiday');
        return  \yii\helpers\Json::encode($result);
    }
    public function afterFind()
    {
        $this->holiday = date("m/d/Y", strtotime($this->holiday));


    }
}
