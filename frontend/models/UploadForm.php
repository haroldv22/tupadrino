<?php

namespace frontend\models;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;

    public function rules()
    {
        return [
            ['imageFile','required'],
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'jpg'],
        ];
    }
    
    public function upload($Dir,$x,$y,$w,$h)
    {
        if ($this->validate()) {
            ///Recorta la imagen
                $targ_w = $targ_h = 263;
                $jpeg_quality = 100;

                $src = $this->imageFile->tempName;
                $img_r = imagecreatefromjpeg($src);
                $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

                imagecopyresampled($dst_r,$img_r,0,0,$x,$y,$targ_w,$targ_h,$w,$h);
                imagejpeg($dst_r,$src,$jpeg_quality);
            //fin de recorte

            $this->imageFile->saveAs('.'.$Dir. $this->imageFile->baseName . '.' . $this->imageFile->extension);
            return true;
        } else {
            return false;
        }
    }
}