<?php

namespace frontend\models;

use Yii;
use yii\base\Security;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "client_invitation".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $invitation_code
 * @property integer $id_status
 * @property integer $id_godfather
 *
 * @property Status $idStatus
 */
class ClientInvitation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_invitation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'invitation_code', 'id_status', 'id_godfather'], 'required'],
            [['id_status', 'id_godfather'], 'integer'],
            [['name'], 'string', 'max' => 25],
            [['email'], 'string', 'max' => 75],
            [['invitation_code'], 'string', 'max' => 128],
            [['email'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Nombre',
            'email' => 'Email',
            'invitation_code' => 'codigo de invitacion para el usuario',
            'id_status' => 'Estatus',
            'id_godfather' => 'id del padre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdStatus(){
        return $this->hasOne(Status::className(), ['id' => 'id_status']);
    }
    
    /**
     * permite verificar el token enviado al usuario
     * mediante correo electronico
     * @return true|false
     */ 
    public function validateToken($token){        
        return static::find()
                    ->where(['invitation_code'=>$token,'id_status'=>13])
                    ->exists();    
    }
    public function searchEmail($token){        
        $result = static::find()
                    ->select('email')
                    ->where(['invitation_code'=>$token,'id_status'=>13])
                    ->one();  

        return $result->email;  
    }

    public function searchGofather($token){        
        $result = static::find()
                    ->select('id_godfather')
                    ->where(['invitation_code'=>$token,'id_status'=>13])
                    ->one();  

        return $result->id_godfather;  
    }

    /**
     * Se encarga de generar un codigo basandose en ciertos
     * patrones para ser asignados al usuario a invitar
     *      
     * return string 
     */ 
    public function getInvitationCode(){
        
        $security = new Security();                
        $code =  utf8_decode($security->generateRandomString(12).''.getdate()['year'].''.getdate()['yday'].''.getdate()['hours'].''.getdate()['seconds']);
        
        return $code;
    }
    
    /**
     * Este metodo se encarga de enviar un mail al usuario 
     * para notificarle que puede hacer uso de la plataforma tupadrino.net
     *
     * @param fromEmail String  Email remitente
     * @param toEmail   String  Email del del invitado
     * @param user      String  Nombre del invitado
     */ 
    public function sentInvitationUser($fromEmail,$toEmail,$user,$code_token){
        
      $sent =   Yii::$app->mailer->compose('layouts/html')
                    ->setFrom($fromEmail)
                    ->setTo($toEmail)
                    ->setSubject('Invitacion tu padrino.net')                                   
                    ->setHtmlBody('Saludos <b>'.$user.'</b>
                                    <br /><br /> La presente es para notificarle
                                    que ha sido seleccionado para adquirir un 
                                    <b>Microcredito</b> en nuestra nueva plataforma 
                                    <b> TuPadrino.net </b> para acceder al panel y hacer 
                                    la presolicitud debe hacer click en: <a href="http://ec2-52-32-73-69.us-west-2.compute.amazonaws.com/frontend/web/index.php/registro/registration?token='.$code_token.'" target="_blank">
                                       Pulsa Aquí </a>')
                    ->send();
    }
       
}
