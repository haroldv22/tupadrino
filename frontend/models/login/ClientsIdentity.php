<?php

namespace frontend\models\login;

use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\web\IdentityInterface;

class ClientsIdentity extends Model implements IdentityInterface
{

  
    public static function findIdentity($id){                        
        return static::findOne(['id' => $id, 'id_status' => self::STATUS_ACTIVE]);        
    }
     
     /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByEmail($username){
        return static::findOne(['username' => $username, 'id_status' => self::STATUS_ACTIVE]);
    }
    
     /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }
    
    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
    
      /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
//        if (!static::isPasswordResetTokenValid($token)) {
//            return null;
//        }
//        return static::findOne([
//            'password_reset_token' => $token,
//            'id_status' => self::STATUS_ACTIVE,
//        ]);
    }
    
    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
//        if (empty($token)) {
//            return false;
//        }
//        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
//        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
//        return $timestamp + $expire >= time();
    }
    
    /**
     * @inheritdoc
     */
    public function getId(){
        return $this->getPrimaryKey();
    }
    
    /**
     * @inheritdoc
     */
    public function getAuthKey(){
//        return $this->auth_key;
    }
    
    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey){
        return $this->getAuthKey() === $authKey;
    }

}
