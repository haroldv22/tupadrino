<?php
namespace frontend\models\login;

use Yii;
use yii\base\Model;
#model active records
use  frontend\models\login\Clients;


/**
 * Login form
 */
class LoginForm extends Model
{
    public $email;
    public $password;
    public $rememberMe = true;

    private $_client;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validateIdentity'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateIdentity($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $client = $this->getClient();                                                                                     
            if (!$client || !$client->validatePassword($this->password)) {
                $this->addError($attribute,'Incorrect username or password.');
            }
        }
    }

    /**
     * Abre una sesión un usuario utilizando el nombre de usuario
     * y la contraseña proporcionada.
     *
     * @return boolean true|false si el usuario sea registrado con exito.
     */
    public function login()
    {
        if ($this->validate()) {                                    
            return Yii::$app->user->login($this->getClient(), $this->rememberMe ? 3600 * 24 * 30 : 0);                        
        } else {
            return false;
        }
    }

    /**
     * Busca el Usuario por [[username]]
     *
     * @return User|null
     */
    protected function getClient()
    {
        if ($this->_client === null) {
            $this->_client = Clients::findByEmail($this->email);
        }

        return $this->_client;
    }

 


}
