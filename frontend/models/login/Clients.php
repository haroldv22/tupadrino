<?php

namespace frontend\models\login;

use Yii;
use yii\web\IdentityInterface;
use yii\base\NotSupportedException;
use frontend\models\ClientsPwd;
use frontend\models\ClientsAdmin;



/**
 * This is the model class for table "clients".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $gender
 * @property integer $id_godfather
 * @property integer $id_level
 * @property integer $count_client
 * @property string $birth_date
 * @property string $identity
 * @property string $date_add
 *
 * @property Address[] $addresses
 * @property BankAccount[] $bankAccounts
 * @property Level $idLevel
 * @property ClientsDefaulter[] $clientsDefaulters
 * @property ClientsPwd $clientsPwd
 * @property Loan[] $loans
 * @property Question[] $questions
 * @property RecordPayment[] $recordPayments
 */
class Clients extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'email', 'gender', 'birth_date', 'date_add'], 'required'],
            [['id_godfather', 'id_level', 'count_client'], 'integer'],
            [['birth_date', 'date_add'], 'safe'],
            [['firstname', 'lastname'], 'string', 'max' => 50],
            [['email'], 'string', 'max' => 75],
            [['gender'], 'string', 'max' => 1],
            [['identity'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id de la tabla',
            'firstname' => 'firstname',
            'lastname' => 'lastname ',
            'email' => 'email',
            'gender' => 'gender',
            'id_godfather' => 'id godfather ',
            'id_level' => 'id level',
            'count_client' => 'contador de creditos completados por el cliente',
            'birth_date' => 'fecha de nacimiento',
            'identity' => 'cedula de identida',
            'date_add' => 'fecha de registro',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(Address::className(), ['id_client' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankAccounts()
    {
        return $this->hasMany(BankAccount::className(), ['id_client' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLevel()
    {
        return $this->hasOne(Level::className(), ['id' => 'id_level']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientsDefaulters()
    {
        return $this->hasMany(ClientsDefaulter::className(), ['id_client' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientsPwd()
    {
        return $this->hasOne(ClientsPwd::className(), ['id_client' => 'id']);
    }

    public function getClientsAdmin()
    {
        return $this->hasOne(ClientsAdmin::className(), ['id_client' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoans()
    {
        return $this->hasMany(Loan::className(), ['id_client' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasMany(\frontend\models\Question::className(), ['id_client' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecordPayments()
    {
        return $this->hasMany(RecordPayment::className(), ['id_client' => 'id']);
    }

    /**
    *@return \frontend\models\login\LoginForm
    **/
    public static function findByEmail($email){
        return static::findOne(['email' => $email]);
    }

    /**
    *@return \frontend\models\login\LoginForm
    **/
    public function validatePassword($password)
    {
        $hash =$this->getClientsPwd()->one()->password;
        return Yii::$app->security->validatePassword($password, $hash);
    }

    /* Login Identity function */


     public static function findIdentity($id){                        
        return static::findOne(['id' => $id]);        
    }

     public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
    
      /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
//        if (!static::isPasswordResetTokenValid($token)) {
//            return null;
//        }
//        return static::findOne([
//            'password_reset_token' => $token,
//            'id_status' => self::STATUS_ACTIVE,
//        ]);
    }
    
    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
//        if (empty($token)) {
//            return false;
//        }
//        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
//        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
//        return $timestamp + $expire >= time();
    }
    
    /**
     * @inheritdoc
     */
    public function getId(){
        return $this->getPrimaryKey();
    }
    
    /**
     * @inheritdoc
     */
    public function getAuthKey(){
//        return $this->auth_key;
    }
    
    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey){
        return $this->getAuthKey() === $authKey;
    }
    public function Shortname(){
        $nombres = $this->firstname;
        $apellidos = $this->lastname;

        $firstname = strstr($nombres, ' ',true);
        $lastname = strstr($apellidos, ' ', true);

        if(empty($firstname))
            $firstname = $nombres;
        else
            $firstname = $firstname;

        if(empty($lastname))
            $lastname = $apellidos;
        else
            $lastname = $lastname;

        return ucwords(strtolower($firstname. ' '. $lastname));
    }
    
    public function ShortFirstname(){
        $name = $this->firstname;
        $name = strstr($name, ' ', true);
        if($name==''){
            $name= $this->firstname;
        }

        return ucwords(strtolower($name));
    }

    //Verifica si el usuario tiene ahijados disponibles
    public function allowInvitation(){
      
        $Avible = $this->getClientsAdmin();
        if($Avible->exists()){
            if($Avible->One()->godson_available > 0){
                return true;
            }
        }
        return false;
    }

    //devuelve el style para la barra de progreso de niveles
    public function getCompleteBar(){
        $required = $this->getIdLevel()->One()->points_required;
        $actual = 0;
        if ($this->getClientsAdmin()->exists()){
            $actual = $this->getClientsAdmin()->One()->experience_points;
        }

        $result=(($actual * 0.78)/$required)*100;
        if($result>78){
            $result = 78;
        }
        $result = 'style="width: '.$result.'%"';
        return $result;
    }

  public function getNameLevel(){
    switch ($this->id_level) {
      case 1:
        return "Ahijado Inicial";
        break;
      
      case 2:
        return "Padrino Junior";
        break;

      case 3:
        return "Padrino Senior";
        break;
      
      case 4:
        return "Padrino Gold";
        break;

      case 5:
        return "Padrino Premium";
        
        break;

      case 6:
        return"Master Padrino";
        break;
    }
  }
    

}
