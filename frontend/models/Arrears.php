<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "arrears".
 *
 * @property integer $id
 * @property integer $id_client
 * @property integer $id_loan_quota
 * @property integer $delay_days
 * @property string $total_interest
 * @property integer $id_status
 * @property string $upd_date
 * @property string $pending_pay
 * @property string $add_date
 *
 * @property Clients $idClient
 * @property LoanQuota $idLoanQuota
 * @property Status $idStatus
 * @property ArrearsDetails[] $arrearsDetails
 * @property ArrearsPayment[] $arrearsPayments
 */
class Arrears extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'arrears';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_client', 'id_loan_quota', 'delay_days', 'total_interest', 'id_status', 'upd_date', 'add_date'], 'required'],
            [['id_client', 'id_loan_quota', 'delay_days', 'id_status'], 'integer'],
            [['total_interest', 'pending_pay'], 'number'],
            [['upd_date', 'add_date'], 'safe'],
            [['id_loan_quota'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_client' => 'Id Client',
            'id_loan_quota' => 'Id Loan Quota',
            'delay_days' => 'Delay Days',
            'total_interest' => 'Total Interest',
            'id_status' => 'Id Status',
            'upd_date' => 'Upd Date',
            'pending_pay' => 'Pending Pay',
            'add_date' => 'Add Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'id_client']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLoanQuota()
    {
        return $this->hasOne(LoanQuota::className(), ['id' => 'id_loan_quota']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'id_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArrearsDetails()
    {
        return $this->hasMany(ArrearsDetails::className(), ['id_arrears' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArrearsPayments()
    {
        return $this->hasMany(ArrearsPayment::className(), ['id_arrears' => 'id']);
    }
}
