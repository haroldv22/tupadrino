<?php

namespace frontend\models;

use Yii;
use frontend\models\CellphonePrefix;
use frontend\models\PhonePrefix;
use yii\helpers\ArrayHelper;
use frontend\models\State;
use frontend\models\Municipality;
use frontend\models\City;


/**
 * This is the model class for table "address".
 *
 * @property integer $id
 * @property integer $id_client
 * @property string $address
 * @property integer $id_municipality
 * @property integer $id_state
 * @property integer $id_parish
 * @property integer $id_city
 * @property string $cellphone
 * @property string $phonenumber
 *
 * @property City $idCity
 * @property Clients $idClient
 * @property Municipality $idMunicipality
 * @property Parish $idParish
 * @property State $idState
 */


class Address extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $cellphoneprefix;
    public $phoneprefix;
    public $newcellphone;
    public $newphonenumber;
    public static function tableName()
    {
        return 'address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_client', 'address', 'id_municipality', 'id_state','id_city', 'cellphone'], 'required'],
            [['id_client', 'id_municipality', 'id_state','id_city'], 'integer'],
            [['address'], 'string', 'max' => 255],
            [['cellphone', 'phonenumber'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_client' => 'Id Client',
            'address' => 'Direccion',
            'id_municipality' => 'Municipio',
            'id_state' => 'Estado',
            'id_city' => 'Ciudad o Localidad',
            'cellphone' => 'numero celular',
            'phonenumber' => 'numero telefonico',
            'newcellphone' => 'Número celular',
            'newphonenumber' => 'Número Telefónico'

        ];
    }

    public function getState(){ 
       return ArrayHelper::map(State::find()->all(), 'id','name');
    }

    public static function getMunicipality($id){ 
       return ArrayHelper::map(Municipality::find()->where(['id_state'=> $id])->all(),'id','name');
    }
    /*
    public static function getParish($id){ 
       return ArrayHelper::map(Parish::find()->where(['id_municipality'=> $id])->all(),'id','name'); 
    }*/
    public static function getCity($id){ 
       return ArrayHelper::map(City::find()->where(['id_state'=> $id])->all(),'id','name');
    }

    public function getCellphonePrefix(){ 
       return ArrayHelper::map(CellphonePrefix::find()->all(), 'prefix','prefix');
    }
    public function getPhonePrefix(){ 
       return ArrayHelper::map(PhonePrefix::find()->all(), 'prefix','prefix');
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCity()
    {
        return $this->hasOne(City::className(), ['id' => 'id_city']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'id_client']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMunicipality()
    {
        return $this->hasOne(Municipality::className(), ['id' => 'id_municipality']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdState()
    {
        return $this->hasOne(State::className(), ['id' => 'id_state']);
    }
}
