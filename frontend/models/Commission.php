<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "commission".
 *
 * @property integer $id
 * @property integer $id_client
 * @property integer $id_commission_round
 * @property integer $id_loan
 * @property string $commission
 * @property string $commission_rate
 * @property integer $id_status
 * @property string $end_date
 *
 * @property Clients $idClient
 * @property CommissionRound $idCommissionRound
 * @property Loan $idLoan
 * @property Status $idStatus
 */
class Commission extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'commission';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_client', 'id_commission_round', 'id_loan', 'commission', 'commission_rate', 'id_status'], 'required'],
            [['id_client', 'id_commission_round', 'id_loan', 'id_status'], 'integer'],
            [['commission', 'commission_rate'], 'number'],
            [['end_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_client' => 'Ahijado',
            'id_commission_round' => 'id_ronda',
            'id_loan' => 'id_credito',
            'commission' => 'Comision',
            'commission_rate' => 'porcentaje de la comision',
            'id_status' => 'Estatus',
            'end_date' => 'Fecha de Finalizacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'id_client']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCommissionRound()
    {
        return $this->hasOne(CommissionRound::className(), ['id' => 'id_commission_round']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLoan()
    {
        return $this->hasOne(Loan::className(), ['id' => 'id_loan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'id_status']);
    }

    //Coloca la clase css a cada estatus
    public function Cssclass(){
        switch ($this->id_status) {
            case 65://En Mora
                return 'label-danger';
                break;
            case 66://En Riesgo
                return 'label-warning';
                break;
            case 67://Puntual
                return 'label-success';
                break;
            case 68://Finalizado
                return 'label-success';
                break;  
        }

    }

    public function Valuehtml(){
        $formatter = \Yii::$app->formatter;
        switch ($this->id_status) {
            case 65://En Mora
                return '<s>'.$formatter->asDecimal($this->commission).'</s>';
                break;
            case 66://En Riesgo
                return '<s>'.$formatter->asDecimal($this->commission).'</s>';
                break;
            default:
                return $formatter->asDecimal($this->commission);
                break;
        }
    }

}
