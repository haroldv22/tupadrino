<?php
namespace frontend\models\registro;

use yii\base\Model;
use frontend\models\Clients;
use frontend\models\State;
use frontend\models\Municipality;
use frontend\models\Parish;
use frontend\models\City;
use frontend\models\Banks;
use frontend\models\Ocupation;
use frontend\models\CellphonePrefix;
use frontend\models\PhonePrefix;
use yii\helpers\ArrayHelper;


class DatosPersonales extends Model
{
	public $identity;//Cedula
    public $pocupation;
    public $email;
    public $firstname;
    public $lastname;
    public $gender;
    public $birth_date;
    public $address;
    public $id_state;
    public $id_municipality;
   // public $id_parish;
    public $id_city;
    public $code_bank;
    public $number_account;
    public $cellphone;
    public $phonenumber;
    public $typeid;
    public $cellphoneprefix;
    public $phoneprefix;
    public $ocupation;


    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [

            
            ['identity','idvalidate','skipOnEmpty' => false,'skipOnError' => false],
            //['identity','unique','targetClass'=>'\frontend\models\Clients'],
            ['pocupation','safe'],
            [['identity','firstname','lastname','gender','birth_date','id_state',
            'id_municipality','id_city','cellphone',
            'number_account','code_bank','address','ocupation'], 'required'],
            ['number_account','string','length' => [16, 16],
            'tooLong'=>'El numero de cuenta debe contener 16 digitos',
            'tooShort'=>'El numero de cuenta debe contener 16 digitos',
            ],
            [['phonenumber','cellphone'],'string','length' => [7, 7],
             'tooLong'=>'El numero debe contener al menos 7 digitos',
            'tooShort'=>'El numero debe contener al menos 7 digitos',
            ],
            [['number_account','cellphone','phonenumber','identity',
            'cellphoneprefix','phoneprefix'],'integer'],
            [['typeid','cellphoneprefix'], 'required','message'=>'Seleccione'],
            ['birth_date', 'validateBirth_date'],
            ['ocupation', 'validocupation'],
            ['number_account','verificarCuenta'],
        //[['identity','SearchIdentity']]
        ];
    }
    public function attributeLabels()
    {
        return [
            'pocupation'=>'Escriba Ocupacion',
            'identity' => 'Cedula',
            'firstname' => 'Nombres',
            'lastname' => 'Apellidos ',
            'gender' => 'Sexo',
            'birth_date' => 'Fecha de nacimiento',
            'address'=>'Direccion',
            'id_state'=>'Estado',
            'id_municipality'=>'Municipio',
           // 'id_parish'=>'Parroquia',
            'id_city'=>'Ciudad o Localidad',
            'code_bank'=>'Banco',
            'number_account'=>'Numero de cuenta',
            'cellphone'=>'Número celular',
            'phonenumber'=>'Número Telefónico',
            'ocupation'=>'ocupación',
                  
        ];
     }

     /*
     ** Validaciones
     **--------------
     */

    public function validocupation(){
        if($this->ocupation=='Otro' && empty(trim($this->pocupation))){
            $this->addError('pocupation','Debe escribir su ocupación');
        }
    }

    public function idvalidate(){
        $valido = Clients::find()->where(['identity'=>$this->typeid.$this->identity])->exists();
         if ($valido) {
            $this->addError('identity', 'El numero de identificación ingresado, ya se encuentra registrado');
        }

     }
    public function validateBirth_date($attribute, $params) {
        $date = new \DateTime();
        date_sub($date, date_interval_create_from_date_string('18 years'));
        $min = date_format($date, 'd-m-Y');

        date_sub($date, date_interval_create_from_date_string('100 years'));
        $max = date_format($date, 'd-m-Y');
            if (strtotime($this->$attribute) > strtotime($min)) {
                $this->addError($attribute, 'Debes ser mayor de edad');
            }
            if (strtotime($this->$attribute) < strtotime($max)) {
                 $this->addError($attribute, 'Eres demasiado viejo para estar vivo');
            }
    }
    public function verificarCuenta(){
        $banco = $this->code_bank;
        $cuentac = $this->number_account;
        $oficina = substr($cuentac,0,4);
        $cuenta = substr($cuentac,6,10);
        $calc1 = str_split($banco.$oficina);
        $calc2 = str_split($oficina.$cuenta);

        $peso =[0=>3,1=>2,2=>7,3=>6,4=>5,5=>4,6=>3,7=>2,8=>7,9=>6,10=>5,11=>4,12=>3,13=>2];
        
        $calc = [0=>$calc1,1=>$calc2];
        $dc = '';
        foreach ($calc as $calvalue) {
            $dig = 0;
            foreach ($calvalue as $key => $value) {
                $dig+=$value*$peso[$key];
            }
            $entero =intval($dig/11);
            $residuo = intval($dig%11);
            $result = 11-$residuo;
            if($result==11){
                $result = 1;
            }
            elseif ($result==10) {
                $result =0;
            }
            $dc.=$result;
        }
        $dco = substr($cuentac,4,2);
        if($dco != $dc){
            $this->addError('number_account', 'La cuenta ingresada es incorrecta.');
        }
    }


     /*
     ** Funciones del View
     **-------------------
     */
    public function getState(){ 
       return ArrayHelper::map(State::find()->all(), 'id','name');
    }

    public static function getMunicipality($id){ 
       return ArrayHelper::map(Municipality::find()->where(['id_state'=> $id])->all(),'id','name');
    }
    /*
    public static function getParish($id){ 
       return ArrayHelper::map(Parish::find()->where(['id_municipality'=> $id])->all(),'id','name'); 
    }*/
    public static function getCity($id){ 
       return ArrayHelper::map(City::find()->where(['id_state'=> $id])->all(),'id','name');
    }
     public function getBanks(){ 
       return ArrayHelper::map(Banks::find()->all(), 'code_bank','name');
    }
    public function getCellphonePrefix(){ 
       return ArrayHelper::map(CellphonePrefix::find()->all(), 'prefix','prefix');
    }
    public function getPhonePrefix(){ 
       return ArrayHelper::map(PhonePrefix::find()->all(), 'prefix','prefix');
    }
    public function getOcupation(){ 
       $return =  ArrayHelper::map(Ocupation::find()->all(), 'ocupation','ocupation');
        return array_merge($return,['Otro'=>'Otro']);
    }


}
