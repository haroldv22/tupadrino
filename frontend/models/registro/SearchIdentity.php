<?php
/**
 * @author Yostin Vargas <Yostinv@gmail.com>
 * @since 20/10/2015
 * @version 1.0
 * Busca Numeros de cedulas enel CNE y SENIAT
 * Devolviendo un array con nombres y apellidos 
 */
namespace frontend\models\registro;
use yii\base\Model;

class SearchIdentity extends Model
{
    public function Identity($cedula) {

        $nac=substr($cedula,0,1);
        $ci=substr($cedula,1);

        #Busqueda CNE 
        $url = "http://www.cne.gov.ve/web/registro_electoral/ce.php?nacionalidad=$nac&cedula=$ci";
        $response = $this->getUrl($url);
        $text = strip_tags($response); //Retira las etiquetas HTML y PHP de un string
   http://192.168.0.102/microcredito/frontend/web/index.php/registro/registration?step=Presolicitud
        $exist = strpos($text, 'Nombre:');
        if($exist == true){
            $result=strstr($text,'Estado:',true);
            $result=strstr($result,'Nombre:',false);
            $result=substr(strrchr($result,':'),1);
            $result = trim(strtoupper($result));
            $result= explode(" ",$result);
            return($this->separate($result));
        }
        else{
             #Busqueda SENIAT
            $url = "http://contribuyente.seniat.gob.ve/getContribuyente/getrif?rif=".$this->getRif($nac,$ci);
            $response = $this->getUrl($url);
            //print_r($response);
            libxml_use_internal_errors(true);
            $xml = simplexml_load_string($response);
            if ($xml!==false){
                $xml = $xml->children('rif');
                $result= $xml->{'Nombre'};
                $result= explode(" ",$result);
                return($this->separate($result));
            }
            else{
                return 'null';
            }


        }
        
    }

    /*
    Esta funcion se encarga de Separar los nombres 
    de los apellidos.
    */
    Private function separate($array){
         $count = count($array);
        
        switch ($count) {
            case ($count==4):
                if( preg_match('/(DI|DA|DE|DEL)\b/i',$array[2])){
                    return array('nombre'=>$array[0], 'apexplode php characterellido'=>$array[1].' '.$array[2].' '.$array[3]);  
                } 
                if (preg_match('/(AL|DEL|EL|LA)\b/i',$array[1])){
                    return array('nombre'=>$array[0].' '.$array[1].' '.$array[2], 'apellido'=>$array[3]);  
                }
                return array('nombre'=>$array[0].' '.$array[1], 'apellido'=>$array[2].' '.$array[3]);

            case ($count==2):
                return array('nombre'=>$array[0], 'apellido'=>$array[1]);

            case ($count==3):
                return array('nombre'=>$array[0], 'apellido'=>$array[1].' '.$array[2]); 
            case($count>4):
                if ($count%2==0){
                   $mitad = ($count/2);
                }
                else{
                    $mitad = (($count-1)/2);
                }
                for ($i=0; $i < $count; $i++) { 
                     //Nombre
                        if($i<=$mitad){
                            if(!isset($nombre)){
                                $nombre =$array[$i];
                                continue;
                            }   
                            if($i == $mitad){
                                if( preg_match('/(DI|DA|DE|DEL)\b/i',$array[$i])){
                                    $apellido =$array[$i];
                                    continue;
                                }
                            }
                            $nombre .=' '.$array[$i];
                        }
                        //apellido
                        if($i>$mitad){
                            if(!isset($apellido)){
                                $apellido =$array[$i];
                                continue;
                            }
                            $apellido .=' '.$array[$i];     
                        }                
                }
                return array('nombre'=>$nombre, 'apellido'=>$apellido);      
        }
    }

    private function getUrl($url) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // almacene en una variable
        curl_setopt($curl, CURLOPT_HEADER, FALSE);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        if (curl_exec($curl) === false) {
            echo 'Curl error: ' . curl_error($curl);
        } else {
            $return = curl_exec($curl);
        }
        curl_close($curl);
        return $return;
    }

    /*
    Esta funcion se encarga de transformar la cedula 
    a Rif necesario para la consulta al seniat.
    */
    Private function getRif($letra, $digitos){
        $vletra =  $this->letravalue($letra);
        $sdigitos = str_split($digitos);
        $factor = 3;
        $countdig = count($sdigitos);

        #pvalido cedula con menos de 8 digitos.
        #Se le agregan 0 al array para completar.
        if($countdig < 8){
            $zerof= 8 - $countdig;
            for ($i=0; $i < $zerof ; $i++) { 
                array_unshift($sdigitos, 0);
            }
        }
        
        $multiplo=$vletra*4;

        for( $i = 0; $i < count($sdigitos); $i++ ) {
            if ($i<= 1){
                $multiplo+=($sdigitos[$i]*$factor);
                --$factor;
                if ($i==1){
                    $factor =7;
                }
            }
            if($i>1){
                $multiplo+=($sdigitos[$i]*$factor);
                --$factor;
            }

        }

        $residuo = $multiplo % 11;
        $verificador = 11-$residuo;
        $rif = $letra.implode($sdigitos).$verificador;
        return $rif;
        
    }
    /*
    Obtine el valor numerico de la letra de la cedula,
    necesario para el calculo del rif.
    */   
    Private function letravalue($letra){
        switch ($letra) {
            case 'V':
                return 1;
            case 'E':
                return 2;
            case 'P':
                return 4;
        }
    }
}


