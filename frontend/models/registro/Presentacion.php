<?php
namespace frontend\models\registro;

use yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
class Presentacion extends Model
{
	public $valido;

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['valido'], 'required'],

        ];
    }
     public function attributeLabels()
    {
        return [
            'valido' => 'Plazo',
        ];
     }

}
