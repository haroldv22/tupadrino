<?php
namespace frontend\models\registro;

use yii\base\Model;
use yii\helpers\ArrayHelper;
use frontend\models\QuestionSystem;
use frontend\models\Clients;


class CreacionUsuario extends Model
{

    public $email;
    public $token;
    public $password;
    public $repassword;
    public $questiona;
    public $questionb;
    public $questionpa;
    public $questionpb;
    public $answera;
    public $answerb;

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            ['email','Emailvalidate'],
            [['questiona','questionb'], 'qvalidate'],
            [['password','repassword','answera','answerb','questiona','questionb'],'required'],
            [['questionpa','questionpb'], 'safe'],
            ['repassword','compare','compareAttribute'=>'password',
                'message'=>'Contraseñas no coinciden','enableClientValidation'=>true],
            ['questionb','compare','compareAttribute'=>'questiona','operator' => '!=',
                'message'=>'Las preguntas deben ser direfentes','skipOnEmpty' => false,
                'skipOnError' => false,
                'enableClientValidation'=>true],
            ['password','string','length' => [6, 12],
             'tooLong'=>'La contraseña es demasiado larga',
            'tooShort'=>'la contraseña es demasiado corta',
            ],
            ['password','match','pattern'=>'/^\S*(?=\S*[a-zA-Z]{1,})(?=\S*[0-9]{1,})\S*$/'],

           /// ^\S*(?=\S*[0-9])(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[#$?*])\S*$
            
        ];
    }

    public function Emailvalidate(){
        $valido = Clients::find()->where(['email'=>$this->email])->exists(); ;
         if ($valido) {
            $this->addError('email', 'Este correo ya se encuentra registrado');
        }

     }
    public function qvalidate(){
        if($this->questiona == 'specific'){
            if(empty(trim($this->questionpa))){
                $this->addError('questionpa', 'Debes rellenar este campo');
            }
        }

        if($this->questionb == 'specific'){
            if(empty(trim($this->questionpb))){
                $this->addError('questionpb', 'Debes rellenar este campo');
            }
        }
    }


     public function attributeLabels()
    {
        return [
            'password' => 'Contraseña',
            'questiona' => 'Pregunta de Seguridad',
            'answera' => 'Repuesta',
            'questionb' => 'Pregunta de Seguridad',
            'answerb' => 'Repuesta',
            'repassword'=>'Re-Contraseña'
        ];
     }

     public function getSystemQuestion(){
        $return = ArrayHelper::map(QuestionSystem::find()->all(), 'question','question');
        return array_merge($return,['specific'=>'Pregunta personalizada']);
     }

   
}
