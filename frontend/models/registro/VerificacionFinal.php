<?php
namespace frontend\models\registro;
use yii\base\Model;
use yii;

 
class VerificacionFinal extends Model
{
	public $term;//Terminos y condiciones
    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
             ['term','safe'],
             ['term','required','requiredValue'=>1,'message'=>'Debe aceptar los terminos y condiciones.'],
        ];
    }
    public function attributeLabels()
    {
        return [
            'term' => 'Acepto los terminos & condiciones',
        ];
    }
    

}
