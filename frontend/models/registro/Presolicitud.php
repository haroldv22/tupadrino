<?php
namespace frontend\models\registro;

use yii;
use yii\base\Model;
use frontend\models\Quota;
use frontend\models\Period;
use frontend\models\PeriodAmount;
use frontend\models\LevelAmount;
use yii\helpers\ArrayHelper;
class Presolicitud extends Model
{
	public $period;
    public $amount;
    public $rate;
    //public $date_of_payment;
    public $quota;
    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['period', 'amount','quota'], 'safe'],
            [['period', 'amount','quota'], 'required']
        ];
    }
     public function attributeLabels()
    {
        return [
            'period' => 'Plazo',
            'amount' => 'Monto',
            'quota' => 'Cuota',
        ];
     }

    public function getQuota(){ 
       return ArrayHelper::map(Quota::find()->where(['id_level'=>Yii::$app->params['plevel']])->all(), 'days','alias');
    }

    public function getAmount(){
        $level=LevelAmount::find()
            ->select('amount')
            ->where(['id_level'=>Yii::$app->params['plevel']])
            ->asArray()
            ->all();
            
            $amounts="";
            foreach ($level as $key => $value) {
                $amounts .="'".$value['amount']."',";
            }
        return $amounts;
    }

}
