<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "clients_admin".
 *
 * @property integer $id_client
 * @property integer $godson_available
 * @property integer $godson_count
 * @property integer $experience_points
 *
 * @property Clients $idClient
 */
class ClientsAdmin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clients_admin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_client'], 'required'],
            [['id_client', 'godson_available', 'godson_count', 'experience_points'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_client' => 'id del cliente',
            'godson_available' => 'Ahijados disponibles para invitar',
            'godson_count' => 'contador de ahijados del cliente',
            'experience_points' => 'Puntos de experiencia',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'id_client']);
    }
}
