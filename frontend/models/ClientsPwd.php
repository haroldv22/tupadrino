<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "clients_pwd".
 *
 * @property integer $id_client
 * @property string $password
 * @property string $temp_password
 * @property string $date_add
 * @property string $date_upd
 *
 * @property Clients $idClient
 */
class ClientsPwd extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $repassword;
    public static function tableName()
    {
        return 'clients_pwd';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_client', 'password', 'date_add'], 'required'],
            [['id_client'], 'integer'],
            [['date_add', 'date_upd'], 'safe'],
            [['password'], 'string', 'max' => 128],
            [['temp_password'], 'string', 'max' => 100],
            [['repassword'],'required','on'=>'update'],
            [['repassword'], 'compare', 'compareAttribute'=>'password' , 
                'message'=>'Las contraseñas deben ser iguales',
                'on'=>'update'
            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_client' => 'id_client of client table',
            'password' => 'Nueva Contraseña ',
            'repassword'=> 'Repita la contraseña',
            'temp_password' => 'temporal password',
            'date_add' => 'first date added',
            'date_upd' => 'password date updated ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'id_client']);
    }

    public function getPasswordHash($password){        
        return Yii::$app->getSecurity()->generatePasswordHash($password);
    }


    public function generateTempPass()
    {
        $newpass = Yii::$app->security->generateRandomString(8);
        $this->password =$this->getPasswordHash($newpass);

        return $newpass;


    }
}
