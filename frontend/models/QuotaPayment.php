<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "quota_payment".
 *
 * @property integer $id
 * @property integer $id_record_payment
 * @property integer $id_loan_quota
 * @property string $amount
 *
 * @property LoanQuota $idLoanQuota
 * @property RecordPayment $idRecordPayment
 */
class QuotaPayment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'quota_payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_record_payment', 'id_loan_quota', 'amount'], 'required'],
            [['id_record_payment', 'id_loan_quota'], 'integer'],
            [['amount'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id de la tabla',
            'id_record_payment' => 'id del pago asociado',
            'id_loan_quota' => 'id de la cuota asociada al pago',
            'amount' => 'monto pagado de la cuota',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLoanQuota()
    {
        return $this->hasOne(LoanQuota::className(), ['id' => 'id_loan_quota']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRecordPayment()
    {
        return $this->hasOne(RecordPayment::className(), ['id' => 'id_record_payment']);
    }
}
