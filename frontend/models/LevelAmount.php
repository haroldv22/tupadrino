<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "level_amount".
 *
 * @property integer $id
 * @property string $amount
 * @property integer $id_level
 *
 * @property Level $idLevel
 * @property PeriodAmount[] $periodAmounts
 */
class LevelAmount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'level_amount';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount'], 'required'],
            [['amount'], 'number'],
            [['id_level'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'identificador de la tabla',
            'amount' => 'monto del prestamo expresado en numeros',
            'id_level' => 'Identificador del nivel con el cual esta relacionado el monto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLevel()
    {
        return $this->hasOne(Level::className(), ['id' => 'id_level']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodAmounts()
    {
        return $this->hasMany(PeriodAmount::className(), ['id_level_amount' => 'id']);
    }
}
