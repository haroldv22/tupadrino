<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "system_bank_account".
 *
 * @property integer $id
 * @property string $code_bank
 * @property string $number_account
 * @property string $beneficiario
 * @property string $identity
 *
 * @property Banks $codeBank
 */
class SystemBankAccount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'system_bank_account';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code_bank', 'number_account', 'beneficiario', 'identity'], 'required'],
            [['code_bank'], 'string', 'max' => 4],
            [['number_account'], 'string', 'max' => 16],
            [['beneficiario'], 'string', 'max' => 50],
            [['identity'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code_bank' => 'Code Bank',
            'number_account' => 'Number Account',
            'beneficiario' => 'Beneficiario',
            'identity' => 'Identity',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodeBank()
    {
        return $this->hasOne(Banks::className(), ['code_bank' => 'code_bank']);
    }

    public function Rebuildaccount(){
        $numberO = $this->number_account;
        $first = substr($numberO,0,4);
        $second = substr($numberO,4,2);
        $third = substr($numberO, 6);

        return $this->code_bank.'-'.$first.'-'.$second.'-'.$third;

    }

    public static function getHtmlAccounts(){
        $modelBanks = self::find()->all();
        if($modelBanks){
            $html='';
            foreach ($modelBanks as $bankSystem) {
                $html.= '<ul class="list-unstyled system-account">';
                    $html.= '<li>Nombre del Beneficiario:<b> '.$bankSystem->beneficiario.'</b></li>';
                    $html.='<li>RIF: <b>'.$bankSystem->identity.'</b></li>';
                    $html.= '<li>Número de cuenta:<b> '.$bankSystem->Rebuildaccount().'</b></li>';
                    $html.= '<li>Banco: <b>'.$bankSystem->codeBank->name.'</b></li>';
                $html.= '</ul><br />';
            }
            return $html;
        }
        return;
    }
}
