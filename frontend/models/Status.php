<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "status".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 *
 * @property ClientInvitation[] $clientInvitations
 * @property Loan[] $loans
 * @property LoanQuota[] $loanQuotas
 * @property User[] $users
 */
class Status extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 25],
            [['description'], 'string', 'max' => 55]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientInvitations()
    {
        return $this->hasMany(ClientInvitation::className(), ['id_status' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoans()
    {
        return $this->hasMany(Loan::className(), ['id_status' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoanQuotas()
    {
        return $this->hasMany(LoanQuota::className(), ['id_status' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id_status' => 'id']);
    }
}
