<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "loan_quota".
 *
 * @property integer $id
 * @property integer $id_loan
 * @property string $amortization
 * @property string $interest
 * @property integer $id_status
 * @property string $date_pay
 * @property string $date_expired
 * @property integer $n_quota
 * @property string $quota
 * @property string $pending_pay
 * @property boolean $have_arrear
 *
 * @property Arrears $arrears
 * @property Loan $idLoan
 * @property Status $idStatus
 * @property QuotaPayment[] $quotaPayments
 */
class LoanQuota extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'loan_quota';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_loan', 'amortization', 'interest', 'id_status', 'date_pay', 'date_expired', 'n_quota', 'quota'], 'required'],
            [['id_loan', 'id_status', 'n_quota'], 'integer'],
            [['amortization', 'interest', 'quota', 'pending_pay'], 'number'],
            [['date_pay', 'date_expired'], 'safe'],
            [['have_arrear'], 'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_loan' => 'clave foranea de tabla loan(prestamo)',
            'amortization' => 'Amortizacion',
            'interest' => 'Interes',
            'id_status' => 'Estatus',
            'date_pay' => 'Fecha pago',
            'date_expired' => 'Fecha vencimiento',
            'n_quota' => 'Nro.',
            'quota' => 'Cuota',
            'pending_pay' => 'Pagp pendiente de quota (la funcion de esta columna es conocer cuando resta de pago el cliente cuando paga parte de lo correspondiente)',
            'have_arrear' => 'Verifica si posee mora',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArrears()
    {
        return $this->hasOne(Arrears::className(), ['id_loan_quota' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLoan()
    {
        return $this->hasOne(Loan::className(), ['id' => 'id_loan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'id_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuotaPayments()
    {
        return $this->hasMany(QuotaPayment::className(), ['id_loan_quota' => 'id']);
    }
        //Coloca la clase css a cada estatus
    public function CssSmfclass(){
        switch ($this->id_status) {
            case 34://En Mora
                return 'label-danger parpadeo';
                break;
            case 33://vencido
                return 'label-warning parpadeo';
                break;
            case 32://Generado
                return 'label-success';
                break;
            case 30://Pagado
                return 'label-success';
                break;  
        }
    }
}
