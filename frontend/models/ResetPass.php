<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\login\Clients;

/**
 * Password reset request form
 */
class ResetPass extends Model
{
    public $email;
    public $answer1;
    public $answer2;
    public $codeans1;
    public $codeans2;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\frontend\models\login\Clients',
                'message' => 'El correo suministrado no existe.'
            ],
            [['codeans1','codeans2','answer1','answer2'], 'required','message' => 'Debe responder la pregunta.'],
            ['answer1','validateAnswer1'],
            ['answer2','validateAnswer2'],

        ];
    }

      public function scenarios(){
        $scenarios = parent::scenarios();
        $scenarios['step1']=['email'];
        $scenarios['step2']=['email','codeans1','codeans2','answer1','answer2'];

        return $scenarios;
    }

    public function validateAnswer1(){
        $model = $this->getOneQuestion($this->codeans1);
        if($model){
            if(strtoupper($this->answer1)!=strtoupper($model->answer)){
                 $this->addErrors(['answer2'=>'Alguna de las repuestas es invalida','answer1'=>'Alguna de las repuestas es invalida']);
            }

        }else{
            $this->addError('answer1','No intente burlar al sistema');
        }
    }

    public function validateAnswer2(){
        $model = $this->getOneQuestion($this->codeans2);
        if($model){
            if(strtoupper($this->answer2)!=strtoupper($model->answer)){
                $this->addErrors(['answer2'=>'Alguna de las repuestas es invalida','answer1'=>'Alguna de las repuestas es invalida']);
            }

        }else{
            $this->addError('answer1','No intente burlar al sistema');
        }
    }

    public function getQuestions(){
        $Questions = Clients::findOne([
            'email' => $this->email,
        ])->getQuestions()->all();

        return $Questions;
    }
    public function getOneQuestion($idQues){
        return Clients::findOne([
            'email' => $this->email,
        ])->getQuestions()
            ->where(['id'=>$idQues])
            ->one();
    }

    public function sendEmail()
    {
        /* @var $user User */
        $client = Clients::findOne([
            'email' => $this->email,
        ]);

        $pwdClient = $client->getClientsPwd()->One();

        if (!$pwdClient) {
            return false;
        }
        
       $newpass = $pwdClient->generateTempPass();
        
        if (!$pwdClient->save()) {
            return false;
        }

         $body ='<!-- Logo -->
                <center>
                <img src="http://tupadrino.net/img/logo.png" width="600" height="133" alt=""/>
                </center>
                <br />
                <!-- Saludos-->
                <center>
                <span style="font-size: 18px; color: #009fc3;"> Saludos ! '.$client->Shortname().'</span>
                <center>
                <br /><br /><br /> 
                <!-- Contenido -->
                <center>
                <span style="font-size: 16px;"> 
                <p><strong>Nueva contraseña:</strong> '.$newpass.'</p>
                <p>Una vez ingrese a tupadrino.net, recuerde actualizar su contraseña en:</p>
                <p><span style="font-size: 16px; color: #009fc3;"><a href="http://tupadrino.net/app/config">
                MI CUENTA/SU NOMBRE/CONFIGURACIÓN/CONTRASEÑA</a></p>
                </span></span>
                </center>
                <br /><br /><br /><br /><br /><br />
                <footer style=" background-color: #10a1b8; border-top: 1px solid #ddd; height: 40px; padding-top: 20px;"> 
                <span style="float:left; margin-left:10px">&copy; TuPadrino.net 2016</span>
                <span style="float:right; margin-right:10px">All Copyright Reserved</span>
                </footer>';
                
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
                ['client' => $client,'pass'=>$newpass]
            )
           /// ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setFrom($this->email)
            ->setTo($this->email)
            ->setSubject('Solicitud de reseteo de contraseña TuPadrino.net')
            ->setHtmlBody($body)
            ->send();
      
    }


    public function Showpass(){
/* @var $user User */
        $client = Clients::findOne([
            'email' => $this->email,
        ]);

        $pwdClient = $client->getClientsPwd()->One();

        if (!$pwdClient) {
            return false;
        }
        
       $newpass = $pwdClient->generateTempPass();
        
        if (!$pwdClient->save()) {
            return false;
        }

         $body ='<!-- Logo -->
                <center>
                <img src="http://tupadrino.net/img/logo.png" width="600" height="133" alt=""/>
                </center>
                <br />
                <!-- Saludos-->
                <center>
                <span style="font-size: 18px; color: #009fc3;"> Saludos ! '.$client->Shortname().'</span>
                <center>
                <br /><br /><br /> 
                <!-- Contenido -->
                <center>
                <span style="font-size: 16px;"> 
                <p><strong>Nueva contraseña:</strong> '.$newpass.'</p>
                <p>Una vez ingrese a tupadrino.net, recuerde actualizar su contraseña en:</p>
                <p><span style="font-size: 16px; color: #009fc3;"><a href="http://tupadrino.net/app/config">
                MI CUENTA/SU NOMBRE/CONFIGURACIÓN/CONTRASEÑA</a></p>
                </span></span>
                </center>
                <br /><br /><br /><br /><br /><br />
                <footer style=" background-color: #10a1b8; border-top: 1px solid #ddd; height: 40px; padding-top: 20px;"> 
                <span style="float:left; margin-left:10px">&copy; TuPadrino.net 2016</span>
                <span style="float:right; margin-right:10px">All Copyright Reserved</span>';

                return $body;
    }


}
