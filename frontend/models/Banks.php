<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "banks".
 *
 * @property integer $id
 * @property string $name
 * @property string $code_bank
 *
 * @property BankAccount[] $bankAccounts
 * @property RecordPayment[] $recordPayments
 */
class Banks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code_bank'], 'required'],
            [['name'], 'string', 'max' => 75],
            [['code_bank'], 'string', 'max' => 4]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id del banco',
            'name' => 'nombre del banco',
            'code_bank' => 'Codigo que define el banco',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankAccounts()
    {
        return $this->hasMany(BankAccount::className(), ['code_bank' => 'code_bank']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecordPayments()
    {
        return $this->hasMany(RecordPayment::className(), ['code_bank' => 'code_bank']);
    }
}
