<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "arrears_payment".
 *
 * @property integer $id
 * @property integer $id_arrears
 * @property integer $id_record_payment
 * @property string $amount
 *
 * @property Arrears $idArrears
 * @property RecordPayment $idRecordPayment
 */
class ArrearsPayment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'arrears_payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_arrears', 'id_record_payment'], 'required'],
            [['id_arrears', 'id_record_payment'], 'integer'],
            [['amount'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id de la tabla',
            'id_arrears' => 'id de la mora asociada al pago',
            'id_record_payment' => 'Id Record Payment',
            'amount' => 'monto pagado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdArrears()
    {
        return $this->hasOne(Arrears::className(), ['id' => 'id_arrears']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRecordPayment()
    {
        return $this->hasOne(RecordPayment::className(), ['id' => 'id_record_payment']);
    }
}
