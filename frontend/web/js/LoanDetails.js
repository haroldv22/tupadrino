(function($) {
	  /**
	   * Decimal adjustment of a number.
	   *
	   * @param {String}  type  The type of adjustment.
	   * @param {Number}  value The number.
	   * @param {Integer} exp   The exponent (the 10 logarithm of the adjustment base).
	   * @returns {Number} The adjusted value.
	   */
	  function decimalAdjust(type, value, exp) {
	    // Ifjquery the exp is undefined or zero...
	    if (typeof exp === 'undefined' || +exp === 0) {
	      return Math[type](value);
	    }
	    value = +value;
	    exp = +exp;
	    // If the value is not a number or the exp is not an integer...
	    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
	      return NaN;
	    }
	    // Shift
	    value = value.toString().split('e');
	    value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
	    // Shift back
	    value = value.toString().split('e');
	    return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
	  }

	  // Decimal round
	  if (!Math.round10) {
	    Math.round10 = function(value, exp) {
	      return decimalAdjust('round', value, exp);
	    };
	  }
	  // Decimal floor
	  if (!Math.floor10) {
	    Math.floor10 = function(value, exp) {
	      return decimalAdjust('floor', value, exp);
	    };
	  }
	  // Decimal ceil
	  if (!Math.ceil10) {
	    Math.ceil10 = function(value, exp) {
	      return decimalAdjust('ceil', value, exp);
	    };
	  }
	//valida dia habiles
	function validarFecha(date){
		//Verificacion de sabados y domingos		
		if(date.getDay()==0 || date.getDay()==6){
			date.setDate(date.getDate() + 1);
			return validarFecha(date);
		}
		//verificacion de dias feriados
		//holidays viene del php presolicitud
		$.each( Holidays, function( key, value ) {
			var holi = new Date(value);
			//holi.setDate(holi.getDate()+1);
			//console.log(holi);
			if(holi.getTime() == date.getTime()){
				//console.log(value);
				date.setDate(date.getDate() + 1);
				return validarFecha(date);
			}
			
		});

		return date;
		

	}
	//agrega Dias
	function addDays(date, days) {
	    var newdate = new Date(date);
    	newdate.setDate(newdate.getDate() + parseInt(days));
    	newdate = validarFecha(newdate);
	    var dd = newdate.getDate();
	    var mm = newdate.getMonth() + 1;
	    var y = newdate.getFullYear();
	    var someFormattedDate = mm + '/' + dd + '/' + y;

	    return someFormattedDate; 
	}

	//cambia formato de fechas
	function formater(date){
		var newdate = new Date(date);
		var dd = newdate.getDate();
	    var mm = newdate.getMonth() + 1;
	    var y = newdate.getFullYear();
	    var someFormattedDate = dd + '-' + mm + '-' + y;
	    return someFormattedDate;
	}
/*
** Tabla pago plugin
*/


    $.fn.LoanDetails = function( options ) {

        // Establish our default settings
        var settings = $.extend({
            quota         : null,
            period        : null,
            rate		  : null,
            amount        : null,
            date          : null,
            exp 		  : null
        }, options);

        return this.each( function() {
        	//Inicializacion de variables
        	var quota = settings.quota;
            var period = settings.period;
            var rate = settings.rate;
            var amount = settings.amount;
            var date = settings.date;
            var exp = settings.exp;
            var html = '';
            var foot = '';

	        var nQuota = Math.round10(period/quota);
	        var payDate = date;
	        var result =[];
	        var rate = rate/100;
	        var total = 0;
	        //Formula para calculo de interes
	        var P = amount;
	        var i = rate;
	        var n = nQuota;
	        var R = P*((i*Math.pow((1+i),n))/(Math.pow((1+i),n)-1));
	       /// Fin Formula
	        var R= Math.round10(R,-2);
	        var balance = amount;

	        var calendarDate=[];
			//calendarDate.push("Kiwi");
	        
	        for (i=0; i < nQuota ; i++) { 
	            payDate=  addDays(payDate,quota); 
	            expDate = addDays(payDate,exp);
	            
	            interest = Math.round10((rate * balance),-2);
	            amort=Math.round10(R-interest,-2);
	            total += R;
	            /*
	            array = {'n_quota': i+1,
	                      'date_pay':formater(payDate),
	                      'date_expired':formater(expDate),
	                      'interest':interest,
	                      'amortization':amort,
	                      'quota':R};
	            result.push(array);*/
	        	calendarDate.push(payDate);
	            balance = Math.round10(balance -amort,-2);
	        }
	        
	        html+='<tr>';
	        html+='<th class="currency">'+P+'</th>';
	        html+='<th class="currency">'+R+'</th>';
	        html+='<th>'+n+'</th>';
	       	html+='</tr>';

	        $(this).html(html);
	        $('#loandate').calendario({dates:calendarDate});
	        //$(this).siblings('tfoot').html(foot);
	        /*$('#loandate').html('');
	        $('#loandate').DatePicker({
				flat: true,
				date: foot,
				current: foot[1],
				format: 'm/d/Y',
				calendars: 3,
				mode: 'multiple',
				starts: 0
			});*/
        });

    }

    $.fn.calendario = function( options ) {

    	var Selector = $(this);
    	Selector.html('')
    	function calculaNumeroDiaSemana(dia,mes,ano){
		   var objFecha = new Date(ano, mes, dia);
		   var numDia = objFecha.getDay();
		   if (numDia == 0) 
		      numDia = 6;
		   else
		      numDia--;
		   return numDia;
		}

		//función para ver si una fecha es correcta
		function checkdate ( m, d, y ) {
		   // función por http://kevin.vanzonneveld.net
		   // extraida de las librerías phpjs.org manual en http://www.desarrolloweb.com/manuales/manual-librerias-phpjs.html
		   return m > 0 && m < 13 && y > 0 && y < 32768 && d > 0 && d <= (new Date(y, m, 0)).getDate();
		}

		//funcion que devuelve el último día de un mes y año dados
		function ultimoDia(mes,ano){ 
		   var ultimo_dia=28; 
		   while (checkdate(mes+1,ultimo_dia + 1,ano)){ 
		      ultimo_dia++; 
		   } 
		   return ultimo_dia; 
		}

		function getMes(fecha){
			var newdate = new Date(fecha);
	    	return (newdate.getMonth()+1)+'/1/'+newdate.getFullYear();
		}

		function getDia(fecha){
			var newdate = new Date(fecha);
			return newdate.getDate();
		}

		var months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
		
		
		var fechas = [];
		$.each( options.dates, function( key, value ){
			  	if(!(getMes(value) in fechas)){
			  		fechas[getMes(value)] = [getDia(value)];
			  	}
			  	else{
			  		fechas[getMes(value)].push(getDia(value));
			  	}
		});

		var keys = (Object.keys(fechas));
		$.each( keys, function( key, value ){
			var compa = fechas[value];

			function DiaPago(day,fechat){
				if(fechat.indexOf(day)>=0){
					return'<th class="active">'+ day +'</th>';
				}
				else{
					return'<th>' + day + '</th>';
				}
			}
			//muestro los días del mes
	    	var contadorDias = 1;
	     	var capaDiasMes = $('<tbody id="diasmes"></tbody>')
	      	//un objeto de la clase date para calculo de fechas
	      	var objFecha = new Date(value);
	      	//mes y año actuales
	      	var mes = objFecha.getMonth();
	      	var ano = objFecha.getFullYear();
	      	//calculo la fecha del primer día de este mes
	      	var primerDia = calculaNumeroDiaSemana(1, mes, ano);
	      	//calculo el último día del mes
	      	var ultimoDiaMes = ultimoDia(mes,ano);
	      
	      	var titulo =$('<div class="mesyear"><span class="mes">'+months[mes]+'</span><span class="year">'+ano+'</span></div>'   )
	      	//escribo la primera fila de la semana
	      	var codigoDia = '<tr>';
	      
	      for (var i=0; i<7; i++){
	         if (i < primerDia){
	            //si el dia de la semana i es menor que el numero del primer dia de la semana no pongo nada en la celda
	            codigoDia += '<th></th>';
	         } else { 
	            codigoDia += DiaPago(contadorDias,compa);
	            contadorDias++;
	         }

	      }
	      	codigoDia += '</tr>';
	        var diaActual = $(codigoDia);
	        capaDiasMes.append(diaActual);
	      
	      //recorro todos los demás días hasta el final del mes
	      var diaActualSemana = 1;
	      var codigoDia = '';
	      while (contadorDias <= ultimoDiaMes){

	         //si estamos a principio de la semana escribo la clase primero
	        if (diaActualSemana % 7 == 1){
	         	codigoDia = '<tr>';
	        }
	        codigoDia += DiaPago(contadorDias,compa);

	         //si estamos al final de la semana es domingo y ultimo dia
	        if (diaActualSemana % 7 == 0){
	           codigoDia += '</tr>';
	           var diaActual = $(codigoDia);
	         	capaDiasMes.append(diaActual);
	         	codigoDia ='';
	        }
	         
	         contadorDias++;
	         diaActualSemana++;

	      }
	      
	      

	      //compruebo que celdas me faltan por escribir vacias de la última semana del mes
	     /// var codigoDia = '<tr>';
	      for (var i=(diaActualSemana%7); i<=7; i++){
	        // codigoDia += '<th';
	         //codigoDia += '<th></th>';
	      }
	      codigoDia +='</tr>';
	      var diaActual = $(codigoDia);
	      capaDiasMes.append(diaActual);
	      
	      //calendario

	      var sem = '<thead id="diasemana">';
	      sem += '<tr>';
	      sem += '<th>l</th>';
	      sem += '<th>m</th>';
	      sem += '<th>m</th>';
	      sem += '<th>j</th>';
	      sem += '<th>v</th>';
	      sem += '<th>s</th>';
	      sem += '<th>d</th> ';
	      sem += '</tr>';
	      sem += '</thead>';

	      var capaDiasSemana = $(sem);
	      var calendario = $('<div class="meses col-lg-4 col-sm-6 col-xs-12"></div>');
	      var calendarioBorde = $('<table class="table calendario"></table>');
	      calendarioBorde.append(capaDiasSemana);
	      calendarioBorde.append(capaDiasMes);
	      calendario.append(titulo);
	      calendario.append(calendarioBorde);
	      //inserto el calendario en el documento

	      Selector.append(calendario);
	      //lo posiciono con respecto al boton

	      //muestro el calendario
	      calendario.show("slow");
    	});// end each keys
    }

}(jQuery));

