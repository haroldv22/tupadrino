$(function () {
	if(mercadopagoStart==true){
		mercadopagoInit();
	}

    $.fn.methodpaychange = function(){
    	if($('#recordpayment-method').val()=="TRF"){
            $("#recordpayment-code_bank_origin").prop("disabled",false);
            $('.trans').slideDown();
            $('.tdc').hide();

        }
        if($('#recordpayment-method').val()=="DEP"){
          	$("#recordpayment-code_bank_origin").prop("disabled",true);
            $("#recordpayment-code_bank_origin").val("");
            $('.trans').hide();
            $('.tdc').hide();
            $('.depo').slideDown();
        }
        if($('#recordpayment-method').val()=="TDC"){
            $("#recordpayment-code_bank_origin").prop("disabled",true);
            $("#recordpayment-code_bank_origin").val("");
            $('.trans').slideUp();
            mercadopagoInit();
            $('.tdc').slideDown();
        }

    }
	$.fn.QuotaDetails = function(data){
		$('.disabled-field').prop('disabled',false);
		$('#recordpayment-info-total .result').html(data.total);
		$('#recordpayment-info-quota .result').html(data.quota);
		if(mercadopagoStart==false){
		$('#recordpayment-type').val('');
		}
		$('#recordpayment-type option[value="mora"]').prop("disabled",false);
		$('#recordpayment-type option[value="ambos"]').prop("disabled",false);
		$('#recordpayment-type option[value="cuota"]').prop("disabled",false);

		if( data.quota<=0){
			$('#recordpayment-type option[value="cuota"]').prop("disabled",true);
			$('#recordpayment-type option[value="ambos"]').prop("disabled",true);

		}
		if(data.havearrear==true){
			$('#recordpayment-info-arrear .result').html(data.arrear);
			$('#recordpayment-info-arrear-date .result').html(data.arreardate);
		}
		else{
			$('#recordpayment-type option[value="mora"]').prop("disabled",true);
			$('#recordpayment-type option[value="ambos"]').prop("disabled",true);
			$('#recordpayment-info-arrear .result').html('-');
			$('#recordpayment-info-arrear-date .result').html('-');

		}

	};
	$.fn.QuotaChange = function(valor, url){
		if(valor!=""){
            $.post(url,          
            {id:valor}, 
                function( data ) {
                    $().QuotaDetails(data);
                });
            }else{
                $(".disabled-field").prop("disabled",true);

            }
	}

	//otro monto funcion
	$('.amount-active').click(function(){
		$("#recordpayment-amount").prop("readonly",false);
		$("#recordpayment-amount").val('');
		$("#recordpayment-amount").focus();
	})
	$('.amount-disable').click(function(){
		$("#recordpayment-type").change();
		$("#recordpayment-amount").prop("readonly",true);
	})


	$.fn.QuotaAmount = function(data){
		//console.log(data);
		$('#recordpayment-amount').val(data.amount);
	};
    $.fn.selector = function (data) {
    	if(data.type=='cuota'){
	        if(data.response=="noLoan"){
	            alert("No posee credito activo");
	            $("#'.Html::getInputId($model, 'type').'").val("");
	        }
	        else if(data.response=="noQuota"){
	            alert("No posee cuotas pendientes");
	        }
	        else{
	        	var html = '<div class="form-group field-quotapayment-id_loan_quota required">';
				html+= '<label class="control-label col-sm-3" for="quotapayment-id_loan_quota">No. Quota a Pagar</label>';
				html+= '<div class="col-sm-6">';
				html+= data.response;
				html+= '</div>';
				html+='</div>';
	        	$('#newselector').html(html);
			}

    	}

    	if (data.type=='mora'){
	        if(data.response=="noArrears"){
    			alert("No posee mora a pagar");
	        }
	        else{
	        	var html = '<div class="form-group field-arrearspayment-id_arrears required">';
				html+= '<label class="control-label col-sm-3" for="arrearspayment-id_arrears">Mora a Pagar</label>';
				html+= '<div class="col-sm-6">';
				html+= data.response;
				html+= '</div>';
				html+='</div>';
	        	$('#newselector').html(html);
    		};
	    }
    };

    $("#quota-info-details-btn").click(function(){
    	$('#quota-info-details').slideToggle('slow');
    });
/*
    $("#submit-btn").click(function(e){
    	e.preventDefault();
    	if(confirm('¿Esta seguro de la informacion suministrada?')){
    		$('#w0').submit();
    	}

    })*/

	
	//Mercado Pago
	function mercadopagoInit(){
		Mercadopago.setPublishableKey("APP_USR-746ebdff-ed36-40f5-b874-74b5f5d4a5fb");
    	Mercadopago.getIdentificationTypes();
	}
	//tipo tdc
	function addEvent(el, eventName, handler){
	    if (el.addEventListener) {
	           el.addEventListener(eventName, handler);
	    } else {
	        el.attachEvent('on' + eventName, function(){
	          handler.call(el);
	        });
	    }
	};

	function getBin() {
	    var ccNumber = document.querySelector('input[data-checkout="cardNumber"]');
	    return ccNumber.value.replace(/[ .-]/g, '').slice(0, 6);
	};

	function guessingPaymentMethod(event) {
	    var bin = getBin();

	    if (event.type == "keyup") {
	        if (bin.length >= 6) {
	            Mercadopago.getPaymentMethod({
	                "bin": bin
	            }, setPaymentMethodInfo);
	        }
	    } else {
	        setTimeout(function() {
	            if (bin.length >= 6) {
	                Mercadopago.getPaymentMethod({
	                    "bin": bin
	                }, setPaymentMethodInfo);
	            }
	        }, 100);
	    }
	};

	function setPaymentMethodInfo(status, response) {
	    if (status == 200) {
	        // do somethings ex: show logo of the payment method
	        var form = document.querySelector('#pay');

	        if (document.querySelector("input[name=paymentMethodId]") == null) {
	            var paymentMethod = document.createElement('input');
	            paymentMethod.setAttribute('name', "paymentMethodId");
	            paymentMethod.setAttribute('type', "hidden");
	            paymentMethod.setAttribute('value', response[0].id);

	            form.appendChild(paymentMethod);
	        } else {
	        	console.log(response[0].id);
	            document.querySelector("input[name=paymentMethodId]").value = response[0].id;
	        }
	    }
	};

	addEvent(document.querySelector('input[data-checkout="cardNumber"]'), 'keyup', guessingPaymentMethod);
	addEvent(document.querySelector('input[data-checkout="cardNumber"]'), 'change', guessingPaymentMethod);





    doSubmit = false;
    addEvent(document.querySelector('#pay'),'submit',doPay);
    console.log(doSubmit);
    function doPay(event){
    	if($('#recordpayment-method').val()=="TDC"){
	        event.preventDefault();
	        if(!doSubmit){
	            var $form = document.querySelector('#pay');
	            
	            Mercadopago.createToken($form, sdkResponseHandler,event); // The function "sdkResponseHandler" is defined below

	            return false;
	        }
    	}
    };


    function sdkResponseHandler(status, response) {
        if (status != 200 && status != 201) {
            alert("Verifique los datos de la trajeta suministrada");
        }else{
            var form = document.querySelector('#pay');

            var card = document.createElement('input');
            card.setAttribute('name',"token");
            card.setAttribute('type',"hidden");
            card.setAttribute('value',response.id);
            form.appendChild(card);
            doSubmit=true;
           console.log('response');
           	form.submit();
        }
    };

});
