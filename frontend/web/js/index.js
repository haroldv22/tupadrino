/*
 * Author: Yostin Vargas
 * Date: 09 Jan 2016
 * Description:
 *      This is a demo file used only for the main dashboard (index.html)
 **/

$(function () {

  "use strict";
  if(comisionGrafic !=false){

  /* Morris.js Charts */
  // Grafico de comisiones
  var line = new Morris.Line({
    element: 'line-chart',
    resize: true,
    data: comisionGrafic,
    xkey: 'y',
    ykeys: ['item1'],
    labels: ['Ganancia'],
    lineColors: ['#aac22c'],
    lineWidth: 2,
    hideHover: 'auto',
    gridTextColor: "#58585a",
    gridStrokeWidth: 0.4,
    pointSize: 4,
    pointStrokeColors: ["#aac22c"],
    gridLineColor: "#aac22c",
    gridTextFamily: "Helvetica",
    gridTextSize: 10,
    grid:false,
    smooth:false
  });
}
$('.index-comision .botonera .next').on('click',function(){
   var  next = $('.index-comision table.active').next();
   if(next.length){
     $('.index-comision table.active').removeClass('active');
     next.addClass('active');
  }

  if ( $(".index-comision .botonera .last").length <= 0 ) {
    var put = '<button type="button" class="btn btn-default active last"><< Anterior</button>';
    $(".index-comision .botonera").prepend(put);
  }

})

$('.index-comision .botonera').on('click','.last',function(){
   var  prev = $('.index-comision table.active').prev();
   if(prev.length){
     $('.index-comision table.active').removeClass('active');
     prev.addClass('active');
  }
});



});