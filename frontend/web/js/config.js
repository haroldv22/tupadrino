function preventDoubleActive(){
		if($('a.active').size()>0){
			$('a.active').remove();
			$('a.hide').removeClass('hide');
		}
	}

	$('.list-group').on('click','.call',function(event){
			event.preventDefault();
			var self = $(this);
			var url = self.attr('href');
			if(url!='#'&&url!=''&&url!=null){
				$.ajax({
		    		url: url,
		        	//type: 'post',
		        	success: function (data) {
   						preventDoubleActive();
		        		self.addClass('hide');
		        		self.after($(data.response).animate({opacity: 1}, 600));
		        		//$('a.active').animate({opacity: 1}, 2000)

		       		}
		  		});
		  	}
		});


	$('.list-group').on('submit','.submit', function() {
		event.preventDefault();
		var form = $(this);
		$.ajax({
          url: form.attr('action'),
          type: 'post',
          data: form.serialize(),
          success: function (data) {
          	$('a.active').remove();
	        $('a.hide').after($(data.response).animate({opacity: 1}, 600));
          	if(data.success == true){
          		$('a.hide').remove();
		    }
          }
    	});
	});
	//funcion sumit para cambio de imagenes
	$('.list-group').on('submit','.submitImg', function() {
		event.preventDefault();
		$('.list-group #loading-img').removeClass('hide');
		$('.list-group #overligth').removeClass('hide');
		var form = new FormData($(this)[0]);
		$.ajax({
          url: $(this).attr('action'),
          type: 'post',
          data: form,
          processData:false,
          contentType:false,
          success: function (data) {
          	$('#loading-img').addClass('hide');
			$('#overligth').addClass('hide');
          	$('a.active').remove();
	        $('a.hide').after($(data.response).animate({opacity: 1}, 600));
          	if(data.success == true){
          		$('a.hide').remove();
          		$('#avatar .user-img').attr('src', data.newImage);
          		
		    }
          }
    	});
	});
	//funcion del boton subir **cambiar imagen**
	$('.list-group').on('click','#upload-file', function() {
		$('.list-group #uploadform-imagefile').click();

	});
	var jcrop_api;
	//aplicacion de vista previa de la imagen e inicio del plugin jcrop
	$(".list-group").on('change','#uploadform-imagefile',function(){
		if ($(this)[0].files && $(this)[0].files[0]) {

		    var sizeByte = this.files[0].size;
		    var siezekiloByte = parseInt(sizeByte / 1024);

		    if(siezekiloByte > 2048){
		        alert('El tamaño de la imagen supera el limite permitido');
		        $(this).val('');
		        return;
		     }
		    var Extension = ($(this).val().substring($(this).val().lastIndexOf("."))).toLowerCase();
		    console.log(Extension);
		    if(Extension!='.jpg'){
		    	alert('formato no permitido. Solo imagenes jpg');
		        $(this).val('');
		        return;
		     }

	        var reader = new FileReader();

	        reader.onload = function (e) {
	        	if(jcrop_api){
	        		jcrop_api.destroy();//lo destruyo en caso de existencia
	        	}
	            $('#img-priview').attr('src', e.target.result);
	           
	           //obtengo el tamaño real de la imagen
	            var img = new Image;
				img.src = e.target.result;
				img.onload = function() {
				  var nWidth = this.width;
				  var nHeight = this.height;
	           		
	           		//inicializo Jcrop
		        	$('#img-priview').Jcrop({
		        		setSelect:   [ 0, 0, 263, 263 ],
		        		trueSize: [nWidth,nHeight],
	      				aspectRatio: 1,
	      				onSelect: updateCoords
	    			},function(){
					    jcrop_api = this;
					});
				}
				$('#update-btn').removeClass('hide');
	        }

	        reader.readAsDataURL($(this)[0].files[0]);
   		}
	})

	function updateCoords(c){
	    $('#x').val(c.x);
	    $('#y').val(c.y);
	    $('#w').val(c.w);
	    $('#h').val(c.h);
	 };