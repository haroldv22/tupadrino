$(document).ready(function () {




	Object.defineProperty(HTMLMediaElement.prototype, 'playing', {
	    get: function(){
	        return !!(this.currentTime > 0 && !this.paused && !this.ended && this.readyState > 2);
	    }
	})
	$('#video1').click(function(){
		if(document.querySelector('video').playing){
      		$("#carousel").carousel('pause');
		}

	})

	$('#carousel').on('slide.bs.carousel', function () {

  // do something…
		if(document.querySelector('video').playing){
				$("#carousel").carousel('prev');
		}

	})

});