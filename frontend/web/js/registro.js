/*Funciones Js registro Controller */

/*
** Presolicitud functions
*/

function loadPeriod(){
	var key = $('#presolicitud-amount').val();
	jQuery.ajax({
		url: 'getperiod',
		type: 'post',
		data: {key:key},
		async:false, 
		success: function (data) {
			if(data !=''){
			   	$('#presolicitud-period').html(data);
			}
			else{
				$('#presolicitud-period').html('');
			}

		}	
	});
}

function GetPeriod(){
	var str = $('#presolicitud-period').val();
	var result = str.split("-");
	return result[1];
}

/* Datos Personales
** ----------------
*/

jQuery('#searchIdentity').click(function(e){
	e.preventDefault();
	var  cedula = $('#datospersonales-identity').val();
	var type = $('#datospersonales-typeid').val();
	$('.datospersonales-load').slideToggle('slow');

	if(cedula!='' && type !=''){
		jQuery.ajax({
			url: 'searchidentity',
			type: 'post',
			data: {cedula: type+cedula},
		    success: function (data) {
		    	if(typeof data.error == 'undefined'){
				    $('.datospersonales-load').slideToggle('slow');
				    	if(data !='null'){
				    		$('#datospersonales-firstname').val(data.nombre);
				    		$('#datospersonales-lastname').val(data.apellido);
				    	}else{
				    		$('#datospersonales-firstname').val('');
				    		$('#datospersonales-lastname').val('');
				    	}
				     $('.datospersonales-form').slideDown('slow');
				     $('.show-btn').show();
				     $('.hide-btn').hide();
		   		}
		   		else{
		   			$('.datospersonales-load').slideToggle('slow');
		   			$('.field-datospersonales-identity').addClass('has-error');
		   			$('#datospersonales-identity').after('<div class="help-block help-block-error ">'+data.error+'</div>');

		   		}
			},
		    error:function(){
		    	$('.datospersonales-load').slideToggle('slow');
		    	$('.datospersonales-form').slideDown('slow');

		    }


		});
	}
	else{
		$('.datospersonales-load').slideToggle('slow');
	}
});