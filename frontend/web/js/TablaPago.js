(function($) {
	  /**
	   * Decimal adjustment of a number.
	   *
	   * @param {String}  type  The type of adjustment.
	   * @param {Number}  value The number.
	   * @param {Integer} exp   The exponent (the 10 logarithm of the adjustment base).
	   * @returns {Number} The adjusted value.
	   */
	  function decimalAdjust(type, value, exp) {
	    // Ifjquery the exp is undefined or zero...
	    if (typeof exp === 'undefined' || +exp === 0) {
	      return Math[type](value);
	    }
	    value = +value;
	    exp = +exp;
	    // If the value is not a number or the exp is not an integer...
	    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
	      return NaN;
	    }
	    // Shift
	    value = value.toString().split('e');
	    value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
	    // Shift back
	    value = value.toString().split('e');
	    return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
	  }

	  // Decimal round
	  if (!Math.round10) {
	    Math.round10 = function(value, exp) {
	      return decimalAdjust('round', value, exp);
	    };
	  }
	  // Decimal floor
	  if (!Math.floor10) {
	    Math.floor10 = function(value, exp) {
	      return decimalAdjust('floor', value, exp);
	    };
	  }
	  // Decimal ceil
	  if (!Math.ceil10) {
	    Math.ceil10 = function(value, exp) {
	      return decimalAdjust('ceil', value, exp);
	    };
	  }


	//agrega Dias
	function addDays(date, days) {
	    var newdate = new Date(date);
    	newdate.setDate(newdate.getDate() + parseInt(days));
	    var dd = newdate.getDate();
	    var mm = newdate.getMonth() + 1;
	    var y = newdate.getFullYear();
	    var someFormattedDate = mm + '/' + dd + '/' + y;
	    return someFormattedDate;
	    
	}
	//cambia formato de fechas
	function formater(date){
		var newdate = new Date(date);
		var dd = newdate.getDate();
	    var mm = newdate.getMonth() + 1;
	    var y = newdate.getFullYear();
	    var someFormattedDate = dd + '-' + mm + '-' + y;
	    return someFormattedDate;
	}
/*
** Tabla pago plugin
*/


    $.fn.TablaPago = function( options ) {

        // Establish our default settings
        var settings = $.extend({
            quota         : null,
            period        : null,
            rate		  : null,
            amount        : null,
            date          : null,
            exp 		  : null
        }, options);

        return this.each( function() {
        	//Inicializacion de variables
        	var quota = settings.quota;
            var period = settings.period;
            var rate = settings.rate;
            var amount = settings.amount;
            var date = settings.date;
            var exp = settings.exp;
            var html = '';
            var foot = '';

	        var nQuota = Math.round10(period/quota);
	        var payDate = date;
	        var result =[];
	        var rate = rate/100;
	        var total = 0;
	        //Formula para calculo de interes
	        var P = amount;
	        var i = rate;
	        var n = nQuota;
	        var R = P*((i*Math.pow((1+i),n))/(Math.pow((1+i),n)-1));
	       /// Fin Formula
	        var R= Math.round10(R,-2);
	        var balance = amount;
	        for (i=0; i < nQuota ; i++) { 
	            payDate=  addDays(payDate,quota); 
	            expDate = addDays(payDate,exp);
	            
	            interest = Math.round10((rate * balance),-2);
	            amort=Math.round10(R-interest,-2);
	            total += R;
	            /*
	            array = {'n_quota': i+1,
	                      'date_pay':formater(payDate),
	                      'date_expired':formater(expDate),
	                      'interest':interest,
	                      'amortization':amort,
	                      'quota':R};
	            result.push(array);
	            */
	            balance = Math.round10(balance -amort,-2);

	        html+='<tr>';
	        html+='<th>'+(i+1)+'</th>';
	        html+='<th>'+formater(payDate)+'</th>';
	        html+='<th>'+formater(expDate)+'</th>';
	        html+='<th>'+R+'</th>';
	       	html+='</tr>';
	        }

	      
			foot+='<tr class="bg-olive" style="font-weight:bold;">';
			foot+='<td class="text-center" colspan="3">Total a pagar</td>';
			//html+='<td class="hidden">&nbsp;</td>';
			//html+='<td class="hidden">&nbsp;</td>';
			//html+='<td>0</td>';
			foot+='<td>'+Math.round10(total,-2)+'</td>';
			foot+='</tr>';

	        $(this).html(html);
	        $(this).siblings('tfoot').html(foot);
        });

    }

}(jQuery));

