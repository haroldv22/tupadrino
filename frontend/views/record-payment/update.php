<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\RecordPayment */

$this->title = 'Editar Pago';

?>
<div class="record-payment-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_update', [
        'model' => $model,
    ]) ?>

</div>
