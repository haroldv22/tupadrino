<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Historico de Pagos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="record-payment-index">


    <p>
        <?= Html::a('Registrar Pago', ['create'], ['class' => 'btn btn-arrow']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions'=>['class' => 'table tablenf table-newgray ','cellspacing'=>"0"],

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'type',
            'amount',
            'reference',
            'date_add',
            //'transference:boolean',
            // 'code_bank',
            // 'type',
            [
                'attribute' => 'id_status',
                'value'    => function($model){
                            if($model->id_status)
                                return $model->getIdStatus()                                            
                                             ->one()
                                             ->name;                                                                                                         },
            ],
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}', 
                'visibleButtons'=> [
                    'update' => function ($model, $key, $index) { 
                        if($model->id_status == Yii::$app->params['procesando'])
                            return true;
                        else 
                            return false;
                        },
                ],
            ],
        ],
    ]); ?>

</div>
