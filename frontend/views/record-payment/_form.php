<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model frontend\models\RecordPayment */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile(Yii::$app->request->baseUrl.'/js/RecordPayment.js',[
    'depends' => [\yii\web\JqueryAsset::className()]
]); 
$this->registerJsFile('https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js',[
    'depends' => [\yii\web\JqueryAsset::className()]
]); 
$getDetailsUrl = Yii::$app->urlManager->createUrl('record-payment/get-quota-details');

//desbloqueo campos para cuando ocurre una validacion
 if($model->ncuota !=""){
        $this->registerJs('$().QuotaChange("'.$model->ncuota.'","'.$getDetailsUrl.'")', \yii\web\View::POS_READY);
    }
$mercadopagoStart = ($model->method=='TDC')?true:0;
$this->registerJs('var mercadopagoStart = '.$mercadopagoStart.'', \yii\web\View::POS_HEAD);

?>
<style type="text/css">
    .quota-info{
        min-height: 44px;
    }
    .quota-info-result{
        padding-top: 7px;
    }
    #quota-info-details-btn{
        margin-left: 20px;
        color: #019fc4;
        transition: all .2s ease-in-out;
    }
    #quota-info-details-btn:hover{
        cursor: pointer;
        transform: scale(1.5); 
    }
    #quota-info-details{
        display: none;
    }
    .trans, .depo, .tdc{
        display: none;
    }


</style>

<div class="record-payment-form">


    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 
        'options'=>['id'=>'pay']
        ]); 



    $cuotas = $model->GetQuotaList();
    if( $cuotas == 'noQuota' || $cuotas == 'noLoan' ) { 
        $cuotas = [];
    }

   
    ?>  
    <div class="row">
        <h4 class="col-sm-offset-3 col-sm-9">
         Para información de como y donde pagar haz <?= Html::a('Click Aquí',['/site/pago-cancelaciones']) ?>
        </h4>

    </div>

    


    <?=  $form->field($model, 'ncuota')->dropDownList(['ALL'=>'Todas']+$cuotas, [
            'prompt'=>'-- Seleccione --',
            'onchange'=>'
                $().QuotaChange($(this).val(),"'.$getDetailsUrl.'");
            '
        ]);
    ?>
    <div class="quota-info">
        <div class="form-group">
            <label class="control-label col-sm-3" for="recordpayment-info-total">Total a pagar:</label>
            <div id="recordpayment-info-total" class="col-sm-6 quota-info-result">
                <span class="result">0.00</span> <span id="quota-info-details-btn" class="glyphicon glyphicon-eye-open" alt="Ver detalle"></span>
            </div>
        </div>
        <div id="quota-info-details">
            <div class="form-group ">
                <label class="control-label col-sm-3" for="recordpayment-info-quota">Cuota:</label>
                <div id="recordpayment-info-quota" class="col-sm-6 quota-info-result">
                    <span class="result">0.00</span> 
                </div>
            </div>
            <div class="form-group  ">
                <label class="control-label col-sm-3" for="recordpayment-info-arrear">Mora:</label>
                <div id="recordpayment-info-arrear" class="col-sm-6 quota-info-result">
                    <span class="result">-</span> 
                </div>
            </div>
            <div class="form-group  ">
                <label class="control-label col-sm-3" for="recordpayment-info-arrear-date">Inicio de mora:</label>
                <div id="recordpayment-info-arrear-date" class="col-sm-6 quota-info-result">
                    <span class="result">-</span> 
                </div>
            </div>
        </div>
    </div>

    <?=  $form->field($model, 'type')->dropDownList([
            'cuota'  => 'Cuota',
            'mora' => 'Mora',
            'ambos'=>'Cuota + Mora',
        ],
        ['prompt'=>'-- Seleccione --',
        'onchange'=>'
            if($(this).val()!="" && $("#recordpayment-ncuota").val()!=""){
                $.post("'.Yii::$app->urlManager->createUrl('record-payment/get-quota-amount') . '",          
                {idtype:$(this).val(),id:$("#recordpayment-ncuota").val()},
                function( data ) {
                    $().QuotaAmount(data);
                });
            }',
        'class'=>'disabled-field form-control',
        'disabled'=>'true',
        ]);
    ?>
    <div class="form-group">
        <div class="col-sm-6 col-sm-offset-3">
            <label class="radio-inline">
                <input type="radio" name="optradio" class='amount-disable disabled-field' disabled='true' checked>Monto fiel
            </label>
            <label class="radio-inline">
                <input type="radio" name="optradio" class='amount-active disabled-field' disabled='true'>Otro monto
            </label>
        </div>
    </div>
    <?=  $form->field($model, 'amount')->textInput(['readonly'=>'true']) ?>



    <?=  $form->field($model, 'method')->dropDownList([
        
        'TRF'  => 'Transferencia',
        'DEP' => 'Deposito',
        'TDC'=>'Tarjeta de crédito'],

        [
        'class'=>'disabled-field form-control',
        'disabled'=>'true',
        'prompt'=>'-- Seleccione --',
        'onchange'=>'$().methodpaychange()']);
    ?>
    <?php
        $disabled = true;
        if($model->method=='TRF'){
            $disabled = false;
        }
    ?>

    <div class="trans">
        <?=  $form->field($model, 'code_bank_origin')->dropDownList($model->GetBanks(), [
            'prompt'=>'-- Seleccione --',
            'disabled'=>$disabled]); ?>
    </div>
    <div class="trans depo">
        <?=  $form->field($model, 'code_bank_destiny')->dropDownList($model->GetSystemBanks(),[
            'class'=>'disabled-field form-control',
            'disabled'=>'true',
            'prompt'=>'-- Seleccione --',
            ]); ?>

        <?= $form->field($model, 'pay_date')->widget(DatePicker::className(), [
              'language' => 'es-ES',
                'dateFormat' => 'dd-MM-yyyy',
                'clientOptions' => [
                    'changeMonth' => true,
                    'changeYear'  => true,
                    'maxDate'     => 0,
                    'showAnim'    => 'fade'
                ]
            ])->textInput(['class'=>'form-control disabled-field','disabled'=>'true']); ?>

        <?=  $form->field($model, 'reference')->textInput(['disabled'=>'true', 'class'=>'disabled-field form-control']) ?>
    </div>

    <div class="tdc" <?=($model->method=='TDC')?'style="display: block;"':''?> >
        <?php if(Yii::$app->session->hasFlash('errorTDC')):?>
            <div class="form-group">
               <div class="alert alert-error col-sm-6 col-sm-offset-3">
                    <?= Yii::$app->session->getFlash('errorTDC'); ?>
               </div>
            </div>
         <?php endif; ?>
        <div class="form-group">
            <label class="control-label col-sm-3" for="cardNumber">Número de tarjeta</label>
            <div class="col-sm-6">
                <input type="text" id="cardNumber" class="form-control" data-checkout="cardNumber" placeholder="4966 3823 3110 9310" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="">Fecha vencimiento</label>
            <div class="col-sm-2">
                <input type="text" id="cardExpirationMonth" class="form-control" data-checkout="cardExpirationMonth" placeholder="12" />
            </div>
            <div class="col-sm-2">
                <input type="text" id="cardExpirationYear" class="form-control" data-checkout="cardExpirationYear" placeholder="2016" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="securityCode">Código seguridad</label>
            <div class="col-sm-2">
                <input type="text" id="securityCode" class="form-control" data-checkout="securityCode" placeholder="123" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="cardholderName">Nombre y apellido</label>
            <div class="col-sm-6">
                <div class="">
                    <input type="text" id="cardholderName" class="form-control" data-checkout="cardholderName" placeholder="APRO" />
                </div>
                <div class="">
                    Tal como esta impreso en la tarjeta
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="docNumber">Documento</label>
            <div class="col-sm-2">
                <select id="docType" class="form-control" data-checkout="docType"></select>
            </div>
            <div class="col-sm-4">
                <input type="text" id="docNumber" class="form-control" data-checkout="docNumber" placeholder="12345678" />
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Enviar', ['class' => 'btn btn-arrow center-block', 'id'=>'submit-btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
window.onload = function() {


}
</script>