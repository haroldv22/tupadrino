<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\RecordPayment */

$this->title = 'Pago #'.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Record Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="record-payment-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'amount:decimal',
            'date_add',
            [
                //'attribute'=>'type',
                'attribute'=>'method',
                'value'=>$model->GetMethodFullName(),
            ],
            'type',
            'pay_date',
            [
                'label'=>'Banco Origen',
                'attribute'=>'codeBankOrigin.name',
                'visible'=>(trim($model->method)=='TRF')?true:false,
            ],
            [
                'label'=>'Banco Destino',
                'attribute'=>'codeBankDestiny.name',
                'visible'=>(trim($model->method)!='TDC')?true:false,
            ],
            'reference',
            'idStatus.name:text:Estatus',
        ],
    ]) ?>
<?php

?>
</div>
