<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model frontend\models\RecordPayment */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile(Yii::$app->request->baseUrl.'/js/RecordPayment.js',[
    'depends' => [\yii\web\JqueryAsset::className()]
]); 

$getDetailsUrl = Yii::$app->urlManager->createUrl('record-payment/get-quota-details');

//desbloqueo campos para cuando ocurre una validacion
 if($model->ncuota !=""){
        $this->registerJs('$().QuotaChange("'.$model->ncuota.'","'.$getDetailsUrl.'")', \yii\web\View::POS_READY);
    }
?>
<style type="text/css">
    .quota-info{
        min-height: 44px;
    }
    .quota-info-result{
        padding-top: 7px;
    }
    #quota-info-details-btn{
        margin-left: 20px;
        color: #019fc4;
        transition: all .2s ease-in-out;
    }
    #quota-info-details-btn:hover{
        cursor: pointer;
        transform: scale(1.5); 
    }
    #quota-info-details{
        display: none;
    }


</style>
<div class="record-payment-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal']); 



    $cuotas = $model->GetQuotaList();
   
    if( $cuotas == 'noQuota' || $cuotas == 'noLoan' ) { 
        $cuotas = [];
    }

   
    ?> 


    <?=  $form->field($model, 'method')->dropDownList([
        
        'TRF'  => 'Transferencia',
        'DEP' => 'Deposito',
        'TDC'=>'Tarjeta de crédito'],

        [
        'class'=>' form-control',
        'prompt'=>'-- Seleccione --',
        'onchange'=>'
            if($(this).val()=="TRF"){
                $("#recordpayment-code_bank_origin").prop("disabled",false);
                $("#recordpayment-code_bank_destiny").prop("disabled",false);

            }
            else{
                $("#recordpayment-code_bank_origin").prop("disabled",true);
                $("#recordpayment-code_bank_origin").val("");
                
                if($(this).val()=="DEP"){
                    $("#recordpayment-code_bank_destiny").prop("disabled",false);
                }
                else{
                    $("#recordpayment-code_bank_destiny").prop("disabled",true);
                    $("#recordpayment-code_bank_destiny").val("");
                }
            }
        ']);
    ?>
    <?php
        $disabled = true;
        if($model->method=='TRF'){
            $disabled = false;
        }
    ?>
    <?= $form->field($model, 'pay_date')->widget(DatePicker::className(), [
          'language' => 'es-ES',
            'dateFormat' => 'dd-MM-yyyy',
            'clientOptions' => [
                'changeMonth' => true,
                'changeYear'  => true,
                'maxDate'     => 0,
                'showAnim'    => 'fade'
            ]
        ])->textInput(['class'=>'form-control',]); ?>

    <?=  $form->field($model, 'code_bank_origin')->dropDownList($model->GetBanks(), [
        'prompt'=>'-- Seleccione --',
        'disabled'=>$disabled]); ?>

    <?=  $form->field($model, 'code_bank_destiny')->dropDownList($model->GetSystemBanks(),[
        'class'=>'disabled-field form-control',
        'disabled'=>'true',
        'prompt'=>'-- Seleccione --',
        ]); ?>
    <?=  $form->field($model, 'reference')->textInput([ 'class'=>'disabled-field form-control']) ?>
   

    <div class="form-group">
        <?= Html::submitButton('Enviar', ['class' => 'btn btn-arrow center-block', 'id'=>'submit-btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
