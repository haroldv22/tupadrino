<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use beastbytes\wizard\WizardMenuP;
use frontend\models\BankHoliday;
use yii\web\View;


$this->registerCssFile(Yii::$app->request->baseUrl.'/css/registro.css', [
    'depends' => 'yii\bootstrap\BootstrapAsset',
]);


$this->title = 'Presentación';

//echo WizardMenu::widget(['step' => $event->step, 'wizard' => $event->sender]);

//print_r(WizardMenuP::widget(['step' => $event->step, 'wizard' => $event->sender]));
$model->valido ='hola mundo';//no borrar
?>


<div class="col-lg-12" >
	
 <!---
	

		<h3 class="text-center presentacion-text-ini">
			Este sistema te facilitará el acceso a un novedoso apoyo financiero 
			con el que crecerás de forma constante, iniciando el proceso
			con una pequeña solicitud que promete multiplicarse en el tiempo, 
			convirtiéndose en una interesante oportunidad de negocio
		</h3>
		<h1 class="presentacion-title">
			Asi funciona <?= Html::img('@web/img/logo.png', ['width'=>240, 'alt'=>Yii::$app->name]) ?>
		</h1>
		<div class="row presentacion-item">
			<div class="col-xs-3 presentacion-item-img">
				<?= Html::img('@web/img/ahijado.png', ['width'=>120, 'alt'=>Yii::$app->name, 'class'=>'img-responsive']) ?>
			</div>
			<div class="col-xs-9 presentacion-text-item">
				Naces como <spam class='presentacion-text-strong'>Ahijado</spam> al ser invitado<br>
				y progresas al mantener<br>
				al <spam class='presentacion-text-strong'>dia tus pagos.</spam>
			</div>
		</div>
		<div class="row text-center presentacion-arrow" >
			<?= Html::img('@web/img/downarrow.png', ['width'=>30, 'alt'=>Yii::$app->name, ]) ?>
		</div>
		<div class="row presentacion-item" >
			<div class="col-xs-3 presentacion-item-img">
				<?= Html::img('@web/img/padrino_1.png', ['width'=>120, 'alt'=>Yii::$app->name, 'class'=>'img-responsive']) ?>
			</div>
			<div class="col-xs-9 presentacion-text-item"> 
				A medida que creces te conviertes<br>
				en <spam class='presentacion-text-strong'>Padrino</spam> y pódras invitar<br>
				a tus propios <spam class='presentacion-text-strong'>Ahijados.</spam><br>
				Gracias al cabal cumplimiento <br>
				de los compromisos de pago de tus <spam class='presentacion-text-strong'>Ahijados</spam><br>
				recibes <spam class='presentacion-text-strong'>un % a tu favor.</spam>
			</div>
		</div>
		<div class="row text-center presentacion-arrow" >
			<?= Html::img('@web/img/downarrow.png', ['width'=>30, 'alt'=>Yii::$app->name, ]) ?>
		</div>
		<div class="row presentacion-item">
			<div class="col-xs-3 presentacion-item-img">
				<?= Html::img('@web/img/padrino.png', ['width'=>120, 'alt'=>Yii::$app->name, 'class'=>'img-responsive']) ?>
			</div>
			<div class="col-xs-9 presentacion-text-item"> 
				Tu cabal desempeño como <spam class='presentacion-text-strong'>Padrino</spam> <br>
				te garantizará una <spam class='presentacion-text-strong'>evolución</spam><br>
				<spam class='presentacion-text-strong'>financiera constante.</spam><br>
			</div>
		</div>
	-->
	
		<div  align="center" class="">
			<video id="video1"  autoplay controls class="img-responsive">
	    		<source src="<?= Yii::$app->request->baseUrl.'/video/TuPadrino.mp4'?>" type="video/mp4">
	    		Tu navegador no soporta videos HTML5.
	  		</video>
  		</div>


</div>




<?php
$form = ActiveForm::begin();
$form->errorSummary($model); 
echo $form->field($model, 'valido')->hiddenInput()->label(false);
//echo Html::img(Yii::$app->request->baseUrl.'/img/presentacion.png',['class' => 'img-responsive']);
echo Html::beginTag('div', ['class' => 'col-lg-12 presentacion-btn']);
	echo Html::submitButton('Pre-solicitud de apoyo financiero', [
		'class' => 'btn btn-next center-block', 
		'name' => 'next', 
		'value' => 'next'
	]);
echo Html::endTag('div');


ActiveForm::end();




?>



