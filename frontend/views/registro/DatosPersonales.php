<?php
use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use yii\bootstrap\ActiveForm;
use beastbytes\wizard\WizardMenu;
use beastbytes\wizard\WizardMenuP;

$this->title = 'Datos Personales';


$this->registerCssFile(Yii::$app->request->baseUrl.'/css/wizzard.css', [
    'depends' => 'yii\bootstrap\BootstrapAsset',
]);
$this->registerCssFile(Yii::$app->request->baseUrl.'/css/registro.css', [
    'depends' => 'yii\bootstrap\BootstrapAsset',
]);
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/registro.js',[
    'depends' => [\yii\web\JqueryAsset::className()]
    ]); 

//print_r($model->verificarCuenta());
if($model->identity){
  $this->registerCss(".datospersonales-form{ display:block; }
                      .show-btn{display:inline-block;}
                      .hide-btn{display:none;");

}

?>

<style type="text/css">
  .ui-datepicker{
    z-index: 999 !important;
  }
</style>
<div id='wizard-menu' class="row">

    <div class="step success-step col-xs-12 col-sm-6 col-lg-3">
      <div class="step-header center-block text-center">
        <div class="step-img">
          <img src="<?=Yii::$app->request->baseUrl.'/img/registro/presolicitud.png'?>" />
        </div>
      </div>
        <div class="step-content">
          <h3 class="step-title text-center">Pre-solicitud</h3>
          <div class="step-info text-center">
            Introduce el monto requerido
            y tiempo de pago.
          </div>
        </div>
    </div><!--/ .step-->

    <div class="step current-step  col-xs-12 col-sm-6 col-lg-3">
      <div class="step-header center-block text-center">
        <div class="step-img">
          <img src="<?=Yii::$app->request->baseUrl.'/img/registro/personales.png'?>" />
        </div>
      </div>
        <div class="step-content">
          <h3 class="step-title text-center">Datos personales</h3>
            <div class="step-info text-center">
              Introduce tus datos de identificaciíon 
              y contacto.
            </div>
        </div>
    </div><!--/ .step-->

    <div class="step col-xs-12 col-sm-6 col-lg-3">
      <div class="step-header center-block text-center">
        <div class="step-img">
          <img src="<?=Yii::$app->request->baseUrl.'/img/registro/usuario.png'?>" />
        </div>
      </div>
        <div class="step-content">
          <h3 class="step-title text-center">Creación de usuario</h3>
          <div class="step-info text-center">
            Introduce una contraseña y
            preguntas de seguridad.
          </div> 
        </div>
    </div><!--/ .step-->

    <div class="step col-xs-12 col-sm-6 col-lg-3">
      <div class="step-header center-block text-center">
        <div class="step-img">
          <img src="<?=Yii::$app->request->baseUrl.'/img/registro/datos.png'?>" />
        </div>
      </div>
        <div class="step-content">
          <h3 class="step-title text-center">Confirmación de datos</h3>
          <div class="step-info text-center">
            Verifica los datos suministrados.
            <br>&nbsp;
          </div> 
        </div>
    </div><!--/ .step-->
</div>
<div class="col-xs-12" style="height:50px;"></div>


<?php

$form = ActiveForm::begin(['validateOnSubmit' => false,
  'layout' => 'horizontal']);
?>

<?php echo $form->errorSummary($model); ?>
<div class="row">
  <label class="control-label col-sm-3" for="datospersonales-identity">Tipo y número <br> de identificación</label>
  <div class="col-sm-6">
    <div class="col-md-2 col-xs-3">
      <?= $form
        ->field($model, 'typeid',[
           'horizontalCssClasses' => [
              'wrapper'=>'',
              'offset'=>''
          ]])
        ->dropDownList([
          'V'  => 'V-',
          'E' => 'E-',
          ],
          ['prompt'=>'--'])
        ->label(false);
      ?>
    </div>
    <div class="col-md-10 col-xs-9">
      <?= 
      
      $form
        ->field($model, 'identity',[
           'horizontalCssClasses' => [
              'wrapper'=>'',
              'offset'=>''
          ]])
        ->textInput(['placeholder'=>'Ej. 5555555'])
        ->label(false);
        /*
        $form->field($model, 'identity', ['template' => '
           <div class="input-group">
              {input}
              <span class="input-group-btn ">
                <button type="button" class="btn btn-default" id="searchIdentity">
                &nbsp;<span class="glyphicon glyphicon-search" aria-hidden="true"></span>&nbsp;
                  </button>
              </span>
           </div>
           {error}{hint}
        '])->textInput(['placeholder'=>'Ej. 5555555']);
        */

      ?>
    </div>
  </div>
</div>
<div class="datospersonales-load text-center">
  <?= Html::img('@web/img/loading.gif', ['alt'=>Yii::$app->name, ]) ?>
</div>

<div class="datospersonales-form">
<?php
$readonly = 'false';
if($model->firstname =='' && $model->firstname==''){
  $readonly = 'true';
}

echo $form->field($model, 'firstname');
echo $form->field($model, 'lastname');

echo $form->field($model, 'gender')->dropDownList([
    'm'  => 'Masculino',
    'f' => 'Femenino'],
    ['prompt'=>'- Seleccione -']);

echo $form->field($model, 'birth_date')->widget(DatePicker::className(), [
  'language' => 'es-ES',
    'dateFormat' => 'dd-MM-yyyy',
    'clientOptions' => [
        'changeMonth' => true,
        'changeYear'  => true,
        'maxDate'     => 0,
        'yearRange' =>'-100:+10',
        'showAnim'    => 'fade'
    ]
])->textInput(['class'=>'form-control']);

//Ocupacion
echo $form->field($model, 'ocupation')->dropDownList($model->getOcupation(),
    ['prompt'=>'- Seleccione -',
    'onchange'=>"
      if($(this).val()!='' && $(this).val()=='Otro'){
        $('.field-datospersonales-pocupation').removeClass('hidden');
      }else{
         $('.field-datospersonales-pocupation').addClass('hidden');
      }
    "]);


//Ocupation hide
$ocupation = ($model->ocupation =='Otro')?'':'hidden'; 
echo $form->field($model, 'pocupation',
                ['options'=>
                  ['class'=>'form-group '.$ocupation]
                ])
          ->textInput(['placeholder'=>'Escriba su ocupacion'])
          ->label(false);
?>

<!--*/Ocupation hide-->

<!--Movil-->
<div class="row">
  <?= html::activeLabel($model, 'cellphone',['class'=>'control-label col-sm-3']) ?>
 <div class="col-sm-6">
    <div class="col-xs-2">
      <?= $form
        ->field($model, 'cellphoneprefix',[
           'horizontalCssClasses' => [
              'wrapper'=>'',
              'offset'=>''
          ]])
        ->dropDownList($model->getCellPhonePrefix(),
          ['prompt'=>'--'])
        ->label(false);
      ?>
    </div>
    <div class="col-xs-10">
      <?= $form
      ->field($model, 'cellphone',[
           'horizontalCssClasses' => [
              'wrapper'=>'',
              'offset'=>''
          ]])
      ->label(false)
      ->textInput(['maxlength'=>7]) 
      ?>
    </div>
  </div>
</div>
<!--*/movil-->
<!--Local-->
<div class="row">
  <?= html::activeLabel($model, 'phonenumber',['class'=>'control-label col-sm-3']) ?>
 <div class="col-sm-6">
    <div class="col-xs-2">
      <?= $form
        ->field($model, 'phoneprefix',[
           'horizontalCssClasses' => [
              'wrapper'=>'',
              'offset'=>''
          ]])
        ->dropDownList($model->getPhonePrefix(),
          ['prompt'=>'--'])
        ->label(false);
      ?>
    </div>
    <div class="col-xs-10">
      <?= $form
      ->field($model, 'phonenumber',[
           'horizontalCssClasses' => [
              'wrapper'=>'',
              'offset'=>''
          ]])
      ->label(false)
      ->textInput(['maxlength'=>7]) 
      ?>
    </div>
  </div>
</div>
<!--*/Local-->

<?php
echo $form->field($model, 'address')->textInput(['placeholder'=>'Ej. Av. 15A con calle 55 casa #34']);
echo $form->field($model, 'id_state')->dropDownList(
      $model->getState(), ['prompt'=>'-- Seleccione --',
      'onchange'=>'
                $.post("'.Yii::$app->urlManager->createUrl('registro/list') . '",          
                {id:$(this).val(),
                key:"state"
                }, 
                function( data ) {
                     $( "#'.Html::getInputId($model, 'id_municipality').'" ).html( data.municipality );
                     $( "#'.Html::getInputId($model, 'id_city').'" ).html( data.city );

                });
            ']);
//cargo los select si es un previo
if(is_numeric($model->id_state)){
  $municipality=$model->getMunicipality($model->id_state);
  $city=$model->getCity($model->id_state);
}else{
  $municipality=[];
  $city=[];
}

echo $form->field($model, 'id_municipality')->dropDownList($municipality,['prompt'=>'-- Seleccione --',
  /*'onchange'=>'
                $.post("'.Yii::$app->urlManager->createUrl('registro/list') . '",          
                {id:$(this).val(),
                key:"municipality"
                }, 
                function( data ) {
                     $( "#'.Html::getInputId($model, 'id_parish').'" ).html( data );
                });'*/
  ]);
echo $form->field($model, 'id_city')->dropDownList($city,
      ['prompt'=>'-- Seleccione --']
  );

echo $form->field($model, 'code_bank')->dropDownList($model->getBanks(),
      ['prompt'=>'-- Seleccione --',
      'onchange'=>'$( "#ac1" ).val($(this).val());']
  );
?>
<!--cuenta-->
<div class="row">
    <?= html::activeLabel($model, 'number_account',['class'=>'control-label col-sm-3']) ?>
  <div class="col-sm-6">
    <div class="col-xs-2">
      <div class="form-group">
      <input id="ac1" class="form-control" value="<?=$model->code_bank ?>" readonly="true" type="text">
      </div>
    </div>
    <div class="col-xs-10">
    <?= $form
      ->field($model, 'number_account',[
           'horizontalCssClasses' => [
              'wrapper'=>'',
              'offset'=>''
          ]])
      ->label(false)
      ->textInput(['maxlength'=>16])
    ?>
    <p>Usted debe ser el titular de la Cuenta (Obligatoriamente)</p>
    </div>
  </div>
</div>
<!--*/cuenta-->
</div><!--*/DatosPersonalesForm-->
<?php

echo Html::beginTag('div', ['class' => 'col-lg-12 text-center']);
echo Html::beginTag('div', ['class' => 'registro-botonera']);
echo Html::submitButton('Anterior', ['class' => 'btn btn-previous', 'name' => 'prev', 'value' => 'prev']);
echo Html::Tag('div','',['class' => 'btn-separador']);
echo Html::submitButton('Siguiente', ['class' => 'btn btn-next hide-btn', 'id'=>'searchIdentity']);
echo Html::submitButton('Siguiente', ['class' => 'btn btn-next show-btn', 'name' => 'next', 'value' => 'next']);
echo Html::endTag('div');
echo Html::endTag('div');
ActiveForm::end();
?>

