<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
$this->registerCssFile(Yii::$app->request->baseUrl.'/css/registro.css', [
    'depends' => 'yii\bootstrap\BootstrapAsset',
]);


$this->title = 'Registro Completado';

?>
<style type="text/css">
    body{
        color:#6e6f73;
        background: url(<?=Yii::$app->request->baseUrl?>/img/deco/completado.png);
        background-repeat: no-repeat;
        background-size:25%;
        background-position: 100% 100%;
    }
</style>

<div class="registrocompleto">

        <h1 class="text-center">
        	<span class='bienvenida'>
        		Ya eres parte de la familia
       			<?=Html::img('@web/img/registro/bienvenida.png',['class' => 'img-responsive']) ?>
        	</span>
        </h1>
        <br>
        <br>
        <br>
        <br>
        <br>
        <div class="text-center">
            <?= Html::a('<spam style="/*box-shadow: inset 0 0px 0 white, inset 0 -1px 0 #fdb912*/">Inicia sesión aqui<spam>',
             ['site/login'], 
             ['class' => 'btn btn-arrow','style'=>"font-size:25px;"]) ?>           
        </div>

</div>



