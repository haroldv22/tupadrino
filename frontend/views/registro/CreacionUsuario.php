<?php
use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;
use beastbytes\wizard\WizardMenu;
use frontend\models\ClientInvitation;
use yii\bootstrap\BootstrapPluginAsset;
BootstrapPluginAsset::register($this);

$this->title = 'Creacion de Usuario';

$this->registerCssFile(Yii::$app->request->baseUrl.'/css/wizzard.css', [
  'depends' => 'yii\bootstrap\BootstrapAsset',
]);
$this->registerCss('
            .popover{
                width:300px;
            }');
$this->registerJs('jQuery("#'.Html::getInputId($model, 'password').'").popover({html:true});', \yii\web\View::POS_READY);

?>
<div id='wizard-menu' class="row">

    <div class="step success-step col-xs-12 col-sm-6 col-lg-3">
      <div class="step-header center-block text-center">
        <div class="step-img">
          <img src="<?=Yii::$app->request->baseUrl.'/img/registro/presolicitud.png'?>" />
        </div>
      </div>
        <div class="step-content">
          <h3 class="step-title text-center">Pre-solicitud</h3>
          <div class="step-info text-center">
            Introduce el monto requerido
            y tiempo de pago.
          </div>
        </div>
    </div><!--/ .step-->

    <div class="step success-step  col-xs-12 col-sm-6 col-lg-3">
      <div class="step-header center-block text-center">
        <div class="step-img">
          <img src="<?=Yii::$app->request->baseUrl.'/img/registro/personales.png'?>" />
        </div>
      </div>
        <div class="step-content">
            <h3 class="step-title text-center">Datos personales</h3>
            <div class="step-info text-center">
              Introduce tus datos de identificaciíon 
              y contacto.
            </div>
        </div>
    </div><!--/ .step-->

    <div class="step current-step col-xs-12 col-sm-6 col-lg-3">
      <div class="step-header center-block text-center">
        <div class="step-img">
          <img src="<?=Yii::$app->request->baseUrl.'/img/registro/usuario.png'?>" />
        </div>
      </div>
        <div class="step-content">
          <h3 class="step-title text-center">Creación de usuario</h3>
          <div class="step-info text-center">
            Introduce una contraseña y
            preguntas de seguridad.
          </div> 
        </div>
    </div><!--/ .step-->

    <div class="step col-xs-12 col-sm-6 col-lg-3">
      <div class="step-header center-block text-center">
        <div class="step-img">
          <img src="<?=Yii::$app->request->baseUrl.'/img/registro/datos.png'?>" />
        </div>
      </div>
        <div class="step-content">
          <h3 class="step-title text-center">Confirmación de datos</h3>
          <div class="step-info text-center">
            Verifica los datos suministrados.
            <br>&nbsp;
          </div> 
        </div>
    </div><!--/ .step-->
</div>
<div class="col-xs-12" style="height:50px;"></div>
<?php
$form = ActiveForm::begin(['validateOnSubmit' => false,'layout' => 'horizontal']);

echo 
'<div class="form-group">
    <label class="col-sm-3 control-label">Usuario</label>
    <div class="col-sm-6">
      <p class="form-control-static">'.$model->email.'</p>
      '.$form->field($model, 'email',['template'=>"{error}"]).'
    </div>
</div>';

echo $form->field($model, 'password')
  ->passwordInput(['maxlength' => true,
    'data-toggle'=>"popover",
    'data-trigger'=>"hover",
    'data-content'=>"Debe tener una longitud entre 6 a 12 caracteres.</br> 
                     Debe contener letras y numeros al menos uno de cada una."
  ]);
echo $form->field($model, 'repassword')->passwordInput(['maxlength' => true]);

echo $form->field($model, 'questiona')->dropDownList($model->getSystemQuestion(),
			['prompt'=>'-- Seleccione --',
			'onchange'=>'
				if($(this).val()=="specific"){
					$("#cuqp0").show();
				}else{
					$("#cuqp0").hide();
					$("#'.Html::getInputId($model, 'questionpa').'").val("");
				}

            ']);

$display='style="display: none"';
$disabled='true';
if($model->questiona=='specific'){
	$display='';
	$disabled='false';
}
echo $form->field($model, 'questionpa',['template' => '
   <div id="cuqp0" class="col-sm-6 col-sm-offset-3" '.$display.'>
        {input}
       {error}{hint}
   </div>'])->textInput(['placeholder'=>'Escriba su pregunta']);;


echo $form->field($model, 'answera');

//segunda pregunta
echo $form->field($model, 'questionb')->dropDownList($model->getSystemQuestion(),
			['prompt'=>'-- Seleccione --',
			'onchange'=>'
				if($(this).val()=="specific"){
					$("#cuqp1").show();
				}else{
					$("#cuqp1").hide();
					$("#'.Html::getInputId($model, 'questionpb').'").val("");
				}
            ']);


$display='style="display: none"';
$disabled='true';
if($model->questionb=='specific'){
	$display='';
	$disabled='false';
}

echo $form->field($model, 'questionpb',['template' => '
   <div id="cuqp1" class="col-sm-6 col-sm-offset-3" '.$display.'>
        {input}
       {error}{hint}
   </div>'])->textInput(['placeholder'=>'Escriba su pregunta']);;

echo $form->field($model, 'answerb');


//print_r($event->data);


echo Html::beginTag('div', ['class' => 'col-lg-12 text-center']);
echo Html::beginTag('div', ['class' => 'registro-botonera']);
echo Html::submitButton('Anterior', ['class' => 'btn btn-previous', 'name' => 'prev', 'value' => 'prev']);
echo Html::Tag('div','',['class' => 'btn-separador']);
echo Html::submitButton('Siguiente', ['class' => 'btn btn-next', 'name' => 'next', 'value' => 'next']);
echo Html::endTag('div');
echo Html::endTag('div');
ActiveForm::end();


?>