<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use beastbytes\wizard\WizardMenu;
use yii\widgets\DetailView;
use frontend\models\Loan;
use frontend\models\Quota;
use frontend\models\Period;

$this->title = 'Final';

$this->registerCssFile(Yii::$app->request->baseUrl.'/css/wizzard.css', [
    'depends' => 'yii\bootstrap\BootstrapAsset',
]);
$this->registerCssFile(Yii::$app->request->baseUrl.'/css/registro.css', [
    'depends' => 'yii\bootstrap\BootstrapAsset',
]);

//separo el numero de dias del id
function Myperiod($data){
	$Myperiod = explode("-", $data);
	return $Myperiod[1];
}
?>
<div id='wizard-menu' class="row">

    <div class="step success-step col-xs-12 col-sm-6 col-lg-3">
      <div class="step-header center-block text-center">
        <div class="step-img">
          <img src="<?=Yii::$app->request->baseUrl.'/img/registro/presolicitud.png'?>" />
        </div>
      </div>
        <div class="step-content">
          <h3 class="step-title text-center">Pre-solicitud</h3>
          <div class="step-info text-center">
            Introduce el monto requerido
            y tiempo de pago.
          </div>
        </div>
    </div><!--/ .step-->

    <div class="step success-step  col-xs-12 col-sm-6 col-lg-3">
      <div class="step-header center-block text-center">
        <div class="step-img">
          <img src="<?=Yii::$app->request->baseUrl.'/img/registro/personales.png'?>" />
        </div>
      </div>
        <div class="step-content">
            <h3 class="step-title text-center">Datos personales</h3>
            <div class="step-info text-center">
              Introduce tus datos de identificaciíon 
              y contacto.
            </div>
        </div>
    </div><!--/ .step-->

    <div class="step success-step col-xs-12 col-sm-6 col-lg-3">
      <div class="step-header center-block text-center">
        <div class="step-img">
          <img src="<?=Yii::$app->request->baseUrl.'/img/registro/usuario.png'?>" />
        </div>
      </div>
        <div class="step-content">
          <h3 class="step-title text-center">Creación de usuario</h3>
          <div class="step-info text-center">
            Introduce una contraseña y
            preguntas de seguridad.
          </div> 
        </div>
    </div><!--/ .step-->

    <div class="step current-step col-xs-12 col-sm-6 col-lg-3">
      <div class="step-header center-block text-center">
        <div class="step-img">
          <img src="<?=Yii::$app->request->baseUrl.'/img/registro/datos.png'?>" />
        </div>
      </div>
        <div class="step-content">
          <h3 class="step-title text-center">Confirmación de datos</h3>
          <div class="step-info text-center">
            Verifica los datos suministrados.
            <br>&nbsp;
          </div> 
        </div>
    </div><!--/ .step-->
</div>
<div class="col-xs-12" style="height:50px;"></div>


<div class="row">
	<div class="col-sm-6">
		<div class="panel panel-newgreen">
		  <div class="panel-heading">
		    <h3 class="panel-title">Datos del Cliente</h3>
		  </div>
		  <div class="panel-body">
		  	<?php
		    echo DetailView::widget([
		    'model' => $data['DatosPersonales'][0],
		    'attributes' => [
		    	'identity',
		    	'firstname',
		    	'lastname',
		    	[                     
		        	'label' => 'Cuenta bancaria',
		        	'value' => $data['DatosPersonales'][0]['code_bank'].$data['DatosPersonales'][0]['number_account'],
		        ],
		    ],
		    'options'=>['class' => 'table']
			]);
		    ?>
		  </div>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="panel panel-newblue">
		  <div class="panel-heading">
		    <h3 class="panel-title">Datos del préstamo</h3>
		  </div>
		  <div class="panel-body">
		    <?php
		    $periodalias =Period::findOne(['n_days'=>Myperiod($data['Presolicitud'][0]['period'])])->alias;
		    $quotaalias =Quota::findOne(['days'=>$data['Presolicitud'][0]['quota']])->alias;

		    echo DetailView::widget([
		    'model' => $data['Presolicitud'][0],
		    'attributes' => [

		        [                      
		        	'label' => 'Monto Solicitado',
		        	'value' => $data['Presolicitud'][0]['amount'].' Bs',
		        ],
		        [                      
		        	'label' => 'Plazo',
		        	'value' => $periodalias,
		        ],
		        [                      
		        	'label' => 'Cuota',
		        	'value' => $quotaalias,
		        ],
		    ],
		    'options'=>['class' => 'table']
			]);
		    ?>
		  </div>
		</div>
	</div>
</div>
	<div class="panel panel-newyellow">
	  <div class="panel-heading">
	    <h3 class="panel-title">Detalle de los pagos</h3>
	  </div>
	  <div class="panel-body">
		<table class="table">
	      <thead>
	        <tr>
	          <th># Cuota</th>
	          <th>Fecha de Pago</th>
	          <th>Fecha de Vencimiento</th>
	          <th>Cuota</th>
	        </tr>
	      </thead>
	      <tbody>
		      <?PHP
				$tablaPago = Loan::TablaPago($data['Presolicitud'][0]['quota'],
												Myperiod($data['Presolicitud'][0]['period']),
												$data['Presolicitud'][0]['rate'],
												$data['Presolicitud'][0]['amount']);

				foreach ($tablaPago as $key => $value) {
					echo"
			      	<tr>
			          <th scope='row'>".$value['n_quota']." </th>
			          <td>".$value['date_pay']."</td>
			          <td>".$value['date_expired']."</td>
			          <td>".$value['quota']."</td>
			        </tr>";
				}
		      	
		      ?>
	      </tbody>
		</table>
		</div>
	</div>




<?PHP

$form = ActiveForm::begin(['validateOnSubmit' => false,
  'layout' => 'horizontal']);

echo $form->field($model, 'term')->checkbox()->label("Acepto los ".Html::a('terminos & condiciones', '@web/docs/term.pdf', ['target'=>"_blank"]));
echo Html::beginTag('div', ['class' => 'col-lg-12 text-center']);
echo Html::beginTag('div', ['class' => 'registro-botonera']);
echo Html::submitButton('Anterior', ['class' => 'btn btn-previous', 'name' => 'prev', 'value' => 'prev']);
echo Html::Tag('div','',['class' => 'btn-separador']);
echo Html::submitButton('Siguiente', ['class' => 'btn btn-next', 'name' => 'next', 'value' => 'next']);
echo Html::endTag('div');
echo Html::endTag('div');
ActiveForm::end();


?>
