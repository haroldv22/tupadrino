<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use beastbytes\wizard\WizardMenuP;
use frontend\models\BankHoliday;
use yii\web\View;

$this->registerCssFile(Yii::$app->request->baseUrl.'/plugin/ion.rangeSlider/css/ion.rangeSlider.css', [
    'depends' => 'yii\bootstrap\BootstrapAsset',
]);
$this->registerCssFile(Yii::$app->request->baseUrl.'/plugin/ion.rangeSlider/css/ion.rangeSlider.skinHTML5.css', [
    'depends' => 'yii\bootstrap\BootstrapAsset',
]);
$this->registerCssFile(Yii::$app->request->baseUrl.'/css/calendario.css', [
    'depends' => 'yii\bootstrap\BootstrapAsset',
]);

$this->registerCssFile(Yii::$app->request->baseUrl.'/plugin/datepicker/css/datepicker.css', [
    'depends' => 'yii\bootstrap\BootstrapAsset',
]);

$this->registerCssFile(Yii::$app->request->baseUrl.'/css/wizzard.css', [
    'depends' => 'yii\bootstrap\BootstrapAsset',
]);

$this->registerCssFile(Yii::$app->request->baseUrl.'/css/registro.css', [
    'depends' => 'yii\bootstrap\BootstrapAsset',
]);




$this->registerJsFile(Yii::$app->request->baseUrl.'/plugin/ion.rangeSlider/js/ion.rangeSlider.min.js',[
	'depends' => [\yii\web\JqueryAsset::className()]
]); 
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/registro.js',[
	'depends' => [\yii\web\JqueryAsset::className()]
]); 


$this->registerJsFile(Yii::$app->request->baseUrl.'/plugin/datepicker/js/datepicker.js',[
	'depends' => [\yii\web\JqueryAsset::className()]
]); 

$this->registerJsFile(Yii::$app->request->baseUrl.'/js/LoanDetails.js',[
	'depends' => [\yii\web\JqueryAsset::className()]
]); 

$this->registerJsFile(Yii::$app->request->baseUrl.'/js/jquery.formatCurrency-1.4.0.js',[
	'depends' => [\yii\web\JqueryAsset::className()]
]); 

$this->registerJs('var Holidays = '.BankHoliday::getHolidays(date("Y")),View::POS_BEGIN);

$this->title = 'Presolicitud de Credito';

//echo WizardMenu::widget(['step' => $event->step, 'wizard' => $event->sender]);

//print_r(WizardMenuP::widget(['step' => $event->step, 'wizard' => $event->sender]));
?>

<div id='wizard-menu' class="row">

		<div class="step current-step col-xs-12 col-sm-6 col-lg-3">
			<div class="step-header text-center center-block ">
				<div class="step-img">
					<img src="<?=Yii::$app->request->baseUrl.'/img/registro/presolicitud.png'?>" />
				</div>
			</div>
				<div class="step-content">
         			<h3 class="step-title text-center" style="li">Pre-solicitud </h3>
					<div class="step-info text-center">
						Introduce el monto requerido
						y tiempo de pago.
					</div>
				</div>
		</div><!--/ .step-->

		<div class="step col-xs-12 col-sm-6 col-lg-3">
			<div class="step-header center-block text-center">
				<div class="step-img">
					<img src="<?=Yii::$app->request->baseUrl.'/img/registro/personales.png'?>" />
				</div>
			</div>
				<div class="step-content">
					<h3 class="step-title text-center">Datos personales</h3>
						<div class="step-info text-center">
							Introduce tus datos de identificaciíon 
							y contacto.
						</div>
				</div>
		</div><!--/ .step-->

		<div class="step col-xs-12 col-sm-6 col-lg-3">
			<div class="step-header center-block text-center">
				<div class="step-img">
					<img src="<?=Yii::$app->request->baseUrl.'/img/registro/usuario.png'?>" />
				</div>
			</div>
				<div class="step-content">
					<h3 class="step-title text-center">Creación de usuario</h3>
					<div class="step-info text-center">
						Introduce una contraseña y
						preguntas de seguridad.
					</div> 
				</div>
		</div><!--/ .step-->

		<div class="step col-xs-12 col-sm-6 col-lg-3">
			<div class="step-header center-block text-center">
				<div class="step-img">
					<img src="<?=Yii::$app->request->baseUrl.'/img/registro/datos.png'?>" />
				</div>
			</div>
				<div class="step-content">
					<h3 class="step-title text-center">Confirmación de datos</h3>
					<div class="step-info text-center">
						Verifica los datos suministrados.
						<br>&nbsp;
					</div> 
				</div>
		</div><!--/ .step-->
</div>
<div class="col-xs-12" style="height:50px;"></div>

<?php
$form = ActiveForm::begin();
echo Html::beginTag('div', ['class' => 'row']);
	echo Html::beginTag('div', ['class' => 'col-lg-6']);

		echo $form->field($model, 'amount')->textInput(['class'=>'change']);

		echo $form->field($model, 'period')->dropDownList([],['class'=>'form-control change']
			);
		echo $form->field($model, 'quota')->dropDownList(
				$model->getQuota(),['class'=>'form-control change']
			);

	
	echo Html::endTag('div');

	echo Html::beginTag('div', ['class' => 'col-lg-6']);
		echo '<div class="panel panel-newblue">
					<div class="panel-heading">
					    <h3 class="panel-title">Detalle del Crédito</h3>
					</div>
					<div class="panel-body">
						<table class="table detallecredito">
					      <thead>
					        <tr>
					          <th>Monto Solicitado</th>
					          <th>Monto Cuota</th>
					          <th>No. Cuotas</th>
					        </tr>
					      </thead>
					      <tbody id="value">
					      </tbody>
					      <tfoot>
					      </tfoot>
						</table>						
					</div>
				</div>';
		echo '<div class="panel panel-newgreen">
					<div class="panel-heading">
					    <h3 class="panel-title">Fecha de Pago</h3>
					</div>
					<div class="panel-body">
						<br>
						<div id="loandate"></div>
					</div>
				</div>';
	echo Html::endTag('div');


echo Html::endTag('div');
echo Html::beginTag('div', ['class' => 'col-lg-12']);
	echo Html::submitButton('Siguiente', [
		'class' => 'btn btn-next center-block', 
		'name' => 'next', 
		'value' => 'next'
	]);
echo Html::endTag('div');


ActiveForm::end();

$this->registerJs("
	var arr = [".$model->getAmount()."];
	var position = $.inArray('".$model->amount."',arr);
	$('#presolicitud-amount').ionRangeSlider({
	    type: 'single',
	    values: arr,
	    grid: true,
	    from:position,
	});

	loadPeriod();
	$('.change').change(function() {
		$('#value').LoanDetails({
            quota         : $('#presolicitud-quota').val(),
            period        : GetPeriod(),
            rate		  : ".$model->rate.",
            amount        : $('#presolicitud-amount').val(),
            date          : '".date('m/j/Y')."',
            exp 		  : ".\Yii::$app->params['payExp']."
		});  

		$('.currency').formatCurrency({region: 'es-VE' });

	});
	$('#value').LoanDetails({
        quota         : $('#presolicitud-quota').val(),
        period        : GetPeriod(),
        rate		  : ".$model->rate.",
        amount        : $('#presolicitud-amount').val(),
        date          : '".date('m/j/Y')."',
        exp 		  : ".\Yii::$app->params['payExp']."
	});  

	
	$('.currency').formatCurrency({region: 'es-VE' });

	
", View::POS_READY);



?>



