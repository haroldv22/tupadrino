<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="<?= Yii::$app->request->baseUrl ?>/favicon.png?v=1"/>

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?php  
        /*
        $this->registerCssFile('http://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css', [
        'depends' => 'yii\bootstrap\BootstrapAsset',
        ]);*/
        $this->registerCssFile(Yii::$app->request->baseUrl.'/css/nomain.css', [
        'depends' => 'yii\bootstrap\BootstrapAsset',
        ]);
       // $this->registerCssFile('https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css', [
       // 'depends' => 'yii\bootstrap\BootstrapAsset',
       // ]);
    ?>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,100,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    
</head>
<body>

<?php $this->beginBody() ?>

<div class="wrap">


    <div class="container">
    <?php
        echo Breadcrumbs::widget([
            'homeLink' => ['label' => 'Inicio',
            'url' => ['/site/index']],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]); 
        echo Alert::widget();
        echo $content;
    ?>
     
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; TuPadrino.net <?= date('Y') ?></p>

        <p class="pull-right">All Copyright Reserved</p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>        