<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="<?= Yii::$app->request->baseUrl ?>/favicon.png?v=1"/>

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?php  
       /* $this->registerCssFile('http://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css', [
        'depends' => 'yii\bootstrap\BootstrapAsset',
        ]);*/
        $this->registerCssFile(Yii::$app->request->baseUrl.'/css/nomain.css', [
        'depends' => 'yii\bootstrap\BootstrapAsset',
        ]);
        $this->registerCssFile(Yii::$app->request->baseUrl.'/css/lsite.css', [
        'depends' => 'yii\bootstrap\BootstrapAsset',
        ]);
       // $this->registerCssFile('https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css', [
       // 'depends' => 'yii\bootstrap\BootstrapAsset',
       // ]);

    ?>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,100,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>


</head>
<body>

<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Html::img('@web/img/logo.png', ['alt'=>Yii::$app->name,'class'=>'img-responsive']),
        'brandUrl' => Yii::$app->homeUrl.'site/index',
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'INICIO', 'url' => ['/site/index'],'options'=>['class'=>'especialitem ini']],
        ['label' => 'MI CUENTA', 'url' => ['/app/index'],'options'=>['class'=>'especialitem pc']],  
        ['label' => 'SISTEMA', 
            'items'=> [
              //  ['label' => 'INVITACIÓN AL SISTEMA', 'url' => ['/site/invitacion'],'options'=>['class'=>'bg-dm-st1']],
              //  '<li class="divider"></li>',
              //  ['label' => 'PREGUNTAS FRECUENTES', 'url' =>['/site/faq'],'options'=>['class'=>'bg-dm-st2']],
                '<li class="divider"></li>',
                ['label' => 'CULTURA PADRINO', 'url' => ['/site/sistema'],'options'=>['class'=>'bg-dm-st1']],                
        ]],
        ['label' => 'ADMINISTRACIÓN', 
            'items'=> [
                ['label' => 'RESPONSABILIDAD Y COMPROMISO', 'url' => ['/site/pago-cancelaciones'],'options'=>['class'=>'bg-dm-st1']],
                '<li class="divider"></li>',
                ['label' => 'SOLICITUDES', 'url' =>['credito/solicitud'],'options'=>['class'=>'bg-dm-st2']],
                
        ]],
        ['label' => 'CONTACTO', 'url' => ['/site/contact']],
        ['label' => 'SALIR', 'url' => ['/site/logout'],
            'linkOptions' => ['data-method' => 'post'],
            'options'=>['class'=>'bg-dm-ct2']
        ],

    ];

    echo Nav::widget([
        'encodeLabels' => false,
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
    <?php

        echo Breadcrumbs::widget([
                'homeLink' => ['label' => 'Panel de Control',
                'url' => ['/app/index']],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]); 
        echo Alert::widget();
        echo $content;
    ?>
     
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; TuPadrino.net <?= date('Y') ?></p>

        <p class="pull-right">All Copyright Reserved</p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>        