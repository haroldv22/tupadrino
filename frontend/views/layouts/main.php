<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use frontend\models\Clients;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="<?= Yii::$app->request->baseUrl ?>/favicon.png?v=1"/>

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    
    <?php  
        /*
        $this->registerCssFile('http://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css', [
        'depends' => 'yii\bootstrap\BootstrapAsset',
        ]);
        
        $this->registerCssFile('https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css', [
        'depends' => 'yii\bootstrap\BootstrapAsset',
        ]);
        */
        

        if (!Yii::$app->user->isGuest) {
            $this->registerCssFile(Yii::$app->request->baseUrl.'/css/offcanvas.css', [
            'depends' => 'yii\bootstrap\BootstrapAsset',
            ]);
            $this->registerJsFile(Yii::$app->request->baseUrl.'/js/offcanvas.js',[
            'depends' => 'yii\bootstrap\BootstrapAsset'
            ]); 
        }
    ?>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,100,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

</head>
<body>

<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Html::img('@web/img/logo.png', ['alt'=>Yii::$app->name,'class'=>'img-responsive']),
        'brandUrl' => Yii::$app->homeUrl.'app/index',
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [];
    if (Yii::$app->user->isGuest) {

        $menuItems = [];
    } else {
        $menuItems =[
        ['label' => 'INICIO', 'url' => ['/site/index'],'options'=>['class'=>'especialitem ini']],
        ['label' => 'MI CUENTA', 'url' => ['/app/index'],'options'=>['class'=>'especialitem pc']],

            ['label' => 'PAGOS', 
            'items'=> [
                    ['label' => 'REGISTRAR', 'url' => ['/record-payment/create'],'options'=>['class'=>'bg-dm-ct1']],
                    '<li class="divider"></li>',
                    ['label' => 'HISTORICO', 'url' => ['/record-payment/index'],'options'=>['class'=>'bg-dm-ct2']],                
            ]],
            
            ['label' => 'SOLICITUD', 
                 'items'=> [
                    ['label' => 'NUEVA', 'url' => ['/credito/solicitud'],'options'=>['class'=>'bg-dm-ct1']],
                    '<li class="divider"></li>',
                    ['label' => 'VIGENTE', 'url' =>['/credito/vigente'],'options'=>['class'=>'bg-dm-ct2']],
                    '<li class="divider"></li>',
                    ['label' => 'HISTORICO', 'url' => ['/credito/historico'],'options'=>['class'=>'bg-dm-ct3']],                
            ]],
            ['label' => 'COMISIONES', 
                 'items'=> [
                    ['label' => 'ACTIVAS', 'url' => ['/commission/activa'],'options'=>['class'=>'bg-dm-ct1']],
                    '<li class="divider"></li>',
                    ['label' => 'HISTORICO', 'url' =>['/commission/historico'],'options'=>['class'=>'bg-dm-ct2']],               
            ]],
            ['label' => 'AHIJADOS', 
                 'items'=> [
                    ['label' => 'INVITAR', 'url' => ['/godson/create'],'options'=>['class'=>'bg-dm-ct1']],
                    '<li class="divider"></li>',
                    ['label' => 'PENDIENTES', 'url' =>['/godson/pending'],'options'=>['class'=>'bg-dm-ct2']],
                    '<li class="divider"></li>',
                    ['label' => 'AHIJADOS', 'url' => ['/godson/index'],'options'=>['class'=>'bg-dm-ct3']],                
            ]],
            [
                'label' => '[' .Yii::$app->user->identity->Shortname(). ']',
                'items'=> [
                    ['label' => 'CONFIGURACION', 'url' => ['/app/config'],'options'=>['class'=>'bg-dm-ct1']],
                    '<li class="divider"></li>',
                
                    ['label' => 'SALIR', 'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'POST'],
                    'options'=>['class'=>'bg-dm-ct2']
                    ],
                                  
            ]],
        ];
    }
    echo Nav::widget([
        'encodeLabels' => false,
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container main-container">
    <?php
        if (Yii::$app->user->isGuest) {
            echo Alert::widget();
            echo $content;
        }
        else{
    ?>
        <div class="row row-offcanvas row-offcanvas-right">
        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Perfil</button>
          </p>
        </div>
        <div class="col-xs-12 col-sm-9">

        <?php
            echo Alert::widget();
            echo $content;
        ?>
        </div><!--/.col-xs-12.col-sm-9-->

        <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
            <div id="profile-header" class="clearfix">            
                <div id="settings" class="pull-right">
                    
                    <a href="">
                        <i class="glyphicon glyphicon-envelope"></i>
                    </a>
                    
                    <?= Html::a('<i class="glyphicon glyphicon-cog"></i>',
                        ['/app/config']) 
                     ?>

                   
                </div>
            </div>
            <div id='avatar'>
                
                <img src="<?=Yii::$app->request->baseUrl.Yii::$app->user->identity->image?>" width="100%"  class='user-img'>
                <img src="<?=Yii::$app->request->baseUrl?>/img/levels/level-<?=Yii::$app->user->identity->id_level?>.png" width="100%"  class='avatar-deco center-block'>

            </div>

            <div id="profile-info">
                <h4 class="name" style="color:#58585a"><?= Yii::$app->user->identity->Shortname(); ?></h4>
                <h3 class="name"> <?=  Yii::$app->user->identity->getNameLevel(); ?></h3>
            </div>

            <div id="reputation" >
                
                                    
            </div>
            <br>
            <br>
            <div id="notice" class="clearfix hide">
             <div class="pull-left">
                <i class="fa fa-exclamation-circle"></i>
            </div>
           
            <div class="pull-left col-xs-11 text-justify"> 
            Para pasar al siguiente nivel, es requerido solicitar un credito con los ultimos montos 
            habilitados.
            </div>
            </div>

        </div><!--/.sidebar-offcanvas-->
      </div>
     <?php 
        }
     ?>
    
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; TuPadrino.net <?= date('Y') ?></p>

        <p class="pull-right">All Copyright Reserved</p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
