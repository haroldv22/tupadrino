<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use beastbytes\wizard\WizardMenu;
use yii\widgets\DetailView;
use frontend\models\Loan;
use frontend\models\Clients;
use frontend\models\Quota;
use frontend\models\Period;

$this->title = 'Final';

$this->registerCssFile(Yii::$app->request->baseUrl.'/css/wizzard.css', [
    'depends' => 'yii\bootstrap\BootstrapAsset',
]);
$this->registerCssFile(Yii::$app->request->baseUrl.'/css/registro.css', [
    'depends' => 'yii\bootstrap\BootstrapAsset',
]);
$this->title = 'Solicitud de Crédito';


$banks = Clients::findOne(Yii::$app->user->identity->id)
				->getBankAccounts()->One();


//separo el numero de dias del id
function Myperiod($data){
	$Myperiod = explode("-", $data);
	return $Myperiod[1];
}

?>

<div class="row">
<div class="col-xs-12" style="height:50px;"></div>
	<div class="col-lg-6">
		<div class="panel panel-newgreen">
		  <div class="panel-heading">
		    <h3 class="panel-title">Datos del Apoyo Financiero</h3>
		  </div>
		  <div class="panel-body">
		    <?php
		    $periodalias =Period::findOne(['n_days'=>Myperiod($data['Presolicitud'][0]['period'])])->alias;
		    $quotaalias =Quota::findOne(['days'=>$data['Presolicitud'][0]['quota']])->alias;
		    echo DetailView::widget([
		    'model' => $data['Presolicitud'][0],
		    'attributes' => [


		        [                      
		        	'label' => 'Monto Solicitado',
		        	'value' => $data['Presolicitud'][0]['amount'].' Bs',
		        ],
		        [                      
		        	'label' => 'Plazo',
		        	'value' => $periodalias,
		        ],
		        [                      
		        	'label' => 'Cuota',
		        	'value' => $quotaalias,
		        ],
		        
		        [
		        	'label' => 'Cuenta a creditar',
		        	'value'=>$banks->code_bank.'-'.$banks->number_account,
		        ],

		    ],
		    'options'=>['class' => 'table']

			]);
		    ?>
		  </div>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="panel panel-newyellow">
		  <div class="panel-heading">
		    <h3 class="panel-title">Detalle de los pagos</h3>
		  </div>
		  <div class="panel-body">
			<table class="table">
		      <thead>
		        <tr>
		          <th># Cuota</th>
		          <th>Fecha de Pago</th>
		          <th>Fecha de Vencimiento</th>
		          <th>Cuota</th>
		        </tr>
		      </thead>
		      <tbody>
			      <?PHP
					$tablaPago = Loan::TablaPago($data['Presolicitud'][0]['quota'],
													Myperiod($data['Presolicitud'][0]['period']),
													$data['Presolicitud'][0]['rate'],
													$data['Presolicitud'][0]['amount']);

					foreach ($tablaPago as $key => $value) {
						echo"
				      	<tr>
				          <th scope='row'>".$value['n_quota']." </th>
				          <td>".$value['date_pay']."</td>
				          <td>".$value['date_expired']."</td>
				          <td>".$value['quota']."</td>
				        </tr>";
					}
			      	
			      ?>
		      </tbody>
			</table>
			</div>
		</div>
	</div>
</div>





<?PHP

$form = ActiveForm::begin(['validateOnSubmit' => false,
  'layout' => 'horizontal']);

echo $form->field($model, 'term')->hiddenInput()->label(false);

echo Html::beginTag('div', ['class' => 'col-lg-12 text-center']);
	echo Html::submitButton('Anterior', [
		'class' => 'btn btn-previous ', 
		'name' => 'prev', 
		'value' => 'prev'
	]);
	echo Html::submitButton('Finalizar', [
		'class' => 'btn btn-next margenbtn', 
		'name' => 'next', 
		'value' => 'next'
	]);
echo Html::endTag('div');

ActiveForm::end();


?>
