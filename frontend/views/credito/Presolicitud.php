<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use beastbytes\wizard\WizardMenuP;
use frontend\models\BankHoliday;
use yii\web\View;


$this->registerCssFile(Yii::$app->request->baseUrl.'/plugin/ion.rangeSlider/css/ion.rangeSlider.css', [
    'depends' => 'yii\bootstrap\BootstrapAsset',
]);
$this->registerCssFile(Yii::$app->request->baseUrl.'/plugin/ion.rangeSlider/css/ion.rangeSlider.skinHTML5.css', [
    'depends' => 'yii\bootstrap\BootstrapAsset',
]);
$this->registerCssFile(Yii::$app->request->baseUrl.'/css/calendario.css', [
    'depends' => 'yii\bootstrap\BootstrapAsset',
]);

$this->registerCssFile(Yii::$app->request->baseUrl.'/plugin/datepicker/css/datepicker.css', [
    'depends' => 'yii\bootstrap\BootstrapAsset',
]);

$this->registerCssFile(Yii::$app->request->baseUrl.'/css/wizzard.css', [
    'depends' => 'yii\bootstrap\BootstrapAsset',
]);

$this->registerCssFile(Yii::$app->request->baseUrl.'/css/registro.css', [
    'depends' => 'yii\bootstrap\BootstrapAsset',
]);




$this->registerJsFile(Yii::$app->request->baseUrl.'/plugin/ion.rangeSlider/js/ion.rangeSlider.min.js',[
	'depends' => [\yii\web\JqueryAsset::className()]
]); 
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/registro.js',[
	'depends' => [\yii\web\JqueryAsset::className()]
]); 


$this->registerJsFile(Yii::$app->request->baseUrl.'/plugin/datepicker/js/datepicker.js',[
	'depends' => [\yii\web\JqueryAsset::className()]
]); 

$this->registerJsFile(Yii::$app->request->baseUrl.'/js/LoanDetails.js',[
	'depends' => [\yii\web\JqueryAsset::className()]
]); 

$this->registerJsFile(Yii::$app->request->baseUrl.'/js/jquery.formatCurrency-1.4.0.js',[
	'depends' => [\yii\web\JqueryAsset::className()]
]); 

$this->registerJs('var Holidays = '.BankHoliday::getHolidays(date("Y")),View::POS_BEGIN);
$this->title = 'Solicitud de Crédito';

//echo WizardMenu::widget(['step' => $event->step, 'wizard' => $event->sender]);

//print_r(WizardMenuP::widget(['step' => $event->step, 'wizard' => $event->sender]));
?>

<div class="col-xs-12" style="height:50px;"></div>

<?php
$form = ActiveForm::begin();
echo Html::beginTag('div', ['class' => 'row']);
	echo Html::beginTag('div', ['class' => 'col-lg-6']);

		echo $form->field($model, 'amount')->textInput(['class'=>'change']);

		echo $form->field($model, 'period')->dropDownList([],['class'=>'form-control change']
			);
		echo $form->field($model, 'quota')->dropDownList(
				$model->getQuota(),['class'=>'form-control change']
			);

	
	echo Html::endTag('div');

	echo Html::beginTag('div', ['class' => 'col-lg-6']);
		echo '<div class="panel panel-newblue">
					<div class="panel-heading">
					    <h3 class="panel-title">Detalle del Crédito</h3>
					</div>
					<div class="panel-body">
						<table class="table detallecredito">
					      <thead>
					        <tr>
					          <th>Monto Solicitado</th>
					          <th>Monto Cuota</th>
					          <th>No. Cuotas</th>
					        </tr>
					      </thead>
					      <tbody id="value">
					      </tbody>
					      <tfoot>
					      </tfoot>
						</table>						
					</div>
				</div>';
		echo '<div class="panel panel-newgreen">
					<div class="panel-heading">
					    <h3 class="panel-title">Fecha de Pago</h3>
					</div>
					<div class="panel-body">
						<br>
						<div id="loandate"></div>
					</div>
				</div>';
	echo Html::endTag('div');


echo Html::endTag('div');
echo Html::beginTag('div', ['class' => 'col-lg-12']);
	echo Html::submitButton('Siguiente', [
		'class' => 'btn btn-next center-block', 
		'name' => 'next', 
		'value' => 'next'
	]);
echo Html::endTag('div');


ActiveForm::end();

$this->registerJs("
	var arr = [".$model->getAmount()."];
	var position = $.inArray('".$model->amount."',arr);
	$('#presolicitud-amount').ionRangeSlider({
	    type: 'single',
	    values: arr,
	    grid: true,
	    from:position,
	});
	loadPeriod();


	$('.change').change(function() {
		$('#value').LoanDetails({
            quota         : $('#presolicitud-quota').val(),
            period        : GetPeriod(),
            rate		  : ".$model->rate.",
            amount        : $('#presolicitud-amount').val(),
            date          : '".date('m/j/Y')."',
            exp 		  : ".\Yii::$app->params['payExp']."
		});  

		$('.currency').formatCurrency({region: 'es-VE' });

	});
	$('#value').LoanDetails({
        quota         : $('#presolicitud-quota').val(),
        period        : GetPeriod(),
        rate		  : ".$model->rate.",
        amount        : $('#presolicitud-amount').val(),
        date          : '".date('m/j/Y')."',
        exp 		  : ".\Yii::$app->params['payExp']."
	});  

	
	$('.currency').formatCurrency({region: 'es-VE' });

	
", View::POS_READY);



?>
