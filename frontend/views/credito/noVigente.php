
<?php
use yii\helpers\Html;


/* @var $this yii\web\View */

$this->title = 'Solicitud | Tupadrino.net';
$this->params['breadcrumbs'][] = ['label' => 'Creditos'];
$this->params['breadcrumbs'][] = 'Vigente';
?>
<div class="app-novigente">
	<div class="callout callout-info">
	    <h3>
	    <?php
	    if($status=='noposee'){
	    echo '<p>Actualmente no posee ninguna solicitud vigente. Le invitamos a
	    	 '.Html::a('solicitar una solicitud',['/credito/solicitud']);
	    }
	   	if($status=='espera'){
	    echo '<p>Actualmente su solicitud se encuentra en proceso de liquidación</p>';
	    }
	    ?>
	    </h3>
	</div>
</div>