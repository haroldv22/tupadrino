<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Historico de solicitudes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions'=>['class' => 'table tablenf table-newgray ','cellspacing'=>"0"],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'id_client',
            'amount:decimal',
            //'rate',
            'idStatus.name:text:Estatus',
            // 'quota',
            // 'period',
            'date_add',

            ['class' => 'yii\grid\ActionColumn','template' => '{view}'],
        ],
    ]); ?>

</div>
