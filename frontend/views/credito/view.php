<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Loan */

$this->title = 'Solicitud #'.$modLoan->id;
$this->params['breadcrumbs'][] = ['label' => 'Historico de Solicitudes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


$formatter = \Yii::$app->formatter;



#Asignacion de Valores totales
$amortization =$modLoan->columTotal($modQuota->models,'amortization');                                      
#calculo de saldo                                   );
$saldo = ($modLoan->amount)-$amortization;
#numero de cuotas pagadas y pendientes
$countq = $modLoan->countQ($modQuota->models);

?>

<div class="app-creditovigente">
    <div class="row">
        <div class="col-lg-8  col-lg-offset-2">
            <table class="table table-bordered">
                <tbody> 
                    <tr> 
                        <td colspan="2"> Nro. Solicitud</td>
                        <td colspan="2" class="bg-newgreen">
                            <?php echo $modLoan->id; ?>
                        </td>
                    </tr>
                    <tr> 
                        <td>Monto Aprobado</td>
                        <td class="bg-newgreen">
                            <?php echo $formatter->asDecimal($modLoan->amount); ?>
                        </td>
                        <td>Saldo a la fecha</td>
                        <td class="bg-newgreen">
                            <?php echo $formatter->asDecimal($saldo); ?>
                        </td>
                    </tr>
                    <tr> 
                        <td>Monto Pagado</td>
                        <td class="bg-newgreen">
                            <?php echo $formatter->asDecimal($amortization); ?>
                        </td>
                        <!--<td>Tasa Aplicada</td>
                        <td class="bg-newgreen"><?php echo $modLoan->rate; ?>%</td> -->
                    </tr> 
                    <tr> 
                        <td>Cuotas Pendientes</td>
                        <td class="bg-newgreen">
                            <?php echo $countq['pending']; ?>
                        </td>
                        <td>Cuotas Pagadas</td>
                        <td class="bg-newgreen">
                            <?php echo $countq['pay']; ?>
                        </td>
                    </tr> 
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">

            <?= GridView::widget([
                'dataProvider' => $modQuota,
                'summary'=>"",
                'showFooter'=>TRUE,
                'footerRowOptions'=>['class'=>'bg-newgreen','style'=>'font-weight:bold;'],
                'columns' => [

                    [
                        'attribute' => 'n_quota',
                        'footer'=>'Totales',
                        'footerOptions'=>['colspan'=>'3','class'=>'text-center'],
                    ],
                    [
                        'attribute' => 'date_pay',
                        'footerOptions'=>['class'=>"hidden"],
                      
                    ],
                    [
                        'attribute' => 'date_expired',
                        'footer'=>'',
                        'footerOptions'=>['class'=>"hidden"],
                    ],/*
                    [
                        'attribute' => 'amortization',
                        'footer'=>$formatter->asDecimal($amortization),
                        'format'=>'decimal',
                    ],
                    [
                        'attribute' => 'interest',
                        'footer'=>$formatter->asDecimal(
                                                $modLoan->columTotal(
                                                                $modQuota->models,
                                                                'interest'
                                                                )
                                                        ),
                        'format'=>'decimal',

                    ],*/
                    [
                        'attribute' => 'quota',
                        'footer'=>$formatter->asDecimal(
                                                $modLoan->columTotal(
                                                                $modQuota->models,
                                                                'quota'
                                                                )
                                                        ),
                        'format'=>'decimal',
                    ],

                    'idStatus.name:text:Estatus',
                    [
                        'attribute'=>'arrears.total_interest',
                        'label' => 'Mora',
                        'footer'=>$formatter->asDecimal(
                                                $modLoan->columTotal(
                                                                $modQuota->models,
                                                                'arrears.total_interest'
                                                                )
                                                        ),
                        'format'=>'decimal',

                    ]

                    

                    // ...
                ],
            ]) ?>
        </div>
    </div>
</div>