<?php
use yii\helpers\Html;

$this->context->layout = 'main';
$this->title = 'Nueva solicitud';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="app-novigente">
	<div class="callout callout-info">
		<?= Html::tag('h3', strtr('Actualmente posee una solicitud activa.', [])); ?>
	</div>
</div>