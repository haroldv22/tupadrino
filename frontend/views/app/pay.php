<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->params['breadcrumbs'][] = ['label' => 'Registro de Pago'];

$form = ActiveForm::begin(['validateOnSubmit' => false,
  'layout' => 'horizontal']);
?>


    <div class="btn-group">
        <?= Html::submitButton('Registrar', ['class' => 'btn btn-success']) ?>
    </div>

<?php
ActiveForm::end();

?>