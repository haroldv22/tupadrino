
<?php
use yii\helpers\Html;
use yii\helpers\Url;
use  yii\widgets\LinkPager;
use frontend\models\Loan;
use frontend\models\LoanQuota;
use frontend\models\Clients;
use frontend\models\CommissionRound;


$this->registerCssFile(Yii::$app->request->baseUrl.'/plugin/morris/morris.css', [
    'depends' => 'yii\bootstrap\BootstrapAsset',
]);
$this->registerCssFile(Yii::$app->request->baseUrl.'/css/index.css', [
    'depends' => 'yii\bootstrap\BootstrapAsset',
]);
$this->registerJsFile(Yii::$app->request->baseUrl.'/plugin/raphael-min.js',[
    'depends' => [\yii\web\JqueryAsset::className()]
]); 

$this->registerJsFile(Yii::$app->request->baseUrl.'/plugin/morris/morris.js',[
    'depends' => [\yii\web\JqueryAsset::className()]
]); 
$this->registerJs('var comisionGrafic = '.$comisionGrafic, \yii\web\View::POS_BEGIN);


$this->registerJsFile(Yii::$app->request->baseUrl.'/js/index.js',[
    'depends' => [\yii\web\JqueryAsset::className()]
]); 


$formatter = \Yii::$app->formatter;

$this->title = 'Panel de Control | Tupadrino.net';
$this->params['breadcrumbs'][]='';
$userid = Yii::$app->user->identity->id;

$endloan = Loan::find()->where(['id_client'=>$userid,'id_status'=>20])->count();//solicitudes

$quota = Loan::find()->where(['id_client'=>$userid, 'id_status'=>[41,43]]);
if($quota->exists()){
  $quota = LoanQuota::find()->where(['id_status'=>[33,34], 'id_loan'=>$quota->one()->id])
                             ->count();
}
else{
  $quota = 0;
}

$clients = Clients::find()->where(['id_godfather'=>$userid])->count();///Ahijados
$comisiones = CommissionRound::find()->where(['id_client'=>$userid, 'id_status'=>60])->count();//comisiones
//$end

$activeNoti ='icon-noti-active parpadeo';

?>
<style type="text/css">
body{
  background: url(<?=Yii::$app->request->baseUrl?>/img/deco/panel.png);
  background-repeat: no-repeat;
  background-size:20%;
  background-position: 100% 100%;
}
</style>
<div class="site-index">
<!--
    <div class="jumbotron">
        <h1>Bienvenido!</h1>

    </div>
-->
    <div class="body-content">

      <div class="row notify-wrap">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="icon-box">
            <a class="icon" href="<?= Url::toRoute(['/credito/historico']) ?>">
              <div class="icon-img" style="width:109px">
                <div class="icon-noti   <?= ($endloan>0)?$activeNoti:''?>"  style="top:-5px; left:-18px">
                  <?= $endloan ?>
                </div>
                <img height="120" width="109"  src="<?= Yii::$app->request->baseUrl.'/img/iconPC/credito_finalizado.png'?> " />
              </div>
            </a>
            <div class="icon-text">
              <div class="icon-title">Solicitudes</div>
            
              <?= Html::a('Ver Historial
                          <img height="14" width="14"  src="'.Yii::$app->request->baseUrl.'/img/iconPC/flecha.png" />',
                          ['/credito/historico'], 
                          ['class' => 'small-box-footer']) 
              ?>
            </div>

          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="icon-box">
            <a class="icon" href="<?= Url::toRoute(['/godson/index']) ?>">
              <div class="icon-img" style="width:167px">
                <div class="icon-noti <?= ($clients>0)?$activeNoti:''?>" style="top:9px; left:-12px">
                  <?= $clients ?>
                </div>
                <img height="118" width="167"  src="<?= Yii::$app->request->baseUrl.'/img/iconPC/ahijados_asociados.png'?> " />
              </div>
            </a>
            <div class="icon-text">
              <div class="icon-title">Ahijados Asociados</div>
              <?= Html::a('Más informacion
                          <img height="14" width="14"  src="'.Yii::$app->request->baseUrl.'/img/iconPC/flecha.png" />',
                          ['/godson/index'], 
                          ['class' => 'small-box-footer']) 
              ?>
            </div>

          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="icon-box">
            <a class="icon" href="<?= Url::toRoute(['/commission/activa']) ?>">
              <div class="icon-img" style="width:167px">
                <div class="icon-noti <?= ($comisiones>0)?$activeNoti:''?>" style="top:8px; left:-17pxx">
                  <?= $comisiones ?>
                </div>
                <img height="105" width="167"  src="<?= Yii::$app->request->baseUrl.'/img/iconPC/comisiones_pendientes.png'?> " />
              </div>
            </a>
            <div class="icon-text">
              <div class="icon-title">Comisiones</div>
            
              <?= Html::a('Más informacion
                          <img height="14" width="14"  src="'.Yii::$app->request->baseUrl.'/img/iconPC/flecha.png" />',
                          ['/commission/activa'], 
                          ['class' => 'small-box-footer']) 
              ?>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="icon-box">
            <a class="icon" href="<?= Url::toRoute(['/record-payment/create']) ?>">
              <div class="icon-img" style="width:167px">
                <div class="icon-noti <?= ($quota>0)?$activeNoti:''?>" style="top:-4px; left:3px">
                  <?= $quota ?>
                </div>
                <img height="120" width="167"  src="<?= Yii::$app->request->baseUrl.'/img/iconPC/cuotas_vencidas.png'?> " />
              </div>
            </a>
            <div class="icon-text">
              <div class="icon-title">Compromisos Vencidos</div>
              <?= Html::a('Registrar pago 
                          <img height="14" width="14"  src="'.Yii::$app->request->baseUrl.'/img/iconPC/flecha.png" />',
                          ['/record-payment/create'], 
                          ['class' => 'small-box-footer']) 
              ?>
            </div>
          </div>
        </div>
        <!-- ./col -->
      </div>

        <div class="row">
            <div class="col-lg-6">
              <div class="box box-newgreen">
                <div class="box-header">
                  <img  src="<?= Yii::$app->request->baseUrl.'/img/check.png' ?>" />
                  <h3 class="box-title">Comisiones Activas</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding index-comision">
                <?php
                  if(empty($models)){
                    echo '<div class="col-lg-9">
                          <div class="alert " role="alert">
                            <strong>Opps!</strong> Actualmente no posee comisiones activas
                          </div>
                          </div>';
                  }
                  $count = 0;
                  echo '<div class="alltablas">';
                  foreach ($models as $model) {
                    $html=  '<table class="table table-hover">';

                    if($count == 0)
                      $html=  '<table class="table table-hover active">';

                    $html.=  ' <thead>';
                    $html.=  '   <tr>';
                    $html.=  '     <th>Ahijado</th>';
                    $html.=  '     <th>Finaliza</th>';
                    $html.=  '     <th>Estatus</th>';
                    $html.=  '     <th>Comision</th>';
                    $html.=  '   </tr>';
                    $html.=  ' </thead>';
                    $html.=  ' <tbody>';                
                    echo $html;

                    $commissions = $model->getCommissions()->all();
                    foreach ($commissions as $commission) {
                      $html='<tr>';
                      $html.=' <td>'.$commission->getIdClient()->one()->Shortname().'</td>';
                      $html.=' <td>'.$formatter->asDate($commission->end_date).'</td>';
                      $html.=' <td><span class="label '.$commission->Cssclass().'">'.$commission->getIdStatus()->one()->name.'</span></td>';
                      $html.=' <td class="text-right">'.$commission->Valuehtml().'</td>';
                      $html.='</tr>';
                      echo $html;
                    }
                    $html=  ' </tbody>';
                    $html.='<tfoot>';
                    $html.='  <tr class="" style="font-weight:bold;">';
                    $html.='    <td class="text-right" colspan="3">Total Esperado</td>';
                    $html.='    <td class="text-right">'.$formatter->asDecimal($model->expected_pay).'</td>';
                    $html.='  </tr>';
                    $html.='  <tr class="" style="font-weight:bold;color:red;">';
                    $html.='    <td class="text-right" colspan="3">Total en Riesgo</td>';
                    $html.='    <td class="text-right">'.$formatter->asDecimal($model->risk_pay).'</td>';
                    $html.='  </tr>';
                    $html.='  <tr class="bg-newgreen" style="font-weight:bold;">';
                    $html.='    <td class="text-right" colspan="3"><h4>Total a Pagar</h4></td>';
                    $html.='    <td class="text-right"><h4>'.$formatter->asDecimal($model->to_pay).'</h4></td>';
                    $html.='  </tr>';
                    $html.='</tfoot>';
                    $html.='</table>';
                    echo $html;

                    $count++;
                  }
                  echo '</div>';
                  if($count>1){
                    echo '<div class="botonera text-center">';
                    echo '<button type="button" class="btn btn-default active next">Siguiente >></button>';
                    echo '</div>';
                  }
                ?>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>
            <div class="col-lg-6">
                <!-- solid sales graph -->
                <div class="box box-solid box-neworange">
                <div class="box-header">
                  <img  src="<?= Yii::$app->request->baseUrl.'/img/chart.png' ?>" />
                  <h3 class="box-title">Comisiones Obtenidas</h3>
                </div>
                <div class="box-body border-radius-none">
                  <div class="chart" id="line-chart" style="height: <?=$comisionGrafic=='false' ? '72px' : '256px'?>"></div>
                </div>
                <!-- /.box-body -->

                </div>
              <!-- /.box -->
            </div>
        </div>

    </div>
</div>

