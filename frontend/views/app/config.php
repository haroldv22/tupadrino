<?php
/*
** Config View
*/
use yii\bootstrap\ActiveForm;

$this->title = 'Configuracion | Tupadrino.net';
$this->params['breadcrumbs'][] = ['label' => 'Configuracion'];
$this->params['breadcrumbs'][] = 'General';
$formatter = \Yii::$app->formatter;

$this->registerJsFile(Yii::$app->request->baseUrl.'/plugin/jcrop/js/jquery.Jcrop.min.js',[
    'depends' => [\yii\web\JqueryAsset::className()]
    ]); 
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/config.js',[
    'depends' => [\yii\web\JqueryAsset::className()]
    ]); 
$this->registerCssFile(Yii::$app->request->baseUrl.'/plugin/jcrop/css/jquery.Jcrop.min.css', [
    'depends' => 'yii\bootstrap\BootstrapAsset',
]);
?>
<style type="text/css">
	#img-priview{
		max-width: 100%;
		border-radius: 4px;
	}
	.img-priview-content{
		background: #FFF;
		border-radius: 4px;
		border-bottom: 1px #555 solid;
		border-right: 1px #555 solid;
		padding: 2px;
		text-align: center;
		position: relative;

	}
	#overligth{
		position: absolute;
	    width: 100%;
	    height: 100%;
	    opacity: 0.7;
	    background: #fff;
	    z-index: 700;
	    border-radius: 4px;
	    
	}
	#loading-img{
		color: #000;
	}
	#loading-img{
	    position: absolute;
		top: 36%;
	    right: 41%;
	    z-index: 701;
	}
</style>

<div class="list-group">
	<a class="list-group-item">
	  	<div class="list-config-group-item row">
			<div class="list-config-group-item-title col-lg-3">
				<strong>Nombre</strong>
			</div>
			<div class="list-config-group-item-content">
			  	<div class='col-lg-9'>
			  	 	<?php echo $modClient->firstname.' '.$modClient->lastname; ?>
			  	</div>
			</div>
		</div>
	</a>

	<a class="list-group-item">
	  	<div class="list-config-group-item row">
			<div class="list-config-group-item-title col-lg-3">
				<strong>Cédula de identidad</strong>
			</div>
			<div class="list-config-group-item-content">
			  	<div class='col-lg-9'>
			  	 	<?php echo $modClient->identity; ?>
			  	</div>
			</div>
		</div>
	</a>
	<a href="emailupdate" class="list-group-item call">
	  	<div class="list-config-group-item row">
			<div class="list-config-group-item-title col-lg-3">
				<strong>Correo electrónico</strong>
			</div>
			<div class="list-config-group-item-content">
			  	<div class='col-lg-8'>
			  	 	<?php echo $modClient->email; ?>
			  	</div>
			  	<div class='col-lg-1 edit'>
			  		Editar
			  	</div>
			</div>
		</div>
	</a>
	<a class="list-group-item">
	  	<div class="list-config-group-item row">
			<div class="list-config-group-item-title col-lg-3">
				<strong>Fecha de nacimiento</strong>
			</div>
			<div class="list-config-group-item-content">
			  	<div class='col-lg-9'>
			  	 	<?php echo $modClient->birth_date; ?>
			  	</div>
			</div>
		</div>
	</a>
	<a href="pwdupdate" class="list-group-item call">
	  	<div class="list-config-group-item row">
			<div class="list-config-group-item-title col-lg-3">
				<strong>Contraseña</strong>
			</div>
			<div class="list-config-group-item-content">
			  	<div class='col-lg-8'>
				  	Su contraseña fue cambiada por última vez el 
				  	<?php 
				  		$date = $modClient->getClientsPwd()->One()->date_upd;
				  		echo $formatter->asDate($date);
				  	?>.
			  	</div>
			  	<div class='col-lg-1 edit'>
			  		Editar
			  	</div>
			</div>
			<div class="list-config-group-item-form col-lg-10 hide">
			 /**Contenido**//
			</div>
		</div>
	</a>
	<a href="bankupdate" class="list-group-item call">
	  	<div class="list-config-group-item row">
			<div class="list-config-group-item-title col-lg-3">
				<strong>Numero de Cuenta</strong>
			</div>
			<div class="list-config-group-item-content">
			  	<div class='col-lg-8'>
			  	 	<?php  
			  	 		echo $modBank->code_bank.'-'.$modBank->number_account; 
			  	 	?>
			  	</div>
			  	<div class='col-lg-1 edit'>
			  		Editar
			  	</div>
			</div>
			<div class="list-config-group-item-form col-lg-10 hide">
			</div>
		</div>
	</a>

	<a class="list-group-item">
	  	<div class="list-config-group-item row">
			<div class="list-config-group-item-title col-lg-3">
				<strong>Nivel</strong>
			</div>
			<div class="list-config-group-item-content">
			  	<div class='col-lg-9'>
			  	 	<?php echo $modClient->id_level; ?>
			  	</div>
			</div>
		</div>
	</a>


	<a href="imagenupdate" class="list-group-item call">
	  	<div class="list-config-group-item row">
			<div class="list-config-group-item-title col-lg-3">
				<strong>Imagen de Perfil</strong>
			</div>
			<div class="list-config-group-item-content">
			  	<div class='col-lg-8'>

			  	</div>
			  	<div class='col-lg-1 edit'>
			  		Editar
			  	</div>
			</div>
			<div class="list-config-group-item-form col-lg-10 hide">
			</div>
		</div>
	</a>

	<a href="dirupdate" class="list-group-item call">
	  	<div class="list-config-group-item row">
			<div class="list-config-group-item-title col-lg-3">
				<strong>Dirección y Telefonos de contacto</strong>
			</div>
			<div class="list-config-group-item-content">
			  	<div class='col-lg-8'>
				  	Modificado por última vez el 
				  	<?php 
				  		$date = $modClient->getAddresses()->One()->upd_date;
				  		if(empty($date)){
				  			$date =$modClient->date_add;
				  		}
				  		echo $formatter->asDate($date);
				  	?>.
			  	</div>
			  	<div class='col-lg-1 edit'>
			  		Editar
			  	</div>
			</div>
			<div class="list-config-group-item-form col-lg-10 hide">
			</div>
		</div>
	</a>
</div>



