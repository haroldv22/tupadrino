<?php
/*
**Password Update Config
*/
use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
$formatter = \Yii::$app->formatter;
if($success==false){
?>
<a class="list-group-item active" style="opacity:0;">
	<div class="list-config-group-item row">
		<div class="list-config-group-item-title col-lg-3">
			<strong>Contraseña</strong>
		</div>
		<div class="list-config-group-item-content">
		  	<div class='col-lg-9'>
		  	 	<?php 
			  
					$form = ActiveForm::begin(['layout' => 'horizontal',
												'options'=>[
													'class'=>'submit'
													],
												]);


					echo $form->field($model, 'password')->passwordInput();

					echo $form->field($model, 'repassword')->passwordInput();

					echo Html::submitButton('Actualizar',['class' =>'btn btn-success center-block']);
					  
					ActiveForm::end();
				?>
			</div>
		</div>
	</div>
</a>
<?php 
}
elseif($success==true){
?>
<a href="pwdupdate" class="list-group-item call bg-success" style="opacity:0">
	  	<div class="list-config-group-item row">
			<div class="list-config-group-item-title col-lg-3">
				<strong>Contraseña</strong>
			</div>
			<div class="list-config-group-item-content">
			  	<div class='col-lg-7'>
			  		Su contraseña fue cambiada por última vez el 
				  	<?php 
				  		echo $formatter->asDate($model->date_upd);
				  	?>.
			  	</div>
			  	<div class='col-lg-2 edit'>
			  	 	Cambios Guardados
			  	</div>
			</div>
		</div>
	</a>
<?php } ?>
