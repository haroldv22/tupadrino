<?php
/*
**Email Update Config
*/
use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;

if($success==false){
?>
<a class="list-group-item active" style="opacity:0;">
	<div class="list-config-group-item row">
		<div class="list-config-group-item-title col-lg-3">
			<strong>Correo electrónico</strong>
		</div>
		<div class="list-config-group-item-content">
		  	<div class='col-lg-9'>
		  	 	<?php 
			  
					$form = ActiveForm::begin(['layout' => 'horizontal',
												'options'=>[
													'class'=>'submit'
													],
												]);


					echo $form->field($model, 'email',
							['template' => '
						   			<div class="col-lg-6 col-lg-offset-3">
								        {input}
								       {error}
								    </div>
					   			']);

					echo Html::submitButton('Actualizar',['class' =>'btn btn-success center-block']);
					  
					ActiveForm::end();
				?>
			</div>
		</div>
	</div>
</a>
<?php 
}
elseif($success==true){
?>
<a href="emailupdate" class="list-group-item call bg-success" style="opacity:0">
	  	<div class="list-config-group-item row">
			<div class="list-config-group-item-title col-lg-3">
				<strong>Correo electrónico</strong>
			</div>
			<div class="list-config-group-item-content">
			  	<div class='col-lg-7'>
			  	 	<?php echo $model->email; ?>
			  	</div>
			  	<div class='col-lg-2 edit'>
			  		Cambios Guardados
			  	</div>
			</div>
		</div>
	</a>
<?php } ?>