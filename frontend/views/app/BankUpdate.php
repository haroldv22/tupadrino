<?php
/*
**BankAccount Update Config
*/
use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
$formatter = \Yii::$app->formatter;
if($success==false){
?>
<a class="list-group-item active" style="opacity:0;">
	<div class="list-config-group-item row">
		<div class="list-config-group-item-title col-lg-3">
			<strong>Numero de Cuenta</strong>
		</div>
		<div class="list-config-group-item-content">
		  	<div class='col-lg-9'>
		  	 	<?php 
			  
					$form = ActiveForm::begin(['layout' => 'horizontal',
												'options'=>[
													'class'=>'submit'
													],
												]);


					echo $form->field($model, 'code_bank')->dropDownList($model->getBanks(),
							['prompt'=>'-- Seleccione --',
							'onchange'=>'$( "#ac1" ).val($(this).val());']
					);

					echo $form->field($model, 'number_account', ['template' => '
					   {label}
					   	<div class="row">
					   		<div class="col-lg-2">
					      		<input id="ac1" class="form-control" value="'.$model->code_bank.'" readonly="true" type="text">
					    	</div>
					    	<div class="col-lg-4">
					          	{input}
					          	{error}{hint}
					    	</div>
					   </div>']);


					echo Html::submitButton('Actualizar',['class' =>'btn btn-success center-block']);
					  
					ActiveForm::end();
				?>
			</div>
		</div>
	</div>
</a>
<?php 
}
elseif($success==true){
?>
<a href="bankupdate" class="list-group-item call bg-success" style="opacity:0">
	  	<div class="list-config-group-item row">
			<div class="list-config-group-item-title col-lg-3">
				<strong>Numero de Cuenta</strong>
			</div>
			<div class="list-config-group-item-content">
			  	<div class='col-lg-7'>
			  	 	<?php  
			  	 		echo $model->code_bank.'-'.$model->number_account; 
			  	 	?>
			  	</div>
			  	<div class='col-lg-2 edit'>
			  	 	Cambios Guardados
			  	</div>
			</div>
		</div>
	</a>
<?php } ?>