<?php
/*
**BankAccount Update Config
*/
use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
$formatter = \Yii::$app->formatter;
if($success==false){
?>
<a class="list-group-item active" style="opacity:0;">
	<div class="list-config-group-item row">
		<div class="list-config-group-item-title col-lg-3">
			<strong>Imagen de Perfil</strong>
		</div>
		<div class="list-config-group-item-content">
		  	<div class='col-lg-9'>
		  		<div class="text-center img-priview-content">
		  			<div id="loading-img" class="hide">
		  				<img src="<?=Yii::$app->request->baseUrl.'/img/loading.gif'?>"/>
		  				<p><strong>Cargando imagen.. </strong></p>
		  			</div>
		  			<div id="overligth" class="hide"></div>
		  			<img id="img-priview" src="<?=Yii::$app->request->baseUrl.$actualimg?>" alt="your image" class="center-block"/>
		  		</div>
			  		
				<?php	$form = ActiveForm::begin(['layout' => 'horizontal',
												'options'=>[
													'class'=>'submitImg',
													'enctype' => 'multipart/form-data'
													],
												]); ?>
												
					<input type="hidden" id="x" name="x" />
					<input type="hidden" id="y" name="y" />
					<input type="hidden" id="w" name="w" />
					<input type="hidden" id="h" name="h" />

					<?=	 $form->field($model,'imageFile')
									->fileInput(['class'=>'hidden'])
									->label(false);
					?>
					<p class="text-center">
					<?=	 Html::Button('Subir Imagen',['class'=>'btn btn-danger','id'=>'upload-file']); ?>
					<?= Html::submitButton('Recortar & Actualizar',['class' =>'btn btn-success hide','id'=>'update-btn']); ?>
					</p> 
					<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</a>
<?php 
}
elseif($success==true){
?>
<a href="imagenupdate" class="list-group-item call bg-success" style="opacity:0">
	  	<div class="list-config-group-item row">
			<div class="list-config-group-item-title col-lg-3">
				<strong>Imagen de Perfil</strong>
			</div>
			<div class="list-config-group-item-content">
			  	<div class='col-lg-7'>

			  	</div>
			  	<div class='col-lg-2 edit'>
			  	 	Cambios Guardados
			  	</div>
			</div>
		</div>
	</a>
<?php } ?>