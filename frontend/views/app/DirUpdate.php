<?php
/*
**Email Update Config
*/
use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
$formatter = \Yii::$app->formatter;

if($success==false){
?>
<a class="list-group-item active" style="opacity:0;">
	<div class="list-config-group-item row">
		<div class="list-config-group-item-title col-lg-3">
			<strong>Dirección y Telefonos de contacto</strong>
		</div>
		<div class="list-config-group-item-content">
		  	<div class='col-lg-9'>
		  	 	<?php 
			  
					$form = ActiveForm::begin(['layout' => 'horizontal',
												'options'=>[
													'class'=>'submit'
													],
												]);


				?>	
					<!--Movil-->
					<div class="row">
					  <?= html::activeLabel($model, 'newcellphone',['class'=>'control-label col-sm-3']) ?>
					 <div class="col-sm-6">
					    <div class="col-xs-4">
					      <?= $form
					        ->field($model, 'cellphoneprefix',[
					           'horizontalCssClasses' => [
					              'wrapper'=>'',
					              'offset'=>''
					          ]])
					        ->dropDownList($model->getCellPhonePrefix(),
					          ['prompt'=>'--'])
					        ->label(false);
					      ?>
					    </div>
					    <div class="col-xs-8">
					      <?= $form
					      ->field($model, 'newcellphone',[
					           'horizontalCssClasses' => [
					              'wrapper'=>'',
					              'offset'=>''
					          ]])
					      ->label(false)
					      ->textInput(['maxlength'=>7]) 
					      ?>
					    </div>
					  </div>
					</div>
					<!--*/movil-->
					<!--Local-->
					<div class="row">
					  <?= html::activeLabel($model, 'newphonenumber',['class'=>'control-label col-sm-3']) ?>
					 <div class="col-sm-6">
					    <div class="col-xs-4">
					      <?= $form
					        ->field($model, 'phoneprefix',[
					           'horizontalCssClasses' => [
					              'wrapper'=>'',
					              'offset'=>''
					          ]])
					        ->dropDownList($model->getPhonePrefix(),
					          ['prompt'=>'--'])
					        ->label(false);
					      ?>
					    </div>
					    <div class="col-xs-8">
					      <?= $form
					      ->field($model, 'newphonenumber',[
					           'horizontalCssClasses' => [
					              'wrapper'=>'',
					              'offset'=>''
					          ]])
					      ->label(false)
					      ->textInput(['maxlength'=>7]) 
					      ?>
					    </div>
					  </div>
					</div>
					<!--*/Local-->

					<?php
					echo $form->field($model, 'address')->textInput(['placeholder'=>'Ej. Av. 15A con calle 55 casa #34']);
					echo $form->field($model, 'id_state')->dropDownList(
					      $model->getState(), ['prompt'=>'-- Seleccione --',
					      'onchange'=>'
					                $.post("'.Yii::$app->urlManager->createUrl('registro/list') . '",          
					                {id:$(this).val(),
					                key:"state"
					                }, 
					                function( data ) {
					                     $( "#'.Html::getInputId($model, 'id_municipality').'" ).html( data.municipality );
					                     $( "#'.Html::getInputId($model, 'id_city').'" ).html( data.city );

					                });
					            ']);
					//cargo los select si es un previo
					if(is_numeric($model->id_state)){
					  $municipality=$model->getMunicipality($model->id_state);
					  $city=$model->getCity($model->id_state);
					}else{
					  $municipality=[];
					  $city=[];
					}

					echo $form->field($model, 'id_municipality')->dropDownList($municipality,['prompt'=>'-- Seleccione --',
					  /*'onchange'=>'
					                $.post("'.Yii::$app->urlManager->createUrl('registro/list') . '",          
					                {id:$(this).val(),
					                key:"municipality"
					                }, 
					                function( data ) {
					                     $( "#'.Html::getInputId($model, 'id_parish').'" ).html( data );
					                });'*/
					  ]);
					echo $form->field($model, 'id_city')->dropDownList($city,
					      ['prompt'=>'-- Seleccione --']
					  );
					
					echo Html::submitButton('Actualizar',['class' =>'btn btn-success center-block']);
					  
					ActiveForm::end();
				?>
			</div>
		</div>
	</div>
</a>
<?php 
}
elseif($success==true){
?>
<a href="dirupdate" class="list-group-item call bg-success" style="opacity:0">
	  	<div class="list-config-group-item row">
			<div class="list-config-group-item-title col-lg-3">
				<strong>Dirección y Telefonos de contacto</strong>
			</div>
			<div class="list-config-group-item-content">
			  	<div class='col-lg-7'>
			  	Modificado por última vez el 
			  	 	<?php echo $formatter->asDate($model->upd_date); ?>
			  	</div>
			  	<div class='col-lg-2 edit'>
			  		Cambios Guardados
			  	</div>
			</div>
		</div>
	</a>
<?php } ?>