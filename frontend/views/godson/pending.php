<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Invitaciones del Sistema';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-invitation-index">

    <!--<h1><?//= Html::encode($this->title) ?></h1>-->

    <p>
        <?= Html::a('Invitar', ['create'], ['class' => 'btn btn-arrow']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions'=>['class' => 'table tablenf table-newgray ','cellspacing'=>"0"],

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'email:email',
            [
                'attribute' => 'id_status',
                'value'    => function($model){
                            if($model->id_status)
                                return $model->getIdStatus()                                            
                                             ->one()
                                             ->name;                                                                                                         },
            ],            

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
