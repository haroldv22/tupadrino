<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ClientInvitation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-invitation-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>
    
    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

<!--    <?//= $form->field($model, 'invitation_code')->textInput(['maxlength' => true]) ?>-->
    
<!--    <?//= $form->field($model, 'id_status')->dropDownList($items,   ['prompt'=>'Seleccione']) ?>-->

<!--    <?//= $form->field($model, 'id_godfather')->textInput() ?>-->

    <div class="form-group">
        <?= Html::submitButton('Enviar invitación', ['class' => 'btn btn-arrow center-block']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
