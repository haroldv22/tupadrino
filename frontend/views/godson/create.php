<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ClientInvitation */

$this->title = 'Invitacion';
$this->params['breadcrumbs'][] = ['label' => 'Ahijados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-invitation-create">

    <!--<h1><?//= Html::encode($this->title) ?></h1>-->
    <?php 
    if(Yii::$app->user->identity->allowInvitation()){
	   echo $this->render('_form', [
	        'model' => $model,
	    ]);
	}
	else{
		echo '
		<div class="alert alert-info" role="alert">
            <strong> Actualmente no posee invitaciones disponibles..</strong>
        </div>';
	}
	?>

</div>
