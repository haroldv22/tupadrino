<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\models\Loan;

/* @var $this yii\web\View */
/* @var $model app\models\ClientInvitation */

$this->title = 'Invitado: '.$model->name;
$this->params['breadcrumbs'][] = ['label' => 'Ahijados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$modelLoan = new Loan(); // instancia del modelo Loan

?>
<div class="client-invitation-view">

    <!--<h1><?//= Html::encode($this->title) ?></h1>-->

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Estas seguro que deseas eliminar estos datos ?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'name',
            'email:email',
            'invitation_code',            
            [                      // the owner name of the model
                'label' => 'Estatus',
                'value' => $model->getIdStatus()
                                ->one()
                                ->name                                                                                     
            ],
                  
        ],
    ]) ?>

</div>
