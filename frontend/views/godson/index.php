<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ahijados';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs('jQuery(".godson-box .name-box").popover({html:true,placement:"top"});', \yii\web\View::POS_READY);
?>
<style type="text/css">

.wrap{
    background: url(<?=Yii::$app->request->baseUrl?>/img/deco/ahijados.png);
    background-repeat: no-repeat;
    background-size:30%;
    background-position: 100% 100%;
}

.name-box{
    height:2%;
    border:1px solid #9a9b9f;
    border-radius: 5px;
    margin-top: 6px;
    color: #6e6d72;
    font-size: 120%;
}

.godfather-box img{
    margin: 0 auto;
    padding-right: 13px;
    padding-left: 8px;

}
.godfather-box,.godson-box{
    margin-bottom: 25px;
}
.godson-box img{
    margin: 0 auto;
    padding-left: 8px;
    padding-right: 8px;
}
.godson-status-box-item-push{
    width: 21.5%;
    float: left;
    height: auto;
     padding-top: 15.63%;

}
.godson-status-box {
    margin-top: 5px;
}
.godson-box .name-box{
    cursor: pointer;
}

.godson-status-box-item{
    border:1px solid #9a9b9f;
    border-radius: 50%;
    width: 17%;
    height: auto;
    padding-top: 15.63%;
    float: left;
    margin-left:1%;
    margin-right: 1%;

}
.popover{
    min-width: 250px;
}

</style>
<div class="client-invitation-index">
    <div class="row">
        <div class="center-block col-lg-3 col-sm-4 col-xs-7 godfather-box" style="float:none">
            <?= Html::img('@web/img/'.((Yii::$app->user->identity->gender=='m')?'padrino':'madrina').'.png',['class' => 'img-responsive']) ?>
            <div class="name-box text-center">
            <?= Yii::$app->user->identity->ShortFirstname(); ?>
            </div>
        </div>
    </div>
    <!--<h1><?//= Html::encode($this->title) ?></h1>-->
    <div class="row">
    <?php
   // print_r($dataProvider);
        foreach ($dataProvider->getModels() as $model) {
            //echo $model->ShortFirstname();
            $popContent = "<strong>Nombre:</strong> ".ucwords(strtolower($model->firstname))."<br>";
            $popContent.= "<strong>Apellido:</strong> ".ucwords(strtolower($model->lastname))."<br>";
            $popContent.= "<strong>Correo:</strong> ".$model->email."<br>";
            $popContent.= "<strong>Fecha Inicio:</strong> ".$model->date_add."<br>";
            $status = $model->creditEstatus();
            $popContent.= "<strong>Estado:</strong>".$status['text'];
            $gender= ($model->gender == 'm')?'ahijado':'ahijada';
    ?>


            <div class="col-lg-2 col-sm-3 col-xs-6 godson-box">
                <?= Html::img('@web/img/'.$gender.'.png',['class' => 'img-responsive']) ?>
                <div class="name-box text-center" 
                data-toggle="popover"
                data-trigger="hover"
                data-content="<?= $popContent ?>">
                    <?= $model->ShortFirstname(); ?>
                </div>
                <div class="godson-status-box row">
                    <div class="godson-status-box-item-push">
                    </div>
                    <div class="godson-status-box-item <?= $status['success'] ?>">
                    </div>
                    <div class="godson-status-box-item <?= $status['warning'] ?>">
                    </div>
                    <div class="godson-status-box-item <?= $status['danger'] ?>">
                    </div>
                </div>
            </div>


    <?php
        }
    ?>
    </div>

</div>
