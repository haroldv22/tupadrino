<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Detalle de Comision #'.$id;
if($type=='activa')
    $this->params['breadcrumbs'][] = ['label' => 'Comisiones Activas', 'url' => ['activa']];

if($type=='historico')
    $this->params['breadcrumbs'][] = ['label' => 'Historico de Comisiones', 'url' => ['historico']];

$this->params['breadcrumbs'][] = $this->title;

?>
<div class="commission-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions'=>['class' => 'table tablenf table-newgray ','cellspacing'=>"0"],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute'=>'id_client',
                'value'=>function($model){
                    return $model->getIdClient()->one()->Shortname();
                }
            ],
            //'id_commission_round',
            //'id_loan',
            'commission',
            // 'commission_rate',
            [
                'attribute'=>'id_status',
                'value'=>'idStatus.name'
            ],
            'end_date',

           // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
