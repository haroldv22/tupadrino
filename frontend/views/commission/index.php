<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Historico de Comisiones';
if($type=='activa')
    $this->title = 'Comisiones Activas';

$this->params['breadcrumbs'][] = $this->title;


?>
<div class="commission-round-index">



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions'=>['class' => 'table tablenf table-newgray ','cellspacing'=>"0"],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_client',
            'end_date',
            //'id_status',
            'expected_pay',
            'risk_pay',
            'to_pay',

            ['class' => 'yii\grid\ActionColumn',
             'template' => '{view}',
             'buttons' => [
                'view' => function ($url, $model) {
                    $type = 'historico';
                    if($model->id_status==60)
                            $type= 'activa';
    
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', 
                                     Url::to(['commission/details', 'id' => $model->id, 'type'=>$type]), 
                                     ['title' => Yii::t('app', 'Detalle'),
                                ]);
                        }
                ],
             ],
        ],
    ]); ?>

</div>
