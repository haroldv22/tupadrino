<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = '¿Olvidó su contraseña?';
?>
<style type="text/css">
    body{
        color:#6e6f73;
        background: url(<?=Yii::$app->request->baseUrl?>/img/deco/login.png);
        background-repeat: no-repeat;
        background-size:25%;
        background-position: 100% 100%;
    }
</style>
<div class="site-request-password-reset text-center">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Por favor ingrese su correo electronico de acceso al sistema.<p>

    <div class="row">
        <div class="col-lg-6  col-lg-offset-3 ">
            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

                <?= $form->field($model, 'email')->label(false) ?>

                <div class="form-group">
                    <?= Html::submitButton('Siguiente', ['class' => 'btn btn-arrow']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
