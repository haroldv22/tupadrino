<?php
use yii\helpers\Html;

$this->title = 'Sistema Padrino | Tupadrino.net';
?>

<style type="text/css">
body{
	background: url(<?=Yii::$app->request->baseUrl?>/img/deco/pagos.png);
	background-repeat: no-repeat;
	background-size:40%;
	background-position: 101% 101%;
}
</style>
	
<div class="site-content info-content">
	<h2 class="titulo-seccion green">
		<?=	Html::img('@web/img/site/rapidez.png',['height' =>'45']) ?>
		Rapidez - Confianza
	</h2>
	<div class="wrap-site-content"  style="padding-left:5.5%">
		<p>Ser parte de este sistema te garantizará acceso casi inmediato a apoyo financiero 
		en efectivo depositados en tu cuenta bancaria. Recuerda que este sistema está basado en 
		la confianza, tu comportamiento afectará directamente los beneficios y la reputación de 
		quienes te invitaron a formar parte.
		</p>
		
	</div>

</div>
