<?php
use yii\helpers\Html;
use frontend\models\SystemBankAccount;

$this->title = 'Preguntas Frecuentes | Tupadrino.net';

$this->registerJs('
	jQuery(".item-faq h4").click(function(){
		jQuery(this).siblings(".item-text").slideToggle();
	});
	', \yii\web\View::POS_READY);
?>
<style type="text/css">
.wrap{
	background: url(<?=Yii::$app->request->baseUrl?>/img/deco/faq.png);
	background-repeat: no-repeat;
	background-size:40%;
	background-position: 90% 100%;
}
</style>
	
	<div class="site-content info-content">

		<h2 class="titulo-seccion">
			<?=	Html::img('@web/img/site/faq.png',['height' =>'45']) ?>
			Preguntas Frecuentes
		</h2>

		<div class="wrap-faq">
			<div class="item-faq">
				<h4 style="padding-left: 4.7%;"><?=	Html::img('@web/img/site/faq/solicitud.png',['height' =>'25']) ?> Solicitud <span class="glyphicon glyphicon-triangle-bottom"></span></h4>
				<div class="item-text">
					<h5>¿Quién puede realizar una solicitud en Tupadrino.net?</h5> 
					<p>Todos los que han sido invitados a integrar la familia Tupadrino.net y sus ahijados. Emprendedores que compartan la misma visión de evolución financiera constante y que respeten su compromiso para mantenerse creciendo financieramente.</p>

					<h5>¿Cómo sé si he sido recomendado por un Padrino? </h5>
					<p>Cuando un Padrino te recomiende, Tupadrino.net, te enviará un email con la notificación y el link para formalizar la solicitud.</p>

					<h5>¿Qué necesito para realizar una solicitud? </h5>
					<p>Además de ser referido por un Padrino conocido, necesitas ser mayor de edad, tener una cuenta bancaria donde seas el titular, una dirección de email y un número de teléfono celular.</p>


					<h5>¿Necesito entregar algún tipo de requisito especial?</h5>
					<p>No, venir referido de un Padrino es el requisito de confianza principal que valoramos en Tupadrino.net</p>

					<h5>¿Debo completar todos los datos para registrarme en Tupadrino.net?</h5>
					<p> Los datos son fundamentales para entrar en Tupadrino.net</p>

					<h5>¿Necesito tener una cuenta bancaria o puedo utilizar la de un familiar?</h5>
					<p> Es indispensable que seas el titular de la cuenta bancaria.</p>
				</div>
			</div>
			<div class="item-faq">
				<h4 style="padding-left: 5.1%;"><?=	Html::img('@web/img/site/faq/funcionamiento.png',['height' =>'25']) ?>Funcionamiento<span class="glyphicon glyphicon-triangle-bottom"></span></h4>
				<div class="item-text">
					<h5>¿Cómo funciona Tupadrino.net?</h5>
					<p>Funciona a través de niveles de solicitudes, en los cuales progresarás en la medida que mantengas 
					al día tus pagos, obteniendo el derecho de invitar a otras personas (Ahijados) de tu plena confianza 
					para que se integren a la familia, la cantidad de ahijados que puedes tener es ilimitada.
					La inclusión de dichos Ahijados y el cabal cumplimiento de sus compromisos de pago, significarán
					un aumento en tus ingresos a través del sistema, que se verá reflejado en un aporte monetario a 
					tu favor.</p>

					<h5>¿Cómo subo de nivel?</h5>
					<p>Solo necesitas mantener al día el pago de tus compromisos adquiridos con el sistema. De no 
					ser así perjudicarás a tu Padrino directamente y no podrás aumentar de nivel.</p>

					<h5>¿Cuántas solicitudes puedo hacer al inicio?</h5>
					<p>Al comenzar tienes la oportunidad de solicitar un apoyo financiero, por una cantidad a 
					convenir que debe cancelarse puntualmente. Al finalizar esa solicitud exitosamente y dependiendo de 
					tu rapidez, tienes la oportunidad de solicitar una nueva, por un monto mayor o igual. </p>
 
					<h5>¿Cómo subo mi status?</h5>
					<p>Luego de cancelar tu primera solicitud a tiempo, pasas al siguiente nivel, en el que 
					puedes invitar a tus propios Ahijados, quienes al mantener su compromiso de pago al día, 
					te permitirán percibir un aporte adicional a tu cuenta.</p>
 
 
					<h5>¿Cuántas solicitudes puedo hacer luego que realice la primera?</h5>
					<p>Puedes realizar una nueva solicitud, pero esta vez con posibilidades de aumentar el limite de dinero y 
					el tiempo para cancelarlo.</p>
 
					<h5>En todos los niveles ¿la cantidad de dinero es la misma?</h5>
					<p>No. A medida que subas de nivel, si es de tu elección, podrás aumentar también el monto de la 
					solicitud requerida.</p>
 
					<h5>¿Cómo me indica Tupadrino.net que he cambiado de nivel?</h5>
					<p>El sistema señala el cambio de nivel y además, se te enviará un email para informarte.</p>
 
					<h5>¿Cómo sé si fue aprobada mi solicitud?</h5>
					<p>Una vez que se haya realizado la transferencia, se registra en el sistema y se te envía un email 
					con los datos y puedes disponer de tu solicitud.</p>


					<h5>¿Qué son las comisiones activas?</h5>

					<p>Es el porcentaje que cada Padrino, podría llegar a recibir si el comportamiento de su ahijado es 
					correcto y se mantiene al día. Son las comisiones potenciales que un Padrino puede llegar a tener.</p>

					<h5>¿Qué son las comisiones Obtenidas?</h5>

					<p>Es el porcentaje real obtenido por Padrino, como resultado de su buen comportamiento y el de sus 
					ahijados con Tupadrino.net. Es un monto ya cobrado por el Padrino, o disponible para hacerlo.</p>
				</div>
			</div>
			<div class="item-faq">
				<h4 style="padding-left: 5.3%;"><?=	Html::img('@web/img/site/faq/ahijados.png',['height' =>'25']) ?>Ahijados<span class="glyphicon glyphicon-triangle-bottom"></span></h4>
				<div class="item-text">
					<h5>¿En qué parte de la página encuentro las invitaciones para mis Ahijados?</h5>
					<p>Al culminar exitosamente una solicitud se te abre la opción en la sección de tus Ahijados  
					para que puedas invitar uno nuevo.</p>
					 
					<h5>¿Cómo hago para tener Ahijados?</h5>
					<p>Luego de realizar tu proceso de selección, registras los datos de la persona y le enviamos un 
					email con la invitación.</p>
					 
					<h5>¿Qué beneficio obtengo de tener Ahijados?</h5>
					<p>Ganas un porcentaje de comisión por cada pago completo de tus Ahijados, a mayor cantidad de Ahijados
					 responsables, mayor será tu crecimiento.</p>
					 
					<h5>¿En todos los niveles puedo invitar Ahijados?</h5>
					<p>Sí, puedes invitar nuevos Ahijados por solicitud, puedes tener los Ahijados que te hayas ganado con
					 el cumplimiento cabal de tus compromisos con Tupadrino.net.</p>
					 
					<h5>¿Mi Ahijado puede ser Padrino?</h5>
					<p>Sí, cada Ahijado inicial al culminar exitosamente su solicitud, puede comenzar a invitar a sus 
					propios Ahijados, lo que significará un incremento en el porcentaje de ganancias para el Padrino 
					que genere la invitación.</p>
					 
					<h5>¿Qué beneficio obtengo del cambio de status de mi Ahijado a Padrino?</h5>
					<p>Se incrementará el porcentaje de tus ganancias recibidas. Recuerda que recibes un porcentaje por el 
					pago puntual de tus Ahijados.</p>
					 
					<h5>Cuándo mi Ahijado se convierte en Padrino, deja de ser mi Ahijado?</h5>
					<p>No, aunque él eleve su status a Padrino la línea que los une se mantiene e incluso se fortalece, 
					recuerda que en este sistema estimulamos el crecimiento colectivo basados en la confianza.</p>
					 
					<h5>¿Solo me beneficio una vez del cambio de status de mi Ahijado a Padrino?</h5>
					<p>No, cada vez que tu Ahijado hace una nueva solicitud y es cancelada en el tiempo y cuotas correctas,
					 tú te beneficias como Padrino, por tu Ahijado responsable.</p>
					 
					<h5>¿Qué características debe tener un Ahijado?</h5>
					<p>Debe ser una persona cercana a ti y de tu confianza, que sea responsable, recuerda que sus acciones pueden
					 disminuir tus ingresos.</p>
					<p>Debe ser una persona que desees que sus negocios y sueños prosperen.</p>
					<p>Debe ser una persona, cuyos negocios y sueños tú desees apoyar.</p>
					Te recomendamos que escojas a personas con proyectos de empredimiento ya que ellos utilizaran el dinero 
					para aumentar sus ingresos y de esta forma pagaran el apoyo recibido con mayor facilidad.</p>
				</div>
			</div>
			<div class="item-faq">
				<h4 style="padding-left: 4.8%;"><?=	Html::img('@web/img/site/faq/devolucion.png',['height' =>'25']) ?>Pago y Devoluciones<span class="glyphicon glyphicon-triangle-bottom"></span></h4>
				<div class="item-text">
					<h5>¿Cómo devuelvo el monto de mi solicitud?</h5>
					<br>
					<strong>Para depósitos y transferencias:</strong> 
					<div class="payment-type-content"><br/>
						<?= SystemBankAccount::getHtmlAccounts()?>

					</div>
					<br><strong>Para pagos con Tarjeta de Crédito:</strong>
					<br>Directamente en <?= Html::a('tupadrino.net', '../record-payment/create') ?>

					<h5>¿Cómo sé el número de cuotas que debo pagar?</h5>
					<p>Lo decides tú, tienes un periodo de 3 meses.</p>
					<p>Una vez decidas el intervalo de tiempo entre cada cuota, el sistema te ofrecerá el número de ellas.</p>
					 
					<h5>¿Puedo pagar mi solicitud al poco tiempo de recibirlo?</h5>
					<p>Si, todas las solicitudes pueden ser pagadas totalmente en el momento que lo desees.</p>
					 
					<h5>¿Cómo sé si mi pago se hizo efectivo?</h5>
					<p>Se te enviará un email confirmando el registro del pago realizado.</p>
					 
					<h5>Si me atraso en alguna cuota ¿Cómo será el proceso de información?</h5>
					<p>No olvides que este sistema está basado en la confianza entre el Padrino y sus Ahijados, tu 
					comportamiento puede afectar los beneficios y reputación de quienes te dieron la oportunidad de acceder 
					a nuestro sistema. De retrasarte en tus pagos, los primeros 3 días se te enviarán correos electrónicos 
					informativos.</p>
					 
					<h5>¿Al cabo de cuántos días de vencimiento de la cuota paso a mora?</h5>
					<p>Al tercer día de vencimiento.</p>
					 
					<h5>Si mi transacción no se ha hecho efectiva el día que debo pagar mi cuota ¿Cómo será el proceso?</h5>
					<p>La transacción quedará pendiente los próximos 2 días hábiles. Al pasar ese tiempo, se te enviará un 
					email para informarte.</p>
					 
					<h5>Si mi transacción no se hace efectiva luego de los 2 días hábiles ¿Cómo será el proceso?</h5>
					<p>Se te enviará un email solicitando nuevamente los datos de tu transferencia para hacerla de manera 
					manual y si hay algún tipo de dificultad, te contactamos.</p>
					 
					<h5>¿Cómo sé si mi ahijado no ha pagado?</h5>
					<p>Al segundo día que venza la cuota de tu ahijado se te mandará un email informándote. Si al tercer día, 
					tu Ahijado aún no ha pagado, se te enviará otro email. De igual forma en la sección de ahijados 
					tendrás información actualizada del status de tus ahijados.  </p>
					 
					<h5>¿Qué riesgo tengo si mi Ahijado no cancela la solicitud?</h5>
					<p>Por ser un sistema basado en la confianza entre el Padrino y sus Ahijados, el comportamiento de tus 
					Ahijados puede afectar tus beneficios y reputación, por eso es importante mantener constancia en el 
					pago de los compromisos financieros adquiridos. Si alguno de tus ahijados no cancela, queda fuera 
					automáticamente de Tupadrino.net y la persona que lo apadrino, como penalización, pierde el Ahijado y la 
					posibilidad de realizar una nueva invitación. Ahora le toca a ese Padrino penalizado mantener un buen 
					comportamiento para a futuro tener un nuevo Ahijado.</p>
				</div>
			</div>
			<div class="item-faq">
				<h4 style="padding-left: 4.7%;"><?=	Html::img('@web/img/site/faq/datos.png',['height' =>'27']) ?>Datos<span class="glyphicon glyphicon-triangle-bottom"></span></h4>
				<div class="item-text">
					<h5>¿Siempre debo trabajar con el mismo banco que suministré al registrarme en Tupadrino.net?</h5>
					<p>Puedes cambiar la información del banco si así lo deseas. Solo debes ser titular de la cuenta bancaria
					 con la que quieres trabajar.</p>
					 
					<h5>¿Cómo hago si se me olvidó  la contraseña?</h5>
					<p>En caso de olvido de la contraseña, el sistema te pedirá la información de tu pregunta de seguridad.</p>
					 
					<h5>¿Cómo hago si deseo cambiar alguno de mis datos personales?</h5>
					<p>Solo debes volver a ingresar en tus datos de contacto y modificarlo.</p>
				</div>
			</div>

		</div>

	</div>

</div>
