<?php
use yii\helpers\Html;

$this->title = 'Sistema Padrino | Tupadrino.net';
?>

<style type="text/css">
.wrap{
	background: url(<?=Yii::$app->request->baseUrl?>/img/deco/sistema.png);
	background-repeat: no-repeat;
	background-size:40%;
	background-position: 85% 101%;
}
</style>
	
<div class="site-content info-content">
	<h2 class="titulo-seccion">
		<?=	Html::img('@web/img/site/padrino.png',['height' =>'45']) ?>
		Cultura Padrino
	</h2>
	<div class="wrap-site-content" style="padding-left:5%">
		<p>Estimado Ahijado,</p>
		<p><strong>TUPADRINO.NET</strong>, nace para darte ese apoyo financiero que no puedes alcanzar en la estructura rígida y 
		fría de las instituciones financieras tradicionales, cargadas de requisitos, garantías, papeleo, que 
		van excluyéndote y desanimándote cada vez más a solicitar ese apoyo que tanto necesitas para crecer.
		 Con esta iniciativa llamada<strong> TUPADRINO.NET</strong>, queremos fortalecer los principios de confianza, familia, 
		 amistad y por eso creemos en ti y apostamos a tu crecimiento.<p>
		<p>Ahora tienes un Padrino, que cree en ti y te apoya incondicionalmente porque sabe que tienes mucho 
		potencial por demostrar y que no lo defraudarás, quiere verte crecer y convertirte también en un
		 <strong>PADRINO</strong> para alguien que lo necesita. Juntos formaremos una gran familia de Padrinos y Ahijados 
		 responsables demostrando que los valores de confianza, fraternidad y amistad son lazos fuertes que se consolidan con el tiempo.</p>
		<p>Hoy como <strong>TUPADRINO</strong>, hacemos el esfuerzo para que salgas adelante sabiendo que cumpliendo con tus compromisos devolverás el favor
		 para que otros en un futuro se beneficien al igual que Tú.</p>
		 <p>A partir de hoy siéntete afortunado de tener a alguien que confía en ti, recuerda lo difícil que te había sido conseguir ese apoyo sin antes 
		 tener que dar avales, garantías y constancias. No desaproveches esta oportunidad ni te arriesgues a quedarte por fuera.</p>
		 <br>
		 <br>
	</div>

</div>
