<?php
use yii\helpers\Html;
use yii\jui\JuiAsset;
use yii\helpers\Url;
JuiAsset::register($this);


$this->title = 'Sistema Padrino | Tupadrino.net';


$this->registerJs('
	function changePadrino(url,text){
		if( $("#simulation-name h3").html()!=text){
        		$("#simulation-img img").prop("src",url);
        		$("#simulation-name h3").html(text);
    	}
    	/*
    	var percent = $( "#simulation-bar .base" ).slider( "value");
    	if(percent >=96){
    		percent = 100;
    	}
    	$("#simulation-percent-content span").html(percent);
    	*/
	}

	function changeValues(data,nson,html){
		$("#simulation-percent-content span").html(data);
		 var newhtml ="";
		for (var i = 0; i < nson; i++) {
			newhtml+=html;
		};
		$("#simulation-godson").html(newhtml);

	}
	function refreshPadrino(){
		var value =  $( "#simulation-bar .base" ).slider( "value");
		switch (true) {
			case value >= 0 && value <=19:
				changePadrino("'.Url::to('@web/img/site/padrinos/level-1.png').'","Ahijado Inicial");
				changeValues("0 Bs.","0",html);
				break;
			
			case value >= 20 && value <=39:
				changePadrino("'.Url::to('@web/img/site/padrinos/level-2.png').'","Padrino Junior");
				changeValues("5.200 Bs.","3",html);
				break;

			case value >= 40 && value <=59:
				changePadrino("'.Url::to('@web/img/site/padrinos/level-3.png').'","Padrino Senior");
				changeValues("10.800 Bs.","6",html)
				break;
			
			case value >= 60 && value <=79:
				changePadrino("'.Url::to('@web/img/site/padrinos/level-4.png').'","Padrino Gold");
				changeValues("15.600 Bs.","9",html)
				break;

			case value >= 80 && value <=95:
				changePadrino("'.Url::to('@web/img/site/padrinos/level-5.png').'","Padrino Premium");
				changeValues("20.800 Bs.","12",html)
				break;

			case value >= 96:
				changePadrino("'.Url::to('@web/img/site/padrinos/level-6.png').'","Master Padrino");
				changeValues("26.000 Bs.","15",html)
				break;
		}
	}

	$( "#simulation-bar .base").slider({
      orientation: "horizontal",
      range: "min",
      max: 100,
      value: 0,
      slide: refreshPadrino,
      change: refreshPadrino
    });
	$("#simulation-bar .first-lvl").click(function(){
	 	 $( "#simulation-bar .base" ).slider( "value", 0);
	 });
	$("#simulation-bar .last-lvl").click(function(){
	 	 $( "#simulation-bar .base" ).slider( "value", 100);
	 });
	$("#simulation-bar .lvl-2").click(function(){
	 	 $( "#simulation-bar .base" ).slider( "value", 22);
	 });
	$("#simulation-bar .lvl-3").click(function(){
	 	 $( "#simulation-bar .base" ).slider( "value", 42);
	 });
	$("#simulation-bar .lvl-4").click(function(){
	 	 $( "#simulation-bar .base" ).slider( "value", 62);
	 });
	$("#simulation-bar .lvl-5").click(function(){
	 	 $( "#simulation-bar .base" ).slider( "value", 82);
	 });

	
	', \yii\web\View::POS_READY);
?>
<div class="site-content info-content">
	<h2 class="titulo-seccion green">
		<?=	Html::img('@web/img/site/oportunidades.png',['height' =>'45']) ?>
		Oportunidades
	</h2>
	<div class="wrap-site-content"  style="padding-left:4.6%">

		<p>
		Este sistema ha sido diseñado para que tu evolución financiera sea constante y efectiva. Obtienes 
		una ganancia por cada pago completo de tus Ahijados y a mayor cantidad de Ahijados responsables, mayor 
		será tu crecimiento. Por eso es muy importante que escojas bien a quienes serán tus ahijados.
		</p>
		<br>
		<p>
		Aquí vemos un ejemplo de como sería tu evolución cada vez que consigas sumar 3 ahijados, y como aumentan 
		tus ingresos por bonificaciones en un trimestre a medida que creces con nosotros. 
		(Tomando en cuenta que todos los ahijados piden el máximo monto del NIVEL I)*
		</p>
		<br>
		<div class="row">
			<div class="col-lg-12">
				<div id="simulation-godson" class="row">
					
				</div>
			</div>
			<div class="col-lg-12">
				<div id="simulation-bar">

	                    <div class="base"></div>
	                    <div class="first-lvl"></div>
	                    <div class="lvls lvl-2"></div>
	                    <div class="lvls lvl-3"></div>
	                    <div class="lvls lvl-4"></div>
	                    <div class="lvls lvl-5"></div>
	                    <div class="last-lvl"></div>
	            </div>
	        </div>
			<div class="col-md-3 col-md-push-8  ">
				<div id="simulation-percent-content"  class="text-right">
					<?=	Html::img('@web/img/site/padrinos/sol.png') ?>
					<span>0 Bs.</span>
				</div>
			</div>

	        <div class="col-md-4 col-md-offset-4 col-md-pull-3">   
	            <div id="simulation-img" class="">
	            	<?=	Html::img('@web/img/site/padrinos/level-1.png',['class' =>'']) ?>
	            </div>
	            <div id="simulation-name" class="text-center">
	            	<h3>Ahijado Inicial</h3>
	            </div>
            </div>
		</div>
		<p>
			<strong>* Los montos pueden ser mayores a medida que tus Ahijados se convierten en 
			PADRINOS y acceden a solicitudes con montos mayores</strong>
		</p>
		<br>
		<br>
	</div>

</div>

<script type="text/javascript">
var html = '<div class="col-lg-1 col-xs-3 col-md-1 simulation-godson-item">';
html+='<?=	Html::img('@web/img/ahijado.png',['class'=>'img-responsive']) ?>';
html+="</div>";


</script>
