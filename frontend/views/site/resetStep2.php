<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Responda sus preguntas de seguridad';
?>
<style type="text/css">
    body{
        color:#6e6f73;
        background: url(<?=Yii::$app->request->baseUrl?>/img/deco/login.png);
        background-repeat: no-repeat;
        background-size:25%;
        background-position: 100% 100%;
    }
</style>


<div class="site-request-password-reset text-center">
    <h3><?= Html::encode($this->title) ?></h3>


    <div class="row">
        <div class="col-lg-6  col-lg-offset-3 ">
            <?php $form = ActiveForm::begin(['action'=>['site/password-reset-step2'],'id' => 'request-password-reset-form']); ?>
                <br>
                <br>
                <div class="text-left">
                <?php $modQuestion = $model->getQuestions();
                 ?>


                <?= $form->field($model, 'answer1')
                        ->label($modQuestion[0]->question) ?>

                <?= $form->field($model, 'codeans1')
                        ->hiddenInput(['value'=>$modQuestion[0]->id])
                        ->label(false) ?>
                
                <?= $form->field($model, 'answer2')
                        ->label($modQuestion[1]->question) ?>

                <?= $form->field($model, 'codeans2')
                        -> hiddenInput(['value'=>$modQuestion[1]->id])
                        ->label(false) ?>


                </div>
            
                <div class="form-group">
                    <?= Html::submitButton('Enviar', ['class' => 'btn btn-arrow']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
