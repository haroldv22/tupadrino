<?php
use yii\helpers\Html;

$this->title = 'Sistema Padrino | Tupadrino.net';
?>


	
<div class="row sistema-grafico" >
	<div class="col-lg-7 col-lg-offset-3">

		<div class="row sistema-item">
			<div class="col-xs-3 sistema-item-img">
				<?= Html::img('@web/img/ahijado.png', ['width'=>120, 'alt'=>Yii::$app->name, 'class'=>'pull-right img-responsive']) ?>
			</div>
			<div class="col-xs-9 sistema-text-item">
				<h3>Ahijado inicial</h3>
				<a class="sistema-text-label">Primer Solicitud</a>	<br>
				A cancelar en <span class="sistema-strong">3 meses</span>, <br>
				según te convenga<br>
				puntualmente.
			</div>
		</div>
		<div class="row sistema-arrow" >
			<div class="col-xs-3 col-xs-offset-3 text-center ">
				<?= Html::img('@web/img/downarrow.png', ['width'=>30, 'alt'=>Yii::$app->name, ]) ?>
			</div>
		</div>
		<div class="row sistema-item" >
			<div class="col-xs-3 sistema-item-img">
				<?= Html::img('@web/img/padrino.png', ['width'=>120, 'alt'=>Yii::$app->name, 'class'=>'pull-right img-responsive']) ?>
			</div>
			<div class="col-xs-9 sistema-text-item"> 
				<h3>Padrino</h3>
				Al cancelar la primera Solicitud<br>
				exitosamente, cambias de estatus<br>
				y puedes invitar a nuevos <span class="sistema-strong">Ahijados</span>
				<span id="circle-note1">
					<div class="litle-circle"></div>
					<div class="line-guide"></div>
					<div class="big-circle  text-center">
						<span class="sistema-strong2">Aquí comienzas <br> a ganar dinero</span>
					</div>

				</span>
				<br>
				<span class="sistema-strong">por Solicitud</span>.
			</div>
		</div>
		<div class="row sistema-arrow" >
			<div class="col-xs-3 col-xs-offset-3 text-center ">
				<?= Html::img('@web/img/downarrow.png', ['width'=>30, 'alt'=>Yii::$app->name, ]) ?>
			</div>
		</div>

		<div class="row sistema-item">
			<div class="col-xs-9 text-center">
				<div id="circle-note2">
					<div class="big-circle  text-center">
						Por cada Ahijado<br>
						que demuestre <br>
						su confianza, con el pago <br>
						de su solicitud,<br>
						<span class="sistema-strong">cada Padrino recibe un <br>
						abono de incentivo <br>en su cuenta.</span>
					</div>
					<div class="other-shape">
						<div class="line-guide-up"></div>
						<div class="line-guide-down"></div>
						<div class="litle-circle-up"></div>
						<div class="litle-circle-down"></div>

					</div>
				</div>
			</div>
			<div class="col-xs-9  col-xs-offset-3  sistema-text-item"> 
				<a class="sistema-text-label">Segunda Solicitud</a>	<br>
				Puede ser por un monto mayor a considerar<br>
				o por el mismo monto solicitado anteriormente.<br>
				A cancelar en hasta <span class="sistema-strong">3 meses</span>.
			</div>
			
			<div class="col-xs-12">
				<div class="col-xs-2" > 
					<div class="text-center sistema-bluearrow">
					<?= Html::img('@web/img/bluearrow.png', ['width'=>30, 'alt'=>Yii::$app->name, 'class'=>'center-block img-responsive']) ?><br>
					</div>
					<div>
					<?= Html::img('@web/img/ahijado.png', ['width'=>120, 'alt'=>Yii::$app->name, 'class'=>'center-block img-responsive']) ?>
					</div>
				</div>
				<div class="col-xs-2" > 
					<div class="text-center sistema-bluearrow">
					<?= Html::img('@web/img/bluearrow.png', ['width'=>30, 'alt'=>Yii::$app->name, 'class'=>'center-block img-responsive']) ?><br>
					</div>
					<div>
					<?= Html::img('@web/img/ahijado.png', ['width'=>120, 'alt'=>Yii::$app->name, 'class'=>'center-block img-responsive']) ?>
					</div>
				</div>
				<div class="col-xs-2" > 
					<div class="text-center sistema-bluearrow">
					<?= Html::img('@web/img/bluearrow.png', ['width'=>30, 'alt'=>Yii::$app->name, 'class'=>'center-block img-responsive']) ?><br>
					</div>
					<div>
					<?= Html::img('@web/img/ahijado.png', ['width'=>120, 'alt'=>Yii::$app->name, 'class'=>'center-block img-responsive']) ?>
					</div>
				</div>
				<div class="col-xs-2" > 
					<div class="text-center sistema-bluearrow">
					<?= Html::img('@web/img/bluearrow.png', ['width'=>30, 'alt'=>Yii::$app->name, 'class'=>'center-block img-responsive']) ?><br>
					</div>
					<div>
					<?= Html::img('@web/img/ahijado.png', ['width'=>120, 'alt'=>Yii::$app->name, 'class'=>'center-block img-responsive']) ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row sistema-arrow" >
			<div class="col-xs-3 col-xs-offset-3 text-center ">
				<?= Html::img('@web/img/downarrow.png', ['width'=>30, 'alt'=>Yii::$app->name, ]) ?>
			</div>
		</div>
		<div class="row sistema-item">
			<div class="col-xs-9  sistema-text-item text-center"> 
				<h3><?= Html::img('@web/img/cash.png', ['width'=>100, 'alt'=>Yii::$app->name, ]) ?>Aumenta las posibilidades</h3>
				<br><a class="sistema-text-label center-block">Tercera Solicitud</a>
			</div>
			<div class="col-xs-12  sistema-text-item"> 

			Tu crecimiento como padrino depende de tu buena elección en tus ahijados. <br>
			El aumento en tus ganancias depende que todos tus ahijados cumplan sus compromisos tal <br>
			como lo hiciste Tú.
			</div>
		</div>
		<div class="row sistema-item">
			<div id="circle-note3">
				<div class="big-circle  text-center">
						Los ahijados<br> de hoy,<br>
						son los padrinos <br>
						del mañana. <br>
						Sé uno de ellos.<br>
						
				</div>
			</div>
		</div>


	</div>
</div>
