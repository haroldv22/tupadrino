<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'TuPadrino.net';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>
<style type="text/css">
    body{
        color:#6e6f73;
        background: url(<?=Yii::$app->request->baseUrl?>/img/deco/login.png);
        background-repeat: no-repeat;
        background-size:25%;
        background-position: 100% 100%;
    }
</style>

<div class="site-login">


    <div class="row">
        <?php
            foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
                echo '<div class="alert alert-' . $key . '">' . $message . '</div>';
            }
        ?>
        <div class="col-lg-4 col-lg-offset-4 text-center">
            <?= Html::img('@web/img/logo.png', ['width'=>350, 'alt'=>Yii::$app->name, 'class'=>'img-responsive']) ?>
            <br>
            <br>
            <h1 class="titulo-seccion">
                 <?= Html::img('@web/img/hola.png', ['width'=>55, 'alt'=>Yii::$app->name,]) ?>
            ¡Bienvenido!
            </h1>
            <br>
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form
                    ->field($model, 'email',$fieldOptions1 )
                    ->label(false)
                    ->textInput(['placeholder' => $model->getAttributeLabel('email')])
                ?>
                <br>
                <?= $form
                    ->field($model, 'password',$fieldOptions2)
                    ->label(false)
                    ->passwordInput(['placeholder' => $model->getAttributeLabel('password')]) 
                ?>

                <div class='text-right' style="color:#0da0c2;margin:1em 0;">
                    <?= Html::a('¿Olvidó su contraseña?', ['site/password-reset-step1']) ?>
                </div>
                <br>
                <br>
                <div class="form-group">
                    <?= Html::submitButton('Entrar', ['class' => 'btn btn-arrow', 'name' => 'login-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
