<?php
use yii\helpers\Html;
use frontend\models\SystemBankAccount;

$this->title = 'Sistema Padrino | Tupadrino.net';

?>

<style type="text/css">
.wrap{
	background: url(<?=Yii::$app->request->baseUrl?>/img/deco/pagos.png);
	background-repeat: no-repeat;
	background-size:40%;
	background-position: 101% 101%;
}
</style>
	
<div class="site-content info-content">
	<h2 class="titulo-seccion green">
		<?=	Html::img('@web/img/site/cancelaciones.png',['height' =>'45']) ?>
		 Responsabilidad y Compromiso
	</h2>
	<div class="wrap-site-content"  style="padding-left:7%">
		<p> Recuerda mantener al día tus compromisos a principio de cada mes o según lo 
		hayas convenido desde el inicio del proceso. Este sistema está basado en la confianza
		 entre el Padrino y sus Ahijados, tu comportamiento puede afectar los beneficios y 
		 reputación de quienes te dieron la oportunidad de acceder a nuestro sistema. 
		</p>

		<p>
		Para ayudar a fortalecer nuestro vinculo, a continuación te colocamos nuestros datos para 
		el momento de realizar tus pagos por los compromisos adquiridos:
		</p>

		<div class="payment-type">
			<strong>Para depósitos y transferencias:</strong> 
			<div class="payment-type-content">
				<?= SystemBankAccount::getHtmlAccounts()?>

			</div>
		</div>
		<div class="payment-type">
			<strong>Para pagos con Tarjeta de Crédito:</strong>
			<div class="payment-type-content">
				<p>Directamente en <?= Html::a('tupadrino.net', ['record-payment/create']) ?>
			</div>
		</div>
	</div>
</div>
