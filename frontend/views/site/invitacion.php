<?php
use yii\helpers\Html;

$this->title = 'Invitación al Sistema | Tupadrino.net';
?>

<style type="text/css">
body{
	background: url(<?=Yii::$app->request->baseUrl?>/img/deco/invitacion.png);
	background-repeat: no-repeat;
	background-size:50%;
	background-position: 90% 100%;
}
</style>
	
<div class="site-content info-content">
	<h2 class="titulo-seccion">
		<?=	Html::img('@web/img/site/invitacion.png',['height' =>'45']) ?>
		Invitación al Sistema
	</h2>
	<div class="wrap-site-content" style="padding-left:6%">
		<p>Formas parte de una opción efectiva y confiable de crecimiento financiero.</p> 
		<br>
		<p>Tener el respaldo de un buen Padrino te da el empuje y la seguridad para encaminar nuevos proyectos en todo momento. No olvides que el ahijado de hoy, es el padrino de mañana y que ayudando a los demás, también nos ayudamos a nosotros mismos. Creemos en la honestidad, por eso somos el primer sistema basado en la confianza para propiciar el crecimiento financiero.
		</p>
		<br>
		<p>Tendrás el privilegio de invitar a nuevos miembros a formar parte de nuestra familia, para que también disfruten de estos beneficios.
		Valoramos tu capacidad de selección.
		</p>
		<br>
		<p>
			Recuerda que para disfrutar de este privilegio debes estar solvente en tus compromisos con Tupadrino.net
		</p>
	</div>

</div>
