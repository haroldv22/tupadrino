
<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'Inicio | Tupadrino.net';

$this->registerJsFile(Yii::$app->request->baseUrl.'/js/site-index.js',[
    'depends' => [\yii\web\JqueryAsset::className()]
    ]); 


$html='	<div class="alert-overlay">
		<div class="alert-message col-lg-6 col-lg-offset-3">
			<p>
				Estimado(a):<br>
				<strong>'.Yii::$app->user->identity->Shortname().'</strong>
				<br>
			</p>
			<div class="alert-message-body">
				<p>
					Actualmente tienes compromiso(s) pendiente(s) de pago. Si ya realizaste
					el pago, recuerda registrarlo en: MI CUENTA/PAGOS/REGISTRAR
				</p>

				<p>
					Recuerda mantener al día tus compromisos según lo hayas convenido
					desde el inicio del proceso. Este sistema está basado en la confianza
					entre el Padrino y sus Ahijados, tu comportamiento puede afectar los
					beneficios y reputación de quienes te dieron la oportunidad de acceder a
					nuestro sistema
				<p>
			</div>
			<p class="text-center">
		      '.Html::a("Continuar", ["credito/vigente"], ["class" => "btn btn-arrow"]).'
		   </p>
		</div>
	</div>'
?>
<style type="text/css">
	
	.carousel-indicators{
		display: none;
	}
</style>

	<?= $alert?$html:'' ?>

<div class="site-index">

   <div class="body-content">
	   <br>
	   <br>
		<div id="carousel" class="carousel slide" data-ride="carousel">
			  <!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
				<li data-target="#carousel-example-generic" data-slide-to="1"></li>
				<li data-target="#carousel-example-generic" data-slide-to="2"></li>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<video id="video1" controls class="img-responsive center-block">
		    			<source src="<?= Yii::$app->request->baseUrl.'/video/TuPadrino.mp4'?>" type="video/mp4">
		    			Tu navegador no soporta videos HTML5.
		  			</video>
	  			</div>
			    <div class="item">
			    	<?=	Html::img('@web/img/site/banners/banner1.png',['class' => 'img-responsive']) ?>
			    </div>
			    <div class="item">
			    	<?=	Html::img('@web/img/site/banners/banner2.png',['class' => 'img-responsive']) ?>
			    </div>
			    <div class="item">
			    	<?=	Html::img('@web/img/site/banners/banner3.png',['class' => 'img-responsive']) ?>
			    </div>
			</div>
		</div>
		<br>
		<br>

		<div class="row site-index-icon">
			<div class="col-md-2 col-xs-6 icon  col-md-offset-1">
				<a href="<?= Url::to(['site/sistema-grafico'])?>">
					<div class="imagen">
					<?=	Html::img('@web/img/site/sistema.png',['height'=>76]) ?>
					</div>
					<div class="text">Sistema Padrino</div>
				</a>
			</div>
			<div class="col-md-2 col-xs-6 icon">
				<a href="<?= Url::to(['site/oportunidades'])?>">
					<div class="imagen">
					<?=	Html::img('@web/img/site/oportunidades.png',['height'=>76]) ?>
					</div>
					<div class="text">Oportunidades</div>
				</a>
			</div>
			<div class="col-md-2 col-xs-6 icon">
				<a href="<?= Url::to(['site/rapidez-confianza'])?>">
					<div class="imagen">
					<?=	Html::img('@web/img/site/rapidez.png',['height'=>76]) ?>
					</div>
					<div class="text">Rapidez-Confianza</div>
				</a>
			</div>	
			<div class="col-md-2 col-xs-6 icon">
				<a href="<?= Url::to(['site/pago-cancelaciones'])?>">
					<div class="imagen">
					<?=	Html::img('@web/img/site/pago.png',['height'=>76]) ?>
					</div>
					<div class="text">Responsabilidad y compromiso</div>
				</a>
			</div>	
			<div class="col-md-2 col-xs-6 icon">
				<a href="<?= Url::to(['site/faq'])?>">
					<div class="imagen">
					<?=	Html::img('@web/img/site/faq.png',['height'=>60,]) ?>
					</div>
					<div class="text">Preguntas frecuentes</div>
				</a>
			</div>

		</div>
	</div>
</div>
