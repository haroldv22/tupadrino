<?php
use yii\helpers\Html;

$this->title = 'Sistema Padrino | Tupadrino.net';
?>

<style type="text/css">
.wrap{
	background: url(<?=Yii::$app->request->baseUrl?>/img/deco/calendario.png);
	background-repeat: no-repeat;
	background-size:24%;
	background-position: 101% 100%;
}
</style>
	
<div class="site-content info-content2">
	<h2 class="titulo-seccion green">
		<?=	Html::img('@web/img/site/calendarioIco.png',['height' =>'45']) ?>
		Calendario Bancario
	</h2>
	<div class="wrap-site-calendario">
		<?=	Html::img('@web/img/site/calendario.png',['class' => 'img-responsive','style'=>'background:#FFF']) ?>
	</div>
</div>


