<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use console\models\Clients;
use console\models\Loan;
use console\models\BankAccount;
use console\models\GeneratedRequests;

    
/**
 * Cron controller
 */ 
class CronController extends Controller{
    
    /**
     * encargado de generar las solicitudes 
     * de credito diarias hechas por los 
     * clientes del sistema
     */ 
    public function actionExpSolicitudes(){
        
        $raiz           = 'backend/web/';  
        $resources      = 'resources/solicitudes_prestamos';                         //Carpeta Principal donde se almacenara la data
        $mainFolder     = $raiz.$resources;
        $monthsFolder   = $mainFolder.'/'.date("m-Y");                               //Carpeta del Mes
        $dayFileTXT     = $monthsFolder.'/'.date("d-m-Y").'.txt';                    //archivo.txt Diario        
                
        $modelLoan      = new Loan();
        
        $loansDay = $modelLoan->getSolicitudes();                         
        
        // obtiene las solicitudes del dia        
        if (!empty($loansDay)){
            // verifica si ya la carpeta principal esta creada
            if(is_dir($mainFolder)){

                if (!is_dir($monthsFolder))
                    mkdir($monthsFolder,0777);                                                    

                $this->cTxtSolicitudes($loansDay,$dayFileTXT);      // llama a metodo que se encarga de agregar la informacion en el archivo .txt                                    
            }else{                         
                mkdir(substr($mainFolder,0,-22),0777);              // Crea la carpeta resources
                mkdir($mainFolder,0777);                            // Carpeta Principal donde se almacenara la data
                mkdir($monthsFolder,0777);                          // Crea carpeta del mes
                $this->cTxtSolicitudes($loansDay,$dayFileTXT);            // llama a metodo que se encarga de agregar la informacion en el archivo .txt                                          
            }
        }
    }
    
    public function actionExpCobranza(){
        
    }
    
    /**
     * Metodo encargado de generar un archivo .txt
     * con las solicitudes de creditos hechas en el
     * dia.
     * @param object loan
     * @param string file 
     */ 
    private function cTxtSolicitudes($loans,$file){        
        
        $modelGRequest = new GeneratedRequests();
        $fh            = fopen($file,'w');                           
        
        foreach ($loans as $key => $value){
            $clients        = $value->getIdClient()->one();
            $bankAccounts   = $clients->getBankAccounts()->one();
            $name           = $clients->firstname.' '.$clients->lastname;
            $account        = $bankAccounts->code_bank.''.$bankAccounts->number_account;
            $row            = str_pad($account, 20).' '.
                              str_pad($name, 50).' '.
                              str_pad($clients->identity, 10).' '.
                              str_pad('CodigoProveedor', 12).' '.
                              str_pad($value->amount, 15).' '.
                              str_pad($clients->email, 100).' '.
                              "1  I\n";

            fwrite($fh, $row);                
        }
                        
        $modelGRequest->url        = substr($file,11);        
        $modelGRequest->date_add   = date("d-m-Y");
        $modelGRequest->save(false);
    }
 
}