<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;

use yii\helpers\Url;
use yii\helpers\ArrayHelper;

use yii\data\ActiveDataProvider;
use console\models\Clients;
use console\models\Loan;
use console\models\LoanQuota;
use console\models\CommissionRound;
use console\models\Commission;
use console\models\Arrears;
use console\models\ArrearsDetails;
use console\models\BankAccount;
use console\models\GeneratedRequests;
use console\models\ClientInvitation;
use console\models\User;
use console\models\Config;
use console\models\BankHoliday;
use console\models\Address;
use console\models\RegisteredRequests;
use console\models\MessagesSms;

use common\models\MessagesSystem;

use mongosoft\soapclient\Client;
    
/**
 * Cron controller
 */ 
class CronController extends Controller{
    

  
  /**
   * encargado de generar las solicitudes 
   * de credito diarias hechas por los 
   * clientes del sistema
   */ 
  public function actionExpSolicitudes(){
            
    // $raiz           = $_SERVER['DOCUMENT_ROOT'];
    $raiz           = 'backend/web/';  
    $resources      = 'resources/solicitudes_prestamos';                         //Carpeta Principal donde se almacenara la data
    $mainFolder     = $raiz.$resources;
    $monthsFolder   = $mainFolder.'/'.date("m-Y");                               //Carpeta del Mes
    $dayFileTXT     = $monthsFolder.'/'.date("d-m-Y").'.txt';                    //archivo.txt Diario        
            
    $modelLoan      = new Loan();
    
    $loansDay = $modelLoan->getSolicitudes();      
    $sumTotal = $modelLoan->getSum(); 
                    
    
    // obtiene las solicitudes del dia        
    if (!empty($loansDay)){   
               
        // verifica si ya la carpeta principal esta creada
        if(is_dir($mainFolder)){

            if (!is_dir($monthsFolder))
                mkdir($monthsFolder,0777);                                                    
            $this->cTxtSolicitudes($loansDay,$dayFileTXT,$sumTotal);      // llama a metodo que se encarga de agregar la informacion en el archivo .txt                                    
        }else{                         
            mkdir(substr($mainFolder,0,-22),0777);              // Crea la carpeta resources
            mkdir($mainFolder,0777);                            // Carpeta Principal donde se almacenara la data
            mkdir($monthsFolder,0777);                          // Crea carpeta del mes
            $this->cTxtSolicitudes($loansDay,$dayFileTXT,$sumTotal);            // llama a metodo que se encarga de agregar la informacion en el archivo .txt                                          
        }
    }
  }

  /**
   * Expira las invitaciones hechas por el sistema
   * al tener un mes de haber sido enviadas
   */
  public function actionInvitacionesExpiradas(){
              
      $modelClientInv = new ClientInvitation();              
      $arrClients     = array();                
      $clients        = '';
      $status         = '';        
      $modelClientInv = $modelClientInv::find()                         
                                      ->where(['date_exp'  => '2016-02-02',
                                               'id_status' => Yii::$app->params['invi_espera']])
                                      ->all();
      
      if (!empty($modelClientInv)){
          
          foreach($modelClientInv as $key => $value){            
              
              if ($value->count_invitation == Yii::$app->params['limit_sentEmail']){
                  $modelClientInv[$key]->id_status = Yii::$app->params['invi_expirada'];
                  $status = 'Expirada';
              }else{
                  $modelClientInv[$key]->count_invitation = 1 + $value->count_invitation;
                  $status = 'Reenviada';
              }            
              
              $clients .= 'Nombre: <b>'.$value->name.'</b><br />
                           Email: <b>'.$value->email.'</b><br /> 
                           Estatus Invitacion: <b>'.$status.'</b><br />
                           ----------------------------<br />';       
              
              $modelClientInv[$key]->update();
          }                                     
          
          $this->mailInviExp($clients);
          $this->resendMailInvi($modelClientInv);
      }
  }

  /**
   * Permite enviar un mensaje aquellos usuarios
   * a las cuales se les acerca la fecha de pago dos dias 
   * antes de la fecha de pago
   */
  public function actionRecordatorioPago(){
      
    $after_date = date('Y-m-d',strtotime("".date('Y-m-d')." +2 day")); // suma dos dias a la fecha   
        
    $model = LoanQuota::find()
        ->where(['date_pay'=>date('Y-m-d')])
        ->orWhere(['date_pay'=> $after_date])        
        ->orWhere(['date_expired'=> date('Y-m-d')])
        ->all();     

    if(!empty($model)){
      $this->sentRememberPay($model);
    }
    
  }   
  
   /**
    * Se encarga de enviar un email a los administradores con el fin 
    * de que conozcan aquellas invitaciones que ya pasaron hacer expiradas
    * asi como a que usuarios les seran enviadas nuevamente las invitaciones
    *
    * @param array $clients Clientes con invitaciones expiradas | reenviadas
    */
  private function mailInviExp($clients){         
    
        $modelUser = new User();
        $modelUser = $modelUser::find()
                              ->select('email,firstname')
                              ->asArray()
                              ->where(['id_role'=>Yii::$app->params['superUserRole']])
                              ->orWhere(['id_role'=>Yii::$app->params['adminUserRole']])
                              ->all();

        $senTo = ArrayHelper::map($modelUser, 'email','firstname');

        if (!empty($senTo)){
            $sent =   Yii::$app->mailer->compose('layouts/html')
                          ->setFrom('no-reply@tupadrino.net')                    
                          ->setTo($senTo)
                          ->setSubject('Notificacion de invitaciones TuPadrino.Net')                                   
                          ->setHtmlBody(' <!-- Logo -->
                            <center>
                              <img src="http://tupadrino.net/img/logo.png" width="600" height="133" alt=""/>
                            </center>
                          <br />
                          <!--  Saludos-->
                          <center>
                            <span style="color: #999c92; font-size: 16px;"> 
                                Saludos <b>Administradores</b>
                               <br /><br /> La presente es para notificarle
                                que debido a que estos clientes no han accedido<br /> al sistema
                                                              algunos les sera enviada nuevamente la invitación y a otros
                                les sera expirada<br /> la misma debido a que no presentan interés alguno, 
                                <br />aquí los detalles:<br /><br />'.$clients.' <br />
                            </span>
                          </center>
                          <br /><br /><br /><br /><br /><br />
                          <footer style=" background-color: #10a1b8; border-top: 1px solid #ddd; height: 40px; padding-top: 20px;">                                        
                              <span style="float:left; margin-left:10px">&copy; TuPadrino.net 2016</span>
                              <span style="float:right; margin-right:10px">All Copyright Reserved</span>
                          </footer>')
                          ->send();              
        }else 
            return false;
  }
  
  /**
   * Permite enviar un mensaje al usuario para notificarle
   * que sus cuotas han sido generadas
   */
  private function sentQuoteGenerated($email,$client){

    $sent =   Yii::$app->mailer->compose('layouts/html')
                      ->setFrom('no-reply@tupadrino.net')
                      ->setTo($email)
                      ->setSubject('Cuotas Generadas Tupadrino.net')                                                           
                      ->setHtmlBody('
                          <!-- Logo -->
                            <center>
                              <img src="http://tupadrino.net/img/logo.png" width="600" height="133" alt=""/>
                            </center>
                          <br />
                          <!--  Saludos-->
                          <center>
                            <span style="font-size: 18px; color: #009fc3;"> Saludos ! '.$client.'</span>
                          <center>
                          <br /><br /><br />    
                          <!-- Contenido -->
                          <center>
                            <span style="color: #999c92; font-size: 16px;"> 
                              La presente es para notificarle que sus cuotas han sido generadas 
                              <br /><br />A traves del siguiente link <a href="http://tupadrino.net" target="_blank">Haz Click Aqui</a>, podra ingresar en nuestra plataforma <strong>tupadrino.net</strong> y verificar!!<br />  <br />
                          </center>
                          <br /><br /><br /><br /><br /><br />
                          <footer style=" background-color: #10a1b8; border-top: 1px solid #ddd; height: 40px; padding-top: 20px;">                                        
                              <span style="float:left; margin-left:10px">&copy; TuPadrino.net 2016</span>
                              <span style="float:right; margin-right:10px">All Copyright Reserved</span>
                          </footer>')
                    ->send();
  }

  /**
   * Este metodo permite enviar a los usuarios nuevamente 
   * una invitacion para que puedan acceder al sistema y solicitar
   * un credito
   * @param [[model]] $model [[modelo ClientInvitation]]
   */    
  private function resendMailInvi($model){
      
      $modelClient = new Clients();
      
      foreach($model as $key => $value ){
          
           $sent =   Yii::$app->mailer->compose('layouts/html')
                      ->setFrom('no-reply@tupadrino.net')
                      ->setTo($value->email)
                      ->setSubject('Invitacion Tupadrino.net')                                                           
                      ->setHtmlBody('
                        <!-- Logo -->
                          <center>
                            <img src="http://tupadrino.net/img/logo.png" width="600" height="133" alt=""/>
                          </center>
                        <br />
                        <!--  Saludos-->
                        <center>
                          <span style="font-size: 18px; color: #009fc3;"> Saludos ! '.$value->name.'</span>
                        <center>
                        <br /><br /><br />    
                        <!-- Contenido -->
                        <center>
                          <span style="color: #999c92; font-size: 16px;"> 
                            <b>¡Lo lograste!</b> Un buen amigo te recomendó para ser parte de nuestra familia<br /> y comenzar a crecer juntos
                            <br /><br />A traves del siguiente link, podras entrar a tu <strong>tupadrino.net</strong> y solicitar un Microcredito<br />
                            Para comenzar: <span style="font-size: 16px; color: #009fc3;"> <a href="http://tupadrino.net/index.php/registro/registration?token='.$value->invitation_code.'" target="_blank">Haz Click Aqui</a></span></span>
                        </center>
                        <br /><br /><br /><br /><br /><br />
                        <footer style=" background-color: #10a1b8; border-top: 1px solid #ddd; height: 40px; padding-top: 20px;">                                        
                            <span style="float:left; margin-left:10px">&copy; TuPadrino.net 2016</span>
                            <span style="float:right; margin-right:10px">All Copyright Reserved</span>
                        </footer>')
                      ->send();                  

          if($value->id_godfather > 0){                                 
            
            $godFather = $modelClient->getEmailGodFather($value->id_godfather);
            
            $sentPadrino   =   Yii::$app->mailer->compose('layouts/html')
                        ->setFrom('no-reply@tupadrino.net')
                        ->setTo($godFather->email)
                        ->setSubject('Notificacion sobre Ahijado Tupadrino.net')                                   
                        ->setHtmlBody('<!-- Logo -->
                          <center>
                            <img src="http://tupadrino.net/img/logo.png" width="600" height="133" alt=""/>
                          </center>
                        <br />
                        <!--  Saludos-->
                        <center>
                          <span style="font-size: 18px; color: #009fc3;"> Saludos ! '.$godFather->firstname.' '.$godFather->lastname.'</span>
                        <center>
                        <br />   
                        <!-- Contenido -->
                        <center>
                          <span style="color: #999c92; font-size: 16px;"> 
                                    <br /> La presente es para notificarle
                                        que se ha enviado una invitacion nuevamente a su Ahijado<br /><br />
                                        Nombre: <b>'.$value->name.'</b><br />
                                        Email: <b>'.$value->email.'</b><br /> <br /> En vista de no haber respondido a la invitacion, cabe destacar
                                        que esta invitacion solamente <br />sera enviada 2 veces a su ahijado, de no acceder a ella esta invitacion 
                                        quedara cancelada <br />permitiendole registrar nuevamente a otro ahijado.<br /><br />
                                        Gracias por Su Atencion y le deseamos un feliz dia. <br /><br /><br /> Atentamente<br /><br /><b> TuPadrino.net </b></span>
                        </center>
                        <br /><br /><br /><br /><br /><br />
                        <footer style=" background-color: #10a1b8; border-top: 1px solid #ddd; height: 40px; padding-top: 20px;">                                        
                            <span style="float:left; margin-left:10px">&copy; TuPadrino.net 2016</span>
                            <span style="float:right; margin-right:10px">All Copyright Reserved</span>
                        </footer>')
                        ->send();
          

            if ($value->id_status == Yii::$app->params['invi_expirada']){

                $sentPadrino   =   Yii::$app->mailer->compose('layouts/html')
                      ->setFrom('no-reply@tupadrino.net')
                      ->setTo($godFather->email)
                      ->setSubject('Notificacion sobre Ahijado Tupadrino.net')                                   
                      ->setHtmlBody('<!-- Logo -->
                            <center>
                              <img src="http://tupadrino.net/img/logo.png" width="600" height="133" alt=""/>
                            </center>
                          <br />
                          <!--  Saludos-->
                          <center>
                            <span style="font-size: 18px; color: #009fc3;"> Saludos ! '.$value->name.'</span>
                          <center>
                          <br />   
                          <!-- Contenido -->
                          <center>
                            <span style="color: #999c92; font-size: 16px;">                        
                            <br /><br /> La presente es para notificarle
                            que no le seran enviadas mas invitaciones a su Ahijado<br /><br />
                            Nombre: <b>'.$value->name.'</b><br />
                            Email: <b>'.$value->email.'</b><br /> <br /> En vista de no haber respondido a ninguna de las invitaciones, 
                            es importante notificarle<br /> que usted a partir de este momento puede registrar un nuevo ahijado.
                            <br /><br />
                            Gracias por Su Atencion y le deseamos un feliz dia. <br /><br /><br /> Atentamente<br /><br /><b> TuPadrino.net </b></span>
                          </center>
                          <br /><br /><br /><br /><br /><br />
                          <footer style=" background-color: #10a1b8; border-top: 1px solid #ddd; height: 40px; padding-top: 20px;">                                        
                              <span style="float:left; margin-left:10px">&copy; TuPadrino.net 2016</span>
                              <span style="float:right; margin-right:10px">All Copyright Reserved</span>
                          </footer>')
                      ->send();

            }
        }
      }
        
  }

  /**
   * Les notifica a cada usuario sobre su 
   * recordatorio de pago ya sea con dos dias
   * de anticipacion o el dia del pago
   * @param model modelo de LoanQuota
   */
  private function sentRememberPay($model){
    
    $modelSms = new MessagesSms();
    $modelMessagesSystem = new MessagesSystem();

    $after_date = date('Y-m-d',strtotime("".date('Y-m-d')." +2 day")); // suma dos dias a la fecha   

    foreach($model as $key => $value ){
	
	if($value->id_status!=Yii::$app->params['cuota_pagada']){
	
	      switch ($value) {
	        // Dos dias Antes del pago
	        case ($after_date == $value->date_pay):
	          $msg = '<b>¡Hola!</b> te informamos que tu próxima cuota sera generada el <b>'.Yii::$app->formatter->asDate($value->date_pay,'d/MM/Y').'</b>.<br />      
	              Te recomendamos tomar tus previsiones para cancelar puntualmente y no sufrir ninguna penalidad.<br />
	              Confiamos en tu honestidad y en la responsabilidad que adquiriste con tu Padrino y nosotros.';
	
	          $sms = "TUPADRINO.NET INFORMA QUE TU CUOTA Nº ".$value->n_quota." SERA GENERADA Y EL ".Yii::$app->formatter->asDate($value->date_pay,'d/MM/Y')." TOMA TUS PREVISIONES Y CANCELA PUNTUALMENTE. CONFIAMOS EN TI";          
	          break;
	        
	        // Dia de la generacion de la cuota
	        case ($value->date_pay == date("Y-m-d")):        
	          $msg = '<b>¡Hola!</b> Según tus compromisos adquiridos con Tupadrino.net, hoy debe ser cancelada la cuota<br /> correspondiente a tu solicitud. Recuerda mantener tu historial al día con tus pagos,<br /> un retraso puede perjudicar tu crecimiento. ';
	           $sms = "TUPADRINO.NET INFORMA QUE TU CUOTA Nº ".$value->n_quota." HA SIDO GENERADA Y VENCE EL ".Yii::$app->formatter->asDate($value->date_expired,'d/MM/Y')." TOMA TUS PREVISIONES Y CANCELA PUNTUALMENTE. CONFIAMOS EN TI"; 
	          break;
	
        	// Fecha Limite de Pago 
	        case ($value->date_expired == date("Y-m-d")):                  
	          $msg = "<b>¡Hola!</b> Su cuota Nº ".$value->n_quota." debe ser cancelado el día de hoy. Si ya realizó su pago,<br /> por favor registrelo en el portal <a href='http://tupadrino.net/'> Tupadrino.net</a> . ya que un retraso puede perjudicar tu crecimiento.";
	          $sms = "TUPADRINO.NET TE RECUERDA QUE HOY VENCE TU CUOTA. PAGA PUNTUALMENTE Y EVITA SANCIONES. SI YA REALIZASTE EL PAGO RECUERDA REGISTRARLO. GRACIAS"; 
	          break;        
	      
	      }
	}

  
      $client = $value->idLoan->getIdClient()->one()->id;
      $shortNameClient = $value->idLoan->getIdClient()->one()->Shortname();
      $number = $value->idLoan->getIdClient()->one()->getAddresses()->one()->cellphone;
      $subject = 'Notificacion de Pago Tupadrino.net';
      $toEmail = $value->idLoan->getIdClient()->one()->email;

      $userSent = User::find()->one()->id;

      if ($modelSms->saveSms($client,$sms))
        $modelSms->sentSMS($number,$sms);

      if($modelMessagesSystem->saveMsgSys($subject,$msg,$client,$userSent))
        $modelMessagesSystem->sentMessage($shortNameClient,$subject,$msg,$toEmail);
    
    }
  }        


  /**
   * Metodo encargado de generar un archivo .txt
   * con las solicitudes de creditos hechas en el     
   * dia.
   * @param object loan
   * @param string file 
   */ 
  private function cTxtSolicitudes($loans,$file,$total){
      $modelGRequest = new GeneratedRequests();      
      $modelRRequest = new RegisteredRequests();   
      $fecha ='';

      $fecha = $this->diaDisponible(date('Y-m-d'));        

      // Yii::$app->end();

      $fh            = fopen($file,'w');                           
      $countLoans = 0; 


      foreach ($loans as $key => $value){
                      
          $row ='';
      
        //genera el primer codigo del archivo
        if($key==0){
          $row = 'C'.str_pad(sizeof($loans), 5,'0',STR_PAD_LEFT).''.
                     str_pad($total, 13,'0',STR_PAD_LEFT).'000000000000sss00'.PHP_EOL.PHP_EOL;                      
        }
                      
          $clients        = $value->getIdClient()->one();

          $cero = 0; // variable para asignarle los ceros al V0CEDULA o V00CEDULA
          $ced = $clients->identity;
          
          // Elimina la V o E en la referencia ya que se toma 
          // la cedula y el atributo solo acepta numeros
          if(strstr($ced,'V')!=false){
            $refClients = str_replace('V',0, $ced);

            // Se agrega el V0 o V00 depende de la cedula
            while (strlen($ced)<10) {
              $aux = '';
              $aux .= str_replace('V','V'.$cero, $ced);
              $ced = $aux;          
            }
          
          } else {
            $refClients = str_replace('E',0, $ced);
              
            // Se agrega el V0 o V00 depende de la cedula
             while (strlen($ced)<10) {
              $aux = '';
              $aux .= str_replace('E','E'.$cero, $ced);
              $ced = $aux;          
            }
          }

          $bankAccounts   = $clients->getBankAccounts()->one();
          $name           = $clients->firstname.' '.$clients->lastname;
          $account        = $bankAccounts->code_bank.''.$bankAccounts->number_account;
          $row            .= str_pad('D'.Yii::$app->formatter->asDate($fecha,'ddMMY'), 9).''.
                            str_pad(Yii::$app->params['codigoProveedor'], 20).''.
                            str_pad($account, 20).''.                              
                            str_pad($value->amount, 13,'0',STR_PAD_LEFT).'00'.
                            str_pad('Liquidacion de TuPadrino.Net',60).''.
                            str_pad($ced, 10,'9').''.
                            str_pad($name, 80).''.
                            str_pad($clients->email, 100).''.
                            str_pad($refClients, 10,'0',STR_PAD_LEFT).PHP_EOL;
        
          fwrite($fh, $row);         
          $countLoans++;
          //file_put_contents($file, $row);
      }

      $modelGRequest->url           = substr($file,11);        
      $modelGRequest->date_add      = date("d-m-Y");
      $modelRRequest->date_request  = date("d-m-Y");   // solo se guarda la fecha para que esta sea mostrada en la vista     

      
      if($modelGRequest->save(false)){        
	$modelRRequest->save(false);   
        
	$users = User::find()->where(["id_role"=>Yii::$app->params['superUserRole']])->all();
      
        $msg = "Solicitudes del dia:<b> ".$countLoans."</b> <br />Total de Montos: <b>".$total."</b>";

        $this->sentEmail($users,$msg); 
      
      }
  }

 /**
   * [sentEmail envio de emails a los superusuarios]
   * @param  [model] $users [modelo usuarios]
   * @param  [string] $msg   [mensaje]
   * @return [type]        [description]
   */
  private function sentEmail($users,$msg){

      
      foreach ($users as $value) {

        Yii::$app->mailer->compose('layouts/html')
            ->setFrom('no-reply@tupadrino.net')
            ->setTo($value->email)
            ->setSubject('Notificacion de Solicitudes de Pago Tupadrino.net') 
            ->setHtmlBody('
              <!-- Logo -->
                <center>
                  <img src="http://tupadrino.net/img/logo.png" width="600" height="133" alt=""/>
                </center>
              <br />
              <!--  Saludos-->
              <center>
                <span style="font-size: 18px; color: #009fc3;"> Saludos ! '.$value->firstname.' '.$value->lastname.' </span>
              <center>
              <br /><br />    
              <!-- Contenido -->
              <center>
                <span style="color: #999c92; font-size: 16px;"> 
                '.$msg.'
                </span>
                 
              </center>
              <br /><br /><br /><br /><br /><br />
              <footer style=" background-color: #10a1b8; border-top: 1px solid #ddd; height: 40px; padding-top: 20px;">                                        
                  <span style="float:left; margin-left:10px">&copy; TuPadrino.net 2016</span>
                  <span style="float:right; margin-right:10px">All Copyright Reserved</span>
              </footer>')
            ->send(); 

      }


  } 
  

  /**
   * Se encarga de buscar un dia disponible para agregarlo 
   * en la fecha del archivo generado para la liquidacion 
   * y que este no de ningun problema al momento de liquidar
   */
  private function diaDisponible($fecha){

    $modelBankH = new BankHoliday();     

    // Se busca la fecha de ese dia para ver si existe entre los dias festivos
    $diaFestivo = $modelBankH::find()
                          ->where(['holiday'=>$fecha])                              
                          ->exists();

    // verifica que la fecha no sea un dia festivo
    if($diaFestivo){
      $nuevaFecha = date('Y-m-d', strtotime($fecha. ' + 1 days')); // incremento un dia 
      return $this->diaDisponible($nuevaFecha); // Llamo nuevamente a la funcion

    }else if(date("N", strtotime($fecha)) == 6){
    
      $nuevaFecha = date('Y-m-d', strtotime($fecha. ' + 2 days')); // incremento dos dias por ser Sabado
      return $this->diaDisponible($nuevaFecha); // Llamo nuevamente a la funcion                
    
    }else if(date("N", strtotime($fecha)) == 7){
    
      $nuevaFecha = date('Y-m-d', strtotime($fecha. ' + 1 days')); // incremento un dia por ser Domingo
      return $this->diaDisponible($nuevaFecha); // Llamo nuevamente a la funcion                
    
    }else{        
      return $fecha;      
    }
  }


  //Action que Cambia los Status de las Quotas
  public function actionQuotaStatusChange(){
    $this->GeneradoStatus();
    $this->VencidoStatus();
    $this->VencidoconMora();
    $this->MorasDiarias();
  }
  /*-----------------------------
  -------------------------------
  * Metodos Principales De Estatus de Cuota *
  -------------------------------
  -------------------------------*/
    /**
     * Cambia el estatus de las cuotas a generadas
     */     
  public function GeneradoStatus(){
    

     // Obtiene todos los prestamos sean vigentes, vencidos o en mora
     $models = Loan::find()
                  ->where(['id_status'=>[
                                          Yii::$app->params['status_vigente'],
                                          Yii::$app->params['status_vencido'],
                                          Yii::$app->params['status_mora']]
                                        ])
                  ->all();


      // Recorre las cuotas de ese prestamo    
      foreach ($models as $loan) {

        $loanQUpdated = LoanQuota::updateAll(
                                              ['id_status' => Yii::$app->params['cuota_generada']],
                                              'id_status ='.Yii::$app->params["cuota_noGenerada"].'
                                               AND date_pay <= :date AND id_loan = :id_loan',
                                              ['date'=>date('Y-m-j'),'id_loan'=>$loan->id]
                                            );

        // Si es mayor que cero que le envie un mensaje a este cliente
        
        if($loanQUpdated > 0){
          
          $email      = $loan->getIdClient()->one()->email;         // email del cliente
          $clientName = $loan->getIdClient()->one()->Shortname();   // Nombre del cliente           
          
          $this->sentQuoteGenerated($email,$clientName); // envie un mensaje al usuario para 
                                                       // notificarle se han generado sus cuotas
        }       
     }  
  }
  
   /**
    * Cambia el estatus de cuotas a vencido
    */
  public function VencidoStatus(){
      
      // Busca las cuotas generadas con fecha vencida
      $models= LoanQuota::find()
                      ->where(['id_status'=>Yii::$app->params['cuota_generada']])
                      ->andWhere(['<=', 'date_expired', date('Y-m-j')])
                      ->all();

      // Recorre todas las cuotas expiradas para asignarle a su cuota vencida
      foreach ($models as $LoanQuota) {
          
          $LoanQuota->id_status = Yii::$app->params['cuota_vencida'];
          $LoanQuota->update();

          $this->buscarMora($LoanQuota);

      }
  }
    
  /**
  * Busca las cuotas con moras que no cambiaron de vencidas por algun error en el sistema
  * tambien cambia el estatus de las ultima cuota 
  */
  private function VencidoconMora(){
    $Loans = Loan::find()->where(['id_status'=>[
                                          Yii::$app->params['status_vigente'],
                                          Yii::$app->params['status_vencido'],
                                          Yii::$app->params['status_mora']
                                          ]
                                  ])->all();

      foreach ($Loans as $Loan) {
        $LoansQuotas = $Loan->getLoanQuotas()
                                ->where(['id_status'=>Yii::$app->params['cuota_vencida']])
                                ->all();
        foreach ($LoansQuotas as $LoanQuota){
          $n_quota = $LoanQuota->n_quota + 1;          
          //Busco la cuota siguiente
          $NextQuota = LoanQuota::find()->where([
                                        'id_loan'=>$LoanQuota->id_loan,
                                        'n_quota'=>$n_quota
                                      ]);

          $iniDate = null; //doy un valor default
          //Determino la fecha de inicio
          if($NextQuota->exists()){//no es la ultima cuota
            //verifico si la cuota siguiente esta vencida
            if($NextQuota->one()->id_status == Yii::$app->params['cuota_vencida']){
              $iniDate = $NextQuota->one()->date_expired;
            }
          }
          else{//Es la Ultima cuota
            $iniDate= $this->CalcFechaGenMora($LoanQuota); //genero la fecha en la que cae en mora

          }
          if($iniDate!=null){
            if(strtotime(date('Y-m-j'))>=strtotime($iniDate)){

              $this->CalcularGuardarMorasBD(date('Y-m-j'),date('Y-m-j'),$LoanQuota,$LoanQuota->pending_pay);
              $this->CommissionStatus($Loan);
              //Actualizo LoanQuota
              $LoanQuota->id_status= Yii::$app->params['cuota_mora'];
              $LoanQuota->have_arrear = true;
              $LoanQuota->update();

              //actualizo Loan
              if($Loan->id_status != Yii::$app->params['status_mora']){                
                $Loan->id_status = Yii::$app->params['status_mora'];
                $Loan->update();
              }
            }
          }
        }
      }
  }

    /**
     * encargado de buscar Moras Diarias  
     * tambien cambia el estatus
     * de morosida a las que se les ha pagado ya la cuota y no se reflejaron 
     * cuango se hizo la conciliacion. 
     */
  private function MorasDiarias(){
        // Busca las cuotas con en mora
        $models= LoanQuota::find()
                        ->where(['id_status'=>Yii::$app->params['cuota_mora']])
                        ->orderBy('id ASC')
                        ->all();
        foreach ($models as $LoanQuota) {
          //Llamo la tabla arrears(Mora)asociadas con la quota
          $modelArrears = $LoanQuota->getArrears()->one();

          if($modelArrears){///verifico si tiene moras ya creadas para sumarle mas
            if(strtotime($modelArrears->upd_date)< strtotime(date('Y-m-j'))){
              $iniDate =date('Y-m-j', strtotime ('+1 day',strtotime($modelArrears->upd_date)));
              $this->CalcularGuardarMorasBD($iniDate,date('Y-m-j'),$LoanQuota,$LoanQuota->pending_pay);
            }else{//en caso de que tenga las moras al dia, paso al siguiente ciclo
              continue;
            }
          }else{///No tiene moras Asociadas en BD

            #Esta seccion nunca se usara solo en caso que falle
            #La confirmacion de Pagos

            //Llamo todas los pagos asociados a la cuota
            $modelQuotaPayments = $LoanQuota->getQuotaPayments()->orderBy('id ASC')->all();

            $QuotaPendindPay = $LoanQuota->pending_pay; //Guardo temporalmente lo que se debe de la cuota
            $iniDate= $this->CalcFechaGenMora($LoanQuota); //Guardo temporalmente la fecha en que se genero la mora
            //Verifico si tiene pagos hechos
            if($modelQuotaPayments){   
              foreach ($modelQuotaPayments as $modelQuotaPayment) {//Recorro Cada uno de los pagos que tenga asociado
                //Llamo al recordpayment asociado 
                $modelRPayment= $modelQuotaPayment->getIdRecordPayment()->one();
                //verifico si es un pago confirmado
                if($modelRPayment->id_status = Yii::$app->params['confirmado']){

                  $QuotaEndDate =date('Y-m-j', strtotime ('-1 day',strtotime($modelRPayment->date_add)));
                  ///Verifico si lo pagado es mayor o igual a lo correspondiente
                  if($modelQuotaPayment->amount>=$LoanQuota->quota){
                    if(strtotime($modelRPayment->date_add)>strtotime($iniDate)){//Si lafecha de pago esmayor a lafecha vencimiento calcu mora
                      $this->CalcularGuardarMorasBD($iniDate,$QuotaEndDate,$LoanQuota,$QuotaPendindPay);
                    }
                    $QuotaPendindPay = 0; //Establesco 0 porque si es mayor o igual no debe tener pago pendiente
                    $this->ActualizarLoanQuota($LoanQuota,$QuotaPendindPay);
                    continue 2;//Continuo hasta la siguiente interaccion de Quotas (No confundir con lainteraccion de pago)
                  }else{//Si el monto pagado es menor
                    if(strtotime($modelRPayment->date_add)>strtotime($iniDate)){//Si lafecha de pago esmayor a lafecha vencimiento calcu mora
                      $this->CalcularGuardarMorasBD($iniDate,$QuotaEndDate,$LoanQuota,$QuotaPendindPay);
                    }
                    $QuotaPendindPay=$QuotaPendindPay-$modelQuotaPayment->amount;
                    $iniDate = $modelRPayment->date_add;
                    $this->ActualizarLoanQuota($LoanQuota,$QuotaPendindPay);
                  }
                }
              }//fin for each depagos decuota
              
            }
            //En caso de que se escape alguna mora del foreach de Pago de mora
            //Tambien para los que no tienen Pagos asociados
            if($QuotaPendindPay>0){
              $this->CalcularGuardarMorasBD($iniDate,date('Y-m-j'),$LoanQuota,$QuotaPendindPay);
              $this->ActualizarLoanQuota($LoanQuota,$QuotaPendindPay);
             #PD:En esta caso tabla Loan no tiene cambios
            }
          }//Fin Else No moras Asociadas en BD
        }//Fin for each
  }
  /*
  -------------------------------
  * FIN De Medotos principales Estatus de Cuota *
  -------------------------------

    /*--------------------------------
    ----------------------------------
    Metodos segundarios de Estatus de cuota   
    ---------------------------------
    --------------------------*/

  /**
  * Metodo encargado de buscar las 
  * cuotas o prestamos en mora
  * @param $model modelo LoanQuota 
  */
  private function buscarMora($model){

        //Obtengo  el porcentaje de la mora
        $modelConfig = Config::find()
                             ->where(['id_option'=> Yii::$app->params['option_mRate']])
                             ->one();
          
          //resto 1 a n_quota para poder traer la cuota anterior
          $n_quota = $model->n_quota - 1;
          
          if ($n_quota!=0) {
              //Busco la cuota para identificar luego si esta vencidad
              $LoanQuota = LoanQuota::findOne([
                                                'id_status'=>Yii::$app->params['cuota_vencida'],
                                                'id_loan'=>$model->id_loan,
                                                'n_quota'=>$n_quota
                                              ]);
              
              if($LoanQuota){ //verifico si existe  
                  //cambio estatus a lacuota a Mora
                  $LoanQuota->id_status= Yii::$app->params['cuota_mora'];
                  $LoanQuota->have_arrear = true;
                  $LoanQuota->update();
                  //cambio e estatus loan a Mora
                  $Loan = $LoanQuota->getIdLoan()->one();
                  
                  if($Loan->id_status != Yii::$app->params['status_mora']){                
                     $Loan->id_status = Yii::$app->params['status_mora'];
                     $Loan->update();
                  }

                  $this->CalcularGuardarMorasBD(date('Y-m-j'),date('Y-m-j'),$LoanQuota,$LoanQuota->pending_pay);

                  $this->CommissionStatus($Loan);
                  //cambio de estatus comision -> en Mora,
                  // coloco otra comision -> en riesgo

              }
          }else{
              //cambio e estatus loan a vencido
              $Loan = $model->getIdLoan()->one();
              if($Loan->id_status!=43){
                 $Loan->id_status = 42;
                 $Loan->update();
              }
    
              //cambio de estatus comision a en riesgo
              $comission = $Loan->getCommissions();
              if($comission->exists()){
                  $comission = $comission->one();
                  if($comission->id_status!=65){
                      $comission->id_status = 66;
                      $comission->update();
    
                      $round = $comission->getIdCommissionRound()->one();
    
                       $round->to_pay = $round->to_pay - $comission->commission;
                      $round->risk_pay = $rount->risk_pay + $comission->commission;
                      $round->update;
                  }
               }
        }
    return;
  }

  /**
  * Metodo encargado de calcular y guardar las moras generadas
  * en unrango de fecha
  * @param $LoanQuota modelo LoanQuota
  * @param $iniDate fecha que comienza generarse mora
  * @param $pending_pay Pago pendiente con en que serealizaran los calculos de mora(porcentaje*pendientecuota)
  * @param $endDate fecha de finalizacion 
  */
  private function CalcularGuardarMorasBD($iniDate,$endDate,$LoanQuota,$pending_pay){
        //Obtengo  el porcentaje de la mora
        $modelConfig = Config::find()
                             ->where(['id_option'=> Yii::$app->params['option_mRate']])
                             ->one();
        $iniDate=strtotime($iniDate);
        $endDate=strtotime($endDate);
        $moraValue=round($pending_pay*($modelConfig->value/100),2);//calculo valor mora
        $total = 0;
        $days = 0;
        $Loan = $LoanQuota->getIdLoan()->one();

        $modelArrears =Arrears::findOne(['id_loan_quota'=>$LoanQuota->id]);

        if(!$modelArrears){//para saber si tiene arrears y crearlo
          $modelArrears = new Arrears;
          $modelArrears->id_client= $Loan->id_client;
          $modelArrears->id_loan_quota = $LoanQuota->id;
          $modelArrears->delay_days = 0;
          $modelArrears->total_interest= 0;
          $modelArrears->id_status=45;
          $modelArrears->upd_date=date('Y-m-j') ;
          $modelArrears->pending_pay=0;
          $modelArrears->add_date=date('Y-m-j');
          $modelArrears->save();
        }
        //hago un ciclo for desde lafecha de inicio hasta fecha de hoy
        for($i=$iniDate; $i<=$endDate; $i+=86400){
          $total += $moraValue;
          ++$days;
          $modelArDetail= new ArrearsDetails;
          $modelArDetail->id_arrears = $modelArrears->id;
          $modelArDetail->add_date= date('Y-m-j',$i);
          $modelArDetail->interest_rate=$modelConfig->value;
          $modelArDetail->interest_generate= $moraValue;
          $modelArDetail->amount_base=$pending_pay;
          $modelArDetail->save();

        }

        ///Actualizo el registro nuevamente
        $modelArrears->delay_days = $modelArrears->delay_days + $days;
        $modelArrears->total_interest= round($modelArrears->total_interest+$total,2);
        $modelArrears->pending_pay=round($modelArrears->pending_pay+$total,2);
        $modelArrears->upd_date=date('Y-m-j') ;
        $modelArrears->save();

        $SmsText= 'TUPADRINO.NET INFORMA QUE TU CUOTA Nº '.$LoanQuota->n_quota.' SE ENCUENTRA 
        EN MORA. DEBES REALIZAR TU PAGO DE INMEDIATO. INGRESA AL PORTAL PARA MAYOR INFORMACION';
//        $this->SendSMS($Loan->getIdClient()->one(),$SmsText);

	$modelSms = new MessagesSms();
        $client = $Loan->getIdClient()->one()->id;
        $number = $Loan->getIdClient()->one()->getAddresses()->one()->cellphone;
        
        if ($modelSms->saveSms($client,$SmsText))
          $modelSms->sentSMS($number,$SmsText);


        $SmsText='TUPADRINO.NET INFORMA QUE TU AHIJADO NOMBRE APELLIDO TIENE SU COMPROMISO EN MORA. 
        RECUERDA QUE ESTO AFECTA TU CRECIMIENTO Y BENEFICIOS';


  }

  /** 
  * Metodo encargado de calcular y guardar las moras generadas
  * en unrango de fecha
  * @param $LoanQuota modelo LoanQuota
  * @param $pending_pay Pago pendiente 
  */
  private function ActualizarLoanQuota($LoanQuota,$pending_pay){
        $status = ($pending_pay > 0) ? Yii::$app->params['cuota_mora'] : Yii::$app->params['cuota_pagada']; 
        $have_arrear = Arrears::find()->where(['id_status'=>45,'id_loan_quota'=>$LoanQuota->id])->exists();//Si la mora esta Impagada
        $LoanQuota->have_arrear = ($have_arrear == true) ? true:false;
        $LoanQuota->pending_pay = $pending_pay;
        $LoanQuota->id_status = $status;
        $LoanQuota->save();

        //Verifico Si hay otras cuotas con mora o vencida para
        //poder cambiarle el estatus al loan
        $validator= LoanQuota::find()
                        ->where(['id_status'=>Yii::$app->params['cuota_mora']])
                        ->exists();
        if(!$validator){
          $validator= LoanQuota::find()
                        ->where(['id_status'=>Yii::$app->params['cuota_vencida']])
                        ->exists();
          if(!$validator){              
            $Loan = $LoanQuota->getIdLoan()->one();
            $Loan->status=Yii::$app->params['status_pagados'];
            $Loan->save();
          }else{
            $Loan = $LoanQuota->getIdLoan()->one();
            $Loan->status=Yii::$app->params['status_vencido'];
            $Loan->save();

          }

        }
  }

  /**
  * Metodo encargado de calcular la fecha en que 
  * se comienza a generar una mora
  * @param $actualQuota modelo LoanQuota 
  */

  private function CalcFechaGenMora($actualQuota){
          $n_quota = $actualQuota->n_quota + 1;
          //Busco la cuota siguiente
          $NextQuota = LoanQuota::findOne([
                                          'id_loan'=>$actualQuota->id_loan,
                                          'n_quota'=>$n_quota
                                          ]);
          if($NextQuota){
            return $NextQuota->date_expired;
          }else{//en caso de que sea la ultima cuota
            $Loan = $actualQuota->getIdLoan()->one();
            $days = $Loan->quota + Yii::$app->params['expired_days'];
            $date =  $actualQuota->date_pay;
            return date('Y-m-j',$Loan->addDays($days,$date));

          }
  }
  
  /**
  * Metodo encargado de cambiar los estatus de las comisiones
  * @param $Loan modelo Loan
  */
  private function CommissionStatus($Loan){
        $comission = $Loan->getCommissions();
          if($comission->exists()){//Continuo siempre y cuando tenga comisiones asociadas a este loan
            $comission = $comission->one();
            //almaceno el status que tiene
            $beforeStatus= $comission->id_status;
            if($beforeStatus != Yii::$app->params['commission_mora']){
                $comission->id_status = Yii::$app->params['commission_mora'];
                $comission->update();
                //traigo la ronda
                $round = $comission->getIdCommissionRound()->one();
    
                //asigno los valores a la ronda en caso que no este en riesgo
                //Observacion: las comisiones por default deben venir en riesgo
                if($beforeStatus!=Yii::$app->params['commission_riesgo']){
                  $round->to_pay = $round->to_pay - $comission->commission;
                  $round->risk_pay = $rount->risk_pay + $comission->commission;
                }
                //traigo las demas comisiones para colocar una randon en riesgo
                $allcomision = Commission::find()
                                          ->where(['id_status'=>[
                                                                Yii::$app->params['commission_puntual'],
                                                                Yii::$app->params['commission_finalizada']
                                                                ],
                                                  'id_commission_round' => $round->id]);
    
                //Verifico que haya comisiones disponibles para colocar el riesgo
                $allcount =  $allcomision->count();
                          
                if($allcount>=1){
                  $rand = rand(0,$allcount-1);///Selecciono aleatoriamente uno sdisponibles
                  $allcomision = $allcomision->all();
                  $allcomision[$rand]->id_status = Yii::$app->params['commission_riesgo'];
                  $allcomision[$rand]->update();//cambio el status  a la comision obtenida por randon
                  //asigno los neuvos valores a la ronda
                  $round->to_pay = $round->to_pay - $allcomision[$rand]->commission;
                  $round->risk_pay = $rount->risk_pay + $allcomision[$rand]->commission;
                }
          
                $round->update();
            }
        }        
  }

  private function SendSMS($model,$SmsText){
      $tedexis = Yii::$app->tedexisApi; // Api Tedexis
      // Parametros necesarios para el envio del sms
      $parmsSms = array(
        'passport'=> $tedexis->options['passport'],
        'password'=> $tedexis->options['password'],
        'number'=>'58'.$model->getAddresses()->one()->cellphone,
        'text' => $SmsText);
          
      
      $tedexis->sendSMS($parmsSms); // envia el sms al cliente al numero registrado
  }
    /*-------------------------------
     FIn de metodos segundarios estatus de cuota
    --------------------------------*/

}
