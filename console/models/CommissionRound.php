<?php

namespace console\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * This is the model class for table "commission_round".
 *
 * @property integer $id
 * @property integer $id_client
 * @property string $end_date
 * @property string $to_pay
 * @property integer $id_status
 * @property string $add_date
 *
 * @property Commission[] $commissions
 * @property Clients $idClient
 * @property Status $idStatus
 */
class CommissionRound extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'commission_round';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_client', 'end_date', 'to_pay', 'id_status', 'add_date'], 'required'],
            [['id_client', 'id_status'], 'integer'],
            [['end_date', 'add_date'], 'safe'],
            [['to_pay'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_client' => 'Padrino',
            'end_date' => 'Fecha de Finalizacion',
            'to_pay' => 'Total a cobrar ',
            'id_status' => 'Estatus',
            'add_date' => 'fecha que en la que se agrega la ronda',
            'expected_pay'=>'Total Esperado',
            'risk_pay'=>'Total en Riesgo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommissions()
    {
        return $this->hasMany(Commission::className(), ['id_commission_round' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'id_client']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'id_status']);
    }

    public function getLastComision(){
        $result = CommissionRound::find()
        ->select(['end_date','to_pay'])
        ->where(['id_client'=>Yii::$app->user->identity->id, 'id_status'=>62])
        ->limit(6);
        
        if($result->exists()){
            $result= ArrayHelper::toArray($result->all(),[
                'frontend\models\CommissionRound'=>[
                     'y'=>function($commissionround){
                        return date('Y-m',strtotime($commissionround->end_date));
                        },
                     'item1'=>'to_pay',
                    ],
                ]);
            return Json::encode($result);

        }
        else{
           return '';
        }
    }

}
