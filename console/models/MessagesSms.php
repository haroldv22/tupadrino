<?php

namespace console\models;

use Yii;

/**
 * This is the model class for table "messages_sms".
 *
 * @property integer $id
 * @property integer $id_client
 * @property string $message
 * @property integer $id_status
 * @property string $date_add
 *
 * @property Clients $idClient
 * @property Status $idStatus
 */
class MessagesSms extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'messages_sms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_client', 'message', 'id_status', 'date_add'], 'required'],
            [['id_client', 'id_status'], 'integer'],
            [['date_add'], 'safe'],
            [['message'], 'string', 'max' => 160]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_client' => 'Id Client',
            'message' => 'Message',
            'id_status' => 'Id Status',
            'date_add' => 'Date Add',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'id_client']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'id_status']);
    }

    /**
   * [sentSMS envia mensajes de texto]
   * @param  [string] $number [numero telefonico]
   * @param  [string] $msg    [mensaje a enviar]
   */
    public function sentSMS($number,$msg){

      $tedexis = Yii::$app->tedexisApi; // Api Tedexis

      // Parametros necesarios para el envio del sms
      $parmsSms = array(
          'passport'=> $tedexis->options['passport'],
          'password'=> $tedexis->options['password'],
          'number'=>'58'.$number,
          'text' => $msg);
      
      $tedexis->sendSMS($parmsSms);
    }

    /**
     * [findSmsByStatus muestra los sms 
     * enviados tomando en cuenta los status]
     * @param  [integer] $status [id del status]
     * @return [model]         [ modelo con todos los mensajes enviados]
     */
    public function findSmsByStatus($status) {        
	
	return $this::find()
                    ->where(['id_status'=>$status])
                    ->orderBy('id DESC')
                    ->all();

        //return $this::find()->where(['id_status'=>$status]);        
    }

    /**
     * [saveSmsSys Almacena los mensajes de texto
     * enviados]
     * @param  [string] $number [numero telefonico]
     * @param  [string] $sms    [mensaje]
     * @param  [integer] $client [id del usuario a quien se le envio el mensaje]
     * @return [boolean] true | false [verificando el envio de sms al usuario ]
     */
    public function saveSms($id_client,$msg){
        
        $model = new MessagesSms();
        
        $model->id_client = $id_client;
        $model->message = $msg;        
        $model->id_status = Yii::$app->params['msg_enviado'];        
        $model->date_add = date("Y-m-d");        
        
        return $model->save(false);
    }

   /** 
    * Permite contar los mensajes (SMS) enviados
    * @param status integer status
    * @return integer cantidad de registros eliminados | enviados
    */
    public function count($status){
        
        return $this::find()
                    ->where(['id_status'=> $status])
                    ->count();
    }

     /**
     * Muestra el mensaje de forma mas corta
     * para mostrar una breve descripcion
     * del mensaje
     */
    public function ShortMessage(){

        $msg = $this->message;    
        return  substr(strip_tags($msg), 0, 100).'....';
        
    }
}

