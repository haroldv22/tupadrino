<?php

namespace console\models;

use Yii;

/**
 * This is the model class for table "clients".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $gender
 * @property integer $id_godfather
 * @property integer $id_level
 * @property integer $loan_count
 * @property string $birth_date
 * @property string $identity
 * @property string $date_add
 * @property integer $godson_count
 *
 * @property Address[] $addresses
 * @property Arrears[] $arrears
 * @property BankAccount[] $bankAccounts
 * @property Level $idLevel
 * @property ClientsPwd $clientsPwd
 * @property Loan[] $loans
 * @property Question[] $questions
 * @property RecordPayment[] $recordPayments
 */
class Clients extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'email', 'gender', 'birth_date', 'date_add'], 'required'],
            [['id_godfather', 'id_level', 'loan_count', 'godson_count'], 'integer'],
            [['birth_date', 'date_add'], 'safe'],
            [['firstname', 'lastname'], 'string', 'max' => 50],
            [['email'], 'string', 'max' => 75],
            [['gender'], 'string', 'max' => 1],
            [['identity'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id de la tabla',
            'firstname' => 'firstname',
            'lastname' => 'lastname ',
            'email' => 'email',
            'gender' => 'gender',
            'id_godfather' => 'id godfather ',
            'id_level' => 'id level',
            'loan_count' => 'contador de creditos completados por el cliente',
            'birth_date' => 'fecha de nacimiento',
            'identity' => 'cedula de identida',
            'date_add' => 'fecha de registro',
            'godson_count' => 'Contador de ahijados disponibles para invitar',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(Address::className(), ['id_client' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArrears()
    {
        return $this->hasMany(Arrears::className(), ['id_client' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankAccounts()
    {
        return $this->hasMany(BankAccount::className(), ['id_client' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLevel()
    {
        return $this->hasOne(Level::className(), ['id' => 'id_level']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientsPwd()
    {
        return $this->hasOne(ClientsPwd::className(), ['id_client' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoans()
    {
        return $this->hasMany(Loan::className(), ['id_client' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasMany(Question::className(), ['id_client' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecordPayments()
    {
        return $this->hasMany(RecordPayment::className(), ['id_client' => 'id']);
    }

    /**
    * Permite Obtener el Email del padrino
    * @param id_godfather id del Padrino
    * @return model el modelo Cliente    
    */
    public function getEmailGodFather($id_godfather){

        return $this::find()->where(['id'=>$id_godfather])->one();

    }

   /**
    * Permite obtener el nombre y apellido 
    * Mas corto del cliente
    */
    public function Shortname(){
        
       $nombres = $this->firstname;
        $apellidos = $this->lastname;

        $firstname = strstr($nombres, ' ',true);
        $lastname = strstr($apellidos, ' ', true);

        if(empty($firstname))
            $firstname = $nombres;
        else
            $firstname = $firstname;

        if(empty($lastname))
            $lastname = $apellidos;
        else
            $lastname = $lastname;

        return ucwords(strtolower($firstname. ' '. $lastname));
    }
}
