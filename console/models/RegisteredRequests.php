<?php

namespace console\models;

use Yii;

/**
 * This is the model class for table "registered_requests".
 *
 * @property integer $id
 * @property string $date_request
 * @property boolean $registered
 */
class RegisteredRequests extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'registered_requests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_request'], 'required'],
            [['date_request'], 'safe'],
            [['registered'], 'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_request' => 'Fecha de la Solicitud',
            'registered' => 'campo que permite conocer si la solicitud fue registrada en el banco!!',
        ];
    }
}
