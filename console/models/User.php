<?php

namespace console\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $username
 * @property string $password
 * @property string $password_temp
 * @property integer $id_status
 * @property integer $id_role
 *
 * @property Status $idStatus
 * @property UserRole $idRole
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'email', 'username', 'password', 'id_status', 'id_role'], 'required'],
            [['id_status', 'id_role'], 'integer'],
            [['firstname', 'lastname'], 'string', 'max' => 50],
            [['email', 'username'], 'string', 'max' => 75],
            [['password', 'password_temp'], 'string', 'max' => 128],
            [['email'], 'unique'],
            [['username'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Nombre',
            'lastname' => 'Apellido',
            'email' => 'email del usuario',
            'username' => 'username',
            'password' => 'password
',
            'password_temp' => 'password temporal',
            'id_status' => 'status',
            'id_role' => 'role del usuario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'id_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRole()
    {
        return $this->hasOne(UserRole::className(), ['id' => 'id_role']);
    }
}
