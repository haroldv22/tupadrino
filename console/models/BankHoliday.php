<?php

namespace console\models;

use Yii;

/**
 * This is the model class for table "bank_holiday".
 *
 * @property integer $id
 * @property string $holiday
 * @property string $description
 */
class BankHoliday extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bank_holiday';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['holiday'], 'required'],
            [['holiday'], 'safe'],
            [['description'], 'string', 'max' => 75]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'holiday' => 'Holiday',
            'description' => 'Description',
        ];
    }
}
