<?php

namespace console\models;

use Yii;

/**
 * This is the model class for table "client_invitation".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $invitation_code
 * @property integer $id_status
 * @property integer $id_godfather
 * @property string $date_exp
 *
 * @property Status $idStatus
 */
class ClientInvitation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_invitation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'invitation_code', 'id_status', 'id_godfather'], 'required'],
            [['id_status', 'id_godfather'], 'integer'],
            [['date_exp'], 'safe'],
            [['name'], 'string', 'max' => 25],
            [['email'], 'string', 'max' => 75],
            [['invitation_code'], 'string', 'max' => 128],
            [['email'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'nombre del usuario',
            'email' => 'email del usuario a invitar',
            'invitation_code' => 'codigo de invitacion para el usuario',
            'id_status' => 'status de la invitacion',
            'id_godfather' => 'id del padre',
            'date_exp' => 'Fecha de expiracion de la invitacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'id_status']);
    }
}
