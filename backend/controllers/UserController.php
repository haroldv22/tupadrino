<?php

namespace backend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\UploadedFile; 
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\User;
use app\models\UserRole;
use app\models\UploadForm;


/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
              'access' => [
                'class' => AccessControl::className(),                                           
                'rules' => [               
                    [
                        'actions' => ['index','view','create','update','delete','profile','logout'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rule,$action){
                            return User::userRoles(Yii::$app->user->identity->id,Yii::$app->params['superUserRole']);                                                
                        },     
                    ],                                                                                       
                ],                
                'denyCallback' => function ($rule, $action) {
                    $this->redirect(Yii::$app->user->loginUrl);                   
                    // $this->redirect('site/login');
//                    throw new \Exception('You are not allowed to access this page');
                },               
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => User::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate(){
        
        $model          = new User;        
        $modelUserRoles = new UserRole;        
        $roles          = $modelUserRoles->getRoles();
        
        if ($model->load(Yii::$app->request->post())){                                       
                      
            $model->password   = $model->getPasswordHash($model->password);                        
            $model->id_status  = Yii::$app->params['usuario_activo'];     
            $model->image      = Yii::$app->params['img_profile'];
                        
            if($model->save())                             
                return $this->redirect(['view', 'id' => $model->id]);
            else{                                
                return $this->render('create',[
                    'model' => $model,
                    'items' => $roles
                ]);            
            }  
           
        } else {                        
            
            return $this->render('create', [
                'model' => $model,
                'items' => $roles,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id){        
                
        $model          = $this->findModel($id);
        $modelUserRoles = new UserRole();
        
        $roles          = $modelUserRoles->getRoles();
        
        if ($model->load(Yii::$app->request->post())) {
            
            $model->password = $model->getPasswordHash($model->password);
            
            if ($model->save())
                return $this->redirect(['view', 'id' => $model->id]);
        
        } else {
            return $this->render('update', [
                'model' => $model,
                'items' => $roles
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionProfile($id){        

        $model          = $this->findModel($id);
        $modelUpload    = new UploadForm();    // Modelo UploadForm 

        if ($model->load(Yii::$app->request->post())) {            

            //Escenario para que solo valide la imgProfile
            $modelUpload->scenario = 'profile';
            $modelUpload->imgProfile =  UploadedFile::getInstance($modelUpload, 'imgProfile');
            
            if(!empty($modelUpload->imgProfile)){
                $folderUsers  = '/resources/users/'.$id.'/';                          
                        
                // Verifica que la carpeta a sido creada
                if(!is_dir('.'.$folderUsers))                                        
                    mkdir('.'.$folderUsers,0777);              // Crea la carpeta del usuario                                                                                
                    
                $model->image =  $folderUsers.$modelUpload->imgProfile->name;                                                
                $modelUpload->upload($folderUsers);
            }        
            
            if($model->update())            
                $this->redirect('profile?id='.$id);            
                
            // \Yii::$app->session->setFlash('success', 'Modificaciones realizadas satisfactoriamente');
                                        
        }            
     
        return $this->render('profile', [
            'model' => $model,
            'modelUpload' => $modelUpload
            // 'items' => $roles
        ]);
        
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}



 
