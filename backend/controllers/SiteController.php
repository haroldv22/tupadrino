<?php
namespace backend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\helpers\Html;
use app\models\LoginForm;
use app\models\UploadForm;
use yii\web\UploadedFile;   
use app\models\User;
use app\models\Arrears;
use app\models\Loan;
use app\models\Banks;
use app\models\Config;
use app\models\BankClient;
use app\models\Clients;
use app\models\ClientInvitation;
use app\models\RegisteredRequests;
use app\models\GeneratedRequests;
use app\models\Status;
use frontend\models\CommissionRound;
use frontend\models\Commission;
use frontend\models\LoanQuota;
use yii\helpers\ArrayHelper;

/**
 * Site controller
 */
class SiteController extends Controller
{        
        
    /**
     * @inheritdoc
     */
    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error',],
                        'allow' => true,
                        'roles' => ['?'],                                                 
                    ],
                    [
                        'actions' => ['index','logout','solicitudes','liquidaciones-prestamos',
                                      'cartera-credito','cuotas-carteras','registro-liquidaciones',
                                      'mostrar-solicitudes','custom-search','registro-solicitantes',
				      'match-liquidaciones'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],                                        
                ],
                
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
  
    /**
     * @inheritdoc
     */
    public function actions(){
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
    public function actionIndex(){
      
      $modelLoan = new Loan();      
      $modelClient = new Clients();
      $modelArrears = new Arrears();
      $modelClientInvi = new ClientInvitation();

      // cantidad de ahijados
      $ahijCount = $modelClient->findClientsCount();      

      // Cantidad de hombres
      $mCount = $modelClient->findCountByGender(Yii::$app->params['genderM']);            

      // Cantidad de mujeres
      $fCount = $modelClient->findCountByGender(Yii::$app->params['genderF']);      
      
      /**
       * ACTIVOS (Todos Vigentes, Vencidos, Mora)
       */            
      $pActivosSum = $modelLoan->getSumAllByStatus(
        [ Yii::$app->params['status_vigente'],
          Yii::$app->params['status_vencido'],
          Yii::$app->params['status_mora'] ]
      );

      $pActivosAmbos = $modelLoan->findCountByGenderStatus(
        [ Yii::$app->params["genderM"],
          Yii::$app->params["genderF"] ],
        [ Yii::$app->params['status_vigente'],
          Yii::$app->params['status_vencido'],
          Yii::$app->params['status_mora'] ]
      );

      $pActivosHombres = $modelLoan->findCountByGenderStatus(
        Yii::$app->params['genderM'],
        [ Yii::$app->params['status_vigente'],
          Yii::$app->params['status_vencido'],
          Yii::$app->params['status_mora'] ]
      );

      $pActivosMujeres = $modelLoan->findCountByGenderStatus(
        Yii::$app->params['genderF'],
        [ Yii::$app->params['status_vigente'],
          Yii::$app->params['status_vencido'],
          Yii::$app->params['status_mora'] ]
      );

      $pAMontosHombres = $modelLoan->getSumAmountByGender(
          Yii::$app->params['genderM'],
          [ Yii::$app->params['status_vigente'],
          Yii::$app->params['status_vencido'],
          Yii::$app->params['status_mora'] ]
      );

      $pAMontosMujeres = $modelLoan->getSumAmountByGender(
          Yii::$app->params['genderF'],
          [ Yii::$app->params['status_vigente'],
          Yii::$app->params['status_vencido'],
          Yii::$app->params['status_mora'] ]
      );

      /**
       * AL DIA (Solo Vigentes)
       */            
      $pAlDiaSum = $modelLoan->getSumAllByStatus(Yii::$app->params['status_vigente']);

      $pAlDiaAmbos = $modelLoan->findCountByGenderStatus(
        [ Yii::$app->params['genderM'],
          Yii::$app->params['genderF'] ],
        Yii::$app->params['status_vigente']
      );

      $pAlDiaHombres = $modelLoan->findCountByGenderStatus(
        Yii::$app->params["genderM"],
        Yii::$app->params['status_vigente']          
      );

      $pAlDiaMujeres = $modelLoan->findCountByGenderStatus(
        Yii::$app->params["genderF"],
        Yii::$app->params['status_vigente']
      );

      $pADMontosHombres = $modelLoan->getSumAmountByGender(
          Yii::$app->params["genderM"],
          Yii::$app->params['status_vigente']          
      );

      $pADMontosMujeres = $modelLoan->getSumAmountByGender(
          Yii::$app->params["genderF"],
          Yii::$app->params['status_vigente']          
      );

      /**
       * MORA (Solo en Mora)
       */
      $pMoraSum = $modelArrears->getSumMoraAllByGender(
        [ Yii::$app->params['genderM'],
          Yii::$app->params['genderF'] ]
      );

      $pMoraAmbos = $modelLoan->findCountByGenderStatus(
        [ Yii::$app->params['genderM'],
          Yii::$app->params['genderF'] ],
        Yii::$app->params['status_mora']
      );

      $pMoraHombres = $modelLoan->findCountByGenderStatus(
        Yii::$app->params["genderM"],
        Yii::$app->params['status_mora']        
      );

      $pMoraMujeres = $modelLoan->findCountByGenderStatus(
        Yii::$app->params["genderF"],
        Yii::$app->params['status_mora']
      );

      $pMOMontosHombres = $modelArrears->getSumMoraAllByGender(Yii::$app->params["genderM"]);

      $pMOMontosMujeres = $modelArrears->getSumMoraAllByGender(Yii::$app->params["genderF"]);    
  
      /**
       * SOLICITUDES ACUMULADAS / MES / HOY
       */            
      $sumHoy = $modelLoan->getRequestByToday();
      
      // Suma de las Solicitudes del Mes
      $month = date('m');              
      $sumMes = $modelLoan->getRequestByMonth($month);
      
      // Suma de las Solicitudes Totales
      $sumAcumuladas = $modelLoan->getRequestByAll();
                  
      return $this->render('index',[
        'ahijCount' => $ahijCount,
        'mCount' => $mCount,
        'fCount' => $fCount,
        'sumActivos' => $pActivosSum,
        'ambosActivos' => $pActivosAmbos,
        'hombresActivos' => $pActivosHombres,
        'mujeresActivas' => $pActivosMujeres,
        'montoHombresActivos' => $pAMontosHombres,
        'montoMujeresActivas' => $pAMontosMujeres,        
        'sumAlDia' => $pAlDiaSum,
        'ambosAlDia' => $pAlDiaAmbos,
        'hombresAlDia' => $pAlDiaHombres,
        'mujeresAlDia' => $pAlDiaMujeres,
        'montoHombresAlDia' => $pADMontosHombres,
        'montoMujeresAlDia' => $pADMontosMujeres,              
        'sumMora'=>$pMoraSum,
        'ambosMora' => $pMoraAmbos,
        'hombresMora' => $pMoraHombres,
        'mujeresMora' => $pMoraMujeres,
        'montoHombresMora' => $pMOMontosHombres,
        'montoMujeresMora' => $pMOMontosMujeres,        
        'sToday' => $sumHoy,
        'sMonth' => $sumMes,
        'sAcumulate' => $sumAcumuladas        
      ]);        
    }

    public function actionLogin(){                        

        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        
        $model = new LoginForm();  
        
        
        if ($model->load(Yii::$app->request->post()) && $model->login()) {                                                              
            return $this->goBack();        
        } else {        
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout(){
        Yii::$app->user->logout();

        return $this->goHome();
    }
    
    /**
     * Se encarga de redireccionar a la vista 
     * para mostrar las solicitudes de prestamo 
     * que se han generado en el sistema
     */ 
    public function actionSolicitudes(){    
        
        $modelRRequest = new RegisteredRequests();        
        $modelGRequest = new GeneratedRequests();

        // dataProvider de tabla Loan
        $dataProvider = new ActiveDataProvider([
            'query' => Loan::find(),                        
     	    'sort' => [
                    'defaultOrder' => [
                        'date_add' => SORT_DESC,
                    ]
            ],
        ]);

        // dataProvider tabla Registered_request
        $dataProviderRR = new ActiveDataProvider([
                    'query' => RegisteredRequests::find()
                                                ->where(['registered'=>true]),                        
                    'sort' => [
                        'defaultOrder' => [
                            'date_request' => SORT_DESC,                
                        ]
                    ],
        ]);

        
                    
        return $this->render('solicitudes', [
            'dataProvider'  => $dataProvider,                                
            'modelRRequest' => $modelRRequest,
            'modelGRequest' => $modelGRequest,
            'dataProviderRR'=> $dataProviderRR
        ]);                
    }       
    
    /**
     * Encargado de mostrar todas las solicitudes
     * generadas en el dia y tambien permite mostrar 
     * los status de cada una de estas solicitudes
     */ 
    public function actionLiquidacionesPrestamos(){                 
        
        /**
         * DataProvider especificamente para las 
         * solicitudes generadas en los archivos .txt         
         */ 
        $dataProvider = new ActiveDataProvider([
            'query' => GeneratedRequests::find(),                        
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,                
                ]
            ],
        ]);
        
        /**
         * DataProvider especificamente para los clientes 
         * pendientes por ser liquidados por eso se coloca
         * la abreviatura "CPL" (Clientes Pendientes Por Liquidar)
         */         
        $dataProviderCPL = new ActiveDataProvider([
            'query' => Loan::find()
                             ->where(['id_status'=>Yii::$app->params['status_pendiente']]),                                            
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,                
                ]
            ],
        ]);                               
        
         /**
         * DataProvider especificamente para los clientes 
         * aprobados en su liquidacion por eso se coloca
         * la abreviatura "CLA" (Clientes con Liquidaciones Aprobadas)
         */         
        $dataProviderCLA = new ActiveDataProvider([
            'query' => Loan::find()
                             ->where(['id_status'=>[Yii::$app->params['status_vigente'],
                                                    Yii::$app->params['status_vencido']]]),                                            
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,                
                ]
            ],
        ]);
        
      /**
         * DataProvider especificamente para los clientes 
         * rechazados en su liquidacion por eso se coloca
         * la abreviatura "CLR" (Clientes con Liquidaciones Rechazadas)
         */         
        $dataProviderCLR = new ActiveDataProvider([
            'query' => Loan::find()
                             ->where(['id_status'=>Yii::$app->params['status_rechazadas']]),                                            
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,                
                ]
            ],
        ]);

        $modelGRequest = new GeneratedRequests();
        $modelRRequest = new RegisteredRequests();        

        // Obtiene todas las solicitudes generadas
        // $array_requests = $modelGRequest::find()                                                     
        //     ->where(['id_status'=>Yii::$app->params['sGeneradas']])
        //     ->asArray()
        //     ->all();                       

        // Obtiene todas las solicitudes no registradas
        $array_rrequests = $modelRRequest::find()                                                     
            ->where(['registered'=>false])
            ->asArray()
            ->all();   
    
        return $this->render('liquidaciones', [
            'modelGRequest'     => $modelGRequest,
            'modelRRequest'     => $modelRRequest,
            // 'array_requests'    => $array_requests,
            'array_rrequests'   => $array_rrequests,
            'dataProvider'      => $dataProvider,         
            'dataProviderCPL'   => $dataProviderCPL,
            'dataProviderCLA'   => $dataProviderCLA,
            'dataProviderCLR'   => $dataProviderCLR,            
        ]);                
    }
    
    /**
     * Encargado de mostrar aquellos clientes que 
     * se encuentran con solicitudes de prestamos 
     * vigentes, vencidas y en moras
     */
    public function actionCarteraCredito(){
        
        /**
         * PENDIENTE POR MOSTRAR A LOS USUARIOS CON CREDITOS VIGENTES 
         * EN MORA Y VENCIDOS
         */              
      
      /**
         * DataProvider especificamente para los clientes 
         * que presentan creditos Vigentes
         * la abreviatura "CCV" (Clientes con Creditos Vigentes)
         */         
        $dataProviderCCV= new ActiveDataProvider([
            'query' => Loan::find()
                             ->where(['id_status'=>Yii::$app->params['status_vigente']]),                                            
            'pagination' => [
                'pageSize' => 6,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,                
                ]
            ],
        ]);

        /**
         * DataProvider especificamente para los clientes 
         * que presentan creditos Vencidos
         * la abreviatura "CCVE" (Clientes con Creditos VEncidos)
         */         
        $dataProviderCCVE= new ActiveDataProvider([
            'query' => Loan::find()
                             ->where(['id_status'=>Yii::$app->params['status_vencido']]),                                            
            'pagination' => [
                'pageSize' => 6,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,                
                ]
            ],
        ]);

        /**
         * DataProvider especificamente para los clientes 
         * que presentan creditos Mora
         * la abreviatura "CCMO" (Clientes con Creditos Mora)
         */         
        $dataProviderCCMO= new ActiveDataProvider([
            'query' => Loan::find()
                             ->where(['id_status'=>Yii::$app->params['status_mora']]),                                            
            'pagination' => [
                'pageSize' => 6,
            ],                             
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,                
                ]
            ],
        ]);
        
                
        return $this->render('carteras_creditos',[
            'dataProviderCCV'  => $dataProviderCCV,
            'dataProviderCCVE' => $dataProviderCCVE,
            'dataProviderCCMO' => $dataProviderCCMO        
        ]);
        
    }
    
   /**
     * Permite mostrar las Solicitudes pendientes 
     * dependiendo la fecha seleccionada en el tab (Solicitudes pendientes)
     */
    public function actionMostrarSolicitudes(){

  $modelUpload    = new UploadForm();
        $date           = $_POST['fecha'];        
        $users          = $_POST['users'];                    
        
        $modelBank      = new Banks();    
        $modelRRequest  = new RegisteredRequests();        
        
        $dataProvider   = new ActiveDataProvider([
            'query' => Loan::find()
                             ->where(['id_status' => Yii::$app->params['status_pendiente'],
                                      'date_add'  => $date]),                                                        
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,                
                ]
            ],
        ]);
            
        if (!$users){            
            
            // Para verificar si se ha generado un formato de registro primero de los clientes            
            $validateRegister = $modelRRequest::find()
                                               ->where(['date_request' => $date, 'registered'=> false])
                                               ->exists();    
                        
            $download   = Yii::$app->formatter->asDate($date,'dd-MM-Y').'.txt'; // Archivo a descargar asignado en la url
            $url        = '/resources/solicitudes_prestamos/'.Yii::$app->formatter->asDate($date,'MM-Y').'/'.$download; // url de donde se hara la descarga
            

            if($validateRegister)
                return false;
            else{
                
                // Se muestran las liquidaciones que estan pendientes
                return $this->renderPartial('gridLiqPend',[
                    'dataProvider'  => $dataProvider,
                    'model' => $modelUpload,
                    'url' => $url,
                    'download' => $download

                ]);  
            }         

        }else{                
            // Se muestran todas las solicitudes de prestamos
            return $this->renderPartial('gridSolciPrest',[
                'dataProvider'  => $dataProvider,
                'modelBank' => $modelBank
            ]);            
        }       
    

/*      $date           = $_POST['fecha'];        
        $users          = $_POST['users'];                    
        
        $modelBank      = new Banks();    
        $modelRRequest  = new RegisteredRequests();        
        
        $dataProvider   = new ActiveDataProvider([
            'query' => Loan::find()
                             ->where(['id_status' => Yii::$app->params['status_pendiente'],
                                      'date_add'  => $date]),                                                        
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,                
                ]
            ],
        ]);
            
        if (!$users){            
            // Para verificar si se ha generado un formato de registro primero de los clientes            
            $validateRegister = $modelRRequest::find()
                                               ->where(['date_request' => $date, 'registered'=> false])
                                               ->exists();    

            if($validateRegister)
                return false;
            else{
                // Se muestran las liquidaciones que estan pendientes
                return $this->renderPartial('gridLiqPend',[
                    'dataProvider'  => $dataProvider
                ]);  
            }         

        }else{                
            // Se muestran todas las solicitudes de prestamos
            return $this->renderPartial('gridSolciPrest',[
                'dataProvider'  => $dataProvider,
		'modelBank' => $modelBank
            ]);            
        }        */
    }

/**
     * Permite hacer el Match entre el banco y
     * los solicitantes de prestamos y ser confirmado
     * el pago o no, este les notifica a cada uno de los usuarios
     * solicitantes
     */
    public function actionMatchLiquidaciones(){

        $sFecha     = $_POST['sFecha']; // Fecha de la solicitud seleccionadas        
        $model      = new UploadForm();  
        $modelLoan  = new Loan();
        $modelGRequest = new GeneratedRequests();
                
        $model->importLiq = UploadedFile::getInstance($model,'importLiq');  // contiene el archivo .TXT a matchear              
        $model->scenario = 'match';

        if($model->validate()){
            
            $file = $model->importLiq;  // se le asigna a la variable file el archivo
            $fOpen = fopen($file->tempName, "r"); // Abre el archivo
            $fContent = fread($fOpen, filesize($file->tempName)); // se especifica el archivo a leer            
            $content = explode("\n", $fContent); // se lee fila por fila hasta el salto de linea

            $i = 0;            
            $aux = 0;   // verificara si se hizo mas que sea una 
                        // operacion para verificar que el archivo es valido
	    $auxRe = 0; // para llevar un conteo de las solicitudes rechazadas

            
            // foreach para recorer fila por fila del documento
            foreach ($content as $value) {                
                // lee la segunda linea del archivo
                if($i!=0){                    
                    
                    $offsetC = mb_strpos($value,'NRO.'); // Ubica el string NRO. para saber en que linea y 
                                                        // columna esta el numero de cuenta del solicitante

                    $offsetM = mb_strpos($value,'-0'); // Ubica el monto con el cual se liquido NRO. para saber en que linea y 
                                                        // columna esta el numero de cuenta del solicitante
                    if(!empty($offsetC)){
                        
                        $account = substr($value, $offsetC+5,20); // Cuenta
                        $amount = substr($value,$offsetM+1,16); // monto
                        $reference = substr($value,-15); // referencia                        
                        
                        if($modelLoan->matchLiq($account,$amount,$reference))
                            $aux = $aux+1; // para verificar cuantas operaciones fueron efectivas
                    }
                }                
                $i++;
            }

            // Si la variable aux es mayor que cero quiere decir que se ejecuto 
            // tan sea una operacion de no ser asi esto daria a entender que el archivo 
            // cargado no tiene nada referente a lo buscado
            if($aux > 0){

                // busca usuarios a quienes no se les liquido
                $modelLoan = $modelLoan::find()
                                        ->where(['date_add'=>$sFecha,
                                                'id_status'=> Yii::$app->params['status_pendiente']])
                                        ->all();
                
                // se verifica si existen usuarios sin liquidar
                if(!empty($modelLoan)){
                    
                    // recorre uno a uno los usuarios y les modifica su status a solicitud rechazada
                    foreach ($modelLoan as $key => $value) {
                        
                        $modelAux = new Loan();
                        $modelAux = $modelAux::findOne($value->id);

                        $modelAux->id_status = Yii::$app->params['status_rechazadas']; // se asigna el status de rechazado

                        if($modelAux->save(false)){                                    
                            
                            $email = $value->getIdClient()->one()->email; 
                            $user =  $value->getIdClient()->one()->Shortname();
                            
                            $modelAux->sentSolicRechazada($email,$user); // este metodo permite enviar mensajes a los usuarios 
                                                                        // a quienes les fue rechazada la solicitud
                            $auxRe++;
                        }                
                    }

                }
                
                $modelGRequest =  GeneratedRequests::find()->where(['date_add'=>$sFecha])->one();
                $modelGRequest->id_status = Yii::$app->params['sProcesadas'];
                
                if($modelGRequest->save(false)){
                    \Yii::$app->session->setFlash('info', 'Solicitudes Aprobadas: (<b>'.$aux.'</b>)  Solicitudes Rechazadas:(<b>'.$auxRe.'</b>)');
                    return $this->render('matchLiq');                        
                }

            
            }else{

                \Yii::$app->session->setFlash('warning', 'Revise el archivo a importar ya que esta vacio o no contiene la informacion solicitada!!');
                return $this->render('matchLiq');
            }
        }else{

            \Yii::$app->session->setFlash('danger', 'Revise el formato del archivo ya que solo esta permitido el formato .txt');
            return $this->render('matchLiq');
        }           
    }

    /**
    * Permite generar un archivo para poder registrar 
    * a los clientes solicitantes de creditos en los bancos
    * mediante un archivo y un formato en especifico 
    * de acuerdo al banco
    */
    public function actionRegistroSolicitantes(){
                
        $modelRRequest  = new RegisteredRequests();
        $modelLoan      = new Loan();

        $code_bank      = $_POST['bank_code'];
        $date_add       = $_POST['date_add'];

        $model = Loan::find()                        
                        ->where(['id_status'    =>  Yii::$app->params['status_pendiente'],
                                'date_add'      =>  $date_add])
                        ->all();

        foreach ($model as $key => $value) {
            
            $modelBank = new BankClient();    

            $client = $modelBank::find()
                                 ->where(['id_cliente' =>  $value->id_client,
                                          'id_bank'    =>  $code_bank ])                                
                                ->exists();
           
                      
            if (!$client){                
                $modelBank->id_cliente  = $value->id_client;
                $modelBank->id_bank     = $code_bank;
                $modelBank->save(false);
            }
        }
        
        $raiz           = $_SERVER['DOCUMENT_ROOT'];  
        $resources      = 'resources/registrar_solicitudes';                         //Carpeta Principal donde se almacenara la data
        $mainFolder     = $raiz.$resources;
        $monthsFolder   = $mainFolder.'/'.date("m-Y");                               //Carpeta del Mes
        $dayFileTXT     = $monthsFolder.'/'.$code_bank.'_'.date("d-m-Y").'.txt';     //archivo.txt Diario        
	

        if(is_dir($mainFolder)){

            if (!is_dir($monthsFolder))
                mkdir($monthsFolder,0777);                                                    

            $this->bTxtFormatoSolicitudes($model,$dayFileTXT,$date_add,$code_bank);      // llama a metodo que se encarga de agregar la informacion en el archivo .txt                                    

        }else{    

var_dump( $_SERVER['DOCUMENT_ROOT']);
            print_r($mainFolder);
            var_dump(is_dir(substr($mainFolder,0,-21)));    
            Yii::$app->end(); 

	    if (!is_dir(substr($mainFolder,0,-21),0777))
	            mkdir(substr($mainFolder,0,-21),0777);              // Crea la carpeta resources		

            mkdir($mainFolder,0777);                            // Carpeta Principal donde se almacenara la data
            mkdir($monthsFolder,0777);                          // Crea carpeta del mes
            $this->bTxtFormatoSolicitudes($model,$dayFileTXT,$date_add,$code_bank);            // llama a metodo que se encarga de agregar la informacion en el archivo .txt                                          
        }            
    }
    
    /**
    * Permite registrar en la bd todas las 
    * solicitudes de prestamos generadas ese dia    
    */
    public function actionRegistroLiquidaciones(){
     
        $clients        = $_POST['idClients'];
        $options        = $_POST['idOptions']; 
        $referencia     = $_POST['referencs'];
        
        $modelLoan      = new Loan();        
        $modelGRequest  = new GeneratedRequests();
        $count          = 0;

        foreach ($clients as $value){
            if ($options[$count] == 'true')
                $status = Yii::$app->params['status_vigente'];
            else 
                $status = Yii::$app->params['status_rechazadas'];
            
            $modelLoan =   $modelLoan::find()
                                    ->where(['id_client'=>$value])
                                    ->one();
            
            $modelLoan->id_status           = $status;
            $modelLoan->reference_pay       = $referencia;
            $modelLoan->date_transference   = date("Y-m-d");
            $modelLoan->save(false);                                
            
            $this->addComission($value,$modelLoan);
            
            $count++;
        }
            // cambia el status en el archivo colocandolo como procesado
            $modelGRequest = $modelGRequest::find()
                                        ->where(['date_add'=>$modelLoan->date_add])
                                        ->one();
            $modelGRequest->id_status = Yii::$app->params['sProcesadas'];
            
            return $modelGRequest->save(false);                    
    }
    
    public function actionCustomSearch(){

        $option   = $_POST['options'];

        if ($option == 1){
    
            $searchBy = $_POST['searchBy'];   

            switch ($searchBy){
                case 'dia':
                    $date = date('Y-m-d');
//                    $date_part = 'day';
		    $command_added = "loan.date_add ='".$date."'"; // comando agregado para las busquedas especificas por dia
                    break;
                case 'diaAnterior':
		    $date = date('Y-m-d',strtotime("".date('Y-m-d')." -1 day")); // comando agregado para las busquedas especificas por -1 dia
                   // $date = date('d',strtotime('-1 day'));
                    //$date_part = 'day';
		    $command_added = "loan.date_add ='".$date."'";
                    break;
                case 'mes':
                    $date = date('m');
                    //$date_part = 'month';
		    $command_added = "date_part('month',loan.date_add) = ".$date;
                    break;
                case 'mesAnterior':
                    $date = date('m',strtotime('-1 month'));
                    //$date_part = 'month';
		    $command_added = "date_part('month',loan.date_add) = ".$date;
                    break;                    
            }

            $provider = new SqlDataProvider([
                'sql' => "SELECT clients.firstname, clients.lastname, clients.identity, clients.email,
                                 clients.id_level,loan.amount, loan.id_status, loan.date_add,loan.real_date_add, status.name 
                            FROM loan INNER JOIN clients ON (loan.id_client = clients.id) 
                                INNER JOIN status ON (loan.id_status = status.id) 
                                    WHERE $command_added ORDER BY loan.date_add DESC",
                'pagination' => [
                    'pageSize' => 10,
                ],
            
            ]);
        
        }else if ($option == 2){
            
            $date_min = $_POST['min_date'];
            $date_max = $_POST['max_date'];

            $provider = new SqlDataProvider([
                'sql' => "SELECT clients.firstname, clients.lastname, clients.identity, clients.email,
                                 clients.id_level,loan.amount, loan.id_status, loan.date_add,loan.real_date_add, status.name 
                            FROM loan  INNER JOIN clients ON (loan.id_client = clients.id) 
                                INNER JOIN status ON (loan.id_status = status.id)
                                    WHERE loan.date_add >='".$date_min."' AND loan.date_add <='".$date_max."' ORDER BY loan.date_add DESC",               
                'pagination' => [
                    'pageSize' => 10,
                ],
            
            ]);
        
        }else if ($option == 3){
                  
            $provider = new SqlDataProvider([
                'sql' => "SELECT clients.firstname, clients.lastname, clients.identity, clients.email,
                                 clients.id_level,loan.amount, loan.id_status, loan.date_add,loan.real_date_add, status.name 
                            FROM loan  INNER JOIN clients ON (loan.id_client = clients.id) 
                                INNER JOIN status ON (loan.id_status = status.id) ORDER BY loan.date_add DESC",         
                'pagination' => [
                    'pageSize' => 10,
                ],
            
            ]);
        }

        echo GridView::widget([
            'dataProvider' => $provider,    
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'Cliente',
                    'value'    => function($model){
        
                                    return $model['firstname'].' '.$model['lastname'];                                
                                }                         
                ],                                
                [
                    'attribute' => '# Cedula',
                    'value'    => function($model){
                                        return $model['identity'];
                                }                            
                ],            
                [
                  'attribute' => 'Monto',
                  'value'     => function($model){                  
                                    return number_format($model['amount'], 2, ',', ' ');                           
                                }
                ],                                    
                [
                    'attribute' => 'Email',
                    'value'    => function($model){
                                    return $model['email'];
                                }
                ],            
                [
                    'attribute' => 'Level',
                    'value'    => function($model){
                                    return $model['id_level'];
                                }
                ],            
                [
                    'attribute' => 'Estatus',
                    'value'    => function($model){
                                    return $model['name'];
                                }
                ],       
                [
                    'attribute' => 'Fecha/Hora',
                    'value'    => function($model){
                                    return $model['real_date_add'];
                                    },
                ],               
                [
                    'attribute' => 'Fecha Asignada',
                    'value'    => function($model){
                                    return $model['date_add'];
                                 },
                ],                        
             
            ],
        ]);    

    }       

   /*
    ** Calcula la comision y establece a quien se le colocara 
    ** Datos de entrada id del Ahijado y el modelo Loan previamente
    ** cargado cn los datos del credito del ahijado 
    */
    private function addComission($godson,$modLoan){
       
        $padrino = Clients::findOne(['id'=>$godson]);
        if($padrino->id_godfather==0){
            return;
        }
        #calculo de comision y fecha de pago 
        $modLoanQuota= LoanQuota::find()
                                ->where(['id_loan'=>$modLoan->id])
                                ->orderBy('id DESC')
                                ->one();

        // obtiene el porcentaje de la comision a los padrinos
        $modelConfig = Config::find()
                             ->where(['id_option'=> Yii::$app->params['option_cRate']])
                             ->one();


        $comisiontopay = $modLoan->total_interest * ($modelCommission->rate/100);
        $comisiontopay= round($comisiontopay,2);
        $dateend = $modLoanQuota->date_expired;
        #Fin de calculo

        

        $rondas = CommissionRound::find()
           // ->select('id')
            ->Where(['id_status' =>Yii::$app->params['commission_activa'],'id_client'=>$padrino->id_godfather])
            ->orderBy('add_date ASC');

        # verifica si la ronda existe
        if ($rondas->exists()){

            foreach ($rondas->all() as $ronda) {
                $comision = Commission::find()
                    ->where(['=', 'id_client', $godson])
                    ->andWhere(['id_commission_round'=>$ronda->id]);
                if (!$comision->exists()){
                    $transaction = Yii::$app->db->beginTransaction();
                    try {
                        //Insertar en la ronda
                        $modCommission = new Commission;
                        $modCommission->id_commission_round = $ronda->id;
                        $modCommission->id_client = $godson;
                        $modCommission->commission =$comisiontopay;
                        $modCommission->commission_rate = $modelConfig->value;
                        $modCommission->id_status = Yii::$app->params['commission_puntual'];
                        $modCommission->end_date= $dateend;
                        $modCommission->id_loan = $modLoan->id;
                        
                        //calculo si es la fecha mas distante 
                        if($dateend > $ronda->end_date)
                            $ronda->end_date = $dateend;

                        //Establesco el estatus en el que inicia la comision
                        $comisiones = Commission::find()->where(['id_commission_round'=>$ronda->id]);
                        $malos = $comisiones->andWhere(['id_status'=>Yii::$app->params['commission_mora']])->count();
                        $riesgo = $comisiones->andWhere(['id_status'=>Yii::$app->params['commission_riesgo']])->count();
                        $ronda->expected_pay += $comisiontopay;
                        if($malos > $riesgo){
                            $ronda->risk_pay += $comisiontopay;
                            $modCommission->id_status = Yii::$app->params['commission_riesgo'];
                        }
                        else{
                            $ronda->to_pay += $comisiontopay;
                            $modCommission->id_status = Yii::$app->params['commission_puntual'];
                        }

                        $ronda->save();
                        $modCommission->save();

                        $transaction->commit();
                    } catch (Exception $e) {
                        $transaction->rollBack();
                    }
                }
            }
            # Si existe el usuario dentro de la ronda
            # crea nueva ronda e inserta su comision
            $this->InsertCommision($padrino->id_godfather,
                                    $dateend,
                                    $comisiontopay,
                                    $godson,
                                    $modLoan->id);

        }
        else{
            # Si no existe ronda
            # crea nueva ronda e inserta su comision
            $this->InsertCommision($padrino->id_godfather,
                                    $dateend,
                                    $comisiontopay,
                                    $godson,
                                    $modLoan->id);

        }

    }

    /**
    * Inserta en la bd las comisiones, es solo utilizada por
    * el metodo addComisiones
    */
    private function InsertCommision($id_godfather,$dateend,$comisiontopay,$godson,$LoanId){
        $transaction = Yii::$app->db->beginTransaction();
        try {            

            // Obtiene la comision asignada a los padrinos
            $modelConfig = Config::find()
                                 ->where(['id_option'=> Yii::$app->params['option_cRate']])
                                 ->one();

            # ronda comision
            $modCommissionRound                 = new CommissionRound;
            $modCommission                      = new Commission;                        
            $modCommissionRound->id_client      = $id_godfather;
            $modCommissionRound->end_date       = $dateend;
            $modCommissionRound->to_pay         = $comisiontopay;
            $modCommissionRound->id_status      = Yii::$app->params['commission_activa'];
            $modCommissionRound->add_date       = date('Y-m-j');
            $modCommissionRound->expected_pay   = $comisiontopay;
            $modCommissionRound->risk_pay       = 0;
            
            $modCommissionRound->save();

            # comision
            $modCommission->id_commission_round = $modCommissionRound->id;
            $modCommission->id_client = $godson;
            $modCommission->commission =$comisiontopay;
            $modCommission->commission_rate = $modelConfig->value;
            $modCommission->id_status = Yii::$app->params['commission_puntual'];
            $modCommission->end_date= $dateend;
            $modCommission->id_loan = $LoanId;
            $modCommission->save();

            $transaction->commit();
        
        } catch (Exception $e) {
            $transaction->rollBack();
        }
    }

    /**
     * Metodo encargado de generar un archivo .txt
     * con las solicitudes de creditos hechas en el     
     * dia.
     * @param object loan
     * @param string file 
     */ 
    private function bTxtFormatoSolicitudes($loans,$file,$date_add,$code_bank){        
                
        $modelRRequest = new RegisteredRequests();

        $fh            = fopen($file,'w+');                           
        // $id_beneficiario = ;
        
        foreach ($loans as $key => $value){

            $clients        = $value->getIdClient()->one();
            $bankAccounts   = $clients->getBankAccounts()->one();
            $name           = $clients->firstname.' '.$clients->lastname;
            $account        = $bankAccounts->code_bank.''.$bankAccounts->number_account;
            $row            = str_pad($account, 20).''.
                              str_pad($name, 50).''.
                              str_pad($this->ceroIzquierda($clients->identity,1), 10).''.
                              str_pad($this->ceroIzquierda($clients->identity,2), 12,'0',STR_PAD_LEFT).''.
			      str_pad($value->amount, 13,'0',STR_PAD_LEFT).'00'.
                              str_pad($clients->email, 100).''.
                              "02D".PHP_EOL;
            fwrite($fh, $row);                
        }

        $modelRRequest = $modelRRequest::find()
                                       ->where(['date_request'  =>  $date_add,
                                                'registered'    =>  false])
                                       ->one();                

        $modelRRequest->url           = substr('/'.$file,26);        
        $modelRRequest->registered    = true;        
        $modelRRequest->code_bank     = $code_bank;
        $modelRRequest->update();
    }
    
    /**
    * Metodo que se encarga de verificar 
    * cuantos digitos posee la cedula ya que 
    * al poseer 8 digitos esta funcion le asigna
    * cero antes del primer digito
    */
    private function ceroIzquierda($cadena,$option){
        
        $nuevaCadena = '';                
        $cadenaLength = strlen($cadena);
        
            if ($cadenaLength < 10){                                                                    
                
                for($n=0; $n <= $cadenaLength - 1; $n++){                                                                                    
            
                    if ($n==0 && $option == 2){
                        $nuevaCadena .= 0;
                    }else if ($n == 1 && $cadenaLength==8){                    
                        $nuevaCadena .= '00'.$cadena[$n];                                                                    
                    }else if ($n == 1 && $cadenaLength==9){                                                        
                        $nuevaCadena .= '0'.$cadena[$n];                                                                                    
                    }else{                
                        $nuevaCadena .= $cadena[$n];                 
                    }
                                                
                }        
            }
            return $nuevaCadena;
        
            
    }
}
