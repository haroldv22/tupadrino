<?php

namespace backend\controllers;

use Yii;
use app\models\LevelAmount;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\User;
use app\models\PeriodAmount;
use app\models\Level;
use app\models\Period;

/**
 * LevelAmountController implements the CRUD actions for LevelAmount model.
 */
class LevelAmountController extends Controller
{
    public function behaviors()
    {
        return [
             'access' => [
                'class' => AccessControl::className(),                                           
                'rules' => [               
                    [
                        'actions' => ['index','view','update','delete','logout','create'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rule,$action){                  
                            return User::userRoles(Yii::$app->user->identity->id,Yii::$app->params['superUserRole']);                                                
                        },                                         
                    ],                                                                                       
                ],                
               'denyCallback' => function ($rule, $action) {                    
                    $this->redirect(Yii::$app->user->loginUrl);                   
                },  
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all LevelAmount models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => LevelAmount::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LevelAmount model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LevelAmount model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
	$model = new LevelAmount();
        $modelLevel = new Level();
        $modelPeriod = new Period();
        $modelPeriodAmount = new PeriodAmount();

        if ($model->load(Yii::$app->request->post())){
            
            $post = Yii::$app->request->post();            
            $model->id_level = (int)$post['PeriodAmount']['id_level_amount'];
            
            if ($model->save(false)){
            
                $modelPeriodAmount->id_level_amount = $model->id;
                $modelPeriodAmount->id_period = (int)$post['PeriodAmount']['id_period'];
                $modelPeriodAmount->save(false);                        
            }
            
            return $this->redirect(['view', 'id' => $model->id]);
         
        } else {
            return $this->render('create', [
                'model' => $model,
                'modelLevel' => $modelLevel,
                'modelPeriod' => $modelPeriod,
                'modelPeriodAmount' => $modelPeriodAmount,                
            ]);
        }
    }

    /**
     * Updates an existing LevelAmount model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
	$model = $this->findModel($id);
        $modelLevel = new Level();
        $modelPeriod = new Period();
        $modelPeriodAmount = new PeriodAmount();

         if ($model->load(Yii::$app->request->post())){
            
            $post = Yii::$app->request->post();            

            $modelPeriodAmount->id_level_amount = $id;
            $modelPeriodAmount->id_period = (int)$post['PeriodAmount']['id_period'];

            if($modelPeriodAmount->save())
                return $this->redirect(['view', 'id' => $model->id]);
                        
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelLevel' => $modelLevel,
                'modelPeriod' => $modelPeriod,
                'modelPeriodAmount' => $modelPeriodAmount,                

            ]);
        }
    }

    /**
     * Deletes an existing LevelAmount model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LevelAmount model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LevelAmount the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LevelAmount::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
