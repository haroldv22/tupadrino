<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Clients;
use app\models\ClientsSearch;
use app\models\User;
use app\models\Commission;
use app\models\CommissionRound;
use app\models\Loan;
use app\models\LoanQuota;

/**
 * ClientsController implements the CRUD actions for Clients model.
 */
class ClientsController extends Controller{
  
  public function behaviors(){
      return [
           'access' => [
              'class' => AccessControl::className(),                                           
              'rules' => [               
                  [
                      'actions' => [
                        'index',
                        'view',
                        'update',
                        'delete',
                        'logout',
                        'ahijados',
                        'mostrar-cuotas',
                        'historico-creditos',
                        'historico-comisiones',
                        '_details-commissions',
                        '_details-quotas',
                        '_quantityLoans',
                      ],
                      'allow' => true,
                      'roles' => ['@'],
                      'matchCallback' => function($rule,$action){                  
                          return User::userRoles(Yii::$app->user->identity->id,Yii::$app->params['superUserRole']);                                                
                      },                                         
                  ],                                                                                       
              ],                
             'denyCallback' => function ($rule, $action) {                    
                  $this->redirect(Yii::$app->user->loginUrl);                   
              },  
          ],
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'delete' => ['post'],
              ],
          ],
      ];
  }

  /**
   * Lists all Clients models.
   * @return mixed
   */
  public function actionIndex(){
      $searchModel = new ClientsSearch();
      $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

      return $this->render('index', [
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
      ]);
  }

  /**
   * Displays a single Clients model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id){
                
      $modelLoan              = new Loan();
      $modelClient            = new Clients();                
      $modelCommissionRound   = new CommissionRound();

      $query = $modelLoan::find()->where(['id_client'=>$id]); 

      $dataProvider = new ActiveDataProvider([           
          'query' => $query,
           'sort' => [
              'defaultOrder' => [
                  'date_add' => SORT_DESC,                
            ]
          ],
      ]);

      // Contiene la cantidad de Ahijados de este Padrino
      $countAhijados = $modelClient::find()
                                  ->where(['id_godfather'=>$id])
                                  ->count(); 

      return $this->render('view', [  
          'model'                    => $this->findModel($id),            
          'dataProvider'             => $dataProvider,
          'modelLoan'                => $modelLoan,
          'countAhijados'            => $countAhijados,
          'modelCommissionRound'     => $modelCommissionRound
      ]);
  }

  /**
   * Creates a new Clients model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate(){
      $model = new Clients();

      if ($model->load(Yii::$app->request->post()) && $model->save()) {
          return $this->redirect(['view', 'id' => $model->id]);
      } else {
          return $this->render('create', [
              'model' => $model,
          ]);
      }
  }

  /**
   * Para mostrar los Ahijados de ese padrino
   * seleccionado     
   */
  public function actionAhijados($id){
      
      return $this->render('ahijados', [
          'model' => $this->findModel($id),
          'dataProviderAhijados' => $this->findAhijados($id)
      ]);

  }

  /**
   * Updates an existing Clients model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id){
      $model = $this->findModel($id);

      if ($model->load(Yii::$app->request->post()) && $model->save()) {
          return $this->redirect(['view', 'id' => $model->id]);
      } else {
          return $this->render('update', [
              'model' => $model,
          ]);
      }
  }

  /**
   * Deletes an existing Clients model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id){
      $this->findModel($id)->delete();

      return $this->redirect(['index']);
  }


  /**
  * Permite mostrar el historial de credito 
  * de cada usuario dependiendo la opcion seleccionada
  * ya sea creditos cancelados, vigentes, pendientes
  */
  public function actionHistoricoCreditos(){
      
      $idClient   = $_POST['id'];
      $option     = $_POST['options'];

      if ($option == 0){
          $title = 'Cantidad de Creditos';
          $dataProvider = new ActiveDataProvider([           
                              'query' => Loan::find()->where(['id_client'=>$idClient]),
                               'sort' => [
                                  'defaultOrder' => [
                                      'date_add' => SORT_DESC,                
                                ]
                              ],
                          ]);
      
      }else{
          
        if ($option==Yii::$app->params['status_pendiente'])
            $title = 'Credito en Espera';
        else if ($option==Yii::$app->params['status_vigente'])      
            $title = 'Credito Vigente';
        else if ($option==Yii::$app->params['status_pagados'])      
            $title = 'Creditos Cancelados';

        $dataProvider = new ActiveDataProvider([           
                        'query' => Loan::find()->where(['id_client'=>$idClient,'id_status'=>$option]),
                         'sort' => [
                            'defaultOrder' => [
                                'date_add' => SORT_DESC,                
                          ]
                        ],
                    ]);
      }

      return $this->renderPartial('_quantityLoans',[
        'title' => $title,
        'dataProvider' => $dataProvider,
      ]);        
  }

  /**
  * Permite mostrar el historial de comisiones
  * de cada usuario dependiendo la opcion seleccionada
  * ya sea comisiones pendientes, canceladas, en mora
  */
  public function actionHistoricoComisiones(){
      
      $client = $_POST['id'];
      $option = $_POST['options'];
      $arrayId = array();
      
      if ($option == Yii::$app->params['commission_activa'])            
          $title = 'Comisiones Pendientes';            
      else if ($option == Yii::$app->params['commission_pagada'])
          $title = 'Comisiones Canceladas';        
      else if ($option == Yii::$app->params['commission_bloqueada'])
          $title = 'Comisiones Perdidas';
      

      $model = CommissionRound::find()
                          ->where([
                            'id_client' => $client,
                            'id_status'=>$option
                          ])
                          ->asArray()
                          ->all();
              
      foreach ($model as $key => $value) {
        array_push($arrayId,$value['id']);
      }        
      
      $dataProvider = new ActiveDataProvider([           
          'query' => Commission::find()
                            ->where(['id_commission_round'=>$arrayId]),                          
      ]);
    
      return $this->renderPartial('_details-commissions',[
          'dataProvider' => $dataProvider,
          'title' => $title
      ]);

          
      
  }

  /**
  * Permite mostrar con detalles las 
  * cuotas de cualquier credito seleccionado    
  */
  public function actionMostrarCuotas(){

    $modelLoan = new Loan();
    
    $id_quota = $_POST['id'];
    $id_client = $_POST['clnt'];
    
    $modLoan = Loan::find()->where(['id_client'=>$id_client])->one();

    $query = LoanQuota::find()->where(['id_loan'=>$modLoan->id])
                                  ->with(['idStatus'])
                                  ->orderBy('n_quota ASC');

    $modQuota = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [],
        'sort' => false,
        
    ]);

    $dataProvider = new ActiveDataProvider([           
                          'query' => LoanQuota::find()->where(['id_loan'=>$id_quota]),                          
                          'sort' => [
                                  'defaultOrder' => [
                                      'n_quota' => SORT_ASC,                
                                ]
                          ],
                    ]);


    return $this->renderPartial('_details-quotas',[
      'dataProvider' => $dataProvider,
      'modLoan' => $modLoan,
      'modelLoan' => $modelLoan,
      'modQuota' => $modQuota,
    ]);

  }

  /**
   * Finds the Clients model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Clients the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id){
      
      if (($model = Clients::findOne($id)) !== null) {
          return $model;
      } else {
          throw new NotFoundHttpException('The requested page does not exist.');
      }
  }

  /**
  * Metodo que permite obtener a todos los ahijados
  * pertenecientes al padrino.
  * @param integer id
  * @return ahijados cargados en el modelo
  */
  protected function findAhijados($id){


    return $dataProvider = new ActiveDataProvider([           
          'query' => Clients::find()->where(['id_godfather'=>$id]),
           'sort' => [
              'defaultOrder' => [
                  'date_add' => SORT_DESC,                
            ]
          ],
      ]);       
  }
                                            
}
