<?php

namespace backend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\User;
use app\models\Status;
use app\models\ClientInvitation;
use app\models\Clients;
use app\models\UploadForm;
use yii\web\UploadedFile;   
use moonland\phpexcel\Excel;


/**
 * ClientInvitationController implements the CRUD actions for ClientInvitation model.
 */
class ClientInvitationController extends Controller{
    
    public function behaviors(){
        return [
              'access' => [
                'class' => AccessControl::className(),                                           
                'rules' => [               
                    [
                        'actions' => ['index','view','create','update','delete','logout'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rule,$action){
                            return User::userRoles(Yii::$app->user->identity->id,Yii::$app->params['superUserRole']);                                             
                        },                                         
                    ],                                                                                       
                ],                
               'denyCallback' => function ($rule, $action) {                    
                   $this->redirect(Yii::$app->user->loginUrl);                   
//                       throw new \Exception('aaa are not allowed to access this page');
                },  
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ClientInvitation models.
     * @return mixed
     */
    public function actionIndex(){
                
        $model = new UploadForm();
        
        $dataProvider = new ActiveDataProvider([
                'query' => ClientInvitation::find(),                        
                'pagination' => [
                    'pageSize' => 15,            
                ],  
		'sort' => [
                    'defaultOrder' => [
                        'date_exp' => SORT_DESC,                        
                    ]
                ],
        ]);
                              
        if (Yii::$app->request->isPost) {
        
            
            $excel          = new Excel();       // Phpexcel                     
            $errorEmail     = '';
            $fecha          = date("Y-m-d");
        
            // se obtiene el archivo 
            $fileName = UploadedFile::getInstance($model, 'importInvitation')->tempName;
            $model->scenario = 'invitations';
            $model->importInvitation = $fileName;

            if($model->validate()){            
                
                $data = Excel::widget([
                    'mode' => 'import', 
                    'fileName' => $fileName, 
                    'setFirstTitle' =>true,
                    'setFirstRecordAsKeys' => false, 
                    'setIndexSheetByName' => true, 
                    'getOnlySheet' => 'invitaciones', 
                ]);
                
                // Permite conocer si el archivo esta vacio o no!!
                if (sizeof($data) == 4){
                    \Yii::$app->session->setFlash('warning', 'Revise el archivo a importar ya que esta vacio!!');

                    return $this->render('index', [
                        'model' => $model,
                        'dataProvider' => $dataProvider,
                    ]);
                    
                }else{

                    
                    
                    $i = 0;             
                    foreach ($data as $key => $value){                     


                        if ($i>3){                    

                                $modelClientInv = new ClientInvitation();                                      
                                $modelClientInv->invitation_code = $modelClientInv->getInvitationCode();
                                $modelClientInv->id_godfather    = 0;
                                $modelClientInv->id_user_system  = Yii::$app->user->identity->id;
                                $modelClientInv->id_status       = Yii::$app->params['invi_espera'];
                                $modelClientInv->date_exp        = date("Y-m-d", strtotime("$fecha +15 days")); // Fecha de Expiracion                             
                                
                                // verifica si el email existe registrado entre los clientes
                                $validarClients = Clients::find()
                                                        ->where( [ 'email' => strtolower($value['B']) ] )
                                                        ->exists(); 

                                // verficica si el email existe registrado entre los clientes invitados
                                $validarClientsInv = ClientInvitation::find()
                                                                    ->where( [ 'email' => strtolower($value['B']) ] )
                                                                    ->exists();                                                             

                                if ($validarClients == false && $validarClientsInv == false){                                    
                                
                                    $modelClientInv->name            = $value['A'];
                                    $modelClientInv->email           = strtolower($value['B']);                                
                                    $modelClientInv->cant_godson     = $value['C'];

                                    if ($modelClientInv->save()){

                                         // se encarga de enviar la invitacion al usuario registrado                 
                                        $modelClientInv->sentInvitationUser( Yii::$app->user->identity->email,
                                                                             $value['B'],
                                                                             $modelClientInv->name,
                                                                             $modelClientInv->invitation_code );
                                    }

                                }else{                                        
                                    // Contiene todos los email a quienes no se le envio una invitacion
                                    $errorEmail .= $value['B'].'<br />';                             
                                }
                        }   
                        

                        $i++;
                    }
                }
                
                if (!empty($errorEmail)){
                    \Yii::$app->session->setFlash('info', 'Estos correos electronicos ya se encuentran registrados en el sistema: <br /><strong>'.$errorEmail.'</strong> Por lo tanto no se les envio una invitacion');                
                }else{
                    \Yii::$app->session->setFlash('success', 'Se han enviado las invitaciones satisfactoriamente a sus destinatarios');
                }
                
            }   

        }
                                         
        return $this->render('index', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
            
    }

    /**
     * Displays a single ClientInvitation model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id){
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ClientInvitation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate(){

        $model          = new ClientInvitation();                         
        $modelClient    = new Clients();
        $fecha          = date("Y-m-d");
        
        if ($model->load(Yii::$app->request->post())) {
                                
            // verifica si el email existe registrado entre los clientes
            $validarEmail = $modelClient::find()
                                        ->where( [ 'email' => strtolower($model->email) ] )
                                        ->exists(); 
                   
            
            $model->email           = strtolower($model->email);
            $model->invitation_code = $model->getInvitationCode();
            $model->id_godfather    = 0;
            $model->id_user_system  = Yii::$app->user->identity->id;
            $model->id_status       = Yii::$app->params['invi_espera'];
            $model->date_exp        = date("Y-m-d", strtotime("$fecha +15 days")); // Fecha de Expiracion                 
    	    $model->date_add	    = $fecha;
            
            if (!($validarEmail) && $model->save()){
                
                     \Yii::$app->session->setFlash('success', 'Se ha enviado la invitacion satisfactoriamente al email <b>"'.$model->email.'"</b>>');

                    // se encarga de enviar la invitacion al usuario registrado                 
                    $model->sentInvitationUser( Yii::$app->user->identity->email,
                                                $model->email,$model->name,
                                                $model->invitation_code );
                

                    return $this->redirect(['view', 'id' => $model->id]);        
                
            }else{
                    
                if ($validarEmail)
                    \Yii::$app->session->setFlash('error', 'Este email <b>"'.$model->email.'"</b> le pertenece a un Cliente en  <b>TuPadrino.net</b>');
                    
                return $this->render('create', [
                        'model'  => $model,
                    ]);
                }                                                             
                         
        }else {
                return $this->render('create', [
                    'model'  => $model,
                ]);
        }   
    }

    /**
     * Updates an existing ClientInvitation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id){
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ClientInvitation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id){
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ClientInvitation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ClientInvitation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id){
        if (($model = ClientInvitation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
