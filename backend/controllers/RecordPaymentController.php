<?php

namespace backend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use app\models\RecordPayment;
use app\models\RecordPaymentSearch;
use app\models\QuotaPayment;
use app\models\LoanQuota;
use app\models\User;
use app\models\Banks;
use app\models\UploadForm;
use yii\web\UploadedFile;   
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use moonland\phpexcel\Excel;

/**
 * RecordPaymentController implements the CRUD actions for RecordPayment model.
 */
class RecordPaymentController extends Controller
{
    public function behaviors(){
        return [
              'access' => [
                'class' => AccessControl::className(),                                           
                'rules' => [               
                    [
                        'actions' => [
			  'index',
			  'view',
			  'delete',
			  'logout',
			  'pendientes',
			  'registro-pagos',
			  'mostrar-registros',
                          'match-cobranza-auto',
			  'match-cobranza-manual'
			],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rule,$action){
                            return User::userRoles(Yii::$app->user->identity->id,Yii::$app->params['superUserRole']);                                             
                        },                                         
                    ],                                                                                       
                ],                
               'denyCallback' => function ($rule, $action) {                    
                   $this->redirect(Yii::$app->user->loginUrl);                   
//                       throw new \Exception('aaa are not allowed to access this page');
                },  
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all RecordPayment models.
     * @return mixed
     */
    public function actionIndex(){
        
        $modelRecordPayment = new RecordPayment();
        $searchModel = new RecordPaymentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                

       /**
       * DataProvider especificamente para los clientes 
       * que registraron su el dia actual
       * "CPH" (Cobranza Pendientes Hoy)
       */         
        $dataProviderCPH = new ActiveDataProvider([
            
            'query' => RecordPayment::find()
                             ->where(['id_status'=>Yii::$app->params['procesando'],
                                      'date_add'=> date('Y-m-d')]),                                            
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,                
                ]
            ],
        ]);
         
       /**
       * DataProvider especificamente para los clientes 
       * que registraron su pago y esos fueron confirmados
       * "CA" (Cobranza Aprobada)
       */         
        $dataProviderCA = new ActiveDataProvider([
            
            'query' => RecordPayment::find()
                             ->where(['id_status'=>Yii::$app->params['confirmado']]),                                            
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,                
                ]
            ],
        ]);

        /**
         * DataProvider especificamente para los clientes 
         * que registraron su pago y esos fueron confirmados
         * "CR" (Cobranza Rechazada)
         */         
        $dataProviderCR = new ActiveDataProvider([
            
            'query' => RecordPayment::find()
                             ->where(['id_status'=>Yii::$app->params['rechazado']]),                                            
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,                
                ]
            ],
        ]);

        // Permite obtener una lista de las cobranzas que tienen estatus
        // de procesando
        $listCP = $modelRecordPayment::find()
                                    ->where(['id_status'=>Yii::$app->params['procesando']])
                                    ->all();

        return $this->render('index', [
            'modelRecordPayment' => $modelRecordPayment,
            'listCP' => $listCP,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProviderCPH' => $dataProviderCPH,
            'dataProviderCA' => $dataProviderCA,
            'dataProviderCR' => $dataProviderCR,
            'dateRecordPayment' => $this->findRecordPay(),             
        ]);
    }


    /**
     * Muestra los registros de pagos
     * de acuerdo a la fecha seleccionada
     */
    public function actionMostrarRegistros(){

      $model = new RecordPayment();
      $modelUpload    = new UploadForm();
      $modelBank = new Banks();
      // $option = '';      
      // lo que se obtiene del post
      if (Yii::$app->request->post()){
        
        $post = Yii::$app->request->post();
        $date = $post['fecha'];                        
        $aux  = $post['option'] == Yii::$app->params['cAuto'] ? true : false;      
      }              

      $banks = $modelBank::find()
                ->where(['id'=>[3,31]])
                ->all();
      /**
       * DataProvider especificamente para los clientes 
       * que registraron su pago en una fecha en especifico       
       */         
      $dataProvider = new ActiveDataProvider([
          
          'query' => $model::find()
                           ->where(['date_add'=> $date,
                                    'id_status'=>Yii::$app->params['procesando']]),                                            
          'pagination' => [
              'pageSize' => 15,
          ],
          'sort' => [
              'defaultOrder' => [
                  'id' => SORT_DESC,                
              ]
          ],         
      ]);
      
      // Array para enviar mediante 
      $array = array(
          'view' => $this->renderPartial('mostrar-registros',[
                      'dataProvider' => $dataProvider,
                      'date' => $date,
                      'option' => $aux,
                      'modelUpload' => $modelUpload,
                      'banks' => $banks
                    ]),
          'option'=> $aux
        );      
      
      return json_encode($array);

    }

    /**
     * Displays a single RecordPayment model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RecordPayment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RecordPayment();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    /**
     * Updates an existing RecordPayment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RecordPayment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id){
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
    * Permite conciliar los pagos registrados de manera 
    * automatica mediante la importacion de un archivo CSV
    * que otorga el banco
    */
    public function actionMatchCobranzaAuto(){

	$modelUpload = new UploadForm();      
      
      if (Yii::$app->request->isPost){
        
        $post   = Yii::$app->request->Post();    // todo lo que viene por el post
        $cFecha = $post['cFecha']; // fecha seleccionada en cobranza                  
        $fileName = UploadedFile::getInstance($modelUpload, 'importCob'); //contiene el archivo importado

        if ($fileName ==  null){
          \Yii::$app->session->setFlash('warning', 'Debe seleccionar un archivo a importar para hacer la conciliacion automatica!!');
          return $this->redirect(['index']);                                
        }    
        

        $modelUpload->importCob = $fileName;
        $modelUpload->scenario = 'matchCob';      


        if($modelUpload->validate()){

            $modelRecordPayment = RecordPayment::find()
                                        ->where([
                                            'date_add'=>$cFecha,
                                            'id_status'=>Yii::$app->params['procesando']
                                          ]);

            $rows = $modelRecordPayment->all(); // modelo con registros obtenidos
            $count = $modelRecordPayment->count(); // cantindad de registros

          /**
           * Formato para Banco de Venezuela
           */
          if ($post['code_bank'] == "0102"){                  
            
	    if($fileName->getExtension() == 'txt'){
              \Yii::$app->session->setFlash('danger', 'El formato del archivo no es compatible!!');
              return $this->redirect(['index']);                                
            }

            $data = $this->importExcel($fileName->tempName);          

            // Permite conocer si el archivo esta vacio o no!!
            if (sizeof($data) == 1){        

              \Yii::$app->session->setFlash('warning', 'Revise el archivo a importar ya que esta vacio!!');
              return $this->redirect(['index']);                                

            }else{


              $i = 0;
              $conciliados = 0;     // lleva el conteo de la cantidad de clientes la cual su pago fue conciliado          

              // foreach para recorrer fila por fila del archivo
              foreach($data as $key => $value){                                 

                if($i>0){                            

                  // para obtener los pagos del dia especificado y asi recorrer uno a uno
                  // sus referencias para compararlas
                  foreach ($rows as $key => $value2) {              
                    
                    $reference = substr($value2->reference,-8);   // referencia obtenida de la bd 
                    $csvRow = substr($value['B'],-8);             // referencia obtenida del archivo CSV
                    
                    if ($reference == $csvRow){
                      
                      $modelQPay = new QuotaPayment();                   
                      $model = new RecordPayment();                  
                      $model = $model::findOne($value2->id);                      
                      
                      if (!empty($model)){
                        
                        $model->id_status = Yii::$app->params['confirmado'];                    
                        
                        // modifica el status y se verifica que fue modificado
                        if ($model->update() !== false) {

                          $modelQPay->quotaPay((float)$value['F'],$model->id,$model->id_client);                      
                          // $model->sentConfirmPay($model); // metodo que permite notificar al usuario sobre su conciliacion del pago
                          $conciliados++;                                  
                        }
                      }
                    }
                  }
                }
                $i++;
              }                                                          
            }
          
          /**
           * Formato para Banco BNC
           */
          }else if($post['code_bank'] == '0191'){
            
	     if($fileName->getExtension() != "txt" ){
              \Yii::$app->session->setFlash('danger', 'El formato del archivo no es compatible!!');
              return $this->redirect(['index']);                                
            }

            if(filesize($fileName->tempName) > 0){
              
              $fOpen = fopen($fileName->tempName, "r"); // Abre el archivo
              $fContent = fread($fOpen, filesize($fileName->tempName)); // se especifica el archivo a leer            
              $content = explode("\n", $fContent); // se lee fila por fila hasta el salto de linea        
              $lines = count(file($fileName->tempName));  // total de lineas

              $conciliados = 0;     // lleva el conteo de la cantidad de clientes la cual su pago fue conciliado          
              
              foreach ($rows as $key => $value2){        
                
                $i = 0;
                
                // foreach para recorer fila por fila del documento
                foreach ($content as $value) {                
                    
                    // lee la segunda linea del archivo
                    if($i>3 && $i < $lines){                    
                        
                        
                        $initRefer = strpos($value,'/ '); // inicio de la Referencia                    
                        $stReference = str_split(substr(str_replace(',','.',$value), $initRefer+2)); // lee desde la barra y el espacio
                        
                        // $stReference = str_replace(',','.',$stReference);
                        $reference = "";
                        $amount = "";                      
                        $j=0;

                        if (!in_array("/" ,$stReference)){

                          // obtiene referencia
                          foreach($stReference as $wr) {
                            
                              if (!ctype_space ($wr[0])){
                                  
                                if ($j == 2)
                                  $amount .= $wr[0];                              
                                else
                                  $reference .= $wr[0];                          
                              
                              }

                              else if (ctype_space ($wr[0]))
                                $j++;

                              if ($j==3)
                                break 1;  
                              
                          }
                                                                                              
                          // en? caso de que la referencia venga del banco de venezuela
                          if ($value2->code_bank_origin == "0102")
                            $referenceBd = substr($value2->reference,-8);   // referencia obtenida de la bd 
                          else
                            $referenceBd = $value2->reference;                    

                          if (substr((int)$reference,0,-1) === $referenceBd){
                                                        
                            $modelQPay = new QuotaPayment();                   
                            $model = new RecordPayment();                  
                            $model = $model::findOne($value2->id);                      
                            
                            if (!empty($model)){
                              
                              $model->id_status = Yii::$app->params['confirmado'];                    
                              
                              // modifica el status y se verifica que fue modificado
                              if ($model->update() !== false) {

                                $modelQPay->quotaPay((float)$amount,$model->id,$model->id_client);                      
                                // $model->sentConfirmPay($model); // metodo que permite notificar al usuario sobre su conciliacion del pago
                                $conciliados++;                                  
                              }
                            }
                          }                      
                        }                    
                    }                
                  $i++;
                }
              }             
            }
          }  
          \Yii::$app->session->setFlash('info', 'Fecha: '.Yii::$app->formatter->asDate($cFecha,'d/MM/Y').'
            <br /> Cantidad de Pagos Registrados: (<b>'.$count.'</b>)<br />Pagos Conciliados: (<b>'.$conciliados.'</b>)');
              
          return $this->redirect(['index']);  
        }
      }
    }

   /**
    * Permite conciliar los pagos registrados de manera 
    * manual verificando uno a uno los registros de pago    
    */
    public function actionMatchCobranzaManual(){

      $rows = $_POST['rows'];  // Lo que viene del ajax 
      
      $con = 0;     // cantidad de usuarios conciliados
      $noCon = 0;   // cantidad de usuarios no Conciliados
      $recha = 0;   // cantidad de usuarios con pagos Rechazados

      foreach ($rows as $key => $value) {

        $modelQPay = new QuotaPayment();
        $model = new RecordPayment();
        $model = $model::findOne($value['id']);
        
        // "No Recibido" == false        
        if ($value['disable']=="false"){
          
          // Status verificado
          if ($value['status']=="true"){          
            
            $model->id_status = Yii::$app->params['confirmado'];                                            
            $con++; // incrementamos la cantidad de pagos conciliados

          }else{
            $noCon++; // incrementamos la cantidad de pagos conciliados
          }
          
        }else{            

          // "No Recibido" == true
          $model->id_status = Yii::$app->params['rechazado'];
          $recha++; // incrementamos la cantidad pagos rechazados
        }
              
          if ($model->update() !== false) {
            
            if($model->id_status == Yii::$app->params['confirmado']){
	      $modelQPay->quotaPay($model->amount,$model->id,$model->id_client);                      
              $model->sentConfirmPay($model); // metodo que permite notificar al usuario sobre su conciliacion del pago               
            }else if($model->id_status == Yii::$app->params['rechazado']){
              $model->sentRejectedPay($model); // metodo que permite notificar al usuario sobre su conciliacion del pago               
	    }
          }

      }

      $arr = array('con'=>$con,'noCon'=>$noCon,'recha'=>$recha);

      return json_encode($arr);
        
    }

    /**
     * Finds the RecordPayment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RecordPayment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RecordPayment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
    * Encuentra todas aquellas fechas en las que se han 
    * registrado pagos por parte de los clientes pero que 
    * estos pagos no han sido verificados
    */
    protected function findRecordPay(){

        return RecordPayment::find()                                                
                            ->where(['id_status'=> Yii::$app->params['procesando']])                                        
                            ->asArray()
                            ->all();            
    }
}
