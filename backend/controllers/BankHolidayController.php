<?php

namespace backend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\UploadForm;
use app\models\BankHoliday;
use app\models\User;
use yii\web\UploadedFile;   
use moonland\phpexcel\Excel;


/**
 * BankHolidayController implements the CRUD actions for BankHoliday model.
 */
class BankHolidayController extends Controller
{
    public function behaviors()
    {
        return [
             'access' => [
                'class' => AccessControl::className(),                                           
                'rules' => [               
                    [
                        'actions' => ['index','view','create','update','delete','logout'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rule,$action){
                            return User::userRoles(Yii::$app->user->identity->id,Yii::$app->params['superUserRole']);                                                
                        },                                         
                    ],                                                                                       
                ],                
               'denyCallback' => function ($rule, $action) {                    
                   $this->redirect(Yii::$app->user->loginUrl);                   
                },  
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
         
    /**
     * Muestra el calendario Bancario del A&o en que estamos
     * y los que se deseen asi como tambien permite importar
     * las fechas de los siguientes anos mediante un formato
     * establecido en un archivo.
     * @return mixed
     */
    public function actionIndex(){                                           
        
        $model           = new UploadForm();  
        $modelHoliday    = new BankHoliday(); // Model BankHoliday
        
         if (Yii::$app->request->isPost) {
        
            $excel          = new Excel();       // Phpexcel                     
            $array_fechas   = array();           // dia feriado
            $array_cierres  = array();           // cierre bancario
             
            // se obtiene el archivo 
            $fileName = UploadedFile::getInstance($model, 'importFile')->tempName;
                     
            $data = Excel::widget([
                'mode' => 'import', 
                'fileName' => $fileName, 
                'setFirstTitle' =>true,
                'setFirstRecordAsKeys' => false, 
                'setIndexSheetByName' => true, 
                'getOnlySheet' => 'calendarioBancario', 
            ]);
            
            // Permite conocer si el archivo esta vacio o no!!
            if (sizeof($data) == 4){
                \Yii::$app->session->setFlash('warning', 'Revise el archivo a importar ya que esta vacio!!');

                 return $this->render('index', [
                    'dataProvider'  => $dataProvider,           
                    'model'         => $model,
                    'modelHoliday'  => $modelHoliday
                ]);
                
            }else{
                          

             $i = 0;             
             foreach ($data as $key => $value){                 
                 
                 // i mayor que 3 para tomar las fechas y los cierres
                 if ($i>3)                 
                    
                     if (!empty($value['A'] || $value['B'])){
                         array_push($array_fechas,$value['A']);  // fechas
                         array_push($array_cierres,$value['B']); // cierres
                     }                 
                 $i++;
             }
             
             if ($modelHoliday->validateDates($array_fechas))
                 $modelHoliday->saveHolidaysBank($array_fechas,$array_cierres);             
            
            } 
        }
        
        $dataProvider = new ActiveDataProvider([
            'query' => BankHoliday::find(),
	    'sort' => [
                       'defaultOrder' => [
                           'id' => SORT_DESC,                
                        ]
                    ],
        ]);
        
        return $this->render('index', [
            'dataProvider'  => $dataProvider,           
            'model'         => $model,
            'modelHoliday'  => $modelHoliday
        ]);
    }

    /**
     * Displays a single BankHoliday model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    /**
     * Creates a new BankHoliday model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BankHoliday();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing BankHoliday model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing BankHoliday model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BankHoliday model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BankHoliday the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BankHoliday::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
