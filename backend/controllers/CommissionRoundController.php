<?php

namespace backend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\models\CommissionRound;
use app\models\Commission;
use app\models\User;
use app\models\Status;
use app\models\Clients;
use app\models\CommissionRoundSearch;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * CommissionRoundController implements the CRUD actions for CommissionRound model.
 */
class CommissionRoundController extends Controller
{
    public function behaviors()
    {
        return [
        'access' => [
                'class' => AccessControl::className(),                                           
                'rules' => [               
                    [
                        'actions' => ['index','view','update','delete','logout'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rule,$action){                  
                            return User::userRoles(Yii::$app->user->identity->id,Yii::$app->params['superUserRole']);                                                
                        },                                         
                    ],                                                                                       
                ],                
               'denyCallback' => function ($rule, $action) {                    
                    $this->redirect(Yii::$app->user->loginUrl);                   
                },  
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CommissionRound models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModelCRS = new CommissionRoundSearch();        
        $searchModelCL  = new Clients();
        
        $dataProvider = $searchModelCRS->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModelCRS' => $searchModelCRS,
            'searchModelCL' => $searchModelCL,            
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CommissionRound model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id){

        return $this->render('view', [
            'model' => $this->findModel($id),
            'modelCommission' => $this->findCommissionsUsers($id)
        ]);
    }

    /**
     * Creates a new CommissionRound model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CommissionRound();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CommissionRound model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CommissionRound model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CommissionRound model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CommissionRound the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id){
        if (($model = CommissionRound::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
    * Busca a los ahijados que le generan comisiones al padrino 
    * seleccionado
    * @param integer $id
    * @return Commission el modelo cargado
    */
    protected function findCommissionsUsers($id){

        return $dataProvider = new ActiveDataProvider([
            'query' => Commission::find()
                                 ->where(['id_commission_round'=>$id]),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
        ]);
        
        // if(($model = Commission::find()->where(['id_commission_round'=>$id])->all() != null)){
        //     return $model;
        // }else{
        //     throw new NotFoundHttpException('The requested page does not exist.');   
        // }


    }
}
