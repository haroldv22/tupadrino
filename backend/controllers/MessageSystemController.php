<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\User;
use app\models\LevelAmount;
use app\models\Level;
use app\models\Clients;
use app\models\MessagesSystem;
use app\models\MessagesSms;

use mongosoft\soapclient\Client;



class MessageSystemController extends Controller
{

	public function behaviors(){
        return [
              'access' => [
                'class' => AccessControl::className(),                                           
                'rules' => [               
                    [
                        'actions' => ['index','view-sms','view-emails','compose-emails','sent-message','read-emails','read-sms','delete-emails','delete-sms'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rule,$action){
                            return User::userRoles(Yii::$app->user->identity->id,Yii::$app->params['superUserRole']);                                             
                        },                                         
                    ],                                                                                       
                ],                
               'denyCallback' => function ($rule, $action) {                    
                   $this->redirect(Yii::$app->user->loginUrl);                   
                },  
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    public function actionIndex(){

        $modelLevel = new LevelAmount();                    
        $modelMsgSys = new MessagesSystem();
        $modelMsgSms = new MessagesSms();
        
        return $this->render('index',[
            'msgsSent' => $modelMsgSys->count(Yii::$app->params['msg_enviado']),
            'msgsTrash' => $modelMsgSys->count(Yii::$app->params['msg_eliminado']),
            'msgsSms' => $modelMsgSms->count(Yii::$app->params['msg_enviado']),   

            // 'arrayAmounts'=> $modelLevel->findByAmounts(),
        ]);
    }

    /**
     * Permite enviar mensajes a los clientess
     * Seleccionados asi como tambien almacenar 
     * la informacion
     */
    public function actionSentMessage(){

        $modelClient = new Clients();
        $modelMsgSys = new MessagesSystem();
        $tedexis = Yii::$app->tedexisApi; // Api Tedexis

        $level      = $_POST['level']; // id_level
        $message    = $_POST['message']; // mensaje 
        $subject    = $_POST['subject']; // asunto 
        $sms        = $_POST['sms']; // Esta variable esta pendiente por ser desarrollada
        $arrEmails  = array();
    
        $modelClient = $modelClient->findByLevelClient($level);
        
        if(!empty($modelClient)){

            foreach ($modelClient as $key => $value){                        
                
                // Verifica que el registro sea almacenado de ser asi envia el mensaje via email
                if($modelMsgSys->saveMsgSys($subject,$message,$value->id)){
                    $modelMsgSys->sentMessage($value->Shortname(),$subject,$message,$value->email);  // envia el mensaje via email
		    
  	//	    $tedexis = Yii::$app->tedexisApi; // Api Tedexis

                    // Parametros necesarios para el envio del sms
                    $parmsSms = array(
                        'passport'=> $tedexis->options['passport'],
                        'password'=> $tedexis->options['password'],
                        'number'=>'58'.$value->getAddresses()->one()->cellphone,
                        'text' => $sms);
		//  var_dump($parmsSms);
                    $tedexis->sendSMS($parmsSms);
		}else{
                    array_push($arrEmails,$value->email);                    
                }
            }
            // Verificando si existe algun emaill a quien no le fue
            // enviado el mensaje
            if(empty($arrEmails)){
                // \Yii::$app->session->setFlash('success', 'Su mensaje fue enviado Satisfactoriamente a todos los clientes!!');
                return true;
            }else{
                // \Yii::$app->session->setFlash('danger', 'Ocurrio un error al momento de enviar el mensaje!!');
                return false;
            }
            
        }
    }
    

    /**
     * Permite mostrar los mensajes enviados y en la papelera
     * por correo electronico
     */
    public function actionViewEmails(){

//        $id_status = $_POST['status'];
        $modelMsgSys = new MessagesSystem();
	$query = $modelMsgSys::find()->where(['id_status'=>Yii::$app->params['msg_enviado']]);
        $pages = new Pagination(['totalCount' => $query->count()]);

        $models = $query->offset($pages->offset)
                        ->limit($pages->limit)
			->orderBy(['id'=> SORT_DESC])
                        ->all();
        
        return $this->renderPartial('sent-emails',[
            'model'=> $models,
            'pages'=> $pages
        ]);

    }

   /**
    * Permite mostrar los mensajes de textos
    * enviados a los clientes     
    */
    public function actionViewSms(){

        $model = new MessagesSms();

        $model = $model->findSmsByStatus(Yii::$app->params['msg_enviado']);

//        $pages = new Pagination(['totalCount'=> $model->count()]);

  //      $models = $model->offset($pages->offset)
    //                ->limit($pages->limit)
//                    ->all();


        return $this->renderPartial('sent-sms',[
            'model' => $model,
  //          'pages' => $pages
        ]);

    }

    /**
     * Permite mostrar el panel para
     * redactar un mensaje
     */
    public function actionComposeEmails(){

        return $this->renderPartial('compose-emails');
    }

    /**
     * Permite leer el mensaje enviado que fue
     * seleccionado
     */
    public function actionReadEmails(){

        $modelMsgSys = new MessagesSystem();

        $id = $_POST['key'];                

        return $this->renderPartial('read-emails',[
            'model' => $modelMsgSys::findOne($id)
        ]);
    
    }

    /**
     * Permite leer el mensaje enviado que fue
     * seleccionado
     */
    public function actionReadSms(){

        $modelMsgSms = new MessagesSms();

        $model = $modelMsgSms::findOne($_POST['key']);      


        return $this->renderPartial('read-sms',[
            'model' => $model
        ]);
    
    }

    /**
     * Permite Eliminar el Mensaje enviandolo
     * a la Papelera (Cambiando el Status)
     */
    public function actionDeleteEmails(){
        
        $arrEmails = $_POST['id'];        

        foreach ($arrEmails as $key => $value) {
            
            $modelMsgSys = new MessagesSystem();        
            
            $modelMsgSys = $modelMsgSys::find()
                                    ->where(['id'=>$value])
                                    ->one();
            
            $modelMsgSys->id_status = Yii::$app->params['msg_eliminado'];
            $modelMsgSys->update();                        
        }
            
    }

    /**
     * Permite Eliminar el Mensaje enviandolo
     * a la Papelera (Cambiando el Status)
     */
    public function actionDeleteSms(){
        
        $arrEmails = $_POST['id'];        

        foreach ($arrEmails as $key => $value) {
            
            $modelMsgSms = new MessagesSms();        
            
            $modelMsgSms = $modelMsgSms::find()
                                    ->where(['id'=>$value])
                                    ->one();
            
            $modelMsgSms->id_status = Yii::$app->params['msg_eliminado'];
            $modelMsgSms->update();                        
        }
            
    }

}
