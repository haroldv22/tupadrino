<?php

namespace backend\controllers;

use yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\User;
use app\models\Clients;
use app\models\Loan;
use app\models\MessagesSystem;
use app\models\MessagesSms;
use app\models\Arrears;
use app\models\ClientInvitation;
use yii\db\Query;
use yii\data\Pagination;
use yii\web\Request;


class ConsultaController extends Controller{

	 public function behaviors(){
        return [
             'access' => [
                'class' => AccessControl::className(),                                           
                'rules' => [               
                    [
                        'actions' => ['login', 'error',],
                        'allow' => true,
                        'roles' => ['?'],                                                 
                    ],
                    [
                        'actions' => [
				'index',
				'ver-ahijados',
				'ver-prestamos',
				'consultas-ahijados',
				'consultas-prestamos',
				'messages'
			],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rule,$action){                  
                            return User::userRoles(Yii::$app->user->identity->id,Yii::$app->params['superUserRole']);                                                
                        },                                         
                    ],                                                                                       
                ],                
               'denyCallback' => function ($rule, $action) {                    
                    $this->redirect(Yii::$app->user->loginUrl);                   
                },  
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * [actionIndex index]
     * @return [view] [view index]
     */
    public function actionIndex(){
        
        $selected = 0;        
        $gridView = '';
        $model = new Loan();

        
        return $this->render('index',[
            'selected' => $selected,
            'gridView' => $gridView,
            'model' => $model
        ]);
    }

    /**
     * [actionVerAhijados Permite ver a todos
     * los ahijados Activos e Inactivos Mujeres y Hombres
     * del sistema]
     * @param  [integer] opt [Opcion seleccionada en el Dashboard (Ahijados Ver Mas)]
     * @return [render] [vista consulta/index.php]
     */
    public function actionVerAhijados($opt){
        
        // dataProvider de tabla Clients        
        $dataProvider = new ActiveDataProvider([
            'query' => ClientInvitation::find(),                        
            'sort' => [
                    'defaultOrder' => [
                        'date_add' => SORT_DESC,
                    ]
                ],
        ]);

        $gridView = $this->renderPartial('gridViewAhijados',[
            'dataProvider' => $dataProvider
        ]);

        return $this->render('index',[
            'selected' => $opt,
            'gridView' => $gridView,
        ]);
    }

    /**
     * [actionVerPrestamos muestra las consultas a los 
     * Prestamos Activos / En Riesgo / En Mora ]
     * @param  [integer] $opt  [opcion opcion escogida para e dropdownlist]
     * @param  [integer] $stat [id_status id del status]
     * @return [type]       [description]
     */
    public function actionVerPrestamos($opt,$stat){    

	$modelLoan = new Loan();
        $modelArrears = new Arrears();
        
        switch ($stat) {            
            
            case $stat == Yii::$app->params['consulActivos']:
                $query = $modelLoan->findAllByStatus(
                    [Yii::$app->params['status_vigente'],
                     Yii::$app->params['status_vencido'],
                     Yii::$app->params['status_mora'] ]);                 

                $sumTotal = $modelLoan->getSumAmountByGender(
                    [ Yii::$app->params["genderM"],
                      Yii::$app->params["genderF"] ],
                    [ Yii::$app->params['status_vigente'],
                      Yii::$app->params['status_vencido'],
                      Yii::$app->params['status_mora'] ]); 
                $aux = 'date_add';
                break;
            
            case $stat == Yii::$app->params['consulAlDia']:
                $query = $modelLoan->findAllByStatus(Yii::$app->params['status_vigente']);
                $sumTotal = $modelLoan->getSumAmountByGender(
                    [ Yii::$app->params["genderM"],
                      Yii::$app->params["genderF"] ],
                      Yii::$app->params['status_vigente']); 
                $aux = 'date_add';
                break;            

            case $stat == Yii::$app->params['consulEnMora']:
		
		$query = $modelArrears->findAllByMoraGender([
                    Yii::$app->params["genderM"],
                    Yii::$app->params["genderF"] 
                ]); 

                $sumTotal = $modelArrears->getSumMoraAllByGender(
                    [ Yii::$app->params["genderM"],
                      Yii::$app->params["genderF"] ]); 
                $aux = 'add_date';
                break;
        }                

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
/*            'sort' => [
                    'defaultOrder' => [
                        $aux => SORT_DESC,
                    ]
            ],*/
	    'pagination' => [
                'pageSize' => 20,   
            ],

        ]);
                                                                
        $gridView = $this->renderPartial('gridViewPrestamos',[
            'dataProvider' => $dataProvider,
            'total' => $sumTotal
        ]);

        return $this->render('index',[
            'selected' => $opt,
            'gridView' => $gridView            
        ]);
    }

/**
     * [actionConsultasAhijados Hace la consulta
     * directamente en la seccion a los ahijados
     * esten activos o inactivos o ambos]
     * @return [view] [vista renderPartial de gridViewAhijados]
     */
    public function actionConsultasAhijados(){
    
        $modelClientInv = new ClientInvitation();        
        $modelClient = new Clients();    
                    
        $status = $_POST['estatus']; // activo o inactivo o ambos
        $opcion = $_POST['opcionFecha']; // opcion escogida fechas (hoy, mes, totales, rango)

        //opcion por fecha por rango
        if($opcion == 4){
            
            $desde = $_POST['fecha']['desde'];
            $hasta = $_POST['fecha']['hasta'];

            // opcion Activos+Inactivos
            if($status == 3){                    

                $query = ClientInvitation::find()
                    ->where(['between','date_add',$desde,$hasta]);                    

            }else{

                $query = ClientInvitation::find()
                    ->where(['id_status'=>$status])
                    ->andWhere(['between','date_add',$desde,$hasta]);
            }
                                
         // opcion fecha por mes
        }else if($opcion == 2){

            $fecha = $_POST['fecha'];

                // Activos+Inactivos 
            if ($status == 3 ){

                $query = ClientInvitation::find()                        
                    ->where(" date_part('MONTH', date_add) = $fecha");
            
            }else{
                // Activos || Inactivos
                $query = ClientInvitation::find()
                    ->where(['id_status' => $status])
                    ->andWhere(" date_part('MONTH', date_add) = $fecha");
            }
                    
        // opcion fecha por Totales
        }else if ($opcion == 3){

            $fecha = $_POST['fecha'];

            // Activos+Inactivos 
            if ($status == 3 ){

                $query = ClientInvitation::find();                                                
            
            }else{
                // Activos || Inactivos
                $query = ClientInvitation::find()
                    ->where(['id_status' => $status]);
            }

        // opcion fecha por Hoy
        }else if ($opcion == 1){

            $fecha = $_POST['fecha'];

            if($status == 3){
                $query = ClientInvitation::find()
                    ->where(['date_add'=>$fecha]);
            }else{
                $query = ClientInvitation::find()
                    ->where(['date_add'=>$fecha, 'id_status'=>$status]);
            }

        }

        //data Provider tabla ClientInvitation ACTIVOS O INACTIVOS
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
	    'pagination' => false,
            'sort' => [
                    'defaultOrder' => [
                        'date_add' => SORT_DESC,
                    ]
               ],
        ]);

        // envia los datos al gridView para Ahijados
        return $this->renderPartial('gridViewAhijados',[
            'dataProvider' => $dataProvider
        ]);  
            
    }

    /**
     * [actionConsultaPrestamos description]
     * @return [type] [description]
     */
    public function actionConsultasPrestamos(){

        $modelLoan = new Loan();
        $modelArrears = new Arrears();            
        $session = Yii::$app->session;
        
        $post = Yii::$app->request->post();
        

        if(!empty($post)){                    
                        
            $opcion = $post['opcionFecha']; // opcion escogida fechas (hoy, mes, totales, rango)                            
            $status = $post['status']; // status del prestamo Vigente|Riesgo|Mora
            $gender = $post['genero']; // genero a buscar
            $opcion = $post['opcionFecha']; // opcion escogida fechas (hoy, mes, totales, rango)                            
            
            if ($gender == "ambos")
                $gender = [ Yii::$app->params['genderF'],Yii::$app->params['genderM'] ];        
                                
            if($status == Yii::$app->params['status_vigente']){        
                $status =  [ Yii::$app->params['status_vigente'],
                        Yii::$app->params['status_vencido'],
                        Yii::$app->params['status_mora'] ];

                $selected = Yii::$app->params['status_vigente'];
            
            }else if ($status == Yii::$app->params['status_vencido']){
                $status = Yii::$app->params['status_vigente'];    
                $selected = Yii::$app->params['status_vencido'];
            }

            if ($status != $session['status']){

                unset($session['dataProvider']);
                unset($session['query']);
                unset($session['aux']);
                unset($session['sumTotal']);
                unset($session['status']);

            }

            // Total de Prestamo: 
            if($status == Yii::$app->params['status_mora'])
                $sumTotal = $modelArrears->getSumMoraAllByGender($gender);         
            else
                $sumTotal = $modelLoan->getSumAmountByGender($gender,$status); 
                                         
            
            switch ($opcion) {
                case 4:
                    $desde = $post['fecha']['desde'];
                    $hasta = $post['fecha']['hasta'];

                    if($status == Yii::$app->params['status_mora']){                    
                        $query = $modelArrears->findByMoraBetweenDate($gender,$desde,$hasta);
                    }else{
                        $query = $modelLoan->findByStatusBetweenDate(
                            $status,
                            $desde,
                            $hasta,
                            $gender
                        );                          
                        $aux =  "date_add";
                    }
                break;
                
                case 3:
                    $fecha = $post['fecha'];

                    if($status == Yii::$app->params['status_mora']){
                        $query = $modelArrears->findAllByMoraGender($gender);
                    }else{
	                  $query = $modelLoan->findAllByGenderStatus($gender,$status);   
                    }
                break;
                
                case 2:

                    $fecha = $post['fecha'];

                    if($status == Yii::$app->params['status_mora']){
                        $query = $modelArrears->findAllByMoraGenderMonth($gender,$fecha);
                     
                    }else{                    
                        $query = $modelLoan->findByStatusGenderMonth($status,$gender,$fecha);
                    }

                break;
                case 1:
                    
                    $fecha = $post['fecha'];

                    if($status == Yii::$app->params['status_mora']){
                        $query = $modelArrears->findAllByMoraToday($gender,$fecha);
                    }else{
                        $query = $modelLoan->findAllByToday($status,$gender,$fecha);
                    }                        
                break;
            }            

        }else{

            // $session['dataProvider'] = $dataProvider;
            $query = $session['query'];
            $sumTotal = $session['sumTotal'];
            $status =  $session['status'] ;

        }
         
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
/*            'sort' => [
                    'defaultOrder' => [
                        $aux => SORT_DESC,
                    ],                    
            ],*/
	    'pagination' => false,
        ]);       
	
	if ($dataProvider->totalCount == 0) 
            $sumTotal = 0;

//        $session['dataProvider'] = $dataProvider;
        $session['query'] = $query;
        $session['sumTotal'] = $sumTotal;
        $session['status'] = $status;
            
        // envia los datos al gridView para Ahijados
        $gridView =  $this->renderPartial('gridViewPrestamos',[
            'dataProvider' => $dataProvider,
            'total' => $sumTotal            
        ]);  

        return $gridView;

        // return $this->render('index',[
        //     'gridView' => $gridView,
        //     'selected' => $selected
        // ]);
            
    }    

   /**
     * [actionMessages envia mensajes 
     * a los usuarios las cuales estan siendo consultados]
     * @return [boolean] true [devuelve un true para indicar
     * que envio los mensajes]
     */
    public function actionMessages(){

        $modelMessageSystem = new MessagesSystem();
        $modelMessagesSms = new MessagesSms();

        $post = Yii::$app->request->post();
        $subject = $post['subject'];
        $emailText = $post['emailTxt'];
        $smsText = $post['smsTxt'];
        
        foreach ($post['clients'] as $key => $value) {

            if (!empty($smsText)){
                $modelMessagesSms->saveSms($value['id_client'],$smsText);
                $modelMessagesSms->sentSMS($value['phone'],$smsText);
            }
            
            if(!empty($subject) && !empty($emailText)){
                $modelMessageSystem->saveMsgSys($subject,$emailText,$value['id_client']);                
                $modelMessageSystem->sentMessage($value['fullname'],$subject,$emailText,$value['email']);
            }

        }

    }

}

