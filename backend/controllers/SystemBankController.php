<?php

namespace backend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\SystemBankAccount;
use app\models\User;
use app\models\Banks;

/**
 * SystemBankController implements the CRUD actions for SystemBankAccount model.
 */
class SystemBankController extends Controller
{
    public function behaviors()
    {
        return [
        'access' => [ 
               'class' => AccessControl::className(), 
               'rules' => [               
                   [ 
                       'actions' => ['index','view','create','update','delete','logout'], 
                       'allow' => true, 
                       'roles' => ['@'], 
                       'matchCallback' => function($rule,$action){ 
                           return User::userRoles(Yii::$app->user->identity->id,Yii::$app->params['superUserRole']);      
                       },     
                   ],                                                                                       
               ],                
               'denyCallback' => function ($rule, $action) { 
                   $this->redirect(Yii::$app->user->loginUrl);                   
//                   throw new \Exception('You are not allowed to access this page'); 
               },   
           ], 
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SystemBankAccount models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => SystemBankAccount::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SystemBankAccount model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SystemBankAccount model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SystemBankAccount();

        $listBank = Banks::find()->all(); // lista de bancos 

        if ($model->load(Yii::$app->request->post()) && $model->save()) {            
            return $this->redirect(['view', 'id' => $model->id]);        
        } else {
            return $this->render('create', [
                'model' => $model,
                'listBank' => $listBank
            ]);
        }
    }

    /**
     * Updates an existing SystemBankAccount model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $listBank = Banks::findAll($model->code_bank);  // lista de bancos 

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'listBank'=> $listBank
            ]);
        }
    }

    /**
     * Deletes an existing SystemBankAccount model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id){
	
	if ($this->countRowsModel()>1)
            $this->findModel($id)->delete();        
        else
            \Yii::$app->session->setFlash('danger', 'Esta cuenta no puede ser eliminada debe registrar una nueva');
        
        return $this->redirect(['index']);
    }

    /**
     * Finds the SystemBankAccount model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SystemBankAccount the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SystemBankAccount::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * [countRowsModel Verifica si cuantas cuentas 
     * bancarias se encuentran registradas]
     * @return [integer] [numero de cuentas]
     */
    protected function countRowsModel(){
        return SystemBankAccount::find()->count();
    }

}
