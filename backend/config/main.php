<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'language'=>'es',
   // 'defaultRoute'=>'site/login',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [     

         'view' => [
         'theme' => [
             'pathMap' => [
                '@app/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/yiisoft/yii2-app'
             ],
         ],
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=localhost;port=5432;dbname=microdb',
            'username' => 'hvyv',
            'password' => 'perita01',
            'charset' => 'utf8',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
//            'enableAutoLogin' => true,
            'enableSession' => true,
            'authTimeout' => 300,
            'loginUrl' => ['site/login'],   
             'identityCookie' => [
                'name' => '_backendUser', // unique for backend
                'path'=>'http://158.69.204.95/backend/web'  // correct path for the backend app.
            ]
        ],
	'session' => [
	    'name' => '_backendSessionId', // unique for backend
	    'savePath' => __DIR__ . '/../tmp', // a temporary folder on backend
	],       
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],        
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
          'urlManager' => [
            'enablePrettyUrl' => true,
	    'showScriptName' => false,
		'rules' => array(
			'<controller:\w+>/<id:\d+>' => '<controller>/view',
			'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
			'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
		),

        ]
    ],
    'params' => $params,
];
