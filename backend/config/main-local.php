<?php
return [
    'components' => [        
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'Sil1XhSDNScOzmyXT6JzsNB4Ve6tNryg',
        ],
    ],
    'modules' => [       
        'gii' => [
            'class' => 'yii\gii\Module',
            'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.*'] // adjust this to your needs
        ],
    ],
];
