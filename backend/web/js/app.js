$(function () {	
	
	/*
	 * Opciones seleccionadas en la seccion CONSULTA en	 
	 * AHIJADOS seleccion desde el DashBoard
	 */
	if ($('#btnSelectList').val()==40){			
		$("#ahijadosOptions").fadeIn('slow');	
		$("#ckAmbos").attr("checked",true);
		$("input[name='rdConsulGeneros']").fadeOut('slow');
		// $("#rdAmbos").attr("checked",true);
		$("#caldrTotales").attr("checked",true);
	}

	/*
	 * Opciones seleccionadas en la seccion CONSULTA en
	 * PRESTAMOS (ACTIVOS, EN RIESGO, MORA) desde el DashBoard
	 */
	if ($('#btnSelectList').val() != 40 && !isPrompt($('#btnSelectList').val())){			
		$("#rdAmbos").attr("checked",true);
		$("#caldrTotales").attr("checked",true);
	}

	/*
	 *	Muestra activos e inactivos para
	 *	opcion Ahijados
	 */
	$('#btnSelectList').change( function(){
		if($(this).val()==40){
			$("input[name='rdConsulGeneros']").fadeOut('slow');
			$("#ahijadosOptions").fadeIn('slow');
		}else{
			$("input[name='rdConsulGeneros']").fadeIn('slow');
			$("#ahijadosOptions").fadeOut('slow');
		}
	})
	
	/*
	 * Muestra los datepicker para los 
	 * rangos de busqueda desde | hasta
	 */
	$('input[name="rdConsulCalendar"]').click(function(){		
		if($(this).val()== 4)
			$(".range_calendar").fadeIn('slow');
		else
			$(".range_calendar").fadeOut('slow');
	})

	/**
	 * Selecciona todos los checkbox
	 */
	$('#consultGridView').on('click','#allchkGP',function(){		
		checkAll('check-msg');
	});
	
	/**
	 * Boton de envio de mensajes
	 * desde el popup en consultas 
	 */
	$('body').on('click','#btnPopMsg',function(){
		
		var array = new Array();  
		var checked = 0;
		
		$('input[name="check-msg"]').each(function(){                                                      			
			if ($(this).is(':checked')){				
				array.push({
					id_client:$(this).data('client'), 
					fullname: $(this).data('fullname'),
					email: $(this).data('email'),
					phone: $(this).data('phone')
				})            
				checked ++;	
			}		
        });
        

        if(checked > 0){
  	             

        	if(isEmpty($('#subject').val()) && isEmpty($('#emailText').val()) && isEmpty($('#smsText').val())){
        		alert('Debe escribir en la seccion de emails o en la de sms');
        		return false;
        	}
        	
        	var data = { 
        		clients: array,
        		subject: $('#subject').val(),
        		emailTxt: $('#emailText').val(),
        		smsTxt: $('#smsText').val()
        	
        	}

   	    var $btn = $(this).button('loading');	
	    var str = window.location.href;
            var urls;
            
	    if(str.length==82)
              urls = str.substr(0,54)+'messages';
            else
              urls = str.substr(0,54)+'/messages';
       
	    var s = isAjax(urls,data);        
       	    s.done(function(response){
		$btn.button('reset');			
		$('#msgModal').modal('hide');
      	    });

        }else{
        	alert('Debe seleccionar al menos un usuario a enviar un mensaje!!');
        }
               
	});		

	/*
	 * Hace la consulta para obtener a los ahijados
	 * como para conocer cada uno de los prestamos
	 * ya sea Activo|En Riesgo|En Mora
	 */
	$('body').on('click','#btnConsulta',function(){

		var option  	= $("#btnSelectList").val(); // opcion seleccionada dropdownlist
		var estatus 	= $("input[name='rdConsulta']:checked").val();	// Activos o Inactivos
		var genero  	= $("input[name='rdConsulGeneros']:checked").val(); // Genero Seleccionado			
		var opcionFecha = $("input[name='rdConsulCalendar']:checked").val(); // Fecha
		var fecha;

		// Cuando se desea Consultar Ahijados
		if (option == 40){

			if(isEmpty(estatus) || isEmpty(opcionFecha)){		
				alert("Debe seleccionar una opcion por cada atributo a consultar");
				return false;

			}else{

				// en caso de seleccionar la opcion rango se almacena en un array
				// las dos fechas en las cuales se hara la consulta
				if (opcionFecha == 4){					
				
					fecha = {	desde: $('#fromDate').val(), 
								hasta: $('#untilDate').val()	}					

					if (isEmpty(fecha.desde) || isEmpty(fecha.hasta)){				
						alert('Debe de ingresar las fechas correspondientes para hacer la consulta');
						return false;
					
					}else if (fecha.desde > fecha.hasta){
						alert('La fecha de inicio no puede ser mayor a la que donde se quiere llegar hacer la consulta!!')
						return false;
					}

				}else{					
					// obtiene el dato de la fecha seleccionada ya sean todas, la fecha de hoy lo la del mes
					fecha = $("input[name='rdConsulCalendar']:checked").data('fecha');
				}		
							
				var data = {
					opcion : option,
					estatus: estatus,
					genero: genero,
					opcionFecha: opcionFecha,
					fecha: fecha,					
				}

				// Reemplazando la url para que haga la consulta sin 
				// problema alguno ya sea por la seccion consulta o bien
				// sea por llegada desde el dashboard				
				var str = window.location.href;
//                                var urls = str.substr(0,54)+'consultas-ahijados';
                var urls;
// console.log(str.length);
	            if(str.length==43)
	              urls = str.substr(0,43)+'/consultas-ahijados';
	            else
	              urls = str.substr(0,54)+'consultas-ahijados';

  	            var a = isAjax(urls,data); // ajax

				
				a.done(function(respon){		

					$('#consultGridView').children().remove();
					$('#consultGridView').append(respon);
				});

			}
		
		// Cuando se desean Consultar los prestamos Activos
		// En Riesgo y En Mora
		}else if (isPrompt(option) == false && option != 40){
			
			if(isEmpty(genero) || isEmpty(opcionFecha)){				
				alert("Debe seleccionar una opcion por cada atributo a consultar");
				return false;

			}else{

				if (opcionFecha == 4){					
				
					fecha = {	desde: $('#fromDate').val(), 
								hasta: $('#untilDate').val()	}					

					if (isEmpty(fecha.desde) || isEmpty(fecha.hasta)){				
						alert('Debe de ingresar las fechas correspondientes para hacer la consulta');
						return false;
					
					}else if (fecha.desde > fecha.hasta){
						alert('La fecha de inicio no puede ser mayor a la que donde se quiere llegar hacer la consulta!!')
						return false;
					}

				}else{					
					// obtiene el dato de la fecha seleccionada ya sean todas, la fecha de hoy lo la del mes
					fecha = $("input[name='rdConsulCalendar']:checked").data('fecha');
				}		
							
				var data = {	
					status: option,									
					genero: genero,
					opcionFecha: opcionFecha,
					fecha: fecha					
				}	

				// Reemplazando la url para que haga la consulta sin 
				// problema alguno ya sea por la seccion consulta o bien
				// sea por llegada desde el dashboard				
				var str = window.location.href;

				// esto funciona para lo que esta arriba
				var urls;
//console.log(str.length);
                if(str.length==43)
                  urls = str.substr(0,43)+'/consultas-prestamos';
               	else
                  urls = str.substr(0,54)+'consultas-prestamos';  

                var a = isAjax(urls,data); // ajax

			

				a.done(function(respon){
//alert('a');
					$('#consultGridView').children().remove();
					$('#consultGridView').append(respon);

//					$('body').children().remove();	
//		                    $('body').append(respon);                
			});
			}
		}
			
	});	

});
