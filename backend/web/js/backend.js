$( document ).ready(function() {        
    
    var sSelected = $('#generatedrequests-date_add');       // solicitud seleccionada para liquidar 
    var rSelected = $('#registeredrequests-date_request');       // solicitud seleccionada para registrar al banco
    var cSelected = $('#recordpayment-date_add');
    // var cSelected = $('#btnCobPendients');
    var btnConsultarRegistros = $('#btnViewCobranza');
    var btnConsultarPagos = $('#btnSearchPagos');    

        
    /**
    * Solicitudes y Generacion de Formato
    * para registro en el Banco
    */
    rSelected.change(function(){
        
        $("#rGridView").children().remove(); // Limpia el gridView
        $("#sGridView").children().remove(); // Limpia el gridView

        var rDate = rSelected.val();  // Fecha seleccionada

        if (isEmpty(rDate))                                
            return false;                                
        else{            
            var data = {fecha: rDate, users: 1};
            var r = isAjax('mostrar-solicitudes',data);

            r.success(function(respon){

                $("#rGridView").append(respon);                      // Asigna al div un GridView con los solicitantes                              

            });

        }


    });

    /**
    * Liquidaciones Pendientes
    */
    sSelected.change(function(){        
        
        $("#sGridView").children().remove(); // Limpia el gridView
                        
        var sDate = $("#generatedrequests-date_add").val();  // Fecha seleccionada        
        
        if (isEmpty(sDate))                                    
            return false;                                
        else{

            sDate       = sDate;
            var data    = { fecha: sDate, users: 0 };                      
            var x       = isAjax('mostrar-solicitudes',data);                                                                             
            
            x.done(function(respon){

                if (!respon){
                    alert('Debe generar un formato de solicitud para registrar a los clientes, y asi poder liquidar a los mismos');
                    return respon;
                }

                $("#sGridView").append(respon);      // Asigna al div un GridView con los solicitantes
                
                $("#hfechaSolic").attr('value',sDate);
                                                
                var sChecks =  $('input[name="clients-aprobados"]'); // Checkbox de cada solicitante                
                 
                // sChecks.bootstrapSwitch({
                //     'size':     'small',
                //     'onColor':  'success',
                //     'offColor': 'danger',
                //     'offText':  'Rechazado',
                //     'onText':   'Aprobado',
                //     'state':    true,                                        
                // })
                
                // // Este metodo se metodo se encarga de asignarle los status a todos los check
                // sChecks.on('switchChange.bootstrapSwitch', function(event, state) {    
                //     $(this).attr("data-status",state);                            
                // });
                
                // Boton que permite registrar las solicitudes aprobadas                
                $("#btnRegLiq").on('click',function(){
                    
                    var arrClient   = [];
                    var arrOption   = [];                    
                    var referencia  = $('#txtReferencs').val();
                                        
                    if (isEmpty(referencia)){
                        alert('Debe agregar el numero de Referencia');        
                        return false;
                    
                    }else{
                        
                        var r = confirm("Esta seguro que desea realizar esta operacion ?");
                        
                        if (r == true) {
                             
                            sChecks.each(function(){                                                      
                                arrClient.push($(this).data('clnt')); 
                                arrOption.push($(this).data('status'));            
                            });

                            var data = {  idClients :arrClient, 
                                          idOptions: arrOption, 
                                          referencs: referencia 
                                       }

                            var z = isAjax('registro-liquidaciones',data);
                                                        
                            z.success(function(zrespon){                                
                                
                                if (zrespon == true){
                                    $('#generatedrequests-date_add option:selected').remove();

                                    $('#sGridView').children().fadeOut('slow');
                                    $('#btnRegLiq').fadeOut('slow');
                                    $('#txtReferencs').fadeOut('slow');

                                    setTimeout(function(){
                                        $('#sGridView').append('<div class="alert alert-success" role="alert">Datos registrados satisfactoriamente!!</div>').fadeIn('slow');                                    
                                    }, 1000);          

                                    // refresca la pagina
                                    setTimeout(function(){
                                     location.reload();              
                                    }, 1000);   
                                }                                                                                                                            
                            });                                                      
                        }else 
                            return false;                                                
                    
                    }                   
                });                        
            });         
        }
    });

   /**
    * Cobranzas Pendientes
    */
    btnConsultarRegistros.on('click',function(){

        var option = $("input[name=radioOptions]:radio:checked" ).attr('id');  // Obtiene el id del radio seleccionado
        var date = $("select option:selected").val();    
        
        // Verifica que nigun campo este vacio
        if( option != null && !isEmpty(date) ){            
             
             var data = { fecha: date,
                          option: option };                                   
             
             var c    = isAjax('mostrar-registros',data); 
         
            // done
            c.done(function(respon){                
                
                $("#cGridView").children().remove(); // Limpia el gridView                    
                
                var data = JSON.parse(respon);  // se hace un parse para poder obtener 
                                                // los valores del json

                $('#cGridView').append(data.view).fadeIn('slow');                                                    

                if(!data.option){
                    
                    var cChecks =  $('input[name="options"]'); // Checkbox de cada solicitante                                                        

                    cChecks.bootstrapSwitch({
                        'size':     'small',
                        'onColor':  'success',
                        'offColor': 'danger',
                        'offText':  'No Verificado',
                        'onText':   'Verificado',
                        'state':    false,     
                        'disabled': false,                                  
                    })                
                    
                    // Este metodo se metodo se encarga de asignarle los status a todos los check
                    cChecks.on('switchChange.bootstrapSwitch', function(event, state) {    
                        $(this).attr("data-status",state);
                    });

                    // Desahabilita el verificado del registro de pago
                    $(".btn-cancelado").on('click',function(){
                        
                        var padre = $(this).parent(); // ubica al padre del check
                        var thisCheck = padre.find('input[name="options"]'); // encuentra al check
                        
                        // lo habilita y deshabilita
                        if(thisCheck.bootstrapSwitch('disabled')==false)
                            thisCheck.bootstrapSwitch('disabled', true);                                                                    
                        else
                            thisCheck.bootstrapSwitch('disabled', false);                            
                        
                        
            
                    });                

                    // Concilia los registros pagos de manera manual
                    $("#btnConCobManual").on('click',function(){
                        
                        var row      = [];
                        var cFecha   = $('#recordpayment-date_add').val()
                                            
                        var r = confirm("Esta seguro que desea realizar esta operacion ?");
                        
                        if (r == true) {                            
                            
                            cChecks.each(function(){ 
                                row.push({ id: $(this).data('id'),
                                           status: $(this).data('status'),
                                           disable: $(this).is(':disabled')
                                       })                                
                            });                                

                            var data = {    rows: row,
                                            fecha: cFecha };
                            //QUEDE AQUI ESTA PENDIENTE QUE ESTA LISTO Y REBOBINA
                            var r = isAjax('match-cobranza-manual',data);
                                                        
                            r.success(function(respon){        


                                var rst = JSON.parse(respon);  // se hace un parse para poder obtener 
                                                                // los valores del json

                                
                                $('#request_payment').children().fadeOut('slow');
                                // $('#btnRegLiq').fadeOut('slow');
                                // $('#txtReferencs').fadeOut('slow');

                                setTimeout(function(){                                    
                                    $('#request_payment').append('<br /><div class="alert alert-info" role="alert">Pagos Conciliados: <b>('+rst.con+')</b><br />\
                                                                    Pagos No Conciliados:<b>('+rst.noCon+')</b><br />\
                                                                    Pagos Rechazados:<b>('+rst.recha+')</b></div>').fadeIn('slow');                                    
                                }, 1000);          

                                // refresca la pagina
                                setTimeout(function(){
                                 location.reload();              
                                }, 3000);                                                                 
                            });                                                                              
                        }
                    
                    });

                }else{
                    
                    // cambia los check por una imagen "?"
                    $("input[type='checkbox']").each(function(){                    
                        
                        var padre = $(this).parent(); // ubica al padre de ese elemento   
                        
                        $(this).remove(); // remueve el checkbox
                        $(".btn-cancelado").remove(); // remueve el boton
                        
                        // se ubica en el padre para agregar la imagen "?"                                                    
                        padre.append('<center><span data-toggle="tooltip" data-placement="left"\
                                    title="En Espera" aria-hidden="true" style="font-size: 1.4em;"\
                                    class="glyphicon glyphicon-question-sign text-warning col-lg-offset-3"></span>\
                                    </center>');                                            
                    });            
                }                 
             });            

        }else{
            alert('Debe escoger una forma de conciliacion ya sea Automatica o Manual');
        }
    });  
    
    /**
    * Permite mostrar con mas detalles los Creditos ya sean
    * (Cancelados, Vigentes, Pendientes) en la Seccion
    * Historico de Creditos
    */    
    $(".view_details").click(function(e){

        e.preventDefault();    
        
        var gridView = $('#histoGridView');                
        
        gridView.children().remove(); 
                                
        var data = {    id: $(this).data('client'),
                        options: $(this).data('option') 
                    }        

        var h = isAjax('historico-creditos',data);

        h.success(function(respon){                                
            gridView.append(respon).fadeIn('slow')        
        })
    });

    /**
    * Permite mostrar con mas detalle las comisiones 
    * asignadas a el cliente seleccionado
    */
    $(".view_comision").click(function(e){
        e.preventDefault();

        var gridView = $('#histoGridView');                
                
        gridView.children().remove(); 
                                
        var data = {    id: $(this).data('client'),
                        options: $(this).data('option') 
                    }        

        var c = isAjax('historico-comisiones',data);

        c.success(function(respon){                                
            gridView.append(respon).fadeIn('slow')                
        })

    });

    /**
    * Permite mostrar con detalles las cuotas 
    * del credito seleccionado
    */
    $("#histoGridView").on('click','.view_cuotas',function(e){      
        e.preventDefault();        
        
        var gridView    = $('#histoGridView');                
        
        gridView.children().remove();         
        
	var data = { 
            id: $(this).data('quota'),
            clnt: $(this).data('client')
        }

        var q = isAjax('mostrar-cuotas',data);

        q.success(function(respon){                    
            gridView.append(respon).fadeIn('slow');
        })

    });

    /**
    * Tab Aporte Vigente Permite mostrar las cuotas y resaltar
    * al aporte de credito seleccionado
    */
    $("#credit_vigentes").on('click','.view_cAcuotas',function(e){      

        e.preventDefault();      
        
        $('tr').removeClass('selection_green');        
                
        var gridView    = $('#cuotas-cliente-vi');                    
        gridView.children().remove();         
        
        var data = { id: $(this).data('quota') }        
        
        var q = isAjax('../clients/mostrar-cuotas',data);

        q.success(function(respon){                    
            gridView.append(respon).fadeIn('slow');
        })
        
         $(this).parent().parent().addClass("selection_green");

    });

    /**
    * Tab Aporte Vencidos Permite mostrar las cuotas y resaltar
    * al aporte de credito seleccionado
    */
    $("#credit_vencidos").on('click','.view_cAcuotas',function(e){      

        e.preventDefault();      
        
        $('tr').removeClass('selection_blue');        
                
        var gridView    = $('#cuotas-cliente-ven');                    
        gridView.children().remove();         
        
        var data = { id: $(this).data('quota') }        
        
        var q = isAjax('../clients/mostrar-cuotas',data);

        q.success(function(respon){                    
            gridView.append(respon).fadeIn('slow');
        })
        
         $(this).parent().parent().addClass("selection_blue");

    });

    /**
    * Tab Aporte Vencidos Permite mostrar las cuotas y resaltar
    * al aporte de credito seleccionado
    */
    $("#credit_mora").on('click','.view_cAcuotas',function(e){      

        e.preventDefault();      
        
        $('tr').removeClass('selection_danger');        
                
        var gridView    = $('#cuotas-cliente-mo');                    
        gridView.children().remove();         
        
        var data = { id: $(this).data('quota') }        
        
        var q = isAjax('../clients/mostrar-cuotas',data);

        q.success(function(respon){                    
            gridView.append(respon).fadeIn('slow');
        })
        
         $(this).parent().parent().addClass("selection_danger");

    });
    

    /**
    * Permite activar y desactivar los dropdownlist
    * y los datapicker pertenecientes a la seccion
    * solicitud de Prestamos
    */
    $('#radioDel').on('click',function(){        
        $("#btnSearchSolic").attr("disabled",false);
        $("#date_min").attr("disabled",true);
        $("#date_max").attr("disabled",true);

    });

    $('#radioRango').on('click',function(){
        $("#btnSearchSolic").attr("disabled",true);        
        $("#date_min").attr("disabled",false);
        $("#date_max").attr("disabled",false);
    });

    $('#radioTotal').on('click',function(){
        $("#btnSearchSolic").attr("disabled",true);        
        $("#date_min").attr("disabled",true);
        $("#date_max").attr("disabled",true);
    });
    

    /**
    * Permite hacer la Busqueda de las solicitudes de Prestamo
    * ya sean diarias, por mesesm por rangos o totales
    */
    $("#btnSearchSolicitudes").on('click',function(e){
        
            
        var radioOption     = $('input[name="radioSearch"]:checked').val();
        var gridView        = $('#soliciGridView');
        // var optionRango = $('input[name="rango"]:checked').val();

        gridView.children().remove();   

        // console.log(optionDel);
        if (radioOption==1){

            var  data = { searchBy: $('#btnSearchSolic').val(), options: radioOption }

            var b = isAjax('custom-search',data);

            b.success(function(respon){

                gridView.append(respon+'<br /><br /><br /><br />').fadeIn('slow');
                    
            });

        }else if (radioOption==2){
            
            var min = $('#date_min').val();
            var max = $('#date_max').val();
            
            // Verifica que los campos no esten vacios
            if (!isEmpty(min) || !isEmpty(max)){            
                
                // Valida que la fecha hasta: sea mayor a la que desde:
                if(compare_dates(min,max)){
                    alert('La Fecha Hasta:'+max+' no es la Correcta para hacer la Busqueda');
                    return false;
                }
                
                var data = { min_date: min,
                             max_date: max,
                             options: radioOption }                

                var c = isAjax('custom-search',data);                            
                
                c.success(function(respon){
                    gridView.append(respon).fadeIn('slow');
                });

            }else{
                alert("Los Campos deben ser llenados con una fecha en especifica");
            }        
        }else if (radioOption == 3){

            var data = { options: radioOption }                

            var d = isAjax('custom-search',data);                            
            
            d.success(function(respon){
                gridView.append(respon).fadeIn('slow');
            });


        }


    })

    /**
    * Permite registrar las solicitudes de prestamo
    * para que luego se pueda hacer las liquidaciones
    * de las mismas    
    */
    $("#requests").on('click','#btnRegSolict',function(e){      

        e.preventDefault();
        
        var code   = $('#banks-code_bank').val();        
        var dateR  = $('#registeredrequests-date_request').val();
        
        if (!(isEmpty(code)) && !(isEmpty(dateR))){            
            
            if(code!=0191){
                alert('El unico formato de Banco permitido por ahora es el BNC (Banco Nacional de Credito) Seleccionelo!!')
                return false;
            }


            var data = {  bank_code: code,
                          date_add: dateR }

            var e = isAjax('registro-solicitantes',data);                                            
            
            e.success(function(response){                
                
                // return false;
                $('p').fadeOut('slow');
                $('#rGridView').children().fadeOut('slow');
                $('#registeredrequests-date_request').fadeOut('slow');
                $('#btnRegSolict').fadeOut('slow');
                $('#banks-code_bank').fadeOut('slow');

                setTimeout(function(){
                    $('#rGridView').append('<div class="alert alert-success" role="alert">\
                                                Formato de solicitud generado satisfactoriamente en la seccion <b>\
                                            (Solicitudes Generadas)</b>!!</div>').fadeIn('slow');                                    
                }, 1000);          

                // refresca la pagina
                setTimeout(function(){
                 location.reload();              
                }, 2000);   
            })
        
        }else{                    
            alert('Es necesario seleccionar una fecha y un Formato');
        }

    });

    /**
    * Selecciona todos los Checkboxs
    * (Generacion de Mensajes)
    */
    var checkAll = function(){        
        $('input[name=checkMsg]').each(function(){            
            if($(this).is(':checked'))
                $(this).prop('checked',false)
            else
                $(this).prop('checked','checked')
        })
    }

 
    cSelected.change(function(){        

        
    //     $("#sGridView").children().remove(); // Limpia el gridView
        
    //     var sDate = $("#generatedrequests-date_add").val();  // Fecha seleccionada
        
    //     if (isEmpty(sDate))                                    
    //         return false;                                
    //     else{

    //         sDate       = sDate;
    //         var data    = {fecha: sDate, users: 0 };                      
    //         var x       = isAjax('mostrar-solicitudes',data);                                                                             
            
    //         x.done(function(respon){

    //             // detiene o muestra los clientes a liquidar en caso de no ser                 
    //             if (!respon){
    //                 alert('Debe generar un formato de solicitud para registrar a los clientes, y asi poder liquidar a los mismos');
    //                 return respon;
    //             }

    //             $("#sGridView").append(respon);                      // Asigna al div un GridView con los solicitantes                              
                                                
    //             var cChecks =  $('input[name="clients-aprobados"]'); // Checkbox de cada solicitante                
                 
    //             sChecks.bootstrapSwitch({
    //                 'size':     'small',
    //                 'onColor':  'success',
    //                 'offColor': 'danger',
    //                 'offText':  'Rechazado',
    //                 'onText':   'Aprobado',
    //                 'state':    true,                                        
    //             })
                
    //             // Este metodo se metodo se encarga de asignarle los status a todos los check
    //             sChecks.on('switchChange.bootstrapSwitch', function(event, state) {    
    //                 $(this).attr("data-status",state);                            
    //             });
                
    //             // Boton que permite registrar las solicitudes aprobadas                
    //             $("#btnRegLiq").on('click',function(){
                    
    //                 var arrClient   = [];
    //                 var arrOption   = [];                    
    //                 var referencia  = $('#txtReferencs').val();
                                        
    //                 if (isEmpty(referencia)){
    //                     alert('Debe agregar el numero de Referencia');        
    //                     return false;
                    
    //                 }else{
                        
    //                     var r = confirm("Esta seguro que desea realizar esta operacion ?");
                        
    //                     if (r == true) {
                             
    //                         sChecks.each(function(){                                                      
    //                             arrClient.push($(this).data('clnt')); 
    //                             arrOption.push($(this).data('status'));            
    //                         });

    //                         var data = {  idClients :arrClient, 
    //                                       idOptions: arrOption, 
    //                                       referencs: referencia 
    //                                    }

    //                         var z = isAjax('registro-liquidaciones',data);
                                                        
    //                         z.success(function(zrespon){                                
                                
    //                             if (zrespon == true){
    //                                 $('#generatedrequests-date_add option:selected').remove();

    //                                 $('#sGridView').children().fadeOut('slow');
    //                                 $('#btnRegLiq').fadeOut('slow');
    //                                 $('#txtReferencs').fadeOut('slow');

    //                                 setTimeout(function(){
    //                                     $('#sGridView').append('<div class="alert alert-success" role="alert">Datos registrados satisfactoriamente!!</div>').fadeIn('slow');                                    
    //                                 }, 1000);          

    //                                 // refresca la pagina
    //                                 setTimeout(function(){
    //                                  location.reload();              
    //                                 }, 1000);   
    //                             }                                                                                                                            
    //                         });                                                      
    //                     }else 
    //                         return false;                                                
                    
    //                 }                   
    //             });                        
    //         });         
    //     }
    });
    
});
