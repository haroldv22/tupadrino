 /**
 * Permite verificar si lo que se 
 * envia es un objeto vacio 
 * @param {string|boolean|array} obj 
 */ 
function isEmpty(obj){        
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }
    return true;                
}

  /**
  * Selecciona todos los Checkboxs
  * (Generacion de Mensajes)
  */
  var checkAll = function(obj){        
      $('input[name='+obj+']').each(function(){            

        console.log($(this));
          if($(this).is(':checked'))
              $(this).prop('checked',false)
          else
              $(this).prop('checked','checked')
      })
  }

/**
 * Permite conocer si estamos
 * ubicados en la primera opcion del
 * dropdownlist
 * @param {string} obj 
 */ 
function  isPrompt(obj){    
    if (obj=="prompt") 
	return true;
    else
	return false;
}

/**
 * Permite ejecutar un metodo ajax 
 * @param {string} url 
 * @param {string} method 
 * @param {array}  data 
 */ 
function isAjax(url,data){        
    
    return $.ajax({
                url: url,
                method: 'POST',
                data: data,
            })
}

function compare_dates(fecha, fecha2)  
  {  
    var xMonth=fecha.substring(3, 5);  
    var xDay=fecha.substring(0, 2);  
    var xYear=fecha.substring(6,10);  
    var yMonth=fecha2.substring(3, 5);  
    var yDay=fecha2.substring(0, 2);  
    var yYear=fecha2.substring(6,10);  
    if (xYear> yYear)  
    {  
        return(true)  
    }  
    else  
    {  
      if (xYear == yYear)  
      {   
        if (xMonth> yMonth)  
        {  
            return(true)  
        }  
        else  
        {   
          if (xMonth == yMonth)  
          {  
            if (xDay> yDay)  
              return(true);  
            else  
              return(false);  
          }  
          else  
            return(false);  
        }  
      }  
      else  
        return(false);  
    }  
}  


