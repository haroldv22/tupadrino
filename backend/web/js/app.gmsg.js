$(function(){
	
	var btnTrash = $('#trash');
	var btnCompose = $('#view-compose');


 	/**
    * Permite enviar y registrar los mensajes (Generacion de Mensajes)
    * a los clientes de manera masiva (Generacion de mensajes)
    */
    $("#contenido").on('click','#btnSentMsg',function(e){
        
        e.preventDefault();
        var sOption = $("input:checked").val();
        var subject = $("#compose-subject").val();
        var message = $("#compose-textarea").val();
        var composeSms = $('#compose-sms');
        var sms = '';
    
                

        if((sOption != 'on' && isEmpty(subject) == false && isEmpty(message) == false)){
                               
            // verifica que este disponible para escribir un sms
            if(isEmpty(composeSms.attr('readOnly'))){        
                
                //verifica que exista algun escrito en la seccion de sms
                if(composeSms.val() != ''){                
                    sms = composeSms.val();
                }else{                    
                    alert('El Campo para envio de SMS no puede estar vacio!!');
                    return false;
                }            
            }

            data = { level: sOption,
                     subject: subject,
                     message: message,
                     sms: sms }

            var x = isAjax('sent-message',data);

            x.done(function(respon){
                
                if(respon){                
                   
                    alert('Los Mensajes fueron enviados satisfactoriamente');
                    $("#compose-subject").val(''); 
                    $("#compose-textarea").val(''); 
                    $("input:checked").attr('checked',false);
                
                }else{
                    alert('Ocurrio un error al momento de enviar los mensajes!!');
                }
                
            });
        
        }else{
        
            alert('Rellene los campos y seleccione a quien va dirigido el mensaje!!');        
        }
    });


	/**
    * Permite habilitar el textarea para escribir un  
    * mensaje de texto SMS (Generacion de Mensajes)
    */
    $("#contenido").on('click','#btnSms',function(e){

        $('#compose-sms').attr('readOnly',false);

    });

    /**
    * Emails Enviados (Generacion de Mensajes)
    * muestra todos los mensajes que han sido enviados
    */
    $('#view-emails').on('click',function(e){
        // btnView
        // MsgSent.on('click',funsmse){

        e.preventDefault();            

        var data = {status:70}   
        
        var v = isAjax('view-emails',data);
        
        v.done(function(respon){
            
            $('#contenido').children().remove();
            $('#contenido').append(respon).fadeIn('slow');
            
            /**
            * Permite Leer los mensajes enviados via email
            * (Generacion de Mensajes)
            */
            $('tr td.mailbox-name, td.mailbox-subject, td.mailbox-date').on('click',function(e){ 
                
                e.preventDefault();
                // console.log($(this).parent().data('key'));
                var data = { key: $(this).parent().data('key') }
                var t = isAjax('read-emails',data);
                
                t.done(function(respon){
                    $('#contenido').children().remove();
                    $('#contenido').append(respon).fadeIn('slow');            
                })
            });

            //Selecciona todos los mensajes
            $('#checkAll').click(checkAll)   

            // elimina mensajes emails
            $('#btnDelete').on('click',checkDelete)
        });        

    });

    /**
    * Emails Enviados (Generacion de Mensajes)
    * muestra todos los mensajes que han sido enviados
    */
    $('#view-sms').on('click',function(e){

        // btnViewMsgSent.on('click',funsmse){

        e.preventDefault();            

        var data = {status:70}   
        
        var v = isAjax('view-sms',data);
        
        v.done(function(respon){
            
            $('#contenido').children().remove();
            $('#contenido').append(respon).fadeIn('slow');
            
            /**
            * Permite Leer los mensajes enviados via email
            * (Generacion de Mensajes)
            */
            $('tr td.mailbox-name, td.mailbox-subject, td.mailbox-date').on('click',function(e){ 
                
                e.preventDefault();
                // console.log($(this).parent().data('key'));
                var data = { key: $(this).parent().data('key') }
                var t = isAjax('read-sms',data);
                
                t.done(function(respon){
                    $('#contenido').children().remove();
                    $('#contenido').append(respon).fadeIn('slow');            
                })
            });

            //Selecciona todos los mensajes
            $('#checkAll').click(checkAll)   

            // elimina mensajes
            $('#btnDelete').on('click',checkDeleteSms)
        });        

    });


    /**
    * Papepelera (Generacion de Mensajes)
    * muestra todos los mensajes que han sido enviados
    */
    btnTrash.on('click',function(e){

        e.preventDefault();      

        $('#checkAll').remove

        
        var data = {status:71}
        var v = isAjax('view-emails',data);        
        
        v.done(function(respon){
            
            $('#contenido').children().remove();
            $('#contenido').append(respon).fadeIn('slow');
            
            /**
            * Permite Leer los mensajes enviados via email
            * (Generacion de Mensajes)
            */
            $('tr td.mailbox-name, td.mailbox-subject, td.mailbox-date').on('click',function(e){ 
                
                e.preventDefault();
                // console.log($(this).parent().data('key'));
                var data = { key: $(this).parent().data('key') }
                var t = isAjax('read-emails',data);
                
                t.done(function(respon){
                    $('#contenido').children().remove();
                    $('#contenido').append(respon).fadeIn('slow');            
                })
            });        
            
            //Selecciona todos los mensajes            
            $('#checkAll').click(checkAll)                                            
        });        

    });



    /**
    * Ver Redactar Crear mensaje  (Generadaseracion de Mensajes)    
    */
    btnCompose.on('click',function(e){

        e.preventDefault();
        var c = isAjax('compose-emails');
        
        c.done(function(respon){
            
            $('#contenido').children().remove();
            $('#contenido').append(respon).fadeIn('slow');

        });
        
    });

    /**
    * Elimina los Checkbox Selecccionados 
    * (Generacion de Mensajes)
    */
    var checkDelete = function(){    
        
        var array = new Array();  

        $('input[name=checkMsg]').each(function(){            
            if($(this).is(':checked'))    
                array.push($(this).parent().parent().data('key'));
        })
    
        var data = { id: array }
        
        var x = isAjax('delete-emails',data);

        x.done(function(respon){        
            $('input[name=checkMsg]').each(function(){            
                if($(this).is(':checked'))    
                    $(this).parent().parent().remove();
            })        
        })
    };

    /**
    * Elimina los Checkbox Selecccionados 
    * (Generacion de Mensajes)
    */
    var checkDeleteSms = function(){    
        
        var array = new Array();  

        $('input[name=checkMsg]').each(function(){            
            if($(this).is(':checked'))    
                array.push($(this).parent().parent().data('key'));
        })
    
        var data = { id: array }
        
        var x = isAjax('delete-sms',data);

        x.done(function(respon){        
            $('input[name=checkMsg]').each(function(){            
                if($(this).is(':checked'))    
                    $(this).parent().parent().remove();
            })        
        })
    };

    /**
    * Selecciona todos los Checkboxs
    * (Generacion de Mensajes)
    */
    var checkAll = function(){        
        $('input[name=checkMsg]').each(function(){            
            if($(this).is(':checked'))
                $(this).prop('checked',false)
            else
                $(this).prop('checked','checked')
        })
    }
})