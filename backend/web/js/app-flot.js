$(function () {	

    /*
     * DONUT CHART
     * -----------
     */        
    // console.log($('#aHombres').val());
    // var donutData = [
    //   {label: "", data: $('#aMujeres').val(), color: "#dd4b39"},
    //   {label: "", data: $('#aHombres').val(), color: "#0073b7"},
    //   {label: "", data: $('#clActivos').val(), color: "#00a65a"},
    //   {label: "", data: $('#clInactivos').val(), color: "#ff851b"}
    // ];
    // $.plot("#donut-chart", donutData, {
    //   series: {
    //     pie: {
    //       show: true,
    //       radius: 1,
    //       innerRadius: 10.5,
    //       label: {
    //         show: true,
    //         radius: 2 / 3,
    //         formatter: labelFormatter,
    //         threshold: 0.1
    //       }

    //     }
    //   },
    //   legend: {
    //     show: false
    //   }
    // });
    /*
     * END DONUT CHART
     */
    
     /*
     * Custom Label formatter
     * ----------------------
     */
    function labelFormatter(label, series) {  
    	
      return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
          + label
          + "<br>"           
          + Math.round(series.percent) + "%</div>";
    }

    /*
     * BAR CHART ACTIVOS
     * ---------
     */
    var bar_data = {
      data: [["Activos", $('#pActivos').val()],["Mujeres", $('#pAMujeres').val()], ["Hombres", $('#pAHombres').val()]],
      color: "#00a65a",
    };
    $.plot("#bar-activos", [bar_data], {
      grid: {
        borderWidth: 1,
        borderColor: "#f3f3f3",
        tickColor: "#f3f3f3"
      },
      series: {
        bars: {
          show: true,
          barWidth: 0.5,
          align: "center"
        }
      },
      xaxis: {
        mode: "categories",
        tickLength: 0
      }
    });
    /* END BAR CHART ACTIVOS */


    /*
     * BAR CHART Al Dia
     * ---------
     */
    var bar_data = {
      data: [["Al Dia", $('#pRActivos').val()],["Mujeres", $('#pRMujeres').val()], ["Hombres", $('#pRHombres').val()]],
      color: "#f39c12",
    };
    $.plot("#bar-riesgo", [bar_data], {
      grid: {
        borderWidth: 1,
        borderColor: "#f3f3f3",
        tickColor: "#f3f3f3"
      },
      series: {
        bars: {
          show: true,
          barWidth: 0.5,
          align: "center"
        }
      },
      xaxis: {
        mode: "categories",
        tickLength: 0
      }
    });
    /* END BAR CHART EN RIESGO*/

    
    /*
     * BAR CHART EN MORA
     * ---------
     */
    var bar_data = {
      data: [["En Mora", $('#pMActivos').val()],["Mujeres", $('#pMMujeres').val()], ["Hombres", $('#pMHombres').val()]],
      color: "#dd4b39",
    };
    $.plot("#bar-mora", [bar_data], {
      grid: {
        borderWidth: 1,
        borderColor: "#f3f3f3",
        tickColor: "#f3f3f3"
      },
      series: {
        bars: {
          show: true,
          barWidth: 0.5,
          align: "center"
        }
      },
      xaxis: {
        mode: "categories",
        tickLength: 0
      }
    });   
    /* END BAR CHART EN MORA*/
    
    /*
     * BAR CHART SOLICITUDES
     * ---------
     */

    var bar_data = {
      data: [["Acumuladas", $('#sAcumulate').val()],["Del Mes", $('#sMonth').val()], ["De Hoy", $('#sToday').val()]],
      color: "#00c0ef",
    };
    $.plot("#bar-solicitudes", [bar_data], {
      grid: {
        borderWidth: 1,
        borderColor: "#f3f3f3",
        tickColor: "#f3f3f3"
      },
      series: {
        bars: {
          show: true,
          barWidth: 0.5,
          align: "center"
        }
      },
      xaxis: {
        mode: "categories",
        tickLength: 0
      }
    });
    /* END BAR CHART EN MORA*/
 
});
