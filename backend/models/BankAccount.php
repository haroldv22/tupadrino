<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bank_account".
 *
 * @property integer $id
 * @property integer $id_client
 * @property string $code_bank
 * @property string $number_account
 *
 * @property Banks $codeBank
 * @property Clients $idClient
 */
class BankAccount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bank_account';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_client', 'code_bank', 'number_account'], 'required'],
            [['id_client'], 'integer'],
            [['code_bank'], 'string', 'max' => 4],
            [['number_account'], 'string', 'max' => 16]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_client' => 'Id Client',
            'code_bank' => 'codigo identificador del banco',
            'number_account' => 'account user',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodeBank()
    {
        return $this->hasOne(Banks::className(), ['code_bank' => 'code_bank']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'id_client']);
    }
}
