<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "question_system".
 *
 * @property string $question
 * @property integer $id
 */
class QuestionSystem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'question_system';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question'], 'required'],
            [['question'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'question' => 'Pregunta',
            'id' => 'ID',
        ];
    }
}
