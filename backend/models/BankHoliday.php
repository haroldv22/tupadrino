<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bank_holiday".
 *
 * @property integer $id
 * @property string $holiday
 * @property string $description
 */
class BankHoliday extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bank_holiday';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['holiday'], 'required'],
            [['holiday'], 'safe'],
            [['holiday'], 'date', 'format' => 'php:Y-m-d', 'message' => 'Alguna de las fechas no posee el formato indicado'],
            [['holiday'], 'unique','message' => 'Alguna de las fechas ya se encuentra registrada en el sistema'],
            [['description'], 'string', 'max' => 75]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'holiday' => 'Dia Feriado',
            'description' => 'Cierre Bancario',
        ];
    }
    
    /**
     * Recibe un array con todas las fechas obtenidas
     * del archivo importado para validar cada una de ellas     
     * 
     * @param $dates array
     * return true | false
     */     
    public function validateDates($dates){
        
        foreach ($dates as $value){            
            $this->holiday = $value;            
            if(!$this->validate())
                return false;                                                                            
        }                
        return true;                
    }
    
    /**
     * Se encarga de almacenar los dias feriados 
     * junto con su 
     * 
     */ 
    public function saveHolidaysBank($fechas,$description){
          
        $i = 0;
        foreach($fechas as $value ){            
            $command = Yii::$app->db->createCommand();
            $command->insert('bank_holiday',array('holiday'=>$value,'description'=>$description[$i]))->execute();
            $i++;            
        }
        
    }
        
}
