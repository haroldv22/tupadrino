<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "arrears".
 *
 * @property integer $id
 * @property integer $id_client
 * @property integer $id_loan_quota
 * @property integer $delay_days
 * @property string $total_interest
 * @property integer $id_status
 * @property string $upd_date
 * @property string $pending_pay
 * @property string $add_date
 *
 * @property Clients $idClient
 * @property LoanQuota $idLoanQuota
 * @property Status $idStatus
 * @property ArrearsDetails[] $arrearsDetails
 * @property ArrearsPayment[] $arrearsPayments
 */
class Arrears extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'arrears';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_client', 'id_loan_quota', 'delay_days', 'total_interest', 'id_status', 'upd_date', 'add_date'], 'required'],
            [['id_client', 'id_loan_quota', 'delay_days', 'id_status'], 'integer'],
            [['total_interest', 'pending_pay'], 'number'],
            [['upd_date', 'add_date'], 'safe'],
            [['id_loan_quota'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_client' => 'Id Client',
            'id_loan_quota' => 'Id Loan Quota',
            'delay_days' => 'Delay Days',
            'total_interest' => 'Total Interest',
            'id_status' => 'Id Status',
            'upd_date' => 'Upd Date',
            'pending_pay' => 'Pending Pay',
            'add_date' => 'Add Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'id_client']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLoanQuota()
    {
        return $this->hasOne(LoanQuota::className(), ['id' => 'id_loan_quota']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'id_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArrearsDetails(){
        return $this->hasMany(ArrearsDetails::className(), ['id_arrears' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArrearsPayments(){
        return $this->hasMany(ArrearsPayment::className(), ['id_arrears' => 'id']);
    }

    /**
     * [getSumByPendingPay Obtiene la sumatoria
     * de las moras]
     * @return [integer] [Suma de los pagos pendientes en mora]
     */
    public function getSumByPendingPay(){
      
      return $this::find()
                ->where(['id_status'=>Yii::$app->params['mora_impagada']])
                ->sum('pending_pay');    
    }
  
    /**
     * [getAll Mediante este metodo se obtienen
     * todos los prestamos realizados de acuerdo 
     * a su Estatus en el sistema]
     * @param  [integer] $status [id del status]
     * @return [model]  model    [todo el modelo de acuerdo a la busqueda]
     */
    public function findAllByStatus($status){            
      return $this::find()->where(['id_status'=>$status]);
    }

    /**
     * [findAllByMoraGender Obtiene todos los 
     * usuarios que presentan mora]
     * @param  [string] $gender [genero m|f]
     * @return [model]         [modelo]
     */
    public function findAllByMoraGender($gender){

return $this::find()
                ->select('id_client, id_status')
                ->distinct()
                ->innerJoin('clients', 'arrears.id_client = clients.id')
                ->where([
                   'arrears.id_status'=>Yii::$app->params['mora_impagada'],
                   'clients.gender'=>$gender
                ])
                ->orderBy('id_client');

/*	if(is_array($gender)){        
            $strGender = " ['".implode("', '", $gender)."']";
            $gender = "ANY(ARRAY".$strGender.")";
        }else{
            $gender = "'".$gender."'";
        }
        // desarrollo de este sql ya que yii2 no tiene buen soporte para el distinct
        $sql = "SELECT DISTINCT ON (arrears.id_client) arrears.id_client,arrears.id_status,arrears.add_date
                FROM
                   arrears
                   INNER JOIN loan_quota ON (arrears.id_loan_quota = loan_quota.id)
                   INNER JOIN clients ON (arrears.id_client = clients.id)
                WHERE
                   arrears.id_status ='".Yii::$app->params['mora_impagada']."'                   
                AND
                clients.gender = ".$gender."
                ORDER BY arrears.id_client, arrears.add_date";
        
        return $this::findBySql($sql);*/

/*	return  $this::find()                
		->select(['arrears.id_client','arrears.id_status','add_date'])
		->distinct()
                ->innerJoin('loan_quota', 'arrears.id_loan_quota = loan_quota.id')
                ->innerJoin('clients', 'arrears.id_client = clients.id')
                ->where([
                   'arrears.id_status'=>Yii::$app->params['mora_impagada'],
                   'clients.gender'=>$gender
                ])
	      ->orderBy('add_date DESC');*/
    }

    /**
     * [findAllByToday Busca los usuarios que 
     * estan en mora el dia de hoy ]
     * @param  [date] $fecha [fecha]
     * @return [model]       [Modelo]
     */
    public function findAllByMoraToday($gender,$fecha){

	 return $this::find()
            ->select('id_client, id_status')
            ->distinct()                                            
            ->innerJoin('clients', 'arrears.id_client = clients.id')
            ->where([
                  'arrears.id_status'=>Yii::$app->params['mora_impagada'],
                  'clients.gender'=>$gender,
                  'arrears.add_date' => $fecha
            ])            
            ->orderBy('id_client DESC');

/*        return $this::find()                          
                ->innerJoin('loan_quota', 'arrears.id_loan_quota = loan_quota.id')
                ->innerJoin('clients', 'arrears.id_client = clients.id')
                ->where([
                  'arrears.id_status'=>Yii::$app->params['mora_impagada'],
                  'clients.gender'=>$gender,
                  'arrears.add_date' => $fecha
                ]);            */
    }

    /**
     * [findAllByMoraGenderMonth Busca a los usuarios
     * en mora por el mes actual]
     * @param  [string] $gender [m|f]
     * @param  [date] $fecha  [fecha]
     * @return [model]         [Modelo]
     */
    public function findAllByMoraGenderMonth($gender,$fecha){

	return $this::find() 
                ->select('id_client, id_status')
                ->distinct()                                           
                ->innerJoin('clients', 'arrears.id_client = clients.id')
                ->where([
                  'arrears.id_status'=>Yii::$app->params['mora_impagada'],
                  'clients.gender'=>$gender
                ])
                ->andWhere(" date_part('MONTH', arrears.add_date)= $fecha")
                ->orderBy('id_client DESC'); 

/*        return $this::find()                          
                ->innerJoin('loan_quota', 'arrears.id_loan_quota = loan_quota.id')
                ->innerJoin('clients', 'arrears.id_client = clients.id')
                ->where([
                  'arrears.id_status'=>Yii::$app->params['mora_impagada'],
                  'clients.gender'=>$gender
                ])
                ->andWhere(" date_part('MONTH', arrears.add_date)= $fecha");     
*/
    }

     /**
     * [findByMoraBetweenDate busca los usuarios 
     * con mora por rango de fecha]
     * @param  [string] $gender [genero m | f]
     * @param  [date] $from   [fecha desde donde se hara la busqueda]
     * @param  [date] $until  [fecha hasta donde se hara la busqueda]
     * @return [model]        [ Modelo ]
     */
    public function findByMoraBetweenDate($gender,$from,$until){

	return $this::find()   
                ->select('id_client, id_status')
                ->distinct()                             
                ->innerJoin('clients', 'arrears.id_client = clients.id')
                ->where([
                  'arrears.id_status'=> Yii::$app->params['mora_impagada'],
                  'clients.gender'=>$gender
                ])
                ->andWhere(['between','arrears.add_date',$from,$until])
                ->orderBy('id_client DESC');     

/*        return $this::find()                          
                ->innerJoin('loan_quota', 'arrears.id_loan_quota = loan_quota.id')
                ->innerJoin('clients', 'arrears.id_client = clients.id')
                ->where([
                  'arrears.id_status'=> Yii::$app->params['mora_impagada'],
                  'clients.gender'=>$gender
                ])
                ->andWhere(['between','arrears.add_date',$from,$until]);*/
    }

    /**
     * [getSumMoraByGender obtiene todas la suma 
     * de la mora + el pago pendiente por genero (TOTAL PRESTAMOS)]
     * @param  [type] $gender [description]
     * @return [type]         [description]
     */
    public function getSumMoraAllByGender($gender){        
        return $this::find()
                ->innerJoin('loan_quota', 'arrears.id_loan_quota = loan_quota.id')
                ->innerJoin('clients', 'arrears.id_client = clients.id')
                ->where([
                  'arrears.id_status'=>Yii::$app->params['mora_impagada'],
                  'clients.gender'=>$gender
                ])                
                ->sum('arrears.pending_pay + loan_quota.pending_pay');
    }
   
    /**
     * [getSumMoraByClient obtiene la mora + lo que se 
     * debe del pago pendiente de la mora por cliente]
     * @param  [integer] $id_client [id del cliente]
     * @return [string]             [ suma de la mora + pago pendiente cliente]
     */
    public function getSumMoraByClient($id_client){

        return $this::find()
                ->innerJoin('loan_quota', 'arrears.id_loan_quota = loan_quota.id')
                ->where([
                    'arrears.id_client' => $id_client,
                    'arrears.id_status' => Yii::$app->params['mora_impagada']                    
                ])
                ->sum('arrears.pending_pay + loan_quota.pending_pay');
    }


    /**
     * [getMoraByClient Obtiene la suma de los prestamos en mora
     * de un cliente en especifico]
     * @param  [integer] $id_client [id del cliente]
     * @return [float]              [suma de los prestamos en mora]
     */
    public function getPendingPayByClient($id_client){
        
        return $this::find()
                ->innerJoin('loan_quota', 'arrears.id_loan_quota = loan_quota.id')
                ->where([
                    'arrears.id_client' => $id_client,
                    'arrears.id_status' => Yii::$app->params['mora_impagada']                    
                ])
                ->sum('loan_quota.pending_pay');

    }

    /**
     * [getMoraByClient Obtiene la suma de las moras de un 
     * cliente en especifico]
     * @param  [integer] $id_client [id del cliente]
     * @return [float]              [suma de la mora]
     */
    public function getMoraByClient($id_client){
        
        return $this::find()
                ->innerJoin('loan_quota', 'arrears.id_loan_quota = loan_quota.id')
                ->where([
                    'arrears.id_client' => $id_client,
                    'arrears.id_status' => Yii::$app->params['mora_impagada']                    
                ])
                ->sum('arrears.pending_pay');

    }

}
