<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bank_client".
 *
 * @property integer $id
 * @property integer $id_cliente
 * @property string $id_bank
 *
 * @property Clients $idCliente
 */
class BankClient extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bank_client';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_cliente', 'id_bank'], 'required'],
            [['id_cliente'], 'integer'],
            [['id_bank'], 'string', 'max' => 4]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_cliente' => 'Id del cliente registrado en el sistema',
            'id_bank' => 'Id del banco al cual el cliente a sido registrado para hacer la exportacion de un formato',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCliente()
    {
        return $this->hasOne(Clients::className(), ['id' => 'id_cliente']);
    }
}
