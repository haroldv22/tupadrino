<?php

namespace app\models;

use Yii;
use app\models\Banks;

/**
 * This is the model class for table "registered_requests".
 *
 * @property integer $id
 * @property string $date_request
 * @property boolean $registered
 * @property string $url
 */
class RegisteredRequests extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'registered_requests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_request'], 'required'],
            [['date_request'], 'safe'],
            [['registered'], 'boolean'],
            [['url'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_request' => 'Fecha de la Solicitud',
            'registered' => 'campo que permite conocer si la solicitud fue registrada en el banco!!',
            'url' => 'url',
        ];
    }

    /**
     * Permite obtener el nombre del banco 
     * Que fue seleccionado para generar el formato 
     * de registro de los clientes
     * @param string $code_bank codigo del banco
     */
    public function getBankName($code_bank){
        
        $modelBank = new Banks();

        return $modelBank::find()
                         ->where(['code_bank'=>$code_bank])
                         ->one()
                         ->name;

    }
    
}
