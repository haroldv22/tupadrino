<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "commission_round".
 *
 * @property integer $id
 * @property integer $id_client
 * @property string $end_date
 * @property string $to_pay
 * @property integer $id_status
 * @property string $add_date
 * @property string $expected_pay
 * @property string $risk_pay
 *
 * @property Commission[] $commissions
 * @property Clients $idClient
 * @property Status $idStatus
 */
class CommissionRound extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'commission_round';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_client', 'end_date', 'to_pay', 'id_status', 'add_date'], 'required'],
            [['id_client', 'id_status'], 'integer'],
            [['end_date', 'add_date'], 'safe'],
            [['to_pay', 'expected_pay', 'risk_pay'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id de la tabla',
            'id_client' => 'Cliente',
            'end_date' => 'Acreditacion',
            'to_pay' => 'Comision',
            'id_status' => 'Estatus',
            'add_date' => 'Inicio de Comision',
            'expected_pay' => 'Expectativa',
            'risk_pay' => 'Pago en Riesgo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommissions()
    {
        return $this->hasMany(Commission::className(), ['id_commission_round' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'id_client']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdStatus(){
        return $this->hasOne(Status::className(), ['id' => 'id_status']);
    }

    /**
    * Permite obtener el total de comisiones pendientes
    * por ser canceladas al Usuario
    * @param intreger Id  del cliente
    * @param integer  id_status Estatus de la commission
    * @return integer 0 | la comision
    */
    public function getCommisionValue($id,$id_status){        
        
        $command = Yii::$app->db->createCommand("SELECT sum(expected_pay) FROM commission_round where id_client=".$id." AND id_status=".$id_status."");
        
        if ($command->queryScalar()!= NULL)
            return $command->queryScalar();
        else
            return 0;
        
    }
    
    /**
    * Obtiene los status pertenecientes a la tabla 
    * Commission_round y crea un DropdownList
    * para mostrarlo en la vista y asi hacer una busqueda     
    */
    public function getStatusCommission(){

        print_r(Status::find()->where([['between', 'id_status', 60, 63]])->all()->name);
    }
}
