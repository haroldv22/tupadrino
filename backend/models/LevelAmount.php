<?php

namespace app\models;

use Yii;
use yii\db\Query;
use yii\db\Command;

/**
 * This is the model class for table "level_amount".
 *
 * @property integer $id
 * @property string $amount
 * @property integer $id_level
 *
 * @property Level $idLevel
 * @property PeriodAmount[] $periodAmounts
 */
class LevelAmount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'level_amount';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount'], 'required'],
            [['amount'], 'number'],
            [['id_level'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(){
        return [
            'id' => 'identificador de la tabla',
            'amount' => 'Monto',
            'id_level' => 'Nivel',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLevel(){
        return $this->hasOne(Level::className(), ['id' => 'id_level']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodAmounts(){
        return $this->hasMany(PeriodAmount::className(), ['id_level_amount' => 'id']);
    }

    /**
     * Busqueda por Monto de acuerdo al nivel 
     * de credito (Sin ORM)
     * @return Array [id, amount]
     */
    public function findByAmounts(){

        $query = new Query();
        
        $query->select('id, amount')
              ->from('level_amount');        
            
        return $query->all(); 
    }

    /**
     * [getPeriodsAmmounts Obtiene los plazos
     * de cada monto en conjunto]
     * @return [string] [Los plazos del monto]
     */
    public function getPeriodsAmmounts(){

        $model = $this->getPeriodAmounts();
        $plazos = "";
        
        if (!empty($model->one())){

            $id_level_amount = $model->one()->id_level_amount;

            $modelPeriodAmount = PeriodAmount::find()
                        ->where(['id_level_amount'=>$id_level_amount])
                        ->all();
            
            foreach ($modelPeriodAmount as $value) {
                $plazos .= $value->getIdPeriod()->one()->alias."  / ";            
            }

            return $plazos;
        
        }else{
        
            return "N/A";
        
        }
        
    }

}
