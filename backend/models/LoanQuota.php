<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "loan_quota".
 *
 * @property integer $id
 * @property integer $id_loan
 * @property string $amortization
 * @property string $interest
 * @property integer $id_status
 * @property string $date_pay
 * @property string $date_expired
 * @property integer $n_quota
 * @property string $quota
 *
 * @property Arrears[] $arrears
 * @property Loan $idLoan
 * @property Status $idStatus
 * @property QuotaPayment[] $quotaPayments
 */
class LoanQuota extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'loan_quota';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_loan', 'amortization', 'interest', 'id_status', 'date_pay', 'date_expired', 'n_quota', 'quota'], 'required'],
            [['id_loan', 'id_status', 'n_quota'], 'integer'],
            [['amortization', 'interest', 'quota'], 'number'],
            [['date_pay', 'date_expired'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_loan' => 'clave foranea de tabla loan(prestamo)',
            'amortization' => 'monto de amortizacion',
            'interest' => 'interes generado en la cuota',
            'id_status' => 'estatus de la cuota',
            'date_pay' => 'fecha de pago',
            'date_expired' => 'fecha de vencimiento de la cuota',
            'n_quota' => 'Numero de la cuota',
            'quota' => 'Total a pagar en la cuota (interes + amortizacion)',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArrears()
    {
        return $this->hasMany(Arrears::className(), ['id_load_quota' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLoan()
    {
        return $this->hasOne(Loan::className(), ['id' => 'id_loan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'id_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuotaPayments()
    {
        return $this->hasMany(QuotaPayment::className(), ['id_loan_quota' => 'id']);
    }
}
