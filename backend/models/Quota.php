<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "quota".
 *
 * @property integer $days
 * @property integer $id_level
 * @property integer $id
 *
 * @property Level $idLevel
 */
class Quota extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'quota';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['days', 'id_level'], 'integer'],
            [['id_level','alias','days'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'days' => '# Dias',
            'id_level' => 'Level',
            'id' => 'ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLevel()
    {
        return $this->hasOne(Level::className(), ['id' => 'id_level']);
    }
}
