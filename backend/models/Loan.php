<?php

namespace app\models;

use Yii;
use app\models\User;
use app\models\BankAccount;
use app\models\SystemBankAccount;
use yii\db\Exception;

/**
 * This is the model class for table "loan".
 *
 * @property integer $id
 * @property integer $id_client
 * @property string $amount
 * @property integer $rate
 * @property integer $id_status
 * @property integer $quota
 * @property integer $period
 * @property string $date_add
 * @property string $date_transference
 *
 * @property Clients $idClient
 * @property Status $idStatus
 * @property LoanQuota[] $loanQuotas
 */
class Loan extends \yii\db\ActiveRecord { 
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'loan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_client', 'amount', 'rate', 'id_status', 'quota', 'period', 'date_add'], 'required'],
            [['id_client', 'rate', 'id_status', 'quota', 'period','reference_pay'], 'integer'],
            [['amount'], 'number'],
            [['date_add, date_transference','real_date_add'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'id_client' => 'Id Client',
            'amount' => 'Amount',
            'rate' => 'tasa de interes',
            'id_status' => 'Id Status',
            'quota' => 'numeros de dias en el cual se genera una quota',
            'period' => 'Periodo de pago asiganado por el usuario',
            'date_add' => 'fecha de prestamo solicitado',
            'date_transference' => 'fecha de la transferencia',
            'reference_pay' => 'referencia del pago',
	    'real_date_add' => 'Hora de Registro',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdClient(){
        return $this->hasOne(Clients::className(), ['id' => 'id_client']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdStatus(){        
        return $this->hasOne(Status::className(), ['id' => 'id_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoanQuotas(){
        return $this->hasMany(LoanQuota::className(), ['id_loan' => 'id']);
    }
    
    /**
     * Permite conocer quien es el padrino
     * del solicitante     
     * 
     * @param model modelo clientInvitation
     * @return string nombre del Padrino
     */ 
    public function getPadrino($model){
          
        if ($model->id_godfather==0){
          $adminUser = User::findOne($model->id_user_system);
            return "Admin / ".$adminUser->firstname.' '.$adminUser->lastname;
        }else{  
            $pClient = Clients::find()->where(['id'=>$model->id_godfather])->one()->Shortname();
            return "Cliente / ".$pClient;
        }    
    }

    /**
     * [getAll Mediante este metodo se obtienen
     * todos los prestamos realizados de acuerdo 
     * a su Estatus en el sistema]
     * @param  [integer] $status [id del status]
     * @return [model]  model    [todo el modelo de acuerdo a la busqueda]
     */
    public function findAllByStatus($status){
     return $this::find()->where(['id_status'=>$status]);
    }

    /**
     * [findAll extrae todos los prestamos]
     * @return [model] [todos los registros de tabla loan]
     */
    public function findAllByLoan(){
      return $this::find()->all();
    }

    /**
     * [findCountByGenderStatus Obtiene la cantidad
     * de hombres o mujeres que tienen un prestamo ya
     * se Activo | en Riesgo | En Mora]
     * @param  [string]  $gender [genero]
     * @param  [integer] $status [id del status]
     * @return [integer]         [cantidad de hombres | Mujeres]
     */
    public function findAllByGenderStatus($gender,$status){
        
        return $this::find()
                ->innerJoin('clients', 'clients.id = loan.id_client')
                ->where([
                  'loan.id_status'=>$status,
                  'clients.gender'=>$gender
                ]);      
    }

    /**
     * [findCountByGenderStatus Obtiene la cantidad
     * de hombres o mujeres que tienen un prestamo ya
     * se Activo | en Riesgo | En Mora]
     * @param  [string]  $gender [genero]
     * @param  [integer] $status [id del status]
     * @return [integer]         [cantidad de hombres | Mujeres]
     */
    public function findCountByGenderStatus($gender,$status){
        
      return $this::find()
                ->innerJoin('clients', 'clients.id = loan.id_client')
                ->where([
                  'loan.id_status'=>$status,
                  'clients.gender'=>$gender
                ])
                ->count();      
    }

    /**
     * [findCountByGender Obtiene la cantidad de 
     * hombres y mujeres que tienen cualquier prestamo]
     * @param  [string] $gender [tipo de genero]
     * @return [integer]        [cantidad de hombres o mujeres]
     */
    public function findCountByGender($gender){

     return $this::find()->innerJoin('clients', 'clients.id = loan.id_client')
                ->where([     
                  'clients.gender'=>$gender
                ])
                ->count();      
    }

    /**
     * [findAllByToday obtiene la cantidad
     * de clientes que poseen prestamos 
     * el dia de hoy]
     * @param  [type] $gender [descripti]
     * @return [type]         [description]
     */
    public function findAllByToday($status,$gender,$fecha){
        return $this::find()                     
                    ->innerJoin('clients', 'clients.id = loan.id_client')
                    ->where([
                        'id_status'=>$status, 
                        'clients.gender'=>$gender,
                        'loan.date_add'=>$fecha]);                    
    }

    /**
     * [findByStatusBetweenDate busca los usuarios 
     * con activos y al dia por rango de fecha]
     * @param  [string] $gender [genero m | f]
     * @param  [date]   $from   [fecha desde donde se hara la busqueda]
     * @param  [date]   $until  [fecha hasta donde se hara la busqueda]
     * @param  [string] $gender [genero m | f]
     * @return [model]        [ Modelo ]     
     */
    public function findByStatusBetweenDate($status,$from,$until,$gender){

        return $this::find()                     
                    ->innerJoin('clients', 'clients.id = loan.id_client')
                    ->where(['id_status'=>$status, 'clients.gender'=>$gender])
                    ->andWhere(['between','loan.date_add',$from,$until]);   
    }

    public function findByStatusGenderMonth($status,$gender,$fecha){

        return $this::find()                     
                    ->innerJoin('clients', 'clients.id = loan.id_client')
                    ->where(['id_status'=>$status, 'clients.gender'=>$gender])                    
                    ->andWhere(" date_part('MONTH', loan.date_add)= $fecha");
    }

    /**
     * [getSumAllByStatus obtiene la suma del monto 
     * de acuerdo al status del prestamo]
     * @param  [integer] $status [status del prestamo]
     * @return [string]         [suma de los monto de los prestamos]
     */
    public function getSumAllByStatus($status){
        return $this::find()
                    ->where(['id_status'=>$status])
                    ->sum('amount');
    }

    /**
     * [getSumByAmountGender Obtiene la suma de los montos
     * de acuerdo al status solicitado]
     * @param  [integer] $status [id del status]
     * @return [model]         [todo el modelo de acuerdo a la busqueda]
     */
    public function getSumAmountByGender($gender,$status){

        return $this::find()
                ->innerJoin('clients', 'clients.id = loan.id_client')
                ->where([
                  'loan.id_status'=>$status,
                  'clients.gender'=>$gender
                ])                
                ->sum('amount');
    }

    /**
     * [getRequestByToday Obtiene la sumatoria de los montos
     * de las solicitudes del dia]
     * @return [model] [todo el modelo con las solicitudes del dia]
     */
    public function getRequestByToday(){      
      return $this::find()
                ->where(['id_status'=>Yii::$app->params['status_vigente']])
                ->andWhere(['date_add'=>date('Y-m-d')])
                ->sum('amount');      
    }

    /**
     * [getRequestByMonth Obtiene la sumatoria de los montos
     * de todas las solicitudes del mes]
     * @param  [integer] $month [numero del mes 01-12]
     * @return [model]        [todo el modelo con las solicitudes del mes]
     */
    public function getRequestByMonth($month){      
      return $this::find()
                ->where(" date_part('MONTH', date_add) = $month")
                ->sum('amount');
    }

    /**
     * [getRequestByAll Obtiene todas la sumatoria
     * de los montos de todas las solicitudes del sistema]
     * @return [model] [todo el modelo con la suma de todos los montos del sistema]
     */
    public function getRequestByAll(){
      return $this::find()
            ->where(['id_status'=>Yii::$app->params['status_pagados']])
            ->sum('amount');
    }

    /**
     * Permite Matchear las solicitudes de prestamos
     * verifica la cuenta y el monto para colocarlos
     * como liquidados
     *
     * @param account string #Cuenta del cliente
     * @param amount string Monto de la solicitud
     * @param referene string referencia de la transaccion
     */
    public function matchLiq($account,$amount,$reference){
        
        $model = new Loan();    // modelo loan
        
        // Busca que usuario de los solicitantes posee la cuenta obtenida del archivo
        // generado por el banco el cual se hicieron las transacciones
        $modelBankAccount = BankAccount::find()
                                        ->where(['code_bank'=> substr($account,0,4),
                                                'number_account'=> substr($account,4,16)])
                                        ->one();               
        
      
        
        // Verifica si existe un usuario con esa cuenta
        if (!empty($modelBankAccount->id_client)){

            $user = $modelBankAccount->getIdClient()->one()->Shortname();   // Nombre y apellido del cliente
            $email = $modelBankAccount->getIdClient()->one()->email;        // email del cliente        

            // Busca al cliente basandose en el monto y en el status del prestamo y el id_cliente generado
            // al buscar la cuenta del cliente
            $model = $model::find()
                            ->where(['id_client'=>$modelBankAccount->id_client,
                                    'amount'=>str_replace(',','.',$amount),
                                    'id_status'=>Yii::$app->params['status_pendiente']])
                            ->one();

            if(!empty($model)){
                        
              $model->id_status = Yii::$app->params['status_vigente']; // se asigna nuevo status                 
              $model->reference_pay = $reference;
              $model->date_transference = date('Y-m-j');
              // Guarda y verifica que el status fue modificado
              if ($model->save(false))                       
                $this->sentSolicAprobada($user,$email,$reference);  

              return true;
            }

        }    
    }


    /**
     * Envia notificacion al cliente para hacerle saber que
     * su solicitud de prestamos fue aprobada 
     * 
     * @param user string Nombre del Usuario
     * @param email string email del usuario     
     * @param reference string referencia de la transferencia     
     */
    public function sentSolicAprobada($user,$email,$reference){   


        $sent = Yii::$app->mailer->compose('layouts/html')
                        ->setFrom('no-reply@tupadrino.net')
                        ->setTo($email)
                        ->setSubject('Notificacion Tupadrino.net')                                                           
                        ->setHtmlBody('<!-- Logo -->
                                        <center>
                                          <img src="http://tupadrino.net/img/logo.png" width="600" height="133" alt=""/>
                                        </center>
                                      <br />
                                      <!--  Saludos-->
                                      <center>
                                        <span style="font-size: 18px; color: #009fc3;"> Saludos ! '.$user.'</span>
                                      <center>
                                      <br /><br /><br />    
                                      <!-- Contenido -->
                                      <center>
                                        <span style="color: #999c92; font-size: 16px;"> 
                                          <b> ¡Listo! </b> Hemos transferido exitosamente el monto solicitado a tu cuenta. <br />                    
                    En las próximas horas lo tendrás disponible y podras disfrutar de los beneficios que te brinda nuestra familia.
                                    <br /><br />
                                    <b> # Referencia:</b> '.$reference.'<br /><br />                                    
                                    Ahora formas parte de una opción efectiva y confiable de crecimiento financiero. <br />
                                    Tener el respaldo de un buen Padrino te da el empuje y la seguridad para encaminar 
                                    nuevos proyectos en todo momento.<br /> No olvides que el ahijado de hoy, es el padrino 
                                    de maÃ±ana y que ayudando a los demás,  también nos ayudamos a nosotros mismos.<br /><br /> 
                                    Creemos en la honestidad, por eso somos el primer sistema basado en la confianza para propiciar el crecimiento financiero.<br /><br />
                                  Tendrás el privilegio de invitar a nuevos miembros a formar parte de nuestra familia,<br /> 
                                  para que también disfruten de estos beneficios. Valoramos tu capacidad de selección.<br />
                                  <br />
                                  Recuerda que para disfrutar de este privilegio debes estar solvente en tus compromisos con <b>Tupadrino.net</b>
                                      </center>
                                      <br /><br /><br /><br /><br /><br />
                                      <footer style=" background-color: #10a1b8; border-top: 1px solid #ddd; height: 40px; padding-top: 20px;">                                        
                                          <span style="float:left; margin-left:10px">&copy; TuPadrino.net 2016</span>
                                          <span style="float:right; margin-right:10px">All Copyright Reserved</span>
                                      </footer>')
                    ->send();  

        if($sent){

          $model = SystemBankAccount::find()->all(); // obtiene todas las cuentas a las cuales se puede transferir
          $cuentas = '';
          
	  // busca las cuentas agregadas a los cuales los usuarios haran su pago
         foreach ($model as $key => $value) {
           $cuentas .= '<span>Cedula/RIF: <strong> '.$value->identity.'</strong></span><br /><br />
                        <span>Banco:<strong> '.$value->getCodeBank()->one()->name.'</strong></span><br /><br />
                        <span>Nombre del Beneficiario:<strong> '.$value->beneficiario.'</strong></span><br /><br />
                        <span>Numero de cuenta:<strong> '.$value->code_bank.''.$value->number_account.'</strong></span><br/><br /><br />';
         }

            Yii::$app->mailer->compose('layouts/html')
                       ->setFrom('no-reply@tupadrino.net')
                       ->setTo($email)
                       ->setSubject('Formas de Pago Tupadrino.net')
                       ->setHtmlBody('<!-- Logo -->
                                       <center>
                                         <img src="http://tupadrino.net/img/logo.png" width="600" height="133" alt=""/>
                                       </center>
                                     <br />
                                     <!--  Saludos-->
                                     <center>
                                       <span style="font-size: 18px; color: #009fc3;"> Saludos ! '.$user.'</span>
                                     <center>
                                     <br /><br /><br />
                                     <!-- Contenido -->
                                     <center>
                                       <span style="color: #999c92; font-size: 16px;">
                                         <b> ¡Saludos! </b> para facilitar el pago de tu compromiso  a continuación te detallamos<br /> los datos que necesitas para la devolución de tu solicitud:<br />
                                         <br /><b>Para depósitos y transferencias:</b> <br />
                                       </span>
                                       <div style=" color: #999c92;"><br />
                                       <center>  '.$cuentas.'</center>
                                       </div>
                                     </center>
                                     <br /> <span style="color: #999c92; font-size: 16px;">
                                       <strong>Para pagos con Tarjeta de Crédito:</strong>
                                       <br>Directamente en</span> <a href="http://tupadrino.net">tupadrino.net</a>
                                     <br /><br /><br /><br /><br /><br />
                                     <footer style=" background-color: #10a1b8; border-top: 1px solid #ddd; height: 40px; padding-top: 20px;">
                                         <span style="float:left; margin-left:10px">&copy; TuPadrino.net 2016</span>
                                         <span style="float:right; margin-right:10px">All Copyright Reserved</span>
                                     </footer>')
                       ->send();
         
 
	 }

        }

    }

    /**
     * Envia notificacion al cliente para hacerle saber que
     * su solicitud de prestamos fue rechazada
     * 
     * @param email string email del usuario
     * @param user string nombre del usuario     
     */
    public function sentSolicRechazada($email,$user){                    
                

        Yii::$app->mailer->compose('layouts/html')
                        ->setFrom('no-reply@tupadrino.net')
                        ->setTo($email)
                        ->setSubject('Notificacion Tupadrino.net')                                                           
                        ->setHtmlBody('<!-- Logo -->
                                        <center>
                                          <img src="http://tupadrino.net/img/logo.png" width="600" height="133" alt=""/>
                                        </center>
                                      <br />
                                      <!--  Saludos-->
                                      <center>
                                        <span style="font-size: 18px; color: #009fc3;"> Saludos ! '.$user.'</span>
                                      <center>
                                      <br /><br /><br />    
                                      <!-- Contenido -->
                                      <center>
                                        <span style="color: #999c92; font-size: 16px;"> 
                                          <b> ¡Lo Sentimos! </b> no se pudo realizar la transferencia debido a que existe inconsistencia en sus datos. <br />                    
                    Le recomendamos que verifique sus datos y proceda nuevamente a realizar la solicitud.
                                    <br /><br />                                    
                                      </center>
                                      <br /><br /><br /><br /><br /><br />
                                      <footer style=" background-color: #10a1b8; border-top: 1px solid #ddd; height: 40px; padding-top: 20px;">                                        
                                          <span style="float:left; margin-left:10px">&copy; TuPadrino.net 2016</span>
                                          <span style="float:right; margin-right:10px">All Copyright Reserved</span>
                                      </footer>')
                    ->send();  

    }

    public static function columTotal($provider, $fieldName){
        $total=0;
        foreach($provider as $item){
          
            if($item['id_status']==32){
                $total+=$item[$fieldName];
            }
        }
        return $total;
    }   

    public static function countQ($provider){
        $pay=0;
        $pending =0;
        foreach($provider as $item){
          
            if($item['id_status']==32){
                $pay++;
            }
            if(in_array($item['id_status'], [30,33])){
                $pending++;
            }
        }
        return ['pay'=>$pay,'pending'=>$pending];
    }  


}
