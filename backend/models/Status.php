<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "status".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 *
 * @property ClientInvitation[] $clientInvitations
 * @property Loan[] $loans
 */
class Status extends \yii\db\ActiveRecord{    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 25],
            [['description'], 'string', 'max' => 55]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientInvitations()
    {
        return $this->hasMany(ClientInvitation::className(), ['id_status' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoans()
    {
        return $this->hasMany(Loan::className(), ['id_status' => 'id']);
    }
    
    public function getStatusMail(){

        return ArrayHelper::map($this::find()
                                ->where(['id'=>[12,13]])
                                ->all(),'id','name');                    
    }
   
    /**
     * [CssSmfclass Permite colocar
     * el color del status del prestamos
     * entre otras palabras el llamado
     * semaforo]
     */
    public function CssSmfclass(){    
        
        switch ($this->id) {
            case Yii::$app->params['consulEnMora']://En Mora
                return 'label-danger';
                break;
            case Yii::$app->params['consulAlDia']:// Al dia
                return 'label-warning';
                break;
            case Yii::$app->params['consulActivos']://Generado
                return 'label-success';
                break;
            case Yii::$app->params['mora_impagada']://En Mora
                return 'label-danger';            
        }
    }
}
