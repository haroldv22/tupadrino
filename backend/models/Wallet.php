<?php

namespace app\models;

use Yii;
use app\models\WalletHistory;

/**
 * This is the model class for table "wallet".
 *
 * @property integer $id
 * @property integer $id_client
 * @property string $balance
 *
 * @property Clients $idClient
 * @property WalletHistory[] $walletHistories
 */
class Wallet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wallet';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_client'], 'required'],
            [['id_client'], 'integer'],
            [['balance'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_client' => 'Id Client',
            'balance' => 'Balance',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'id_client']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWalletHistories(){
        return $this->hasMany(WalletHistory::className(), ['id_wallet' => 'id']);
    }

    /**
     * [payWallet permite asignar a la cartera del cliente 
     * el monto sobrante de su registro de pago]
     * @param  [integer] $amountRest [monto sobrante]
     * @param  [integer] $idClient   [id del cliente]
     * @return [type]             [description]
     */
    public function payWallet($amountRest,$id_record_payment,$idClient){
        
        $modelWallet        = new Wallet();    // modelo Wallet
        $modelWalletHistory = new WalletHistory();      // modelo WalletHistory     
        
        // busca a ver si consigue al cliente en wallet
        $modelWallet = $this::find()
                    ->where(['id_client'=>$idClient])
                    ->one();

                
        if(empty($modelWallet)){

	    $modelWallet        = new Wallet();    // modelo Wallet           
            $modelWallet->id_client = $idClient; // asignamos al cliente
            $modelWallet->balance = $amountRest; // monto total
            
            if($modelWallet->save(false))
                $modelWalletHistory->id_wallet = $modelWallet->id;
                                
        }else{                        
            
            $modelWallet->balance = $modelWallet->balance + $amountRest;    // monto a agregar        

            if($modelWallet->update()!=false)
                $modelWalletHistory->id_wallet = $modelWallet->id;
        }


            $modelWalletHistory->amount             = $amountRest;
            $modelWalletHistory->description        = 'Cantidad de sobra al momento de conciliar un pago';
            $modelWalletHistory->id_record_payment  = $id_record_payment;
            
            $modelWalletHistory->save(false);
    }    

}
