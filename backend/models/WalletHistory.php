<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "wallet_history".
 *
 * @property integer $id
 * @property integer $id_wallet
 * @property string $amount
 * @property string $description
 * @property integer $id_record_payment
 *
 * @property RecordPayment $idRecordPayment
 * @property Wallet $idWallet
 */
class WalletHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wallet_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_wallet', 'amount', 'description', 'id_record_payment'], 'required'],
            [['id_wallet', 'id_record_payment'], 'integer'],
            [['amount'], 'number'],
            [['description'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_wallet' => 'Id Wallet',
            'amount' => 'Amount',
            'description' => 'Description',
            'id_record_payment' => 'Id Record Payment',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRecordPayment()
    {
        return $this->hasOne(RecordPayment::className(), ['id' => 'id_record_payment']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdWallet()
    {
        return $this->hasOne(Wallet::className(), ['id' => 'id_wallet']);
    }
}
