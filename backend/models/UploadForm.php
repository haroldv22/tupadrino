<?php

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;

class UploadForm extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $importFile; //para los archivos formato excel
    public $imgProfile; // para perfil de imagen del usuario
    public $importInvitation; // para importacion de invitaciones masivamente
    public $importLiq; // para importacion de conciliacion de liquidaciones masivamente
    public $importCob; // para importacion de conciliacion de Registros de pagos Masivamente

    public function rules()
    {
        return [
            [['importFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'xls, xlsx'],
            [['importInvitation'], 'file', 'skipOnEmpty' => true, 'extensions' => 'xls, xlsx', 'on' => 'invitations'],
            [['imgProfile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, png, jpeg', 'on' => 'profile'],
            [['importLiq'], 'file', 'skipOnEmpty' => false, 'extensions' => 'txt','message'=>'Error debe ser formato .txt', 'on' => 'match'],
	   [['importCob'], 'file', 'skipOnEmpty' => false, 'extensions' => ['csv','xls', 'xlsx','txt'],'checkExtensionByMimeType' => false ,'message'=>'Error debe ser formato .xls / .xlsx / .txt', 'on' => 'matchCob'],
        ];
    }
       
     /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'importFile' => 'Seleccione el Calendario a Importar',            
            'imgProfile' => 'Seleccione una imagen de perfil',            
            'importInvitation' => 'Seleccione archivo para hacer invitaciones masivamente',            
            'importLiq' => 'Seleccione archivo para hacer conciliacion de liquidaciones',
	    'importCob' => 'Seleccione archivo para hacer conciliacion con los registros de pagos',
        ];
    }

    /**
    * Permite almacenar el archivo en el directorio que se le
    * especifique
    * @param $directory url del archivo
    */
    public function upload($directory){            
        
        if ($this->validate()) {
            $this->imgProfile->saveAs('.'.$directory . $this->imgProfile->baseName . '.' . $this->imgProfile->extension);
            return true;
        } else {
            return false;
        }
    }

}
