<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "city".
 *
 * @property integer $id
 * @property integer $id_state
 * @property string $name
 * @property boolean $capital
 *
 * @property Address[] $addresses
 * @property State $idState
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_state', 'name', 'capital'], 'required'],
            [['id_state'], 'integer'],
            [['capital'], 'boolean'],
            [['name'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id de la tabla',
            'id_state' => 'id del estado  al que pertenece',
            'name' => 'nombre de la ciudad',
            'capital' => 'establece si es una capital o no',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(Address::className(), ['id_city' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdState()
    {
        return $this->hasOne(State::className(), ['id' => 'id_state']);
    }
}
