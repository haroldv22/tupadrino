<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "messages_system".
 *
 * @property integer $id
 * @property integer $user_system
 * @property string $subject
 * @property string $message
 * @property integer $id_status
 * @property integer $id_client
 * @property string $date_sent
 * @property boolean $read
 *
 * @property Clients $idClient
 * @property Status $idStatus
 * @property User $userSystem
 */
class MessagesSystem extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName(){
        return 'messages_system';
    }
    

    /**
     * @inheritdoc
     */
    public function rules(){
        return [
            [['user_system', 'id_status', 'id_client', 'date_sent'], 'required'],
            [['user_system', 'id_status', 'id_client'], 'integer'],
            [['message'], 'string'],
            [['date_sent'], 'safe'],            
            [['subject'], 'string', 'max' => 75]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(){
        return [
            'id' => 'id',
            'user_system' => 'Id del usuario del sistema',
            'subject' => 'Asunto del mensaje a enviar',
            'message' => 'Contenido en el mensaje',
            'id_status' => 'estatus del mensaje',
            'id_client' => 'Id del cliente a quien se le envia el mensaje',
            'date_sent' => 'fecha en la que fue enviado el mensaje',            
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdClient(){
        return $this->hasOne(Clients::className(), ['id' => 'id_client']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdStatus(){
        return $this->hasOne(Status::className(), ['id' => 'id_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserSystem(){
        return $this->hasOne(User::className(), ['id' => 'user_system']);
    }

    /** 
    * Permite almacenar en la bd el registro
    * @param subject string  titulo del mensaje      
    * @param message string contenido del mensaje
    * @param client integer id del cliente
    * @return true | false si el mensaje fue almacenado o NO
    */
    public function saveMsgSys($subject,$message,$client){
        
       $model = new MessagesSystem();
        
        $model->user_system = Yii::$app->user->identity->id;
        $model->subject = $subject;
        $model->message = $message;
        $model->id_status = Yii::$app->params['msg_enviado'];
        $model->id_client = $client;
        $model->date_sent = date("Y-m-d");        
        
        return $model->save(false);
    }

    /**
    * Permite enviar mensajes a los clientes escogidos
    * @param client integer Id del cliente
    * @param subject string asunto del mensaje
    * @param message string mensaje
    * @param toEmail string email del cliente    
    */
    public function sentMessage($client,$subject,$message,$toEmail){
    
      $sent =   Yii::$app->mailer->compose('layouts/html')
                    ->setFrom(Yii::$app->params['adminEmail'])
                    ->setTo($toEmail)
                    ->setSubject($subject)                                   
                    ->setHtmlBody('
                          <!-- Logo -->
                            <center>
                              <img src="http://ec2-52-32-73-69.us-west-2.compute.amazonaws.com/common/web/img/logo.png" width="600" height="133" alt=""/>
                            </center>
                          <br />
                          <!--  Saludos-->
                          <center>
                            <span style="font-size: 18px; color: #009fc3;"> Saludos ! '.$client.'</span>
                          <center>
                          <br /><br /><br />    
                          <!-- Contenido -->
                          <center>
                            <span style="color: #999c92; font-size: 16px;"> 
                            '.$message.'
                            </span>
                          </center>
                          <br /><br /><br /><br /><br /><br />
                          <footer style=" background-color: #10a1b8; border-top: 1px solid #ddd; height: 40px; padding-top: 20px;">                                        
                              <span style="float:left; margin-left:10px">&copy; TuPadrino.net 2016</span>
                              <span style="float:right; margin-right:10px">All Copyright Reserved</span>
                          </footer>')
                    ->send();
    
    }

    /** 
    * Permite contar los mensajes enviados
    * @param status integer status
    * @return integer cantidad de registros eliminados | enviados
    */
    public function count($status){
        
        return $this::find()
                    ->where(['id_status'=> $status])
                    ->count();
    }

    /**
     * Muestra el mensaje de forma mas corta
     * para mostrar una breve descripcion
     * del mensaje
     */
    public function ShortMessage(){

        $msg = $this->message;    
        return  substr(strip_tags($msg), 0, 100).'....';
        
    }

}
