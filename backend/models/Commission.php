<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "commission".
 *
 * @property integer $id
 * @property integer $id_client
 * @property integer $id_commission_round
 * @property integer $id_loan
 * @property string $commission
 * @property string $commission_rate
 * @property integer $id_status
 * @property string $end_date
 *
 * @property Clients $idClient
 * @property CommissionRound $idCommissionRound
 * @property Loan $idLoan
 * @property Status $idStatus
 */
class Commission extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'commission';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_client', 'id_commission_round', 'id_loan', 'commission', 'commission_rate', 'id_status'], 'required'],
            [['id_client', 'id_commission_round', 'id_loan', 'id_status'], 'integer'],
            [['commission', 'commission_rate'], 'number'],
            [['end_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'id_client' => 'Cliente',
            'id_commission_round' => 'Round Comision',
            'id_loan' => 'Prestamo',
            'commission' => 'Comision',
            'commission_rate' => 'Porcentaje de la comision',
            'id_status' => 'Estatus',
            'end_date' => 'Fecha Limite',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'id_client']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCommissionRound()
    {
        return $this->hasOne(CommissionRound::className(), ['id' => 'id_commission_round']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLoan()
    {
        return $this->hasOne(Loan::className(), ['id' => 'id_loan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdStatus(){
        return $this->hasOne(Status::className(), ['id' => 'id_status']);
    }
    
}
