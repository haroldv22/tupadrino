<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "record_payment".
 *
 * @property integer $id
 * @property integer $id_client
 * @property string $amount
 * @property string $date_add
 * @property string $method
 * @property string $type
 * @property integer $id_status
 * @property string $pay_date
 * @property string $code_bank_origin
 * @property string $code_bank_destiny
 * @property string $money_excess
 * @property string $reference
 *
 * @property ArrearsPayment[] $arrearsPayments
 * @property QuotaPayment[] $quotaPayments
 * @property Banks $codeBankOrigin
 * @property Banks $codeBankDestiny
 * @property Clients $idClient
 * @property Status $idStatus
 */
class RecordPayment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'record_payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_client', 'amount', 'date_add', 'method', 'type', 'id_status', 'pay_date'], 'required'],
            [['id_client', 'id_status'], 'integer'],
            [['amount', 'money_excess'], 'number'],
            [['date_add', 'pay_date'], 'safe'],
            [['method'], 'string', 'max' => 5],
            [['type', 'reference'], 'string', 'max' => 20],
            [['code_bank_origin', 'code_bank_destiny'], 'string', 'max' => 4]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_client' => 'cliente',
            'amount' => 'monto cancelado',
            'date_add' => 'fecha del pago de la cuota',
            'method' => 'Metodo',
            'type' => 'hace referencia al tipo de pago si es a una cuota o mora',
            'id_status' => 'Estatus del pago',
            'pay_date' => 'Pay Date',
            'code_bank_origin' => 'codigo identificador del banco',
            'code_bank_destiny' => 'Codigo del banco receptor',
            'money_excess' => 'Money Excess',
            'reference' => 'numero de referencia del deposito o la transferencia',
                   
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArrearsPayments()
    {
        return $this->hasMany(ArrearsPayment::className(), ['id_record_payment' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuotaPayments()
    {
        return $this->hasMany(QuotaPayment::className(), ['id_record_payment' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodeBankOrigin()
    {
        return $this->hasOne(Banks::className(), ['code_bank' => 'code_bank_origin']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodeBankDestiny()
    {
        return $this->hasOne(Banks::className(), ['code_bank' => 'code_bank_destiny']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'id_client']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdStatus(){
        return $this->hasOne(Status::className(), ['id' => 'id_status']);
    }

     /**
     * Este metodo se encarga de enviar un mail al usuario 
     * para notificarle que su registro de pago fue verificado     
     *
     * @param model modelo RecordPayment del cliente
     *
     */ 
    public function sentConfirmPay($model){
        
        // Se verifica si es el ultimo pago de este usuario
        $finishPay = $this::find()        
                ->where(['id_client'=>$model->id_client,
                         'id_status'=>Yii::$app->params['procesando']])
                ->exists();
      

        if(empty($finishPay)){

            $msg = '<!-- Logo -->
            <center>
              <img src="http://tupadrino.net/img/logo.png" width="600" height="133" alt=""/>
            </center>
            <br />
            <!--  Saludos-->
            <center>
            <span style="color: #999c92; font-size: 16px;"> 
                
                <br /><br />¡Excelente! <b>'.$model->getIdClient()->one()->Shortname().'</b>, has devuelto maravillosamente tu préstamo. Agradecemos tu puntualidad y honestidad. Por haber cumplido exitosamente con tus compromisos disfrutas de nuevos beneficios que te brinda Tupadrino.net<br /><br />
            </span>
            </center>
            <br /><br /><br /><br /><br /><br />
            <footer style=" background-color: #10a1b8; border-top: 1px solid #ddd; height: 40px; padding-top: 20px;">                                        
              <span style="float:left; margin-left:10px">&copy; TuPadrino.net 2016</span>
              <span style="float:right; margin-right:10px">All Copyright Reserved</span>
            </footer>';

        
        }else{
                $msg = '<!-- Logo -->
                <center>
                  <img src="http://tupadrino.net/img/logo.png" width="600" height="133" alt=""/>
                </center>
                <br />
                <!--  Saludos-->
                <center>
                <span style="color: #999c92; font-size: 16px;"> 
                <br /><br />¡Estimado! <b>'.$model->getIdClient()->one()->Shortname().'</b>, hemos
                recibido un pago de Bs. ('.number_format($model->amount, 2, ',', ' ').') correspondiente
                al compromiso que asumiste con <b>TUPADRINO</b>. <br /> Agradecemos tu puntualidad y honestidad,
                sigue cumpliendo con tu responsabilidad para que en el futuro <br />puedas aumentar los beneficios
	        que te brinda <b>TUPADRINO.NET</b>.<br /><br />
                </span>
                </center>
                <br /><br /><br /><br /><br /><br />
                <footer style=" background-color: #10a1b8; border-top: 1px solid #ddd; height: 40px; padding-top: 20px;">                                        
                  <span style="float:left; margin-left:10px">&copy; TuPadrino.net 2016</span>
                  <span style="float:right; margin-right:10px">All Copyright Reserved</span>
                </footer>';
        }

        $sent = Yii::$app->mailer->compose('layouts/html')
            ->setFrom('no-reply@tupadrino.net')                    
            ->setTo($model->getIdClient()->one()->email)
            ->setSubject('Informacion de Pago TuPadrino.Net')                                   
            ->setHtmlBody($msg)
            ->send();              
    }

    public function sentRejectedPay($model){

         $msg = '<!-- Logo -->
                <center>
                  <img src="http://tupadrino.net/img/logo.png" width="600" height="133" alt=""/>
                </center>
                <br />
                <!--  Saludos-->
                <center>
                <span style="color: #999c92; font-size: 16px;"> 
                    
                    <br /><br />¡Lo Sentimos! <b>'.$model->getIdClient()->one()->Shortname().'</b>, tu pago registrado con el siguiente numero de referencia <b>('.$model->reference.')</b><br /> no pudo ser conciliado. <br /><br />Verifique bien los datos y vuelva a registrar el pago para que este sea conciliado y así seguir disfrutando de los beneficios que te ofrece Tupadrino.net<br /><br />
                </span>
                </center>
                <br /><br /><br /><br /><br /><br />
                <footer style=" background-color: #10a1b8; border-top: 1px solid #ddd; height: 40px; padding-top: 20px;">                                        
                  <span style="float:left; margin-left:10px">&copy; TuPadrino.net 2016</span>
                  <span style="float:right; margin-right:10px">All Copyright Reserved</span>
                </footer>';

         $sent = Yii::$app->mailer->compose('layouts/html')
            ->setFrom('no-reply@tupadrino.net')                    
            ->setTo($model->getIdClient()->one()->email)
            ->setSubject('Informacion de Pago TuPadrino.Net')                                   
            ->setHtmlBody($msg)
            ->send(); 

    }
}

