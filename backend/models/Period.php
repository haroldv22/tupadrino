<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "period".
 *
 * @property integer $id
 * @property integer $n_days
 * @property string $alias
 * @property integer $points
 *
 * @property PeriodAmount[] $periodAmounts
 */
class Period extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'period';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['n_days', 'points'], 'required'],
            [['n_days', 'points'], 'integer'],
            [['alias'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'n_days' => '#Dias',
            'alias' => 'Plazo(1 mes, 2 meses,..)',
            'points' => 'Puntos por cumplir ese periodo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodAmounts()
    {
        return $this->hasMany(PeriodAmount::className(), ['id_period' => 'id']);
    }

    /**
     * [getPeriodDefault Obtiene los Periodos
     * que faltantes para poder asignar a un monto
     * en especifico]
     * @param  [integer] id [Id del level Amount a la cual pertenece el monto]
     * @return [type] [description]
     */
    public function getPeriodDefault($id){

        //obtengo los montos 
        $period = PeriodAmount::find()->where(['id_level_amount'=>$id])->all();
        $arrayPer = [];
        $arrNew = [];

        // print_r($period);
        foreach ($period as $value) {           
            $arrayPer [] = $value->id_period;
        }
        // print_r();

        $periodAmount = PeriodAmount::find()
                        ->where(['id_level_amount'=>$id,
                                 'id_period'=>$arrayPer
                                ])
                        ->all();                        
        
        $per = $this::find()->all();
        
        $i = 1;
        $arrCount = count($arrayPer);       
        
        foreach ($per as $valPerDefault) {
            
            if($i > $arrCount){                             
                $arrNew [] = $valPerDefault->id;
            }           
            $i++;
        }

        return $arrNew;
    }

}
