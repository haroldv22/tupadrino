<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "parish".
 *
 * @property integer $id
 * @property integer $id_municipality
 * @property string $name
 *
 * @property Address[] $addresses
 * @property Municipality $idMunicipality
 */
class Parish extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parish';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_municipality', 'name'], 'required'],
            [['id_municipality'], 'integer'],
            [['name'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id de la tabla',
            'id_municipality' => 'id del municipio al que pertenece',
            'name' => 'nombre de la parroquia',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(Address::className(), ['id_parish' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMunicipality()
    {
        return $this->hasOne(Municipality::className(), ['id' => 'id_municipality']);
    }
}
