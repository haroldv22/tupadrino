<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CommissionRound;

/**
 * CommissionRoundSearch represents the model behind the search form about `app\models\CommissionRound`.
 */
class CommissionRoundSearch extends CommissionRound
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_client', 'id_status'], 'integer'],            
            [['end_date', 'add_date'], 'safe'],
            [['to_pay', 'expected_pay', 'risk_pay'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params){
          
        $arr = array();

        // Se verifica si el buscador trae algo
        if (!empty($params)){
            // Busca primero a el nombre y apellido del cliente a buscar
            $bClient = Clients::find()
                              ->orFilterWhere(['ilike', 'firstname', $params['Clients']['firstname']])
                              ->orFilterWhere(['ilike', 'lastname', $params['Clients']['lastname']])
                              ->asArray()
                              ->all();
          
            // Lo asigno en un array para luego buscarlo en
            // la tabla commission_round                              
            foreach ($bClient as $key => $value) {
                array_push($arr, $value['id']);
            }
                
        }
                
        $query = CommissionRound::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 6,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,                
                ]
            ],
        ]);

        $this->load($params);

        

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
// Yii::$app->end();
        $query->andFilterWhere(['id_client'=> $arr])
              ->andFilterWhere(['to_pay' => $this->to_pay])
              ->andFilterWhere(['id_status' => $this->id_status]);
        // andFilterWhere([
                // 'id' => $this->id,
                // 'id_client' => $arr,
                // 'end_date' => $this->end_date,
                // 'to_pay' => $this->to_pay,
                // 'id_status' => $this->id_status,
                // 'add_date' => $this->add_date,
                // 'expected_pay' => $this->expected_pay,
                // 'risk_pay' => $this->risk_pay,
        // ])

        return $dataProvider;
    }


}
