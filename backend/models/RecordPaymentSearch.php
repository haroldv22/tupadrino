<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RecordPayment;

/**
 * RecordPaymentSearch represents the model behind the search form about `app\models\RecordPayment`.
 */
class RecordPaymentSearch extends RecordPayment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_client', 'id_status'], 'integer'],
            [['amount', 'money_excess'], 'number'],
            [['date_add', 'method', 'type', 'pay_date', 'code_bank_origin', 'code_bank_destiny', 'reference'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        

        $query = RecordPayment::find()
                              ->where(['pay_date'=>'2016-05-06']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_client' => $this->id_client,
            'amount' => $this->amount,
            'date_add' => $this->date_add,
            'id_status' => $this->id_status,
            'pay_date' => $this->pay_date,
            'money_excess' => $this->money_excess,
        ]);

        $query->andFilterWhere(['like', 'method', $this->method])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'code_bank_origin', $this->code_bank_origin])
            ->andFilterWhere(['like', 'code_bank_destiny', $this->code_bank_destiny])
            ->andFilterWhere(['like', 'reference', $this->reference]);

        return $dataProvider;
    }
}
