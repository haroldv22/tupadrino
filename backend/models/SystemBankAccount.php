<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "system_bank_account".
 *
 * @property integer $id
 * @property string $code_bank
 * @property string $number_account
 * @property string $beneficiario
 * @property string $identity
 *
 * @property Banks $codeBank
 */
class SystemBankAccount extends \yii\db\ActiveRecord{

    // public $number_account;
    // public $code_bank;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'system_bank_account';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code_bank', 'number_account', 'beneficiario', 'identity'], 'required'],
            [['code_bank'], 'string', 'max' => 4],
            [['number_account'], 'string', 'max' => 16],
            [['beneficiario'], 'string', 'max' => 20],
            [['identity'], 'string', 'max' => 10],
            ['number_account','verificarCuenta']

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code_bank' => 'Banco',
            'number_account' => 'Numero de Cuenta',
            'beneficiario' => 'Beneficiario',
            'identity' => 'Cedula/RIF',
        ];
    }

    /**
     * Permite verificar las cuentas      
     */
    public function verificarCuenta(){
    
        $banco = $this->code_bank;
        $cuentac = $this->number_account;
        $oficina = substr($cuentac,0,4);
        $cuenta = substr($cuentac,6,10);
        $calc1 = str_split($banco.$oficina);
        $calc2 = str_split($oficina.$cuenta);

        $peso =[0=>3,1=>2,2=>7,3=>6,4=>5,5=>4,6=>3,7=>2,8=>7,9=>6,10=>5,11=>4,12=>3,13=>2];

        $calc = [0=>$calc1,1=>$calc2];
        $dc = '';
        foreach ($calc as $calvalue) {
            $dig = 0;
            foreach ($calvalue as $key => $value) {
                $dig+=$value*$peso[$key];
            }
            $entero =intval($dig/11);
            $residuo = intval($dig%11);
            $result = 11-$residuo;
            if($result==11){
                $result = 1;
            }
            elseif ($result==10) {
                $result =0;
            }
            $dc.=$result;
        }
        $dco = substr($cuentac,4,2);
    
        if($dco != $dc){
                $this->addError('number_account', 'La cuenta ingresada es incorrecta.');
        }            
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodeBank()
    {
        return $this->hasOne(Banks::className(), ['code_bank' => 'code_bank']);
    }
}

   