<?php

namespace app\models;

use Yii;
use yii\base\Security;

/**
 * This is the model class for table "client_invitation".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $invitation_code
 * @property integer $id_status
 * @property integer $id_godfather
 * @property string $date_exp
 *
 * @property Status $idStatus
 */
class ClientInvitation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_invitation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'invitation_code', 'id_status','date_add', 'id_godfather'], 'required'],
            [['id_status', 'id_godfather'], 'integer'],
            [['date_exp','date_add'], 'safe'],
            [['name'], 'string', 'max' => 25],
            [['email'], 'string', 'max' => 75],
            [['invitation_code'], 'string', 'max' => 128],
            [['email'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Nombre',
            'email' => 'Email',
            'invitation_code' => 'codigo de invitacion para el usuario',
            'id_status' => 'Estatus',
            'id_godfather' => 'id del padre',
            'date_exp' => 'Fecha de expiracion',
	    'date_add' => 'Fecha de Invitacion',
        ];
    }

    public function getClients(){
        return $this->hasMany(Clients::className(), ['id_client_invitation' => 'id']);
    }

  /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'id_status']);
    }
    
      /**
     * Se encarga de generar un codigo basandose en ciertos
     * patrones para ser asignados al usuario a invitar
     *      
     * return string 
     */ 
    public function getInvitationCode(){
        
        $security = new Security();                
        $code =  utf8_decode($security->generateRandomString(12).''.getdate()['year'].''.getdate()['yday'].''.getdate()['hours'].''.getdate()['seconds']);
        
        return $code;
    }
   
    /**
     * [findCountByStatus Obtiene la cantidad de 
     * Usuarios Invitados Actibvos e Inactivos]
     * @param  [integer] $status [Id del status]
     * @return [integer]         [cantidad de usuarios Activos | Inactivos] 
     */
    public function findCountByStatus($status){
      return $this::find()
              ->where(['id_status'=>$status])
              ->count();
    }   

    /**
     * Este metodo se encarga de enviar un mail al usuario 
     * para notificarle que puede hacer uso de la plataforma tupadrino.net
     *
     * @param fromEmail String  Email remitente
     * @param toEmail   String  Email del del invitado
     * @param user      String  Nombre del invitado
     */ 
    public function sentInvitationUser($fromEmail,$toEmail,$user,$code_token){
      
      $sent =   Yii::$app->mailer->compose('layouts/html')                    
		    ->setFrom('no-reply@tupadrino.net')
                    ->setTo($toEmail)
                    ->setSubject('Invitacion tu padrino.net')                                   
                    ->setHtmlBody('
                          <!-- Logo -->
                            <center>
                              <img src="http://tupadrino.net/img/logo.png" width="600" height="133" alt=""/>
                            </center>
                          <br />
                          <!--  Saludos-->
                          <center>
                            <span style="font-size: 18px; color: #009fc3;"> Saludos ! '.$user.'</span>
                          <center>
                          <br /><br /><br />    
                          <!-- Contenido -->
                          <center>
                            <span style="color: #999c92; font-size: 16px;"> 
                              <b>¡Felicidades!</b> Eres un nuevo afortunado. Un buen amigo te recomendó para ser parte de nuestra familia<br /> y comenzar a crecer juntos
                              <br /><br />A traves del siguiente link, podras entrar a tu <b>Tupadrino.net</b> y realizar una solicitud de apoyo financiero<br />
                              Para comenzar: <span style="font-size: 16px; color: #009fc3;"> <a href="http://tupadrino.net/index.php/registro/registration?token='.$code_token.'" target="_blank">Haz Click Aqui</a></span></span>
                          </center>
                          <br /><br /><br /><br /><br /><br />
                          <footer style=" background-color: #10a1b8; border-top: 1px solid #ddd; height: 40px; padding-top: 20px;">                                        
                              <span style="float:left; margin-left:10px">&copy; TuPadrino.net 2016</span>
                              <span style="float:right; margin-right:10px">All Copyright Reserved</span>
                          </footer>')
                    ->send();
    }
}
