<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "message_default".
 *
 * @property integer $id
 * @property string $message
 * @property string $description
 */
class MessageDefault extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message_default';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message', 'description'], 'required'],
            [['message'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 75]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'message' => 'Mensaje',
            'description' => 'Descripcion',
        ];
    }
}
