<?php

namespace app\models;

use Yii;
use app\models\LoanQuota;
use app\models\Wallet;

/**
 * This is the model class for table "quota_payment".
 *
 * @property integer $id
 * @property integer $id_record_payment
 * @property integer $id_loan_quota
 *
 * @property LoanQuota $idLoanQuota
 * @property RecordPayment $idRecordPayment
 */
class QuotaPayment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'quota_payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_record_payment', 'id_loan_quota'], 'required'],
            [['id_record_payment', 'id_loan_quota'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id de la tabla',
            'id_record_payment' => 'id del pago asociado',
            'id_loan_quota' => 'id de la cuota asociada al pago',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLoanQuota()
    {
        return $this->hasOne(LoanQuota::className(), ['id' => 'id_loan_quota']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRecordPayment(){
        return $this->hasOne(RecordPayment::className(), ['id' => 'id_record_payment']);
    }

    /**
     * Permite revisar cual es la cuota que se esta 
     * cancelando y a su vez cambiar el status 
     * a la cuota cancelada asi como debitar los pagos
     * @param amount string monto a cancelar
     * @param id integer obtiene el id de la quota cancelada (record_payment)
     * @param id_client integer id del cliente
     */    
    public function quotaPay($amount,$id,$id_client){

       $modelLoanQuota = new LoanQuota(); // modelo LoanQuota

        // Busca el registro que se va a debitar
        $modelQuoPay = $this::find()
                ->where(['id_record_payment'=>$id])->one();                                        

        $pendingPay = $modelQuoPay->getIdLoanQuota()->one()->pending_pay; // obtiene el pago pendiente de esa cuota
        $idLoanQuota = $modelQuoPay->getIdLoanQuota()->one()->id; // obtiene el id del registro de esa cuota

        $modelLoanQuota = $modelLoanQuota::findOne($idLoanQuota); // me ubico en el registro que quiero modificar
       
        if ($amount == $pendingPay){        

            $modelLoanQuota->id_status = Yii::$app->params['cuota_pagada'];            
            $modelLoanQuota->pending_pay =  0;        
            $modelLoanQuota->update();
        
        }else if($amount < $pendingPay){                    
            $modelLoanQuota->pending_pay =  $amount - $pendingPay;
            $modelLoanQuota->update();
        
        }else if ($amount > $pendingPay){  

            $modelWallet = new Wallet(); // modelo Wallet

            $modelLoanQuota->id_status = Yii::$app->params['cuota_pagada'];                    
            $modelLoanQuota->pending_pay = 0;
            
            if ($modelLoanQuota->update() != false){
                
                $amountRest = $amount - $pendingPay; // monto restante                            
                
                // se coloca ASC por que lo estaba mostrando al reves
                $model = LoanQuota::find()
                    ->where(['id_loan'=>$modelLoanQuota->id_loan])
                    ->andWhere(['!=','id_status',Yii::$app->params['cuota_pagada']])
                    ->orderBy('id ASC')
                    ->all();                

                // si no cosnigue alguna cuota a donde pueda debitar el monto
                // restante se va de inmediato a la cartera del cliente                
                if(empty($model)){                                
                    $modelWallet->payWallet($amountRest,$id,$id_client);
                
                }else{
                    
                    $amountI = 0; // lleva la cantidad restada
                    foreach ($model as $key => $value) {
                        
                        $modelLoanQuota = new LoanQuota();
                        $modelLoanQuota = $modelLoanQuota::findOne($value->id);
                        $amountI = $amountRest;
                        
                        if ($amountI > $value->pending_pay){                        
                            $modelLoanQuota->id_status = Yii::$app->params['cuota_pagada'];
                            $modelLoanQuota->pending_pay = 0;
                            
                            if($modelLoanQuota->update()!=false)
                                $amountRest = number_format((float)$amountRest - $value->pending_pay , 2, '.', '');                                                        
                        }if($amountI < $value->pending_pay){
                        
                            $modelLoanQuota->pending_pay = number_format((float)$value->pending_pay - $amountRest , 2, '.', '');

                            if($modelLoanQuota->update()!=false){
                                $amountRest = 0;
                                break;                                
                            }

                        }if ($amountI == $value->pending_pay){
                            
                            $modelLoanQuota->id_status = Yii::$app->params['cuota_pagada'];
                            $modelLoanQuota->pending_pay = 0;
                            $amountRest = $value->pending_pay - $amountRest;

                            if($modelLoanQuota->update()!=false)
                                break;                                
                        }                       
                    }

                    if($amountRest > 0)
                        $modelWallet->payWallet($amountRest,$id,$id_client);
                }                            
            }
        }
	
	 // Esta consulta es para verificar que no esta quedando ningun
         // status en vencido ni en mora para luego proceder de ser asi
         // a cambiar el status del prestamo principal en caso de que se
         // encuentre vencido
         $validate_status_quotas = false;
         $validate_status_quotas = LoanQuota::find()
                           ->where(["id_loan"=>$idLoan,
                                    "id_status"=>[
                                      Yii::$app->params['cuota_vencida'],
                                      Yii::$app->params['cuota_mora']
                                     ]])
                           ->exists();

         if($validate_status_quotas == false){
            $modelLoan = Loan::findOne($idLoan);

           if ($modelLoan->id_status == Yii::$app->params['status_vencido']){

               $modelLoan->id_status = Yii::$app->params['status_vigente'];
               $modelLoan->update(false);

           }
         }
         ///////////////////////////////////////////////////////////////////
    }

}
