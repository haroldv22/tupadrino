<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "level".
 *
 * @property integer $id
 * @property string $rate
 * @property integer $id_level_next
 * @property integer $godson_cant
 * @property string $condition
 * @property integer $points_required
 *
 * @property Clients[] $clients
 * @property LevelAmount[] $levelAmounts
 * @property Quota[] $quotas
 */
class Level extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'level';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'rate', 'id_level_next', 'godson_cant'], 'required'],
            [['id', 'id_level_next', 'godson_cant', 'points_required'], 'integer'],
            [['rate'], 'number'],
            [['condition'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [           
           'id' => 'Id',    
           'rate' => 'Tasa de interes',
           'id_level_next' => 'Proximo Level',
           'godson_cant' => 'Ahijados a recomendar',                    
            'condition' => 'Condicion funcion php',
            'points_required' => 'puntos requeridos para pasar nivel',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this->hasMany(Clients::className(), ['id_level' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLevelAmounts()
    {
        return $this->hasMany(LevelAmount::className(), ['id_level' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuotas()
    {
        return $this->hasMany(Quota::className(), ['id_level' => 'id']);
    }
}
