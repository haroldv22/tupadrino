<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "generated_requests".
 *
 * @property integer $id
 * @property string $url
 * @property string $date_add
 * @property integer $id_status
 *
 * @property Status $idStatus
 */
class GeneratedRequests extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'generated_requests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url', 'date_add'], 'required'],
            [['date_add'], 'safe'],
            [['id_status'], 'integer'],
            [['url'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'direccion en donde se encuentran los .txt generados diariamente',
            'date_add' => 'fecha en la cual se genero el archivo.txt el cual contiene las solicitudes.',
            'id_status' => 'estatus del archivo generado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'id_status']);
    }
}
