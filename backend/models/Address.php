<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "address".
 *
 * @property integer $id
 * @property integer $id_client
 * @property string $address
 * @property integer $id_municipality
 * @property integer $id_state
 * @property integer $id_parish
 * @property integer $id_city
 * @property string $cellphone
 * @property string $phonenumber
 *
 * @property City $idCity
 * @property Clients $idClient
 * @property Municipality $idMunicipality
 * @property Parish $idParish
 * @property State $idState
 */
class Address extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(){
        return 'address';
    }

    /**
     * @inheritdoc
     */
    public function rules(){
        return [
            [['id_client', 'address', 'id_municipality', 'id_state', 'id_parish', 'id_city', 'cellphone'], 'required'],
            [['id_client', 'id_municipality', 'id_state', 'id_parish', 'id_city'], 'integer'],
            [['address'], 'string', 'max' => 255],
            [['cellphone', 'phonenumber'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(){
        return [
            'id' => 'ID',
            'id_client' => 'Id Client',
            'address' => 'Address',
            'id_municipality' => 'Id Municipality',
            'id_state' => 'Id State',
            'id_parish' => 'Id Parish',
            'id_city' => 'Id City',
            'cellphone' => 'numero celular',
            'phonenumber' => 'numero telefonico
',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCity(){
        return $this->hasOne(City::className(), ['id' => 'id_city']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdClient(){
        return $this->hasOne(Clients::className(), ['id' => 'id_client']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMunicipality(){
        return $this->hasOne(Municipality::className(), ['id' => 'id_municipality']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdParish(){
        return $this->hasOne(Parish::className(), ['id' => 'id_parish']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdState(){
        return $this->hasOne(State::className(), ['id' => 'id_state']);
    }
}
