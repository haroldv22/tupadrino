<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "clients".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $gender
 * @property integer $id_godfather
 * @property integer $id_level
 * @property integer $loan_count
 * @property string $birth_date
 * @property string $identity
 * @property string $date_add
 * @property integer $godson_count
 * @property integer $id_user_system
 *
 * @property Address[] $addresses
 * @property Arrears[] $arrears
 * @property BankAccount[] $bankAccounts
 * @property Level $idLevel
 * @property ClientsPwd $clientsPwd
 * @property Commission[] $commissions
 * @property CommissionRound[] $commissionRounds
 * @property Loan[] $loans
 * @property Question[] $questions
 * @property RecordPayment[] $recordPayments
 */
class Clients extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'email', 'gender', 'birth_date', 'date_add'], 'required'],
            [['id_godfather', 'id_level', 'loan_count', 'godson_count', 'id_user_system'], 'integer'],
            [['birth_date', 'date_add'], 'safe'],
            [['firstname', 'lastname'], 'string', 'max' => 50],
            // [['firstname', 'lastname'],'string', 'max' => 50 , 'on' => 'commission'],
            [['email'], 'string', 'max' => 75],
            [['gender'], 'string', 'max' => 1],
            [['identity'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id de la tabla',
            'firstname' => 'Nombres',
            'lastname' => 'Apellidos ',
            'email' => 'Email',
            'gender' => 'Sexo',
            'id_godfather' => 'Padrino',
            'id_level' => 'Nivel',
            'loan_count' => 'Contador de Prestamos',
            'birth_date' => 'Fecha de Nacimiento',
            'identity' => 'Cedula',
            'date_add' => 'Fecha de Registro',
            'godson_count' => 'Contador de ahijados disponibles para invitar',
            'id_user_system' => 'Indica el id del usuario del sistema',            
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(Address::className(), ['id_client' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArrears()
    {
        return $this->hasMany(Arrears::className(), ['id_client' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankAccounts()
    {
        return $this->hasMany(BankAccount::className(), ['id_client' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLevel()
    {
        return $this->hasOne(Level::className(), ['id' => 'id_level']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientsPwd()
    {
        return $this->hasOne(ClientsPwd::className(), ['id_client' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommissions()
    {
        return $this->hasMany(Commission::className(), ['id_client' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommissionRounds()
    {
        return $this->hasMany(CommissionRound::className(), ['id_client' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoans()
    {
        return $this->hasMany(Loan::className(), ['id_client' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasMany(Question::className(), ['id_client' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecordPayments()
    {
        return $this->hasMany(RecordPayment::className(), ['id_client' => 'id']);
    }    

    /**
     * Obtiene el sexo del cliente registrado en el sistema
     * @params  char         male o female
     * @return     string       Femenino | Masculino
     */
    public function getGender($gender){        
        
        if ($gender == Yii::$app->params['genderM'])
            return Yii::$app->params['Male'];
        else
            return Yii::$app->params['Female'];
    }  
    
    /**
    * Permite obtener el nombre y apellido 
    * Mas corto del cliente
    */
    public function Shortname(){
        
	$nombres = $this->firstname;
        $apellidos = $this->lastname;
        
        $firstname = strstr($nombres, ' ',true);
        $lastname = strstr($apellidos, ' ', true);
        
        if(empty($firstname))
            $firstname = $nombres;
        else
            $firstname = $firstname;

        if(empty($lastname))
            $lastname = $apellidos;
        else
            $lastname = $lastname;
        
        return ucwords(strtolower($firstname. ' '. $lastname));    
    }

    /**
     * Busca solo el email de cada cliente 
     * de acuerdo Level/Todos
     * @param id_level id del level
     * @return model firstname,lastname,email
     */
    public function findByLevelClient($level){
            
        if($level == Yii::$app->params['contactos_todos'] )
            return $this::find()->all();
        else
            return $this::find()->where(['id_level'=>$level])->all();                
        
    }

    /**
     * [findCountByGender Devuelve la cantidad de
     * mujeres u hombres registrados]
     * @param  [string] $gender [genero o masculino | femenino]
     * @return [integer] [cantidad de hombres | mujeres]     
     */
    public function findCountByGender($gender){    
        return $this::find()
              ->where(['gender'=>$gender])
              ->count();
    }

    /**
     * [findClientsCount Cuenta la cantidad
     * de clientes que se encuentran registrados
     * en el sistema]
     * @return [integer] [Cantidad de usuarios registrados]
     */
    public function findClientsCount(){
        return $this::find()->count(); // se se le suma uno debido a que el conteo lo toma de 0 a n
    }
}

