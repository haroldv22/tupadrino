<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Period */

$this->title = 'Periodo de: '.$model->n_days.' dias';
$this->params['breadcrumbs'][] = ['label' => 'Periodos (Niveles de Credito)', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="period-view">

    <!--<h1><?//= Html::encode($this->title) ?></h1>-->

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Estas Seguro que deseas eliminar estos datos ?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([ 
        'model' => $model,
        'attributes' => [
//            'id',
            'n_days',            
            'alias',
            'points',
        ],
    ]) ?>

</div>
