<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Period */

$this->title = 'Modificar Periodo: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Periodos (Niveles de Credito)', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Periodo: '.$model->n_days.' dias', 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="period-update">

    <!--<h1><?/= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
