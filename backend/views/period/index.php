<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Periodos (Niveles de Credito)';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="period-index">

    <!--<h1><?//= Html::encode($this->title) ?></h1>-->

    <p>
        <?= Html::a('Crear periodo', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'n_days',            
	        'alias',
            'points',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
