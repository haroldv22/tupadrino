<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ClientInvitation */

$this->title = 'Crear invitacion';
$this->params['breadcrumbs'][] = ['label' => 'Invitaciones del Sistema', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-invitation-create">

    <!--<h1><?//= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
//        'items' => $items,
    ]) ?>

</div>
