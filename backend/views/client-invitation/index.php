<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Invitaciones del Sistema';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-invitation-index">

        
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
            
        <?= $form->errorSummary($model); ?>
        
        <div class="well">
            
            <?= $form->field($model,'importInvitation')->fileInput(); ?>                    
            
            <?= Html::submitButton('Importar Invitaciones', ['class' => 'btn btn-success']) ?>
            
        </div>
    <?php ActiveForm::end() ?>

    <div class="form-group">
        <?= Html::a('Crear invitación Manual', ['create'], ['class' => 'btn btn-success']) ?>        
    </div>
   
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'email:email',
	    'date_add',
            'date_exp',
	    [
             	'attribute' => 'id_status',
                'value'    => function($model){
                            if($model->id_status)
                                return $model->getIdStatus()                      
                                             ->one()
                                             ->name;                           
			}
            ],
            ['class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update} {delete}',           
              'visibleButtons'=> ['delete' => function ($model, $key, $index) {                                            
                                                if($model->id_status == Yii::$app->params['invi_espera'])
                                                    return true;
                                                else 
                                                    return false;
                                            },                                
                                  'update'=>  function ($model, $key, $index) {                                            
                                                if($model->id_status == Yii::$app->params['invi_espera'])
                                                    return true;
                                                else 
                                                    return false;
                                            },     
                          ],

            ],
        ],
    ]); ?>

</div>
