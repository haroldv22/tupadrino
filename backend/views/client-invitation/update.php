<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ClientInvitation */

$this->title = 'Modificar Invitacion de: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Invitaciones del Sistema', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="client-invitation-update">

    <!--<h1><?//= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
