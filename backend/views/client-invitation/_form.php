<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ClientInvitation */
/* @var $form yii\widgets\ActiveForm */
?>

<?= \Yii::$app->session->getFlash('flashMessage') ?>

<div class="client-invitation-form">    
    
    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

<!--    <?//= $form->field($model, 'invitation_code')->textInput(['maxlength' => true]) ?>-->
    
<!--    <?//= $form->field($model, 'id_status')->dropDownList($items,   ['prompt'=>'Seleccione']) ?>-->

<!--    <?//= $form->field($model, 'id_godfather')->textInput() ?>-->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear invitación' : 'Modificar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
