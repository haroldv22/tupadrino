
<?php 
use yii\grid\GridView;
?>
 
<h2><?= $title ?></h2>

<?= GridView::widget([        
    'dataProvider' => $dataProvider,
    'columns' => [
      ['class' => 'yii\grid\SerialColumn'],                                                    
      [
      'attribute' => 'Ahijados',
      'value'     => function($model){                  
          return $model->getIdClient()->one()->firstname.'
           '.$model->getIdClient()->one()->lastname ;
      },
      ],
      [
      'attribute' => 'Monto de Comision',
      'value'     => function($model){                  
          return number_format($model->commission, 2, ',', ' ');                           
      },
      ],
      [
      'attribute' => 'Estatus',
      'value'     => function($model){                  
          return $model->getIdStatus()->one()->name;
      },
      ],
      [
      'attribute' => 'Fecha Limite',
      'value'     => function($model){                  
          return $model->end_date;
      },
      ]
    ],                                                                                                                                
  ])
?>
        