<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\bootstrap\Tabs;
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/miscelaneos.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/backend.js',['depends' => [\yii\web\JqueryAsset::className()]]);
/* @var $this yii\web\View */
/* @var $model app\models\Clients */

$this->title = $model->firstname.' '.$model->lastname;
$this->params['breadcrumbs'][] = ['label' => 'Clientes Registrados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clients-view">

     <?= Tabs::widget([
            'items' => [
                [
                    'label' => 'Detalles del Usuario',
                    'content' =>   DetailView::widget([
                                    'model' => $model,
                                    'attributes' => [                            
                                        'identity',
                                        'firstname',
                                        'lastname',
                                        'birth_date',
                                        'email:email',
                                        [                      // the owner name of the model
                                            'label' => 'Sexo',
                                            'value' => $model->getGender($model->gender)                                
                                        ],            
                                        [                      // the owner name of the model
                                            'label' => 'Padrino',
                                            'value' => $modelLoan->getPadrino($model)
                                        ],                        
                                        [                      // the owner name of the model
                                            'label' => 'Nivel de Credito',
                                            'value' => $model->id_level
                                        ],                                    
                                        'date_add',                                
                                        [
                                            'label' => 'Numero de Cuenta',
                                            'value' => $model->getBankAccounts()->one()->code_bank.''.$model->getBankAccounts()->one()->number_account
                                        ],
                                        [
                                            'label' => 'Contacto',
                                            'value' => $model->getAddresses()->one()->cellphone
                                        ],
                                        [
                                            'label' => 'Direccion',
                                            'value' => $model->getAddresses()->one()->address
                                        ],
                                        [
                                            'label' => 'Estado',
                                            'value' => $model->getAddresses()->one()->getIdState()->one()->name
                                        ],
                                        [
                                            'label' => 'Ciudad',
                                            'value' => $model->getAddresses()->one()->getIdCity()->one()->name
                                        ],
                                        [
                                            'label' => 'Municipio',
                                            'value' => $model->getAddresses()->one()->getIdMunicipality()->one()->name
                                        ],
                                        // [
                                        //     'label' => 'Parroquia',
                                        //     'value' => $model->getAddresses()->one()->getIdParish()->one()->name
                                        // ],                                        
                                    ],
                            ]),             
                    'active' => true,                                                                                     
                    'options' => ['id' => 'uDetails'],                                            
                ],
                [
                    'label' => 'Historico de Credito',
                     'content' =>    '<br /><br /><div class="row">
                                        <div class="col-lg-6">
                                            <table class="table table-bordered">
                                                <tbody> 
                                                    <tr> 
                                                        <td colspan="1"> Ahijados</td>
                                                        <td colspan="1" class="bg-blue">'.$countAhijados.'</td>
                                                        <td colspan="1">'.Html::a('<span class="fa fa-users col-lg-offset-1" aria-label="Ahijados" title="Ahijados" aria-hidden="true"></span>','ahijados?&id='.$model->id).'</td>
                                                    </tr>
                                                    <tr> 
                                                        <td colspan="1">Cantidad de Creditos</td>
                                                        <td colspan="1" class="bg-blue">'.$modelLoan::find()
                                                                                                    ->where(['id_client'=>$model->id])
                                                                                                    ->count().'</td>
                                                        <td colspan="1">'.Html::a('<span class="fa fa-eye col-lg-offset-1" aria-label="Creditos" title="Ver Creditos" aria-hidden="true"></span>','#',['class'=>'view_details','data-client'=>$model->id,'data-option'=>'0']).'</td>
                                                    </tr>
                                                    <tr> 
                                                        <td colspan="1">Creditos en Espera</td>
                                                        <td colspan="1" class="bg-blue">'.$modelLoan::find()
                                                                                        ->where(['id_client'=>$model->id,'id_status'=>Yii::$app->params['status_pendiente']])
                                                                                        ->count().'</td>
                                                        <td colspan="1">'.Html::a('<span class="fa fa-eye col-lg-offset-1" aria-label="Creditos en Espera" title="Creditos en Espera" aria-hidden="true"></span>','#',['class'=>'view_details','data-client'=>$model->id,'data-option'=>'20']).'</td>
                                                    </tr>
                                                    <tr>                                                     
                                                        <td colspan="1">Creditos en Vigencia</td>
                                                        <td colspan="1" class="bg-blue">'.$modelLoan::find()
                                                                                        ->where(['id_client'=>$model->id,'id_status'=>Yii::$app->params['status_vigente']])
                                                                                        ->count().'</td>
                                                        <td colspan="1">'.Html::a('<span class="fa fa-eye col-lg-offset-1" aria-label="Creditos en Vigencia" title="Creditos en Vigencia" aria-hidden="true"></span>','#',['class'=>'view_details','data-client'=>$model->id,'data-option'=>'41']).'</td>

                                                    </tr>                                                                                                      
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-lg-6">
                                            <table class="table table-bordered">
                                                <tbody>                                                                                            
                                                    <tr> 
                                                        <td colspan="1">Creditos Cancelados</td>
                                                        <td colspan="1" class="bg-blue">'.$modelLoan::find()
                                                                                        ->where(['id_client'=>$model->id,'id_status'=>Yii::$app->params['status_pagados']])
                                                                                        ->count().'</td>
                                                        <td colspan="1">'.Html::a('<span class="fa fa-eye col-lg-offset-1" aria-label="Creditos Cancelados" title="Creditos Cancelados" aria-hidden="true"></span>','#',['class'=>'view_details','data-client'=>$model->id,'data-option'=>'40']).'</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="1">Comisiones Canceladas</td>
                                                        <td colspan="1" class="bg-blue">'.$modelCommissionRound->getCommisionValue($model->id,Yii::$app->params['commission_pagada']).'</td> 
                                                        <td colspan="1">'.Html::a('<span class="fa fa-eye col-lg-offset-1" aria-label="Comisiones Canceladas" title="Ver Comisiones Canceladas" aria-hidden="true"></span>','#',['class'=>'view_comision','data-client'=>$model->id,'data-option'=>'62']).'</td>
                                                    </tr> 
                                                    <tr>    
                                                        <td colspan="1">Comisiones Pendientes</td>
                                                        <td colspan="1" class="bg-blue">'.$modelCommissionRound->getCommisionValue($model->id,Yii::$app->params['commission_activa']).'</td>
                                                        <td colspan="1">'.Html::a('<span class="fa fa-eye col-lg-offset-1" aria-label="Comisiones Pendientes" title="Ver Comisiones Pendientes" aria-hidden="true"></span>','#',['class'=>'view_comision','data-client'=>$model->id,'data-option'=>'60']).'</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="1">Comisiones Perdidas</td>
                                                        <td colspan="1" class="bg-blue">'.$modelCommissionRound->getCommisionValue($model->id,Yii::$app->params['commission_bloqueada']).'</td>
                                                         <td colspan="1">'.Html::a('<span class="fa fa-eye col-lg-offset-1" aria-label="Comisiones Perdidas" title="Ver Comisiones Perdidas" aria-hidden="true"></span>','#',['class'=>'view_comision','data-client'=>$model->id,'data-option'=>'63']).'</td>
                                                    </tr> 
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <br />
                                    <br /><div id="histoGridView"><h2>Cantidad de Creditos</h2>'.GridView::widget([
                                        'dataProvider' => $dataProvider,
                                        
                                        'columns' => [
                                            ['class' => 'yii\grid\SerialColumn'],                                                    
                                                        [
                                                          'attribute' => 'Monto Solicitado',
                                                          'value'     => function($model){                  
                                                                            return number_format($model->amount, 2, ',', ' ');                           
                                                                        },
                                                        ],
                                                        [
                                                          'attribute' => 'Estatus',
                                                          'value'     => function($model){                  
                                                                            return $model->getIdStatus()->one()->name;
                                                                        },
                                                        ],
                                                        [
                                                          'attribute' => 'Fecha de Solicitud',
                                                          'value'     => function($model){                  
                                                                            return $model->date_add;
                                                                        },
                                                        ],   
                                                        [
                                                          'attribute' => 'Cuotas',
                                                          'value'     => function($model){                  
                                                                            return $model->cant_quota;
                                                                        },
                                                        ],                                                  
                                                        [
                                                          'attribute' => 'Periodo',
                                                          'value'     => function($model){                  
                                                                            return $model->period.' dias';
                                                                        },
                                                        ],                                                  
                                                        [
                                                          'attribute' => 'Total Pagar',
                                                          'value'     => function($model){                  
                                                                            return number_format($model->total_pay, 2, ',', ' ');
                                                                        },
                                                        ],   
							['class' => 'yii\grid\ActionColumn','template' => '{cuota}',           
		                                         'buttons'=> ['cuota' => 
		                                                    function ($url, $model, $key) {
                   			                                     return Html::a('<span class="fa fa-eye col-lg-offset-1" aria-label="Cuotas" title="Cuotas" aria-hidden="true"></span>','#',['class'=>'view_cuotas','data-client'=>$model->id_client,'data-quota'=>$model->id]);
		                                                    }      
		                                                  ],
				                        ],  
                                            
                                            ]
                                    ]).'</div>',
                                'options' => ['id' => 'cHistory'],                                            
                ],
                        
                
                
                
            ],
               
        ]); 
    ?>
 

</div>
