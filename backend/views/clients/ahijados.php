<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model app\models\Clients */

$this->title = 'Ahijados de: '.$model->firstname.' '.$model->lastname;
$this->params['breadcrumbs'][] = ['label' => 'Clientes Registrados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ahijados-view">

    <?= '<br />'.GridView::widget([
                            'dataProvider' => $dataProviderAhijados,
                            
                            'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],                                                    
                                        [
                                          'attribute' => '# Cedula',
                                          'value'     => function($model){                  
                                                            return $model->identity;
                                                        },
                                        ],
                                        [
                                          'attribute' => 'Ahijado',
                                          'value'     => function($model){                  
                                                            return $model->firstname.' '.$model->lastname ;
                                                        },
                                        ],
                                        [
                                          'attribute' => 'Email',
                                          'value'     => function($model){                  
                                                            return $model->email;
                                                        },
                                        ],
                                        [
                                          'attribute' => 'Nivel',
                                          'value'     => function($model){                  
                                                            return $model->id_level;
                                                        },
                                        ],                               
                                        [
                                          'attribute' => 'Fecha de Registro',
                                          'value'     => function($model){                  
                                                            return $model->date_add;
                                                        },
                                        ],                             
                                    ],                                                                                    
                                    
                                ]);     ?>


</div>