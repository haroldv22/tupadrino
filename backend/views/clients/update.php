<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Clients */

$this->title = 'Modificar Clientes: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Clients', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="clients-update">

<!--    <h1><?//= Html::encode($this->title) ?></h1>s-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
