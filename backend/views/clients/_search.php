<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ClientsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="container">

        <div class="col-lg-2 col-sm-2">
            <?php  echo $form->field($model, 'identity',[
                'inputOptions'=>[
                    'placeholder' => ' V17636325',
                    'class'=>'form-control'
                ],]);
            ?>
        </div>
        
        <div class="col-lg-2 col-sm-2">
            <?php  echo $form->field($model, 'firstname',[
                'inputOptions'=>[
                    'placeholder' => ' HAROLD ESTEBAN',
                    'class'=>'form-control'
                ],]); ?>
        </div>
        
        <div class="col-lg-2 col-sm-2">
            <?php  echo $form->field($model, 'lastname',[
                'inputOptions'=>[
                    'placeholder' => ' VILLALOBOS VALBUENA',
                    'class'=>'form-control'
                ],]) ?>
        </div>
        
        <div class="col-lg-2 col-sm-2">
            <?php  echo $form->field($model, 'email',[
                'inputOptions'=>[
                    'placeholder' => ' ejemplo@ejemplo.com',
                    'class'=>'form-control'
                ],]); ?>
        </div>
        
        <div class="col-lg-1 col-sm-1">
            <?php  echo $form->field($model, 'id_level',[
                'inputOptions'=>[
                    'placeholder' => '1/2/3',
                    'class'=>'form-control'
                ],]); ?>
        </div>

      <div class="col-lg-2 col-sm-2 text-left">
        <?= Html::submitButton('<i class="fa fa-search"></i>', ['class' => 'btn btn-primary btnCR-top']) ?>
        <?= Html::resetButton('<i class="fa fa-refresh"></i>', ['class' => 'btn btn-default btnCR-top']) ?>
    </div>

    </div>

    

    <?php  //echo $form->field($model, 'id_godfather') ?>
    
    <?php // echo $form->field($model, 'loan_count') ?>

    <?php // echo $form->field($model, 'birth_date') ?>

    

    <?php // echo $form->field($model, 'date_add') ?>

    <?php // echo $form->field($model, 'godson_count') ?>


    <?php ActiveForm::end(); ?>

</div>
