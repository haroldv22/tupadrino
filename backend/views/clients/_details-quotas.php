<?php 

use yii\grid\GridView;
use yii\helpers\Url;


$formatter = \Yii::$app->formatter;

#Asignacion de Valores totales
$amortization = $modelLoan->columTotal($modQuota->models,'amortization');                    
#calculo de saldo                   );
$saldo = ($modLoan->amount)-$amortization;
#numero de cuotas pagadas y pendientes
$countq = $modLoan->countQ($modQuota->models);
?>


<table class="table table-bordered">
          <tbody> 
            <tr> 
              <td colspan="2"> Nro. Solicitud</td>
              <td colspan="2" class="bg-blue">
                <?= $modLoan->id; ?>
              </td>
            </tr>
            <tr> 
              <td>Monto Prestado</td>
              <td class="bg-blue">
                <?= $formatter->asDecimal($modLoan->amount); ?>
              </td>
              <td>Saldo a la fecha</td>
              <td class="bg-blue">
                <?= $formatter->asDecimal($saldo); ?>
              </td>
            </tr>
            <tr> 
              <td>Monto Pagado</td>
              <td class="bg-blue">
                <?= $formatter->asDecimal($amortization); ?>
              </td>
              <!--<td>Tasa Aplicada</td>
              <td class="bg-blue"><?php //echo $modLoan->rate; ?>%</td> -->
            </tr> 
            <tr> 
              <td>Cuotas Pendientes</td>
              <td class="bg-blue">
                <?= $countq["pending"]; ?>
              </td>
              <td>Cuotas Pagadas</td>
              <td class="bg-blue">
                <?= $countq["pay"]; ?>
              </td>
            </tr> 
          </tbody>
        </table>
 <br /><br />
<h2>Cuotas</h2>
<?= GridView::widget([
    'dataProvider' => $dataProvider,                                
      'columns' => [                                                        
              // [
              //   'attribute' => 'Estatus',
              //   'value'     => function($model){                  
              //                     return $model->getIdStatus()->one()->name;
              //                 },
              // ],
              [
                'attribute' => 'Nro.',
                'value'     => function($model){                  
                                  return $model->n_quota;
                              },
              ],
              [
                'attribute' => 'Fecha Pago',
                'value'     => function($model){                  
                                  return $model->date_pay;
                              },
              ],                                        
              [
                'attribute' => 'Fecha vencimiento',
                'value'     => function($model){                  
                                  return $model->date_expired;
                              },
              ],                                        
              [
                'attribute' => 'Cuota',
                'value'     => function($model){                  
                                  return number_format($model->quota, 2, ',', ' ');                           
                              },
              ],
              
              'idStatus.name:text:Estatus',
                   
              ],

      ]); 
?>