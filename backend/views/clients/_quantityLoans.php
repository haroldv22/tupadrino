<?php 

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

?>
 
<h2><?= $title ?></h2>

<?= GridView::widget([
'dataProvider' => $dataProvider,
'columns' => [
    ['class' => 'yii\grid\SerialColumn'],                                                    
    [
      'attribute' => 'Monto Solicitado',
      'value'     => function($model){                  
                        return number_format($model->amount, 2, ',', ' ');                           
                    },
    ],
    [
      'attribute' => 'Estatus',
      'value'     => function($model){                  
                        return $model->getIdStatus()->one()->name;
                    },
    ],
    [
      'attribute' => 'Fecha de Solicitud',
      'value'     => function($model){                  
                        return $model->date_add;
                    },
    ],   
    [
      'attribute' => 'Cuotas',
      'value'     => function($model){                  
                        return $model->cant_quota;
                    },
    ],                                                  
    [
      'attribute' => 'Periodo',
      'value'     => function($model){                  
                        return $model->period.' dias';
                    },
    ],                                                  
    [
      'attribute' => 'Total Pagar',
      'value'     => function($model){                  
                        return number_format($model->total_pay, 2, ',', ' ');
                    },
    ],
    ['class' => 'yii\grid\ActionColumn','template' => '{cuota}',           
     'buttons'=> ['cuota' => 
                  function ($url, $model, $key) {
                      return Html::a('<span class="fa fa-eye col-lg-offset-1" aria-label="Cuotas" title="Cuotas" aria-hidden="true"></span>','#',['class'=>'view_cuotas','data-client'=>$model->id_client,'data-quota'=>$model->id]);
                  }      
                ],
    ],  
]

]) ?> 