<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ClientsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clientes Registrados';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clients-index">
    
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>     

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],            
            //'id',
            'identity',
            'firstname',
            'lastname',
            'email:email',                      
            //'gender',
            // 'id_godfather',
            'id_level',
            // 'loan_count',
            // 'birth_date',
            // 'identity',
            // 'date_add',
            // 'godson_count',
            ['class' => 'yii\grid\ActionColumn','template' => '{view} {ahijados}',
                                                'buttons'=>['ahijados' => function ($url, $model, $key) {                                                    
                                                                            return Html::a('<span class="fa fa-users col-lg-offset-3" aria-label="Ahijados" title="Ahijados" aria-hidden="true"></span>','ahijados?&id='.$model->id); 
                                                                        },                        
                                                            ]
            ]
            
            

        ],
    ]); ?>

</div>
