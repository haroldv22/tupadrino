<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Banks */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Bancos Registrados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banks-view">

    <!--<h1><?//= Html::encode($this->title) ?></h1>-->

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->code_bank], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->code_bank], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Estas seguro que deseas eliminar estos datos ?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'name',
            'code_bank',
        ],
    ]) ?>

</div>
