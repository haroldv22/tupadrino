<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Configuraciones del Sistema';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="config-index">

    <!-- <h1><?//= Html::encode($this->title) ?></h1> -->
<br />
    <p>
        <?= Html::a('Crear Parametro', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],
            // 'id',
            'id_option',
            'name',
            'value:ntext',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
