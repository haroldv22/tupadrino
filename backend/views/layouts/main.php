<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'TuPadrino.net',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Home', 'url' => ['/site/index']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems = [    
            
            ['label' => 'Creditos', 
             'items'=> [
                ['label' => 'Niveles', 'url' => 'index.php?r=level'],
                '<li class="divider"></li>',
                ['label' => 'Periodos (Niveles de Credito)', 'url' => 'index.php?r=period'],
                '<li class="divider"></li>',
                ['label' => 'Cuotas', 'url' => 'index.php?r=quota'],                
              ],
            ],
            ['label' => 'Sistema', 
             'items'=> [
                ['label' => 'Preguntas', 'url' => 'index.php?r=question-system'],
                '<li class="divider"></li>',
                ['label' => 'Status', 'url' => 'index.php?r=status'],            
                '<li class="divider"></li>',
                ['label' => 'Bancos', 'url' => 'index.php?r=banks'],
                '<li class="divider"></li>',
                ['label' => 'Invitaciones', 'url' => 'index.php?r=client-invitation'],                 
                '<li class="divider"></li>',
                ['label' => 'Empleados', 'url' => 'index.php?r=user'],
              ],
            ],
             ['label' => 'Administracion', 
             'items'=> [
                ['label' => 'Solicitudes de Prestamos',         'url' => 'index.php?r=site%2Fsolicitudes'],                
                ['label' => 'Clientes Registrados',                'url' => 'index.php?r=#'],                                              
                ['label' => 'Liquidaciones de Prestamos',     'url' => 'index.php?r=site%2Fliquidaciones-prestamos'],                   
                ['label' => 'Cobranzas',                             'url' => 'index.php?r=#'],                                                 
                ['label' => 'Cartera de Credito',                   'url' => 'index.php?r=site%2Fcartera-credito'],                
                ['label' => 'Registros Contables',                 'url' => 'index.php?r=#'],                
                ['label' => 'Generacion de Mensajes',       'url' => 'index.php?r=#'],                
                ['label' => 'Procesos Diarios',                     'url' => 'index.php?r=#'],                
//                ['label' => 'Calendario Bancario',                'url' => 'index.php?r=bank-holiday'],     
                ['label' => 'Comisiones a Padrinos',            'url' => 'index.php?r=#'],     
                ['label' => 'Consultas',                              'url' => 'index.php?r=#'],                
                                                  
              ],
            ],
                                    
            [
                'label' => 'Logout ('.Yii::$app->user->identity->username.')',
                'url' => ['/site/logout'],
                'linkOptions' => ['data-method' => 'post']
            ],  
              
            
            
            
        ];
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; TuPadrino.net <?= date('Y') ?></p>

        <p class="pull-right"><?= 'All Copyrigth Reserved' ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
