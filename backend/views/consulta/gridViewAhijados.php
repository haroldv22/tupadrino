<?php 

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
?>

<?= GridView::widget([
      'dataProvider' => $dataProvider,    
      'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],
            [
                  'attribute' => 'Cliente',
                  'value'    => function($model){
                        return $model->name;
                  },
            ],                                    
            [     
                  'attribute' => 'Email',
                  'value'    => function($model){                                                                    
                        return $model->email;                                                                        
                  }                                                                
            ],          
            [     
                  'attribute' => 'Genero',
                  'format' => 'raw',
                  'value'    => function($model){
                        
                        if($model->getClients()->one()['gender'] == Yii::$app->params['genderM']) 
                              return  '<i class="fa fa-male"></i>';
                        else if ($model->getClients()->one()['gender'] == Yii::$app->params['genderF'])
                              return  '<i class="fa fa-female"></i>';
                        else
                              return "------------";
                  }                                                                
            ],                        
            [     
                  'attribute' => 'Estatus',
                  'value'    => function($model){
                        if($model->id_status == Yii::$app->params['invi_aceptada'])
                              return "Activo";
                        else
                              return "Inactivo";
                  }                                                                
            ],
            [     
                  'attribute' => 'Fecha',
                  'value'    => function($model){
                        return $model->date_add;                        
                  }                                                                
            ],
            
      ],
])

?>