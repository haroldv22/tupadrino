<?php 
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\bootstrap\Modal;

Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = [
    'depends' => [                  
        'yii\jui\JuiAsset',
    ],
];
?>


<!-- Suma total de los Prestamos (ACTIVOS/RIESGO/MORA) -->
<h1>Total de Prestamo: <?= number_format($total, 2, ',', ' '); ?> Bs.</h1>

<?php \yii\widgets\Pjax::begin(['id' => 'idofyourpjaxwidget', 
                                'timeout' => false,
                                'enablePushState' => false,
                                'clientOptions' => ['method' => 'POST']]) 
?>

<div class="pull-right">
<?= Html::button('<i class="fa fa-envelope-o" aria-hidden="true"></i> Enviar Mensaje',
['class'=>'btn btn-primary','id'=>'btnMsgGridView','data-toggle'=>'modal','data-target'=>'#msgModal'])?>
</div>

<?= GridView::widget([
      'dataProvider' => $dataProvider,          
      'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],
            [
                  'attribute' => 'Clientes',
                  'value'    => function($model){                                                                                                
                        return $model->getIdClient()->one()->Shortname();                        
                  },
            ],                                                
            [     
                  'attribute' => 'Genero',
                  'format' => 'raw',
                  'value'    => function($model){                        
                        if($model->getIdClient()->one()->gender == Yii::$app->params['genderM']) 
                              return  '<i class="fa fa-male"></i>';
                        else if ($model->getIdClient()->one()->gender == Yii::$app->params['genderF'])
                              return  '<i class="fa fa-female"></i>';
                  }                                                                
            ],          
            [     
                  'attribute' => 'Estatus',
                  'format' => 'raw',
                  'value'    => function($model){                                                             
                        return '<div class="pull-left semaforo-status 
                        '.$model->getIdStatus()->one()->CssSmfclass().'"> </div>';
                  }                                                                
            ],             
                                                                            
              [     
                  'attribute' => 'Prestamos Mora',
                  'value'    => function($model){
                    if($model->id_status == Yii::$app->params['mora_impagada'])                                            
                      return number_format($model->getPendingPayByClient($model->id_client), 2, ',', ' ');                              
                    else
                      return 'N/A';;

                  }
            ],                        
            [     
                  'attribute' => '',                  
                  'value'    => function($model){
                      return '+';
                      
                  }
            ],                        
            [     
                  'attribute' => 'Mora Acum',                  
                  'value'    => function($model){                    
    		       if($model->id_status == Yii::$app->params['mora_impagada'])                           
                         return number_format($model->getMoraByClient($model->id_client), 2, ',', ' ');
                       else
                        return 'N/A';
                    
                  }
            ],    
            [     
                  'attribute' => '',                  
                  'value'    => function($model){
                      return '=';
                      
                  }
            ],                                            
            [     
                  'attribute' => 'Total',
                  'value'    => function($model){
                        
                        if ($model->id_status == Yii::$app->params['mora_impagada'])                              
                              return number_format($model->getSumMoraByClient($model->id_client), 2, ',', ' ');                              
                        else
                              return number_format($model->amount, 2, ',', ' ');
                  }                                                                
            ],      
            
            ['class' => 'yii\grid\ActionColumn',
             'header' =>'',
             'template' => '{ver}',
             'buttons'=>[
                        'ver' => function ($url, $model, $key) {                                                    
                                  return Html::a('<span class="fa fa-eye col-lg-offset-3" 
                                    aria-label="Ver" title="Ver" aria-hidden="true"></span>',
                                    '../clients/view?id='.$model->getIdClient()->one()->id); 
                              },                                                
            ]
            ],
	    ['class' => 'yii\grid\ActionColumn',
             'header' =>'<span class="col-lg-offset-3">All '.Html::checkBox('check-msg',false,
                                    ['id'=>'allchkGP']).'</span>',
             'template' => '{check}',
             'buttons'=>[                        
                        'check' => function($url,$model,$key){
                                  return '<span class="col-lg-offset-3">'.Html::checkBox('check-msg',false,
                                    ['data-client' =>$model->id_client,
                                     'data-fullname' => $model->getIdClient()->one()->Shortname(),
                                     'data-email' => $model->getIdClient()->one()->email,
                                     'data-phone' => $model->getIdClient()->one()->getAddresses()->one()->cellphone]).'
                                  </span>';
                        }
            ]
            ],
      ],
]) ?>

<?php \yii\widgets\Pjax::end(); 

Modal::begin([
    'id'=>'msgModal',
    'header' => '<h2>Envio de Mensajes</h2>',    
]);

echo '  <div class="form-group">          
          '.Html::textInput('subjctEmail',null,[
            'id'=>'subject',
            'class'=>'form-control', 
            'placeholder' => 'Asunto:'
          ]).'    
        </div>
        <div class="form-group">
          '.Html::textarea('txtEmail', '' , [
            'rows'=>'10',
            'cols'=>'90',
            'id'=>'emailText',
            'class'=>'form-control',
            'resize'=>'none',
            'placeholder'=>'Envio de Email' 
            ]).'
        </div>
        <br />
        <div class="form-group">
        '.Html::textarea('txtSms', '' , [
          'rows'=>'2',
          'cols'=>'90',
          'id'=>'smsText',
          'class'=>'form-control textarea',
          'resize'=>'none',
          'maxlength'=>160,
          'placeholder'=>'Envio de SMS (Solo 160 caracteres) ' ] ).'</div>                

        '.Html::button('<i class="fa fa-envelope-o"></i> Enviar',[
          'data-loading'=>'Enviando...',
          'class'=>'btn btn-primary',
          'id'=>'btnPopMsg',
          'autocomplete'=>'off'
        ]);

Modal::end();
?>


