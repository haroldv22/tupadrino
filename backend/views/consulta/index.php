<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\jui\DatePicker;
use yii\bootstrap\Tabs;
use yii\widgets\ActiveForm;

$this->registerJsFile(Yii::$app->request->baseUrl.'/js/miscelaneos.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/app.js',['depends' => [\yii\web\JqueryAsset::className()]]);

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Consultas del Sistema';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="consultas">
<br />

<?= Tabs::widget([
                'items' => [
                    [
                        'label' => 'Consultas',
                         'content' => '<div class="col-lg-12 well">
                                        <div class="row">                                         
                                                    <div class="col-lg-3">
                                                        '.Html::dropDownList('soliciSearch',$selected,[  'prompt' => '¿ Qué desea Consultar ?',
                                                            40 =>'Ahijados',
                                                            41 =>'Prestamos Activos',
                                                            42 =>'Prestamos Al Dia',
                                                            43 =>'Prestamos en Mora'],
                                                         [  'class'=>'form-control',
                                                            'id'=>'btnSelectList', ]).'
                                                    </div>
                                         </div>                                  
                                         <br />          

                                            <div id="ahijadosOptions" class="row">	
                                                <div class="col-lg-1">
                                                    <p> Estatus:</p>
                                                </div>   
	                                            <div class="col-lg-2">
	                                                '.Html::radio('rdConsulta',false,['label'=>'<i class="fa fa-user-plus"></i> Activos','value'=>Yii::$app->params['invi_aceptada'],'id'=>'ckActivos']).'
	                                            </div>
	                                            <div class="col-lg-2">
	                                                '.Html::radio('rdConsulta',false,['label'=>'<i class="fa fa-user-times"></i> Inactivos','value'=>Yii::$app->params['invi_espera'],'id'=>'ckInactivos']).'
	                                            </div>                
	                                            <div class="col-lg-2">
	                                                '.Html::radio('rdConsulta',false,['label'=>'<i class="fa fa-user-plus"></i><i class="fa fa-user-times"></i> Ambos','value'=>3,'id'=>'ckAmbos']).'
	                                            </div>                
	                                        </div><br />                                                                                        
                                            <div class="row">		
                                                <div class="col-lg-1">
                                                    <p> Genero:</p>
                                                </div>   
	                                            <div class="col-lg-1">
	                                                '.Html::radio('rdConsulGeneros',false,['label'=>'<i class="fa fa-female"></i>','value'=>'f','id'=>'rdMujeres']).'
	                                            </div>                                        
	                                            <div class="col-lg-1">
	                                                '.Html::radio('rdConsulGeneros',false,['label'=>'<i class="fa fa-male"></i>','value'=>'m','id'=>'rdHombres']).'
	                                            </div>
	                                            <div class="col-lg-2">
	                                                '.Html::radio('rdConsulGeneros',false,['label'=>'<i class="fa fa-female"></i> <i class="fa fa-male"></i>','value'=>'ambos','id'=>'rdAmbos']).'
	                                            </div>                  
	                                        </div><br />                                                                                        
                                            <div class="row">
                                                <div class="col-lg-1">
                                                    <p> Fecha:</p>
                                                </div> 		
	                                            <div class="col-lg-2">
	                                                '.Html::radio('rdConsulCalendar',false,['label'=>'<i class="fa fa-calendar"></i> Hoy','value'=>1,'data-fecha'=>date('Y-m-d')]).'
	                                            </div>                                        
	                                            <div class="col-lg-2">
	                                                '.Html::radio('rdConsulCalendar',false,['label'=>'<i class="fa fa-calendar"></i> Mes','value'=>2,'data-fecha'=>date('m')]).'
	                                            </div>
	                                            <div class="col-lg-2">
	                                                '.Html::radio('rdConsulCalendar',false,['label'=>'<i class="fa fa-calendar"></i> Totales','value'=>3,'data-fecha'=>'all','id'=>'caldrTotales']).'
	                                            </div>                 
	                                            <div class="col-lg-2">
	                                                '.Html::radio('rdConsulCalendar',false,['label'=>'<i class="fa fa-calendar"></i> Rango','id'=>'rdRango','value'=>4,'date-fecha'=>'rango']).'
	                                            </div>                 
	                                            <br /> <br />    
	                                            <div class="range_calendar pull-left">
		                                            <div class="col-lg-1">Inicio: </div>
		                                            <div class="col-lg-4">		                                            
		                                                '.DatePicker::widget(['name'  => 'from_date',		       					 
		                                                					  'language'=> 'es-Es',
		                                                					  'dateFormat' => 'yyyy-MM-dd',
		                                                					  'id'=> 'fromDate',
		                                                					  'clientOptions'=>[		                                               	
			                                                					   'changeMonth' => true,
			                                                					    'maxDate' => 0,
			                                                					   'changeYear' => true,
			                                                					   'yearRange' => "-100:+0",
			                                                					   'showAnim' => 'fade',                                              
			                                                					]                                                					  
	                                                                         ]).'
		                                            </div> 
		                                            <div class="col-lg-1">Hasta: </div>
		                                            <div class="col-lg-4">		                                            
		                                                '.DatePicker::widget(['name'  => 'until_date',		       					 
		                                                					  'language'=> 'es-Es',
		                                                					  'dateFormat' => 'yyyy-MM-dd',
                                                                              'id'=> 'untilDate',
		                                                					  'clientOptions'=>[		                                               	
			                                                					   'changeMonth' => true,
			                                                					    'maxDate' => 0,
			                                                					   'changeYear' => true,
			                                                					   'yearRange' => "-100:+0",
			                                                					   'showAnim' => 'fade',                                              
			                                                					]                                             					  
	                                                                         ]).'
		                                            </div>              
	                                            </div> 
                                            </div>                                                         
                                    	</div><br /><br />
                                    <div class="text-center">
                                            <button id="btnConsulta" class="btn btn-primary"><i class="fa fa-search"></i> Consultar </button>
                                    </div>
                                    <br /><br />
                                    <div id="consultGridView" class="content">'.$gridView.'</div>',            
                        'active' => true,                                                                                     
                        'options' => ['id' => 'view_request'],                                            
                    ], 
                    
                ],
                                                        
    ]); ?>

    </div>

</div>
