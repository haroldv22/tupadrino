<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SystemBankAccount */

$this->title = 'Modificar Cuenta:' . ' ' . $model->beneficiario;
$this->params['breadcrumbs'][] = ['label' => 'Cuentas Bancarias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->beneficiario, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar Cuenta';
?>
<div class="system-bank-account-update">

    <!-- <h1><?//= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
        'listBank' => $listBank
    ]) ?>

</div>
