<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cuentas Bancarias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-bank-account-index">

    <!-- <h1><?//= Html::encode($this->title) ?></h1> -->
    <div class="well">
        <p>Nota: es importante resaltar que en esta sección siempre debe contener un registro ya que en esta sección<br />
se indicara a cual cuenta los clientes registrados deben realizar los pagos.</p>
    </div>

    <p>
        <?= Html::a('Crear Cuenta', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'attribute' => 'Banco',
                'value'=> function($model){
                        return $model->getCodeBank()->one()->name;
                }
            ],
            [
                'attribute' => 'Numero de Cuenta',
                'value'=> function($model){
                        return $model->number_account;
                }
            ],
            [
                'attribute' => 'Beneficiario',
                'value'=> function($model){
                        return $model->beneficiario;
                }
            ],
            [
                'attribute' => 'Cedula/RIF',
                'value'=> function($model){
                        return $model->identity;
                }
            ],

            // 'code_bank',
            // 'number_account',
            // 'beneficiario',
            // 'identity',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
