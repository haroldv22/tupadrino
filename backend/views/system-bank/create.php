<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SystemBankAccount */

$this->title = 'Crear Cuenta Bancaria';
$this->params['breadcrumbs'][] = ['label' => 'Cuentas Bancarias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-bank-account-create">

    <!-- <h1><?//= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
        'listBank' => $listBank
    ]) ?>

</div>
