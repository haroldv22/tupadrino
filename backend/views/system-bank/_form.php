<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\SystemBankAccount */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="system-bank-account-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= HTML::activeDropDownList($model,'code_bank',  
		                               ArrayHelper::map($listBank,'code_bank','name'), 
		                                 ['class'=>'form-control', 
		                                 'prompt'=>'Selecciona el banco']) ?>    
	<br />
	
    <?= $form->field($model, 'number_account')->textInput(['maxlength' => true,'placeholder'=>'0000000000000000 los 16 digitos siguientes de los primeros 4']) ?>

    <?= $form->field($model, 'beneficiario')->textInput(['maxlength' => true,'placeholder'=>'Miguel Brigati']) ?>

    <?= $form->field($model, 'identity')->textInput(['maxlength' => true,'placeholder','placeholder'=>'Cedula: V11313504 / RIF: J303015700
']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear Cuenta' : 'Modificar Cuenta', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
