<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Level */

$this->title = 'Crear Nivel de Credito';
$this->params['breadcrumbs'][] = ['label' => 'Niveles de Credito', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="level-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
