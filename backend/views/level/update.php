<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Level */

$this->title = 'Modificar Nivel: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Nivel de Credito', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="level-update">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
