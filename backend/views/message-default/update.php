<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MessageDefault */

$this->title = 'Modificar Mensaje: ' . ' ' . $model->description;
$this->params['breadcrumbs'][] = ['label' => 'Mensajes del Sistema', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->description, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="message-default-update">

    <!-- <h1><?//= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
