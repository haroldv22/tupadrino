<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\QuestionSystem */

$this->title = 'Modificar Pregunta: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Preguntas del Sistema', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' =>'Pregunta '. $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar Pregunta';
?>
<div class="question-system-update">

    <!--<h1><?//= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
