<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Tabs;
use yii\widgets\Pjax;

$this->registerJsFile(Yii::$app->request->baseUrl.'/js/miscelaneos.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/backend.js',['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<?php Pjax::begin(); ?> 

<?= GridView::widget(['dataProvider' => $dataProvider,
	                    'summary'      => false,                                            
	                    'columns'      => [
                         ['class' => 'yii\grid\SerialColumn'],
                         [
                          'attribute' => 'Cliente',
                          'value'     => function($model){
                                            return $model->idClient->ShortName();
                                        }                         
                         ],                                                                      
                         [
                          'attribute' => 'Fecha de Solicitud',
                          'value'     => function($model){
                                            return $model->date_add;
                                        }                              
                         ],                                                                 
                         [
                          'attribute' => 'email',
                          'value'     => function($model){
                                            return $model->idClient->email;
                                        }                              
                         ],
                         [
                          'attribute' => 'Monto',
                          'value'     => function($model){
                                            return number_format($model->amount, 2, ',', ' ');
                                        }                                         
                         ],       
                          ['class' => 'yii\grid\ActionColumn','template' => '{accept}',           
                          'buttons'=> ['accept' => 
                                        function ($url, $model, $key) {
                                        	
                                        	if ($model->id_status==Yii::$app->params['confirmado']){
                                            	return '<span class="glyphicon  glyphicon-ok text-success col-lg-offset-3" style="font-size: 1.4em;" aria-hidden="true"></span>';
                                        	}else{
                                        		return '<span class="glyphicon  glyphicon-remove text-danger col-lg-offset-3" style="font-size: 1.4em;" aria-hidden="true"></span>';
                                        	}
                                        }      
                                      ],
                         ],                                                                               
                    ],
])?>

<?php Pjax::end(); ?>