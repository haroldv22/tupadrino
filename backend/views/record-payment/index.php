<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\bootstrap\Tabs;
use yii\web\view;
use yii\widgets\ListView;

$this->registerJsFile(Yii::$app->request->baseUrl.'/js/miscelaneos.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/backend.js',['depends' => [\yii\web\JqueryAsset::className()]]);

/* @var $this yii\web\View */
/* @var $searchModel app\models\RecordPaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Registro de Pagos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="record-payment-index">



  <?php     
    echo Tabs::widget([
    'items' => [    
        [          
            'label' => 'Cobranzas Pendientes', 
            'content' =>'<br />                        
                          <p>Seleccione una fecha para conciliar los registros de pagos y a su vez si estos <br />seran conciliados de manera <b>Automatica</b> o <b>Manual</b></p>
                          <br />
                            <div class="col-lg-3">                              
                                <div class="dropdown">
                                  '.HTML::activeDropDownList($modelRecordPayment,
                                                              'date_add',
                                                              ArrayHelper::map($listCP, 'date_add', 'date_add'),
                                                              ['class'=>'form-control',
                                                              'prompt'=>'Registros de Pagos']).'                              
                              </div>
                            </div>                            
                            <div class="col-lg-2">  
                              <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-info ">
                                  <input type="radio" name="radioOptions" id="option1" autocomplete="off"> Auto
                                </label>
                                <label class="btn btn-info">
                                  <input type="radio" name="radioOptions" id="option2" autocomplete="off"> Manual
                                </label>                            
                              </div>
                            </div>                            
                            <div class="col-lg-4">  
                              '.HTML::button(' <i class="fa fa-search"></i> Consulta',['class'=>'btn btn-primary','id'=>'btnViewCobranza']).'
                            </div>                            
                            <br /><br />                                                      
                            <div id="cGridView">
                              <br />
                              <h3> Registros de Hoy </h3>   
                              '.GridView::widget([
                                'dataProvider' => $dataProviderCPH,
                                'summary'      => false,                                            
                                'columns'      => [
                                   ['class' => 'yii\grid\SerialColumn'],
                                   [
                                    'attribute' => 'Cliente',
                                    'value'     => function($model){
                                                      return $model->idClient->ShortName();                   
                                                  }                         
                                   ],                                                                      
                                   [
                                    'attribute' => 'Codigo Referencia',
                                    'value'     => function($model){
                                                    return $model->reference;
                                                  }
                                   ],
                                   [
                                    'attribute' => 'Monto',
                                    'value'     => function($model){
                                                      return number_format($model->amount, 2, ',', ' ');      
                                                  }                                         
                                   ],       
                                   [
                                    'attribute' => 'Tipo de Pago',
                                    'value'     => function($model){
                                                      return $model->type;
                                                  }                              
                                   ],            
                                   [
                                    'attribute' => 'Banco destino',
                                    'value'     => function($model){
                                                      return $model->getCodeBankDestiny()->one()->name;
                                                  }                              
                                   ],                                                                                        
                                   ['class' => 'yii\grid\ActionColumn','template' => '{accept}',           
                                    'buttons'=> ['accept' => 
                                                  function ($url, $model, $key) {
                                                      return '<span class="glyphicon  glyphicon-ok text-success col-lg-offset-3" style="font-size: 1.4em;" aria-hidden="true"></span>';
                                                  }      
                                                ],
                                   ],                                                                                                                                         
                                ],
                            ]).'</div>',            
            'active' => true,                                                                                     
            'options' => ['id' => 'request_payment'],
        ],     
        [
            'label' => 'Cobranzas Aprobadas',
            'content' =>  $this->render('_gridviewRPAR',[
                            'dataProvider'=>$dataProviderCA
                          ]),                    
            'options' => ['id' => 'request_pAcept'],            
        ],                
        [
            'label' => 'Cobranzas Rechazadas',
            'content' => $this->render('_gridviewRPAR',[
                            'dataProvider'=>$dataProviderCR
                          ]),                    
            'options' => ['id' => 'request_pFail'],
            
        ],                
      
        
        
    ],
       
]); 
?>
    
</div>
