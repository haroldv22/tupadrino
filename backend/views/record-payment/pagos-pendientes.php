<?php 
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
?>

// <?= //GridView::widget(['dataProvider' => $dataProvider,
//                 // 'filterModel' => $searchModel,
//                 'columns' => [
//                     ['class' => 'yii\grid\SerialColumn'],                                                        
//                     [
//                         'attribute' => 'Cliente',
//                         'value'    => function($model){                                                                                
//                                         if($model->id_client)
//                                             return $model->getIdClient()->one()->Shortname();                                                    
//                                       },
//                     ], 
//                     [
//                       'attribute' => 'Monto',
//                       'value'     => function($model){                  
//                                         return number_format($model->amount, 2, ',', ' ');                           
//                                     },
//                     ],  
//                     [
//                       'attribute' => '# Referencia',
//                       'value'     => function($model){                  
//                                         return $model->reference;                           
//                                     },
//                     ], 
//                     [
//                       'attribute' => 'Desde',
//                       'value'     => function($model){                  
//                                         return $model->getCodeBankOrigin()->one()->name;
//                                     },
//                     ], 
//                     [
//                       'attribute' => 'Hacia',
//                       'value'     => function($model){                  
//                                         return $model->getCodeBankDestiny()->one()->name;
//                                     },
//                     ], 
//                     [
//                       'attribute' => 'Fecha de registro',
//                       'value'     => function($model){                  
//                                         return $model->register_date;                           
//                                     },
//                     ], 
//                     [
//                       'attribute' => 'Tipo de Pago',
//                       'value'     => function($model){                  
//                                        return $model->type;
//                                     },
//                     ], 
//                     ['class' => 'yii\grid\ActionColumn','template' => '{opciones}',        
//                                       // 'header' => '<center>Estatus Solicitud</center>',
//                                       'buttons'=> ['opciones' => 
//                                                     function ($url, $model, $key) {
//                                                         return Html::checkBox('pagos-pendientes',true,['class'=>'option','data-status'=>'true','data-record'=>$model->id]);
//                                                     }
//                                                   ],
//                     ],                                                                       
//                 ],
//             ])
?>