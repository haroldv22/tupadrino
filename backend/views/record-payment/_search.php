<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RecordPaymentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="record-payment-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_client') ?>

    <?= $form->field($model, 'amount') ?>

    <?= $form->field($model, 'date_add') ?>

    <?= $form->field($model, 'method') ?>

    <?php // echo $form->field($model, 'type') ?>

    <?php // echo $form->field($model, 'id_status') ?>

    <?php // echo $form->field($model, 'pay_date') ?>

    <?php // echo $form->field($model, 'code_bank_origin') ?>

    <?php // echo $form->field($model, 'code_bank_destiny') ?>

    <?php // echo $form->field($model, 'money_excess') ?>

    <?php // echo $form->field($model, 'reference') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
