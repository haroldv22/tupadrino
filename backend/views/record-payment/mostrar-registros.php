
<?php 
use yii\data\ActiveDataProvider;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
?>
<br />
<?php
if (!$option){
    echo '<div class="well">
      <div class="page-header">
	Definición de Opciones
      </div>
    <p><b>No Recibido:</b>  esta opción al seleccionarla desactivara las verificaciones confirmando que el pago registrado no es correcto.</p>
    <p><b>Verificado:</b>  al dejar la pestaña en <b>"Verificado"</b> se confirma que el pago registrado fue conciliado.</p>
    <p><b>No Verificado:</b>  al dejar la pestaña en <b>"No Verificado"</b> es un registro que queda pendiente para ser conciliado en otro momento.</p>
    </div>';
}
?>
<h3> Registros de pago: <?= Yii::$app->formatter->asDate($date,'d/MM/Y') ?> </h3>   
<?= GridView::widget([
      'dataProvider' => $dataProvider,
      'summary'      => false,                                            
      'columns'      => [
       ['class' => 'yii\grid\SerialColumn'],
       [
        'attribute' => 'Cliente',
        'value'     => function($model){
                          return $model->idClient->Shortname();                   
                      }                         
       ],                                                                      
       [
        'attribute' => 'Codigo Referencia',
        'value'     => function($model){
                        return $model->reference;
                      }
       ],
       [
        'attribute' => 'Monto',
        'value'     => function($model){
                          return number_format($model->amount, 2, ',', ' ');      
                      }                                         
       ],       
       [
        'attribute' => 'Tipo de Pago',
        'value'     => function($model){
                          return $model->type;
                      }                              
       ],            
       [
        'attribute' => 'Banco destino',
        'value'     => function($model){
                          return $model->getCodeBankDestiny()->one()->name;
                      }                              
       ],     
       ['class' => 'yii\grid\ActionColumn',
        'template' => '{opciones} {message}',        
        'header' => '<center>Estatus de Pago</center>',
        'buttons'=> ['opciones' => 
                      function ($url, $model, $key) {
                                                  
                          return Html::checkBox('options',true,['id'=>'switch-disabled','class'=>'options','data-status'=>'true','data-id'=>$model->id,'data-disable'=>0]);
                          
                        // else
                        //    return '<span class="glyphicon glyphicon-question-sign text-warning col-lg-offset-3" style="font-size: 1.4em;" aria-hidden="true" data-id='.$model->id.']></span>';
                      },
                    'message' => 
                      function ($url, $model, $key) {
                                                  
                           return Html::button('No recibido',['class'=>'btn-cancelado btn btn-danger','data-toggle'=>'button','autocomplete'=>'off']);                            
                        
                      },
                    ],                    
       ],    
          
      ],
  ]); ?>

<br /> 

<?php
  // si option es true quiere decir que mostrara el siguiente form
  // ya que se hara de manera masiva y la seleccion fue Auto  
    if ($option){

      echo '<div id="attachCobranza" class="col-lg-12 well">';        
      $form = ActiveForm::begin(['action' => ['match-cobranza-auto'],
                                 'options' => ['enctype' => 'multipart/form-data']]) ?>      

          <?= $form->errorSummary($modelUpload); ?>
          <!-- fecha oculta  de la solicitud seleccionada-->
          <?= Html::hiddenInput('cFecha',$date) ?>          
          <?= '<div class="col-lg-12">'.$form->field($modelUpload, 'importCob')->fileInput().'</div>' ?>  
          
          <?= '<span class="col-lg-12">Formato de Banco con el que desea 
              hacer la conciliacion:</span><br/>
              <div class="col-lg-4 col-md-5 col-sm-6">
              '.Html::dropDownlist('code_bank',null, 
                  ArrayHelper::map($banks, 'code_bank', 'name'),
                  ['id'=>'bankConcil',
                   'class'=>'form-control'
                ]).'</div><br /><br />'
          ?>          
          
          <?= '<div class="col-lg-12"><br />
                '.Html::submitButton('Conciliar Pagos <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>', 
                ['id'=>'btnConCob','class' => 'btn btn-primary']).'
              </div>' ?>
     
      <?php ActiveForm::end();
      echo '</div>';
      }else{
            
        echo Html::button('Conciliar Pagos <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>', 
          ['id'=>'btnConCobManual','class' => 'btn btn-primary']); 
      

      }?>
