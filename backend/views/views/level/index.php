<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Niveles de Credito';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="level-index">
    

    <p>
        <?= Html::a('Crear Nivel', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Nivel',
                'value' => function($model){
                    return $model->id;
                }
            ],
            [
                'label' => 'Tasa de Interes',
                'value' => function($model){
                    return $model->rate;
                }
            ],
            // 'id',
            // 'condition',
            // 'rate',
            [
                'label' => 'Proximo Level',
                'value' => function($model){
                    return $model->id_level_next;
                }
            ],
            // 'id_level_next',

            // 'godson_cant',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
