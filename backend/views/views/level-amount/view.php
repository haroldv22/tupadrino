<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LevelAmount */

$this->title = 'Monto: '.number_format($model->amount, 2, ',', ' ');
$this->params['breadcrumbs'][] = ['label' => 'Montos por Nivel', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="level-amount-view">

    <!-- <h1><?//= Html::encode($this->title) ?></h1> -->

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Estas seguro que desea eliminar este Monto ?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            // 'amount',

            [ 
                'label' => 'Monto',
                'value' => number_format($model->amount, 2, ',', ' ')
            ],
            'id_level',
        ],
    ]) ?>

</div>
