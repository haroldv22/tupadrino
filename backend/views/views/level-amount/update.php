<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LevelAmount */

$this->title = 'Modificar Monto: ' . ' ' . number_format($model->amount, 2, ',', ' ');
$this->params['breadcrumbs'][] = ['label' => 'Montos por Nivel', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="level-amount-update">

    <!-- <h1><?//= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
