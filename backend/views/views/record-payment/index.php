<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\bootstrap\Tabs;
use yii\jui\DatePicker;

$this->registerJsFile(Yii::$app->request->baseUrl.'/js/miscelaneos.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/backend.js',['depends' => [\yii\web\JqueryAsset::className()]]);

/* @var $this yii\web\View */
/* @var $searchModel app\models\RecordPaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cobranzas';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="record-payment-index">

<?= Tabs::widget([
                'items' => [
                    [
                        'label' => 'Registro de Pagos',
                         'content' => '<br /><br /><div class="form-inline">
                                         <div class="form-group">                 
                                            <div class="col-lg-4">Registros de pago:</div> 
                                            <div class="col-lg-offset-1 col-lg-4">
                                                '.HTML::activeDropDownList($modelRecordPayment,'register_date',
                                                                              ArrayHelper::map($dateRecordPayment, 'register_date', 'register_date'),
                                                                              [  'class'=>'form-control',
                                                                                 'id'=>'btnCobPendients',
                                                                                 'prompt'=>'Seleccione Fecha'   ]).'
                                            </div>                                           
                                            <div class="col-lg-1">
                                                <button id="btnSearchPagos" class="btn btn-primary"><i class="fa fa-search"></i> Consultar </button>
                                            </div>
                                      </div>
                                      <br /><br />
                                                                          
                                    <br /><br />
                                    </div> 
                                    <div class="col-lg-3 pull-right">
                                                '.HTML::activeDropDownList($modelRecordPayment,'register_date',
                                                                              ArrayHelper::map($dateRecordPayment, 'register_date', 'register_date'),
                                                                              [  'class'=>'form-control',
                                                                                 'id'=>'btnCobPendients',
                                                                                 'prompt'=>'Ordenar por Banco'   ]).'
                                    </div>

                                    <div id="pagosGridView">                                    
                                    '.GridView::widget(['dataProvider' => $dataProvider,
                                                        // 'filterModel' => $searchModel,
                                                        'columns' => [
                                                            ['class' => 'yii\grid\SerialColumn'],                                                        
                                                            [
                                                                'attribute' => 'Cliente',
                                                                'value'    => function($model){                                                                                
                                                                                if($model->id_client)
                                                                                    return $model->getIdClient()->one()->Shortname();                                                                                                                                                                                                        
                                                                            },
                                                            ], 
                                                            [
                                                              'attribute' => 'Monto',
                                                              'value'     => function($model){                  
                                                                                return number_format($model->amount, 2, ',', ' ');                           
                                                                            },
                                                            ],  
                                                            [
                                                              'attribute' => 'Referencia',
                                                              'value'     => function($model){                  
                                                                                return $model->reference;                           
                                                                            },
                                                            ], 
                                                            // [
                                                            //   'attribute' => 'Desde',
                                                            //   'value'     => function($model){                  
                                                            //                     return $model->getCodeBankOrigin()->one()->name;
                                                            //                 },
                                                            // ], 
                                                            [
                                                              'attribute' => 'Hacia',
                                                              'value'     => function($model){                  
                                                                                return $model->getCodeBankDestiny()->one()->name;
                                                                            },
                                                            ], 
                                                            [
                                                              'attribute' => 'Registrado',
                                                              'value'     => function($model){                  
                                                                                return $model->register_date;                           
                                                                            },
                                                            ], 
                                                            [
                                                              'attribute' => 'Cancelacion',
                                                              'value'     => function($model){                  
                                                                               return $model->type;
                                                                            },
                                                            ],                                                         
                                                            // ['class' => 'yii\grid\ActionColumn','template' => '{opciones}',        
                                                            //                   // 'header' => '<center>Estatus Solicitud</center>',
                                                            //                   'buttons'=> ['opciones' => 
                                                            //                                 function ($url, $model, $key) {
                                                            //                                     return Html::checkBox('pagos-pendientes',true,['class'=>'option','data-status'=>'true','data-record'=>$model->id]);
                                                            //                                 }
                                                            //                               ],
                                                            // ],                                                                       
                                                        ],
                                                    ]).'</div>
                                                        <div class="col-lg-12">
                                                            <div class="col-lg-2 pull-right">
                                                                <button id="btnRegistPagos" class="btn btn-success"><i class="fa fa-floppy-o"></i> Registrar </button>
                                                            </div>
                                                            <div class="pull-right">
                                                                <button id="btnExportPagos" class="btn btn-danger"><i class="fa fa-file-excel-o"></i>
 Exportar </button>
                                                            </div>
                                                        </div>',            
                        'active' => true,                                                                                     
                        'options' => ['id' => 'view_request'],                                            
                    ], 
                    [
                        'label' => 'Pagos Conciliados',
                         'content' => '<div class="col-lg-12"><br />                        
                                       <div class="dropdown col-lg-3">                                      </div><br /><br />
                                      <div id="rGridView"></div>
                                    </div>',                                    
                        'options' => ['id' => 'requests'],                                            
                    ],                   
                    [
                        'label' => 'Pagos Pendientes',
                         'content' => '<div class="col-lg-12"><br />                        
                                       <div class="dropdown col-lg-3">                                      </div><br /><br />
                                      <div id="rGridView"></div>
                                    </div>',                                    
                        'options' => ['id' => 'requests'],                                            
                    ],                   
                    [
                        'label' => 'Historial de Pagos',
                         'content' => '<div class="col-lg-12"><br />                        
                                       <div class="dropdown col-lg-3">                                      </div><br /><br />
                                      <div id="rGridView"></div>
                                    </div>',                                    
                        'options' => ['id' => 'requests'],                                            
                    ],                   
                   
                ],
                                                        
    ]); ?>


    <!-- <h1><?//= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <!-- <p> -->
        <!-- <?//= Html::a('Create Record Payment', ['create'], ['class' => 'btn btn-success']) ?> -->
    <!-- </p> -->

    <!--<?//= GridView::widget([
        // 'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        // 'columns' => [
        //     ['class' => 'yii\grid\SerialColumn'],

        //     // 'id',
        //     [
        //         'attribute' => 'Cliente',
        //         'value'    => function($model){
        //                         if($model->id_client){
        //                             return $model->getIdClient()->one()->Shortname();                                    
        //                             // return $client->firstname.' '.$client->lastname;
        //                         }
        //                     },
        //     ], 
        //     [
        //       'attribute' => 'Monto',
        //       'value'     => function($model){                  
        //                         return number_format($model->amount, 2, ',', ' ');                           
        //                     },
        //     ],  
        //     [
        //       'attribute' => '# Referencia',
        //       'value'     => function($model){                  
        //                         return $model->reference;                           
        //                     },
        //     ], 
        //     [
        //       'attribute' => 'Desde',
        //       'value'     => function($model){                  
        //                         return $model->getCodeBank()->one()->name;
        //                     },
        //     ], 
        //     [
        //       'attribute' => 'Fecha de registro',
        //       'value'     => function($model){                  
        //                         return $model->register_date;                           
        //                     },
        //     ], 
        //     [
        //       'attribute' => 'Metodo',
        //       'value'     => function($model){                  
        //                         if($model->transference)
        //                             return 'Transferencia';
        //                         else
        //                             return 'Deposito';
        //                     },
        //     ], 


            // 'reference',
            // 'date_add',
            // 'transference:boolean',
            // 'code_bank',
            // 'type',
            // 'id_status',
            // 'register_date',

            // ['class' => 'yii\grid\ActionColumn','template' => '{view}' ],
        // ],
    // ]); ?>-->

</div>
