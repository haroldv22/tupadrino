<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RecordPayment */

$this->title = 'Create Record Payment';
$this->params['breadcrumbs'][] = ['label' => 'Record Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="record-payment-create">

    <!-- <h1><?//= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
