<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Quota;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Montos por Nivel';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="level-amount-index">

    <!-- <h1><?//= Html::encode($this->title) ?></h1> -->

    <p>
        <?= Html::a('Crear Monto', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            [ 
                'label' => 'Monto',
                'value' => function($model){
                    return  number_format($model->amount, 2, ',', ' ');
                }
            ],
            'id_level',
	    [
                'label' => 'Plazos',
                'value' => function($model){
                    return $model->getPeriodsAmmounts();
                }
                // getPeriodAmounts()->one()->getIdPeriod()->all()->alias
            ],

             ['class' => 'yii\grid\ActionColumn',
	      'template' => '{view} {delete}'],
        ],
    ]); ?>

</div>
