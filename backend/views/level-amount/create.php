<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LevelAmount */

$this->title = 'Crear Monto';
$this->params['breadcrumbs'][] = ['label' => 'Montos por Nivel', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="level-amount-create">

    <!-- <h1><?//= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
        'modelLevel'=> $modelLevel,
        'modelPeriod' => $modelPeriod,
        'modelPeriodAmount'=>$modelPeriodAmount,
	'readOnly' => false
    ]) ?>

</div>
