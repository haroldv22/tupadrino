<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\PeriodAmount;
use app\models\Period;

// use app\models\Level;

/* @var $this yii\web\View */
/* @var $model app\models\LevelAmount */
/* @var $form yii\widgets\ActiveForm */


$listLvl = ArrayHelper::map($modelLevel::find()->asArray()->all(), 'id', 'id');
$listPeriod = ArrayHelper::map($modelPeriod::find()->asArray()->all(), 'id', 'alias');

if ($readOnly){
	
	$period = $modelPeriod->getPeriodDefault($model->id);
	$listPeriod = ArrayHelper::map($modelPeriod::find()->where(['id'=>$period])->asArray()->all(), 'id', 'alias');	
	
}		

?>

<div class="level-amount-form">

    <?php $form = ActiveForm::begin(); ?>
	
		
   		<?= $form->field($model, 'amount')->textInput(['ReadOnly'=>$readOnly]) ?>		
		
		<?php if ($readOnly==true){ ?>    		
			


	    	<?= $form->field($model, 'id_level')->textInput(['ReadOnly'=>$readOnly]) ?>		

	    	<?= $form->field($modelPeriodAmount,'id_period')->dropDownList($listPeriod,
		    ['prompt'=>'Plazo del Prestamo',
		     'class'=>'form-control'
		    ])?>

	    
	    <?php }else{ ?>
		
		    <?= $form->field($modelPeriodAmount,'id_level_amount')->dropDownList($listLvl,
		    ['prompt'=>'Nivel de Credito',
		     'class'=>'form-control',
		     'disabled'=>$readOnly

		    ])?>
		    
		    <?= $form->field($modelPeriodAmount,'id_period')->dropDownList($listPeriod,
		    ['prompt'=>'Plazo del Prestamo',
		     'class'=>'form-control'
		    ])?>
	    
	    <?php } ?>




    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear Monto' : 'Agregar Plazo', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
