<?php 
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use app\models\Banks;
?>
<br /><br />
<?= GridView::widget(['dataProvider' => $dataProvider,  
                      'summary'      => false,                                            
                      'columns'      => [
                                       ['class' => 'yii\grid\SerialColumn'],
                                       [
                                        'attribute' => 'Cliente',
                                        'value'     => function($model){
                                                          return $model->idClient->firstname.' '.
                                                                   $model->idClient->lastname;                                                                                                  }
                                       ],
                                       [
                                        'attribute' => 'Fecha',
                                        'value'     => function($model){
                                                              return $model->date_add;                                                                                                             
                                                          }                              
                                       ],                                                                 
                                       [
                                        'attribute' => 'Email',
                                        'value'     => function($model){
                                                              return $model->idClient->email;                                                                                                       
                                                          }                           
                                       ],
                                       [
                                        'attribute' => 'Monto',
                                        'value'     => function($model){
                                                              return number_format($model->amount, 2, ',', ' ');                                                                                                                                                                        
                                                          }                          
                                       ],                                                                                                                                               
                                      ],
                                   ])
?> 
<br />
<div class="col-lg-10 col-xs-6 col-md-9 col-sm-8">
<div class="dropdown col-lg-4"><?= HTML::activeDropDownList($modelBank,'code_bank',
                                                       ArrayHelper::map(Banks::find()->all(), 'code_bank', 'name'),
                                                        ['class'=>'form-control','prompt'=>'Seleccione formato de banco']) ?>
</div>  
<div class="col-lg-2 col-xs-6 col-md-3 col-sm-4"><?=HTML::button('Crear formato',
                                                            ['id'=>'btnRegSolict',
                                                            'class'=>'btn btn-danger']) ?>
</div>
</div>
