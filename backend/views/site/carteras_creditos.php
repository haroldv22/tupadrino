<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Tabs;
use yii\widgets\Pjax;

$this->registerJsFile(Yii::$app->request->baseUrl.'/js/miscelaneos.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/backend.js',['depends' => [\yii\web\JqueryAsset::className()]]);
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Cartera de Aportes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cartera-credito">

    <!--<h1><?//= Html::encode($this->title) ?></h1>-->        
          <?php  
            echo Tabs::widget([
            'items' => [
                [
                    'label' => 'Aportes Vigentes',
                    'content' => 
		
				GridView::widget(['dataProvider' => $dataProviderCCV,                                                                                               
                                                    'summary'    => false,                                            
                                                    'columns'      => [
                                                                        ['class' => 'yii\grid\SerialColumn'],                                                                   
                                                                         [
                        'attribute' => 'Cliente',
                        'value'    => function($model){
                                        if($model->id_client){
                                            $client = $model->getIdClient()->one();                                    
                                            return $client->firstname.' '.$client->lastname;
                                        }
                                    },
                    ],                                   
                    [
                      'attribute' => 'Monto',
                      'value'     => function($model){                  
                                        return number_format($model->amount, 2, ',', ' ');                           
                                    },
                    ],                                    
                    [
                        'attribute' => 'Email',
                        'value'    => function($model){
                                        if($model->id_client){
                                            return $model->getIdClient()
                                                         ->one()
                                                         ->email;                                                                        
                                        }
                                    },
                    ],            
                    [
                        'attribute' => 'Level',
                        'value'    => function($model){
                                        if($model->id_client){
                                            return $model->getIdClient()
                                                         ->one()
                                                         ->id_level;                                                                        
                                        }
                                    },
                    ],                        
                    [
                        'attribute' => 'Fecha',
                        'value'    => function($model){
                                        return $model->date_add;
                                        },
                    ],               
                    [
                        'attribute' => 'Padrino',
                        'value'    => function($model){
                                        if($model->id_client)                                                                                                             
                                            return $model->getPadrino($model->idClient);                                               
                                    },
                    ],   
                    ['class' => 'yii\grid\ActionColumn',
                     'template' => '{cuota}',
                     'buttons'=> ['cuota' => 
                                    function ($url, $model, $key) {
                                        return Html::a('<span class="fa fa-eye col-lg-offset-1" aria-label="Ver Detalles" title="Ver Detalles" aria-hidden="true">
                                                        </span>','#',['class'=>'view_cAcuotas','data-quota'=>$model->id,'target'=>'blank']);
                                    }      
                                  ],
                    ],                                                              
                ],                                                            
            ]).'<br /><div id="cuotas-cliente-vi"></div>',            
                    'options'=> ['id'=>'credit_vigentes'],
                    'active' => true            
                ],
                [
                    'label' => 'Aportes Vencidos',
                    'content' =>  GridView::widget(['dataProvider' => $dataProviderCCVE,                                                                                                     
                                                    'summary'      => false,                                            
                                                    'columns'      => [
                                                                        ['class' => 'yii\grid\SerialColumn'],                                                                   
                                                                         [
                        'attribute' => 'Cliente',
                        'value'    => function($model){
                                        if($model->id_client){
                                            $client = $model->getIdClient()->one();                                    
                                            return $client->firstname.' '.$client->lastname;
                                        }
                                    },
                    ],                        
                    [
                        'attribute' => '# Cedula',
                        'value'    => function($model){
                                        if($model->id_client){
                                            return $model->getIdClient()
                                                         ->one()
                                                         ->identity;                                                                        
                                        }
                                    },
                    ],            
                    [
                      'attribute' => 'Monto',
                      'value'     => function($model){                  
                                        return number_format($model->amount, 2, ',', ' ');                           
                                    },
                    ],                                    
                    [
                        'attribute' => 'Email',
                        'value'    => function($model){
                                        if($model->id_client){
                                            return $model->getIdClient()
                                                         ->one()
                                                         ->email;                                                                        
                                        }
                                    },
                    ],            
                    [
                        'attribute' => 'Level',
                        'value'    => function($model){
                                        if($model->id_client){
                                            return $model->getIdClient()
                                                         ->one()
                                                         ->id_level;                                                                        
                                        }
                                    },
                    ],                        
                    [
                        'attribute' => 'Fecha',
                        'value'    => function($model){
                                        return $model->date_add;
                                        },
                    ],               
                    [
                        'attribute' => 'Padrino',
                        'value'    => function($model){
                                        if($model->id_client)                                                                                                             
                                            return $model->getPadrino($model->idClient);                                               
                                    },
                    ],   
                    ['class' => 'yii\grid\ActionColumn',
                     'template' => '{cuota}',
                     'buttons'=> ['cuota' => 
                                    function ($url, $model, $key) {
                                        return Html::a('<span class="fa fa-eye col-lg-offset-1" aria-label="Cuotas" title="Cuotas" aria-hidden="true">
                                                        </span>','#',['class'=>'view_cAcuotas','data-quota'=>$model->id]);
                                    }      
                                  ],
                    ],  
                                                                       ],   
                    ]).'<br /><div id="cuotas-cliente-ven"></div>',           
                    'options' => ['id' => 'credit_vencidos'],
                ],     
                [
                    'label' => 'Aportes Incumplidos',
                    'content' => GridView::widget(['dataProvider' => $dataProviderCCMO,                                                                                                    
                                                   'summary'      => false,                                            
                                                   'columns'      =>[
                                                                        ['class' => 'yii\grid\SerialColumn'],                                                                   
                                                                         [
                        'attribute' => 'Cliente',
                        'value'    => function($model){
                                        if($model->id_client){
                                            $client = $model->getIdClient()->one();                                    
                                            return $client->firstname.' '.$client->lastname;
                                        }
                                    },
                    ],                        
                    [
                        'attribute' => '# Cedula',
                        'value'    => function($model){
                                        if($model->id_client){
                                            return $model->getIdClient()
                                                         ->one()
                                                         ->identity;                                                                        
                                        }
                                    },
                    ],            
                    [
                      'attribute' => 'Monto',
                      'value'     => function($model){                  
                                        return number_format($model->amount, 2, ',', ' ');                           
                                    },
                    ],                                    
                    [
                        'attribute' => 'Email',
                        'value'    => function($model){
                                        if($model->id_client){
                                            return $model->getIdClient()
                                                         ->one()
                                                         ->email;                                                                        
                                        }
                                    },
                    ],            
                    [
                        'attribute' => 'Level',
                        'value'    => function($model){
                                        if($model->id_client){
                                            return $model->getIdClient()
                                                         ->one()
                                                         ->id_level;                                                                        
                                        }
                                    },
                    ],                        
                    [
                        'attribute' => 'Fecha',
                        'value'    => function($model){
                                        return $model->date_add;
                                        },
                    ],               
                    [
                        'attribute' => 'Padrino',
                        'value'    => function($model){
                                        if($model->id_client)                                                                                                             
                                            return $model->getPadrino($model->idClient);                                               
                                    },
                    ],        
                    ['class' => 'yii\grid\ActionColumn',
                     'template' => '{cuota}',
                     'buttons'=> ['cuota' => 
                                    function ($url, $model, $key) {
                                        return Html::a('<span class="fa fa-eye col-lg-offset-1" aria-label="Cuotas" title="Cuotas" aria-hidden="true">
                                                        </span>','#',['class'=>'view_cAcuotas','data-quota'=>$model->id]);
                                    }      
                                  ],
                    ],  
                ],   
            ]).'<br /><div id="cuotas-cliente-mo"></div>',          
                    'options' => ['id' => 'credit_mora'],

                ],                  
            ],
               
        ]); 
            ?>
    
</div>
