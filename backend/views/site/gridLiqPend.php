<?php 
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
?>

  <br />
  <a href="<?= $url ?>" download="<?= $download ?>" >
    <i class="fa fa-file-text" aria-hidden="true"></i> Haz click para descargar el formato de liquidacion
  </a>
  <br /><br />  
  <?= GridView::widget(['dataProvider' => $dataProvider,  
                        'summary'      => false,                                            
                        'columns'      => [
                                         ['class' => 'yii\grid\SerialColumn'],
                                         [
                                          'attribute' => 'Cliente',
                                          'value'     => function($model){
                                                            return $model->idClient->firstname.' '.
                                                                     $model->idClient->lastname;                                                                                                  }
                                         ],
                                         [
                                          'attribute' => 'Fecha',
                                          'value'     => function($model){
                                                                return $model->date_add;                                                                                                             
                                                            }                              
                                         ],                                                                 
                                         [
                                          'attribute' => 'Email',
                                          'value'     => function($model){
                                                                return $model->idClient->email;                                                                                                       
                                                            }                           
                                         ],
                                         [
                                          'attribute' => 'Monto',
                                          'value'     => function($model){
                                                              return number_format($model->amount, 2, ',', ' ');
                                                        }                          
                                         ],       
                                         // ['class' => 'yii\grid\ActionColumn','template' => '{opciones}',        
                                         //  'header' => '<center>Estatus Solicitud</center>',
                                         //  'buttons'=> ['opciones' => 
                                         //                function ($url, $model, $key) {
                                         //                    return Html::checkBox('clients-aprobados',true,['class'=>'option','data-status'=>'true','data-clnt'=>$model->id_client]);
                                         //                }
                                         //              ],
                                         // ],                                                                       
                                    ],
                 ])?>

<br /> 
<div class="well">
<?php $form = ActiveForm::begin(['action' => ['match-liquidaciones'],'options' => ['enctype' => 'multipart/form-data']]) ?>
    <?= $form->errorSummary($model); ?>

    <!-- fecha oculta  de la solicitud seleccionada-->
    <?= Html::hiddenInput('sFecha',null,['id'=>'hfechaSolic']) ?>
    
    <?= $form->field($model, 'importLiq')->fileInput() ?>
   
    <?= Html::submitButton('Conciliar Solicitudes <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>', ['id'=>'btnConLiq','class' => 'btn btn-primary']) ?>
    <!-- <?//= HTML::button('Conciliar Solicitudes <i class="fa fa-thumbs-o-up" aria-hidden="true"></i> ',['id'=>'btnConLiq','class'=>'btn btn-primary']) ?>   -->

<?php ActiveForm::end() ?>
</div>
<!-- <div class="col-lg-2 col-xs-6 col-md-3 col-sm-4">
  <?//= HTML::button('Registrar Solicitudes',['id'=>'btnRegLiq','class'=>'btn btn-primary']) ?>   -->
<!-- </div> -->
  <!-- <div class="col-lg-10 col-xs-6 col-md-9 col-sm-8">
  <div class="col-md-5 col-lg-5 col-sm-8"><?//= HTML::textInput('num_referencia',null,
                                              //             ['id'=>'txtReferencs',
                                                //            'class'=>'form-control',
                                                  //          'placeholder'=>'Numero de Referencia'])?>
  </div>
  </div> -->