<?php


use yii\helpers\Html;
/* @var $this yii\web\View */
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/miscelaneos.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/flot.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/pie.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/app-flot.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/flot.min.categories.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/chart.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);

$this->title = 'TuPadrino.net (Panel Administrativo)';
?>


<div class="row">

  <div class="text-center">
     <h1 style="font-size: 68px">¡Bienvenidos!</h1>
          <p class="lead">Bienvenidos al panel administrativo de<b> TuPadrino.Net </b>¿Qué deseas hacer?.</p>

  </div>


  <div class="col-lg-12">
      
    <div class="col-lg-3">
        <!-- Usuarios Activos  -->
        <div class="box box-primary">
          <div class="box-header with-border box-default">
            <i class="fa fa-bar-chart-o"></i>

            <h3 class="box-title"> AHIJADOS</h3>
            <?= Html::hiddenInput('ahi_hombres',$mCount,['id'=>'aHombres']) ?>
            <?= Html::hiddenInput('ahi_mujeres',$fCount,['id'=>'aMujeres']) ?>            
            

            
          </div>
          <div class="box-body">
            <i class="fa fa-circle-o text-green"></i> Nro. Ahijados: <?= $ahijCount ?><br />
            <i class="fa fa-circle-o text-red"></i> Mujeres: <?= $fCount ?><br />
            <i class="fa fa-circle-o text-blue"></i> Hombres: <?= $mCount ?><br />
            <!-- <i class="fa fa-circle-o text-orange"></i> Inactivos -->
              <!-- <div id="donut-chart" style="height: 300px;"></div> -->
          </div>
          <!-- /.box-body-->
          <div class="text-center box-footer">
<?= Html::a('Ver mas <i class="fa fa-arrow-circle-right"></i>','index.php/consulta/ver-ahijados?opt='.Yii::$app->params["consulAhijados"],['id'=>'verAhijados','class'=>'small-box-footer'])?>            
<!-- <a class="small-box-footer" href="">Ver mas <i class="fa fa-arrow-circle-right"></i></a> -->
          </div>
        </div>
    </div>

    <div class="col-lg-3">
        <div class="box box-primary">
          <div class="box-header with-border">
            <i class="fa fa-bar-chart-o"></i>
            <h3 class="box-title">ACTIVOS</h3>
            <?= Html::hiddenInput('ahi_activos',$sumActivos,['id'=>'pActivos']) ?>
            <?= Html::hiddenInput('ahi_activos_mujeres',$montoMujeresActivas,['id'=>'pAMujeres']) ?>
            <?= Html::hiddenInput('ahi_activos_hombres',$montoHombresActivos,['id'=>'pAHombres']) ?>  
          </div>

          <div class="box-body">
           (<?= $ambosActivos ?>) <i class="fa fa-usd"></i> Activos: <?= number_format($sumActivos, 2, ',', ' ')?> <br />           
           (<?= $mujeresActivas ?>) <i class="fa fa-female" ></i> Mujeres: <?= number_format($montoMujeresActivas, 2, ',', ' ')?> <br />
           (<?= $hombresActivos ?>) <i class="fa fa-male" ></i>  Hombres: <?= number_format($montoHombresActivos, 2, ',', ' ')?> <br /><br />
            <div id="bar-activos" style="height: 300px;"></div>
          </div>
          <!-- /.box-body-->
          <div class="text-center box-footer">
            <?= Html::a('Ver mas <i class="fa fa-arrow-circle-right"></i>','index.php/consulta/ver-prestamos?opt='.Yii::$app->params["consulActivos"].'&stat='.Yii::$app->params["status_vigente"],['id'=>'verAhijados','class'=>'small-box-footer'])?>
          </div>
        </div>

    </div>
   
    <div class="col-lg-3">
        <div class="box box-primary">
          <div class="box-header with-border">
            <i class="fa fa-bar-chart-o"></i>
            <h3 class="box-title">AL DIA</h3>        

            <?= Html::hiddenInput('ahi_riesgo_activos',$sumAlDia,['id'=>'pRActivos']) ?>
            <?= Html::hiddenInput('ahi_riesgo_mujeres',$montoMujeresAlDia,['id'=>'pRMujeres']) ?>
            <?= Html::hiddenInput('ahi_riesgo_hombres',$montoHombresAlDia,['id'=>'pRHombres']) ?> 
          </div>
          <div class="box-body">
           (<?= $ambosAlDia ?>) <i class="fa fa-usd"></i> Activos: <?= number_format($sumAlDia, 2, ',', ' ')?> <br />
            (<?= $mujeresAlDia ?>) <i class="fa fa-female" ></i> Mujeres: <?= number_format($montoMujeresAlDia, 2, ',', ' ')?> <br />
            (<?= $hombresAlDia ?>) <i class="fa fa-male" ></i>  Hombres:<?= number_format($montoHombresAlDia, 2, ',', ' ')?> <br /><br />
            <div id="bar-riesgo" style="height: 300px;"></div>
          </div>
          <!-- /.box-body-->
          <div class="text-center box-footer">
            <?= Html::a('Ver mas <i class="fa fa-arrow-circle-right"></i>','index.php/consulta/ver-prestamos?opt='.Yii::$app->params["consulAlDia"].'&stat='.Yii::$app->params["status_vencido"],['id'=>'verAhijados','class'=>'small-box-footer'])?>

          </div>
        </div>

    </div>

    <div class="col-lg-3">
        <div class="box box-primary">
          <div class="box-header with-border">
            <i class="fa fa-bar-chart-o"></i>

            <h3 class="box-title">EN MORA</h3>
            
            <?= Html::hiddenInput('ahi_mora_activos',$sumMora,['id'=>'pMActivos']) ?>
            <?= Html::hiddenInput('ahi_mora_mujeres',$montoMujeresMora,['id'=>'pMMujeres']) ?>
            <?= Html::hiddenInput('ahi_mora_hombres',$montoHombresMora,['id'=>'pMHombres']) ?> 
          </div>
          <div class="box-body">
            (<?= $ambosMora?>)<i class="fa fa-usd"></i> En Mora: <?= number_format($sumMora, 2, ',', ' ')?> <br />
           (<?= $mujeresMora ?>)  <i class="fa fa-female" ></i> Mujeres: <?= number_format($montoMujeresMora, 2, ',', ' ')?> <br />
            (<?= $hombresMora ?>) <i class="fa fa-male" ></i>  Hombres: <?= number_format($montoHombresMora, 2, ',', ' ')?> <br /><br />
            <div id="bar-mora" style="height: 300px;"></div>
          </div>
          <!-- /.box-body-->
          <div class="text-center box-footer">
            <?= Html::a('Ver mas <i class="fa fa-arrow-circle-right"></i>','index.php/consulta/ver-prestamos?opt='.Yii::$app->params["consulEnMora"].'&stat='.Yii::$app->params["status_mora"],['id'=>'verAhijados','class'=>'small-box-footer'])?>
          </div>
        </div>
    </div>

     <div class="col-lg-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <i class="fa fa-bar-chart-o"></i>

            <h3 class="box-title">SOLICITUDES</h3>

            <?= Html::hiddenInput('solicitudes_hoy',$sToday,['id'=>'sToday']) ?>
            <?= Html::hiddenInput('solicitudes_mes',$sMonth,['id'=>'sMonth']) ?>
            <?= Html::hiddenInput('solicitudes_acum',$sAcumulate,['id'=>'sAcumulate']) ?> 
            
          </div>
          <div class="box-body">
            <i class="fa fa-home" ></i>        
            </i> Acumuladas: <?= number_format($sAcumulate, 2, ',', ' ')?> <br />
            <i class="fa fa-calendar"></i>
            Mes: <?= number_format($sMonth, 2, ',', ' ')?> <br />
            <i class="fa fa-calendar"></i>
            Hoy: <?= number_format($sToday, 2, ',', ' ')?> <br /><br />
            <div id="bar-solicitudes" style="height: 300px;"></div>
          </div>
          <!-- /.box-body-->
          <!-- <div class="text-center box-footer">
            <a class="small-box-footer" href="#">Ver mas <i class="fa fa-arrow-circle-right"></i></a> -->
          </div>
        </div>
    </div>

  </div>
    
</div>

