<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\jui\DatePicker;
use yii\bootstrap\Tabs;


$this->registerJsFile(Yii::$app->request->baseUrl.'/js/miscelaneos.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/backend.js',['depends' => [\yii\web\JqueryAsset::className()]]);

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Solicitudes de Prestamo';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="solcitudes-prestamo">
<br /><br />

<?= Tabs::widget([
                'items' => [
                    [
                        'label' => 'Ver Solicitudes',
                         'content' => '<br /><br /><div class="form-inline">
                                         <div class="form-group">
                                                <div class="col-lg-3">'.Html::radio('radioSearch',false,['label'=>'Del','value'=>1,'id'=>'radioDel']).'</div>
                                                    <div class="col-lg-offset-2 col-lg-4">
                                                        '.Html::dropDownList('soliciSearch',null,[  'dia'=>'Dia',
                                                                                                    'diaAnterior'=>'Dia Anterior',
                                                                                                    'mes'=>'Mes',
                                                                                                    'mesAnterior'=>'Mes Anterior'],
                                                                                                 [  'class'=>'form-control',
                                                                                                    'id'=>'btnSearchSolic',
                                                                                                    'disabled'=>'disabled'] ).'
                                                    </div>
                                            <br /><br /><br />
                                            <div class="col-lg-3">
                                                '.Html::radio('radioSearch',false,['label'=>'Por Rango','value'=>2,'id'=>'radioRango']).'
                                            </div>
                                        <div class="col-lg-3 col-lg-offset-2">
                                            <label>Desde: </label>'.DatePicker::widget(['name'  => 'from_date',               
                                                                                        'dateFormat' => 'php:Y-m-d',
                                                                                        'options'=>[ 'id'=> 'date_min',
                                                                                                     'disabled'=>'disabled',
                                                                                                     'readonly'=>'readonly'],
                                                                                        // 'value'  => '23',                
                                                                                        // 'language' => 'ru',
                                                                                        // 'dateFormat' => 'Y-m-d',
                                                                                ]).'<br />(A-M-D)
                                        </div>
                                        <div class="col-lg-3 col-lg-offset-1">                
                                            <label>Hasta: </label>'.DatePicker::widget(['name'  => 'until_date',
                                                                                       'options'=>[ 'id'=> 'date_max',
                                                                                                    'disabled'=>'disabled',
                                                                                                    'readonly'=>'readonly'],
                                                                                        // 'value'  => '23',                
                                                                                        // 'language' => 'ru',
                                                                                        'dateFormat' => 'php:Y-m-d',
                                                                                ]).'<br />(A-M-D)
                                        </div>
                                        <br /><br /><br />
                                        <div class="col-lg-3">
                                            '.Html::radio('radioSearch',false,['label'=>'Total','value'=>3,'id'=>'radioTotal']).'
                                        </div>
                                      </div>
                                      <br /><br />
                                      <div class="text-center">
                                            <button id="btnSearchSolicitudes" class="btn btn-primary"><i class="fa fa-search"></i> Consultar </button>
                                      </div>
                                    </div>
                                    <br /><br />
                                    <div id="soliciGridView">
                                    '.GridView::widget(['dataProvider' => $dataProvider,    
                                            'columns' => [
                                                ['class' => 'yii\grid\SerialColumn'],
                                                [
                                                    'attribute' => 'Cliente',
                                                    'value'    => function($model){                                                            
                                                                    return $model->getIdClient()->one()->Shortname();
//								return $model->getIdClient()->one()->firstname;
                                                                },
                                                ],                        
                                                [
                                                    'attribute' => '# Cedula',
                                                    'value'    => function($model){
                                                                    if($model->id_client){
                                                                        return $model->getIdClient()
                                                                                     ->one()
                                                                                     ->identity;                                                                        
                                                                    }
                                                                },
                                                ],            
                                                [
                                                  'attribute' => 'Monto',
                                                  'value'     => function($model){                  
                                                                    return number_format($model->amount, 2, ',', ' ');
                                                                },
                                                ],                                                                                    
                                                [
                                                    'attribute' => 'Email',
                                                    'value'    => function($model){
                                                                    if($model->id_client){
                                                                        return $model->getIdClient()
                                                                                     ->one()
                                                                                     ->email;                                                                        
                                                                    }
                                                                },
                                                ],            
                                                [
                                                    'attribute' => 'Level',
                                                    'value'    => function($model){
                                                                    if($model->id_client){
                                                                        return $model->getIdClient()
                                                                                     ->one()
                                                                                     ->id_level;                                                                        
                                                                    }
                                                                },
                                                ],            
                                                [
                                                    'attribute' => 'Estatus',
                                                    'value'    => function($model){
                                                                    if($model->id_client){
                                                                        return $model->getIdStatus()             
                                                                                     ->one()
                                                                                     ->name;                                                
                                                                    }
                                                                },
                                                ],       
						[
                                                    'attribute' => 'Fecha/Hora',
                                                    'value'    => function($model){
                                                                    return $model->real_date_add;
                                                                },
                                                ],
                                                [
                                                    'attribute' => 'Fecha Asignada',
                                                    'value'    => function($model){
                                                                    return $model->date_add;
                                                                 },
                                                ],    
                                                                
                                                // [
                                                //     'attribute' => 'Padrino',
                                                //     'value'    => function($model){
                                                //                     if($model->id_client)                                                                                                             
                                                //                         return $model->getPadrino($model->idClient->id_godfather);                                               
                                                //                 },
                                                // ],               
                                    //            ['class' => 'yii\grid\ActionColumn','template' => '{view}'],
                                            ],
                                    ]).'</div><br /><br /><br />',            
                        'active' => true,                                                                                     
                        'options' => ['id' => 'view_request'],                                            
                    ], 
                    [
                        'label' => 'Crear formato',
                         'content' => '<div class="col-lg-12">
                         <br />
                         <p> Seleccione una de las solicitudes para asi crear un registro de los solicitantes en el banco de su preferencia</p><br />                        
                                       <div class="dropdown col-lg-3">'.HTML::activeDropDownList($modelRRequest,'date_request',
                                                                                         ArrayHelper::map($modelRRequest::find()->where(['registered'=>false])->all(),'date_request', 'date_request'),
                                                                                         ['class'=>'form-control',
                                                                                         'prompt'=>'Seleccione una fecha']).'
                                      </div><br /><br />
                                      <div id="rGridView"></div>
                                    </div>',                                    
                        'options' => ['id' => 'requests'],                                            
                    ],

                    [
                        'label' => 'Formatos generados',
                        'content' =>  '<br />
                         <p> Permite descargar los formatos generados para proceder al registro en el banco indicado.</p><br /><br />
                         '.GridView::widget(['dataProvider' => $dataProviderRR,                                                                                                      
                                                        'summary'      => false,                                            
                                                        'columns'      => [
                                                                             ['class' => 'yii\grid\SerialColumn'],
                                                                             [
                                                                              'attribute' => 'Formato',
                                                                              'value'     => function($model){
                                                                                                    return $model->getBankName($model->code_bank);
                                                                                                }                         
                                                                             ],                                                                      
                                                                             [
                                                                              'attribute' => 'Fecha',
                                                                              'value'     => function($model){
                                                                                                    return $model->date_request;
                                                                                                }                         
                                                                             ],  
                                                                             [
                                                                              'attribute' => 'Archivo',
                                                                              'value'     => function($model){
                                                                                                    return substr($model->url, -19);
                                                                                                }                         
                                                                             ],                                                                      
                                                                             ['class' => 'yii\grid\ActionColumn','template' => '{download}',                               
                                                                              'buttons'=>['download' => function ($url, $model, $key) {
                                                                                                            return Html::a('<span class="glyphicon glyphicon-download col-lg-offset-3" style="font-size: 1.4em;" aria-hidden="true"></span>',Yii::getAlias('@web').$model->url,['target'=>'_blank','download'=> substr($model->url, -19)]);      
                                                                                                        },                        
                                                                                        ]
                                                                             ],                                                                                                                                                             
                                                                        ],
                                                        
                                                     ]),           
                        'options' => ['id' => 'request_generated'],
                        
                    ],   
                   
                ],
                                                        
    ]); ?>

    </div>

</div>
