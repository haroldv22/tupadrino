<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\bootstrap\Tabs;
use yii\web\view;
use yii\widgets\ListView;
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/miscelaneos.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/backend.js',['depends' => [\yii\web\JqueryAsset::className()]]);

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Liquidaciones de Prestamos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="liquidaciones-prestamos">    
        
  <?php     
    echo Tabs::widget([
    'items' => [    
        [          
            'label' => 'Liquidaciones Pendientes', 
            'content' =>'<div class= col-lg-12><br />                        
			<span>Seleccione una fecha para cargar las solicitudes generadas por los clientes:   </span><br /><br />
                          <div class="col-lg-3">
                         <div class="dropdown">'.HTML::activeDropDownList($modelGRequest,'date_add',
                                                                              ArrayHelper::map($modelGRequest::find()->where(['id_status'=>Yii::$app->params['sGeneradas']])->all(), 'date_add', 'date_add'),
                                                                            ['class'=>'form-control','prompt'=>'Solicitudes Generadas']).'
                          </div></div><br /><br />
                        <div id="sGridView"></div>
                        </div>',            
            'active' => true,                                                                                     
            'options' => ['id' => 'request_pending'],
        ],     
        [
            'label' => 'Liquidaciones Aprobadas',
            'content' =>  GridView::widget(['dataProvider' => $dataProviderCLA,                                                                                                      
                                            'summary'      => false,                                            
                                            'columns'      => [
                                                                 ['class' => 'yii\grid\SerialColumn'],
                                                                 [
                                                                  'attribute' => 'Cliente',
                                                                  'value'     => function($model){
                                                                                        return $model->idClient->firstname.' '.
                                                                                                 $model->idClient->lastname;                                                                                                                                                                                                                      
                                                                                    }                         
                                                                 ],                                                                      
                                                                 [
                                                                  'attribute' => 'Fecha de Solicitud',
                                                                  'value'     => function($model){
                                                                                        return $model->date_add;                                                                                                                                                                                                                                
                                                                                    }                              
                                                                 ],                                                                 
                                                                 [
                                                                  'attribute' => 'email',
                                                                  'value'     => function($model){
                                                                                        return $model->idClient->email;                                                                                                                                   
                                                                                    }                              
                                                                 ],
                                                                 [
                                                                  'attribute' => 'Monto',
                                                                  'value'     => function($model){
                                                                                        return number_format($model->amount, 2, ',', ' ');                                                                                                                                                                                                      
                                                                                    }                                         
                                                                 ],       
                                                                  ['class' => 'yii\grid\ActionColumn','template' => '{accept}',           
                                                                  'buttons'=> ['accept' => 
                                                                                function ($url, $model, $key) {
                                                                                    return '<span class="glyphicon  glyphicon-ok text-success col-lg-offset-3" style="font-size: 1.4em;" aria-hidden="true"></span>';
                                                                                }      
                                                                              ],
                                                                 ],                                                                                                                                                                
                                                            ],
                                            
                                         ]),           
            'options' => ['id' => 'request_approved'],
            
        ],                
        [
            'label' => 'Liquidaciones Rechazadas',
            'content' =>  GridView::widget(['dataProvider' => $dataProviderCLR,                                                                                                       
                                            'summary'      => false,                                            
                                            'columns'      => [
                                                                 ['class' => 'yii\grid\SerialColumn'],
                                                                 [
                                                                  'attribute' => 'Cliente',
                                                                  'value'     => function($model){
                                                                                        return $model->idClient->firstname.' '.
                                                                                                 $model->idClient->lastname;                                                                                                                                 
                                                                                    }                              
                                                                 ],      
                                                                 [
                                                                  'attribute' => 'Fecha de Solicitud',
                                                                  'value'     => function($model){
                                                                                        return $model->date_add;                                                                                                                                           
                                                                                    }                              
                                                                 ],                                                                 
                                                                 [
                                                                  'attribute' => 'email',
                                                                  'value'     => function($model){
                                                                                        return $model->idClient->email;                                                                                                                                   
                                                                                    }                              
                                                                 ],
                                                                 [
                                                                  'attribute' => 'Monto',
                                                                  'value'     => function($model){
                                                                                        return number_format($model->amount, 2, ',', ' ');                                                                                                                                                                                                      
                                                                                    }                              
                                                                 ],                                                                                                                                        
                                                                 ['class' => 'yii\grid\ActionColumn','template' => '{reject}',           
                                                                  'buttons'=> ['reject' => 
                                                                                function ($url, $model, $key) {
                                                                                    return '<span class="glyphicon  glyphicon-remove-circle col-lg-offset-3 text-danger" style="font-size: 1.4em;" aria-hidden="true"></span>';
                                                                                }      
                                                                              ],
                                                                 ], 
                                                                
                                                
                                                            ],
                                         ]),           
            'options' => ['id' => 'request_reject'],
        ],
        
        
    ],
       
]); 
    ?>
    
</div>

