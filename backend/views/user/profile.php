<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\UserRole;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Peril del Usuario';
?>

<?= \Yii::$app->session->getFlash('flashMessage') ?>

<div class="profile-form">    

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    
    <?= $form->errorSummary($model); ?>
    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <?= $form->field($modelUpload,'imgProfile')->fileInput(); ?>
    
    <!-- <?//= $form->field($model, 'id_role')->dropDownList($items,   ['prompt'=>'Seleccione']) ?> -->

    <!-- <?//= $form->field($model, 'image')->dropDownList($items,   ['prompt'=>'Seleccione']) ?> -->

    <div class="form-group">
        <?= Html::submitButton('Modificar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
