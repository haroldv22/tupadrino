<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Usuaro: '. $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Trabajadores del Sistema', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <!--<h1><?//= Html::encode($this->title) ?></h1>-->

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Estas seguro que deseas eliminar estos datos ?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'firstname',
            'lastname',
            'email:email',
            'username',
             [                      
                'label' => 'Password (Encriptado)',
                'value' => $model->password                                                                              
            ],
            'password_temp',
            [                      
                'label' => 'Estatus',
                'value' => $model->getIdStatus()
                                ->one()
                                ->name                                                                                     
            ],
            [                      
                'label' => 'Role',
                'value' => $model->getIdRole()
                                ->one()
                                ->description                                                                                     
            ],
//            'id_role',
        ],
    ]) ?>

</div>
