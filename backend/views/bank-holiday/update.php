<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BankHoliday */

$this->title = 'Modificar dia feriado: ' . ' ' . $model->holiday;
$this->params['breadcrumbs'][] = ['label' => 'Calendario Bancario', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->holiday, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="bank-holiday-update">

    <!--<h1><?//= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
