<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Calendario Bancario';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bank-holiday-index">

    <!--<h1><?//= Html::encode($this->title) ?></h1>-->
    <br />
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
    
        <?= $form->errorSummary($modelHoliday); ?>
        
        <div class="form-group">
            <?= $form->field($model,'importFile')->fileInput(); ?>
        </div>
        <div class="form-group">
		<?= Html::submitButton('Importar <i class="fa fa-calendar"></i>', ['class' => 'btn btn-success']) ?>
        </div>
    <?php ActiveForm::end() ?>

<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'id',
            'holiday',
            'description',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?>
</div>
