<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CommissionRoundSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Comisiones a Padrinos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="commission-round-index">

    <?= $this->render('_search', ['modelCRS' => $searchModelCRS,
                                  'modelCL'=> $searchModelCL]); ?> 
    <br />
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],        
            [
                'attribute' => 'Padrinos',
                'value'    => function($model){
                                if($model->id_client){
                                    $client = $model->getIdClient()->one();                                    
                                    return $client->firstname.' '.$client->lastname;
                                }
                            },
            ], 
            [
                'attribute' => 'Estatus',
                'value'    => function($model){
                                if($model->id_client){
                                    return $model->getIdStatus()             
                                                 ->one()
                                                 ->name;                                                
                                }
                            },
            ],
            [
                'attribute' => 'Comision',
                'value'     => function($model){
                                    return number_format($model->to_pay, 2, ',', ' ');                                                          
                            },
            ],
            [
                'attribute' => 'Fecha Acreditacion',    
                'value'    => function($model){
                                    return $model->end_date;
                            },
            ],
            [
                'attribute' => 'Expectativa',
                'value'     => function($model){
                                    return number_format($model->expected_pay, 2, ',', ' ');                          
                                },
            ],
            ['class' => 'yii\grid\ActionColumn',
             'template' => '{view}',                                 
            ], 


            // 'end_date',

            // 'to_pay',
            // 'id_status',
            // 'add_date',
            // 'expected_pay',
            // 'risk_pay',

            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
