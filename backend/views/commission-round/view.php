<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\CommissionRound */

$this->title = 'Detalles de Comision';
$this->params['breadcrumbs'][] = ['label' => 'Comisiones a Padrinos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->getIdClient()->one()->firstname.' '.$model->getIdClient()->one()->lastname;
?>
<br /><br />
<div class="commission-round-view">
    
        <?= GridView::widget(['dataProvider' => $modelCommission,
                              'summary'      => false,                                            
                              'columns'      => [
                                                 ['class' => 'yii\grid\SerialColumn'],
                                                 [
                                                  'attribute' => 'Ahijados',
                                                  'value'     => function($model){
                                                                        return $model->idClient->firstname.'
                                                                         '.$model->idClient->lastname;
                                                                }                              
                                                 ],      
                                                 // [
                                                 //  'attribute' => 'Apellidos',
                                                 //  'value'     => function($model){
                                                 //                        return $model->idClient->lastname;                                                                                                                                 
                                                 //                    }                              
                                                 // ],      
                                                 [
                                                  'attribute' => 'Monto',
                                                  'value'     => function($model){
                                                                        return number_format($model->commission, 2, ',', ' ');
                                                                    }                              
                                                 ],                                                                 
                                                 [
                                                  'attribute' => 'Estatus',
                                                  'value'     => function($model){                                                                  
                                                                        return $model->getIdStatus()             
                                                                                     ->one()
                                                                                     ->name;                                                                                                                  
                                                                },                             
                                                 ],                                                 
                                                 [
                                                  'attribute' => 'Fecha de Pago',
                                                  'value'     => function($model){
                                                                        return $model->end_date;
                                                                    }                              
                                                 ],                                                                                                                                        
                                            ],
                         ]);
        ?>

</div>
