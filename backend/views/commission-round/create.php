<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CommissionRound */

$this->title = 'Create Commission Round';
$this->params['breadcrumbs'][] = ['label' => 'Commission Rounds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="commission-round-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
