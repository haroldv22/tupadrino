<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Status;

/* @var $this yii\web\View */
/* @var $model app\models\CommissionRoundSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="commission-round-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>


    <div class="container">

        <div class="col-lg-2 col-sm-2">
            <?php  echo $form->field($modelCL, 'firstname',[
                'inputOptions'=>[
                    'placeholder' => ' HAROLD',
                    'class'=>'form-control'
                ],]);
            ?>
        </div>
        
        <div class="col-lg-2 col-sm-2">
            <?php  echo $form->field($modelCL, 'lastname',[
                'inputOptions'=>[
                    'placeholder' => ' VILLALOBOS',
                    'class'=>'form-control'
                ],]); ?>
        </div>
        
        <div class="col-lg-2 col-sm-2">
            <?php  echo $form->field($modelCRS, 'to_pay',[
                'inputOptions'=>[
                    'placeholder' => '1000',
                    'class'=>'form-control'
                ],]); ?>
        </div>
        
        <!-- <div class="col-lg-2 col-sm-2"> -->
            <?php //echo $form->field($modelCRS, 'id_status')->dropDownList(
                            // Status::find()->select(['name', 'id'])
                                  // ->where(['between', 'id', 60, 63])
                                  // ->indexBy('id')
                                  // ->column(),['prompt'=>'Seleccione Estatus']);
                            // Status::find()->where([['between', 'id_status', 60, 63]])->all(),
                                                  
             ?>
        <!-- </div> -->
        

        <div class="col-lg-2 col-sm-2 text-left">
            <?= Html::submitButton('<i class="fa fa-search"></i>', ['class' => 'btn btn-primary btnCR-top']) ?>
            <?= Html::resetButton('<i class="fa fa-refresh"></i>', ['class' => 'btn btn-default btnCR-top']) ?>
        </div>

    </div>
    <!-- <?//= $form->field($model, 'id') ?> -->

    <!-- <?//= $form->field($model, 'id_client') ?> -->

    <!-- <?//= $form->field($model, 'end_date') ?> -->

    <!-- <?//= $form->field($model, 'to_pay') ?> -->

    <!-- <?//= $form->field($model, 'id_status') ?> -->

    <!-- <?php //echo $form->field($model, 'add_date') ?> -->

    <!-- <?php //echo $form->field($model, 'expected_pay') ?> -->

    <!-- <?php //echo $form->field($model, 'risk_pay') ?> -->

  <!--   <div class="form-group">
        <?//= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?//= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>
