<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CommissionRound */

$this->title = 'Update Commission Round: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Commission Rounds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="commission-round-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
