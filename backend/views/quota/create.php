<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Quota */

$this->title = 'Crear Cuota';
$this->params['breadcrumbs'][] = ['label' => 'Cuotas (Niveles de Credito)', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="quota-create">

    <!-- <h1><?//= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
	'listLevel' => $listLevel
    ]) ?>

</div>
