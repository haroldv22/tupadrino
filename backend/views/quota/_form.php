<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\Quota */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="quota-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'days')->textInput() ?>

    <?= HTML::label('Nivel de Credito',null) ?>
    <?= HTML::activeDropDownList($model,'id_level',  
		                               ArrayHelper::map($listLevel,'id','id'), 
		                                 ['class'=>'form-control', 
		                                 'prompt'=>'Selecciona el nivel de credito']) ?>    
	<br />
    <?= $form->field($model, 'alias')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear cuota' : 'Modificar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
