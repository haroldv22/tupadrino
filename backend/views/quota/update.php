<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Quota */

$this->title = 'Modificar Cuota de: ' . ' ' . $model->days.' dias';
$this->params['breadcrumbs'][] = ['label' => 'Cuotas (Niveles de Credito)', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Cuota de '.$model->days.' dias', 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="quota-update">

    <!--<h1><?//= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
	'listLevel' => $listLevel
    ]) ?>

</div>
