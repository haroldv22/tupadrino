<div class="box box-primary">
                     
  <div class="box-body no-padding">
    <div class="mailbox-read-info">
      <h3><?= $model->subject ?></h3>
      <h5>to: <?= $model->getIdClient()->one()->email ?>
        <span class="mailbox-read-time pull-right"><?= $model->date_sent ?></span></h5>
    </div>
    <!-- /.mailbox-read-info -->
    <div class="mailbox-controls with-border text-center">
      <div class="btn-group">
        <button title="Delete" data-container="body" data-toggle="tooltip" class="btn btn-default btn-sm" type="button">
          <i class="fa fa-trash-o"></i></button>
        <!-- <button title="Reply" data-container="body" data-toggle="tooltip" class="btn btn-default btn-sm" type="button">
          <i class="fa fa-reply"></i></button>
        <button title="" data-container="body" data-toggle="tooltip" class="btn btn-default btn-sm" type="button" data-original-title="Forward">
          <i class="fa fa-share"></i></button> -->
      </div>
      <!-- /.btn-group -->
      <button title="Print" data-toggle="tooltip" class="btn btn-default btn-sm" type="button">
        <i class="fa fa-print"></i></button>
    </div>
    <!-- /.mailbox-controls -->
    <div class="mailbox-read-message">
      <?= $model->message ?>
    </div>
    <!-- /.mailbox-read-message -->
  </div>                   
</div>