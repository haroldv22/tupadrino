<?php use yii\widgets\LinkPager; ?>
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Email Enviados</h3>

              <div class="box-tools pull-right">
                <div class="has-feedback">
                  <input type="text" placeholder="Search Mail" class="form-control input-sm">
                  <span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="mailbox-controls">
                <!-- Check all button -->
                <button id="checkAll" class="btn btn-default btn-sm checkbox-toggle" type="button"><i class="fa fa-square-o"></i>
                </button>
                <div class="btn-group">
                  <button id="btnDelete" class="btn btn-default btn-sm" type="button"><i class="fa fa-trash-o"></i></button>
                  <!-- <button type="button" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button> -->
                  <!-- <button type="button" class="btn btn-default btn-sm"><i class="fa fa-share"></i></button> -->
                </div>
                <!-- /.btn-group -->
                <!-- <button class="btn btn-default btn-sm" type="button"><i class="fa fa-refresh"></i></button> -->
                <div class="pull-right">
<!--                 <?//= LinkPager::widget(['pagination' => $pages]) ?> -->
                  <!-- 1-50/200 -->
                 <!--  <div class="btn-group">
                    <button class="btn btn-default btn-sm" type="button"><i class="fa fa-chevron-left"></i></button>
                    <button class="btn btn-default btn-sm" type="button"><i class="fa fa-chevron-right"></i></button>
                  </div> -->
                  <!-- /.btn-group -->
                </div>
                <!-- /.pull-right -->
              </div>
              <div class="table-responsive mailbox-messages">
                <table class="table table-hover table-striped">
                  <tbody>
                  <?php 
                        foreach ($model as $key => $value) {
                          
                          echo '<tr data-key='.$value->id.'>
                                <td><input type="checkbox" name="checkMsg"></td>
                                <td class="mailbox-name"><a href="#"><i class="fa fa-paper-plane-o" aria-hidden="true"></i>
'.$value->getIdClient()->one()->Shortname().' </a></td>
                                 <td class="mailbox-subject"><b>'.$value->subject.'</b> '.$value->ShortMessage().'
                                </td>                    
                                <td class="mailbox-date">'.$value->date_sent.'</td>
                                </tr>';
                        }
                  ?>                  
                  </tbody>
                </table>
                <!-- /.table -->
              </div>
              <!-- /.mail-box-messages -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer no-padding">
              <div class="mailbox-controls">
                <!-- Check all button -->
               <!--  <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
                </button> -->
                <!-- <div class="btn-group"> -->
                  <!-- <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button> -->
                  <!-- <button type="button" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button> -->
                  <!-- <button type="button" class="btn btn-default btn-sm"><i class="fa fa-share"></i></button> -->
                <!-- </div> -->
                <!-- /.btn-group -->
                <!-- <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
                <div class="pull-right"> -->
                  <!-- 1-50/200 -->
                  <!-- <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                  </div> -->
                  <!-- /.btn-group -->
                </div>
                <!-- /.pull-right -->
              </div>
            </div>
          
