<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\bootstrap\Tabs;

$this->registerJsFile(Yii::$app->request->baseUrl.'/js/miscelaneos.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/backend.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/app.gmsg.js',['depends' => [\yii\web\JqueryAsset::className()]]);

$this->title = 'Generacion de Mensajes';

?>

<!-- Content Wrapper. Contains page content -->
  <div class="">

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <a id="view-compose" href="#" class="btn btn-primary btn-block margin-bottom">Redactar</a>
          <!-- /. box -->
        <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Contactos</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
				
				<div class="col-lg-12">
					<label for="busqueda">Enviar a:</label>
				</div>
            	<div class="col-lg-12">            		
            		<?= Html::radio('radioContact',false,['label'=>'Todos','value'=>'9'])?>            	                  
            	</div>
            	<div class="col-lg-12">            		
            		<?= Html::radio('radioContact',false,['label'=>'Nivel 1','value'=>'1'])?>            	                  
            	</div>
            	<div class="col-lg-12">            		
            		<?= Html::radio('radioContact',false,['label'=>'Nivel 2','value'=>'2'])?>            	                  
            	</div>
            	<div class="col-lg-6">
            	    <?= Html::radio('radioContact',false,['label'=>'Nivel 3','value'=>'3'])?>            	                             		
            		<br />
            	</div>              
            </div>
           <!-- // /.box-body  -->
        </div>
          <!-- /.box -->

          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Carpetas</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <!-- <li class="active"><a href="#"><i class="fa fa-inbox"></i> Inbox -->
                  <!-- <span class="label label-primary pull-right">12</span></a></li> -->
                <li><a href="#" id="view-emails"><i class="fa fa-envelope-o"></i> Email Enviados (<?= $msgsSent ?>) </a></li>
                <li><a href="#" id="view-sms"><i class="fa fa-envelope-o"></i> SMS Enviados (<?= $msgsSms ?>)</a></li>
                <!-- <li><a href="#"><i class="fa fa-file-text-o"></i> Drafts</a></li> -->
                <!-- <li><a href="#"><i class="fa fa-filter"></i> Junk <span class="label label-warning pull-right">65</span></a> -->
                </li>
                <li><a href="#" id="trash"><i class="fa fa-trash-o"></i> Papelera (<?= $msgsTrash ?>)</a></li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- /.col -->

 		<div class="col-md-9" id="contenido">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Redactar nuevo mensaje </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <!-- <div class="form-group">
                <input class="form-control" placeholder="To:">
              </div> -->
              <div class="form-group">
                <input class="form-control" placeholder="Asunto:" id="compose-subject">
              </div>
              <div class="form-group">
                <?= HTML::textarea('txtEmail', '' , ['rows'=>'10','cols'=>'90','id'=>'compose-textarea','class'=>'form-control'] ) ?>
                    <!-- <textarea id="compose-textarea" class="form-control" style="height: 300px;"></textarea> -->
                    <br />
                    <button id="btnSms" class="btn btn-primary"><i class="fa fa-mobile-phone"></i> Redactar sms</button>
                    <br /><br />

                    <?= HTML::textarea('txtSms', '' , ['rows'=>'2','cols'=>'90','id'=>'compose-sms','class'=>'form-control','readOnly'=>true,
                    'maxlength'=>160,'placeholder'=>'Solo 160 caracteres' ] ) ?>
                    <!-- <textarea rows="2" cols="50" class="form-control" id="compose-textarea-sms"></textarea> -->


              </div>
              <div class="form-group">
                
                <!-- <div class="btn btn-default btn-file">
                  <i class="fa fa-paperclip"></i> Attachment
                  <input type="file" name="attachment">
                </div>
                <p class="help-block">Max. 32MB</p> -->
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <div class="pull-right">
                <!-- <button type="button" class="btn btn-default"><i class="fa fa-pencil"></i> Draft</button> -->
                <button type="submit" class="btn btn-primary" id="btnSentMsg"><i class="fa fa-envelope-o"></i> Enviar</button>
              </div>
              <!-- <button type="reset" class="btn btn-default"><i class="fa fa-times"></i> Discard</button> -->
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->

        
       
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

