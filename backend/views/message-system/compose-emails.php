<div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Redactar nuevo mensaje </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <!-- <div class="form-group">
        <input class="form-control" placeholder="To:">
      </div> -->
      <div class="form-group">
        <input id="compose-subject" placeholder="Asunto:" class="form-control">
      </div>
      <div class="form-group">
            <textarea style="height: 300px" class="form-control" id="compose-textarea"></textarea>
            <br />
            <button id="writeSMS" class="btn btn-primary"><i class="fa fa-mobile-phone"></i> Redactar sms</button>
            <textarea style="display:hidden; height: 300px" class="form-control" id="compose-textarea-sms"></textarea>
      </div>
      <div class="form-group">
        <!-- <div class="btn btn-default btn-file">
          <i class="fa fa-paperclip"></i> Attachment
          <input type="file" name="attachment">
        </div>
        <p class="help-block">Max. 32MB</p> -->
      </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
      <div class="pull-right">
        <!-- <button type="button" class="btn btn-default"><i class="fa fa-pencil"></i> Draft</button> -->
        <button type="submit" class="btn btn-primary" id="btnSentMsg"><i class="fa fa-envelope-o"></i> Enviar</button>
      </div>
      <!-- <button type="reset" class="btn btn-default"><i class="fa fa-times"></i> Discard</button> -->
    </div>
    <!-- /.box-footer -->
</div>