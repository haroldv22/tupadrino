<div class="box box-primary">
                     
  <div class="box-body no-padding">
    <div class="mailbox-read-info">
      <h3><?= $model->getIdStatus()->one()->name ?></h3>
      <h5>Para: <?= $model->getIdClient()->one()->getAddresses()->one()->cellphone ?>
        <span class="mailbox-read-time pull-right"><?= $model->date_add ?></span></h5>
    </div>
    
    <div class="mailbox-read-message">
      <?= $model->message ?>
    </div>
    <!-- /.mailbox-read-message -->
  </div>                   
</div>