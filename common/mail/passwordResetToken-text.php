<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/login']);
?>
Hola <?= Html::encode($client->firstname.' '.$client->lastname ) ?>

La siguiente es tu nueva contraseña:
<?=  $pass ?>

Sigue el siguiente link para iniciar sesion.
<?= $resetLink ?>
