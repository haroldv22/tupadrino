<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/login']);
?>
<div class="password-reset">
    <p>Hola <?= Html::encode($client->firstname.' '.$client->lastname ) ?>,</p>

    <p>La siguiente es tu nueva contraseña:</p>
    <p><strong><?= $pass ?></strong></p>
    <p>Sigue el siguiente link para iniciar sesion.</p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>
