<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $username
 * @property string $password
 * @property string $password_temp
 * @property integer $id_status
 * @property integer $id_role
 *
 * @property Status $idStatus
 * @property UserRole $idRole
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    const STATUS_ACTIVE = 10;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'email', 'username', 'password', 'id_status', 'id_role'], 'required'],
            [['id_status', 'id_role'], 'integer'],
            [['firstname', 'lastname'], 'string', 'max' => 50],
            [['email', 'username'], 'string', 'max' => 75],
            [['password', 'password_temp'], 'string', 'max' => 128],
            [['email'], 'unique'],
            [['username'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Nombre',
            'lastname' => 'Apellido',
            'email' => 'email del usuario',
            'username' => 'username',
            'password' => 'password',
            'password_temp' => 'password temporal',
            'id_status' => 'status',
            'id_role' => 'role del usuario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'id_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRole(){
        return $this->hasOne(UserRole::className(), ['id' => 'id_role']);
    }
    
    public static function findIdentity($id){                        
        return static::findOne(['id' => $id, 'id_status' => self::STATUS_ACTIVE]);        
    }
     
     /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username){
        return static::findOne(['username' => $username, 'id_status' => self::STATUS_ACTIVE]);
    }
    
     /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }
    
    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
    
      /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
//        if (!static::isPasswordResetTokenValid($token)) {
//            return null;
//        }
//        return static::findOne([
//            'password_reset_token' => $token,
//            'id_status' => self::STATUS_ACTIVE,
//        ]);
    }
    
    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
//        if (empty($token)) {
//            return false;
//        }
//        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
//        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
//        return $timestamp + $expire >= time();
    }
    
    /**
     * @inheritdoc
     */
    public function getId(){
        return $this->getPrimaryKey();
    }
    
    /**
     * @inheritdoc
     */
    public function getAuthKey(){
//        return $this->auth_key;
    }
    
    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey){
        return $this->getAuthKey() === $authKey;
    }

}
