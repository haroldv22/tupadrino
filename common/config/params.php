<?php

return [
    'codigoProveedor' =>'01910142872100044346', //Cuenta BNC de donde se Liquidara
    'adminEmail' => 'haroldestebanvillalobos@gmail.com',    
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'superUserRole' => 50,   // Roles de SuperUsuario
    'adminUserRole' => 20,   // Roles de Usuario Administrador
    'simpleUserRole' => 10,  // Roles de Usuario Simple                                                                       
    'genderM' => 'm',       // Genero Masculino                   
    'genderF' => 'f',       // Genero Masculino                   
    'Female' => 'Femenino',                      
    'Male' => 'Masculino',         
    'option_aprobado' => 1, 
    'option_rechazado' => 2,        
    'status_pendiente' => 20, // Solicitud de Prestamo Pendiente
    'status_aprobadas' =>21, // Solicitud de Prestamo Aprobada
    'status_rechazadas' => 22, // Solicitud de Prestamo Rechazada
    'status_pagados' => 40,  // Usuarios con Prestamos Pagados
    'status_vigente' => 41, // Usuarios con Prestamos Vigentes
    'status_vencido' => 42, // Usuarios con Prestamos Vencidos
    'status_mora' => 43, // Usuarios con Prestamos en Mora
    'invi_aceptada'=> 12, // Invitacion del sistema a usuarios aceptada
    'invi_espera'=> 13, // Invitacion del sistema a usuarios en espera
    'invi_expirada'=> 14, // Invitacion del sistema a usuarios expirada
    'sGeneradas' => 47, // Archivos Generados (Solicitudes de Prestamos)
    'sProcesadas' => 48, // Archivos Procesados (Solicitudes de Prestamos)
    'usuario_activo' =>10, 
    'limit_sentEmail' => 2, // Limite de envios de invitaciones por parte del sistema
    'option_cRate'=> 1,     //   Para de la tabla config la variable commission_rate
    'option_mRate'=>2, //Para dela tabla config la variable Mora_rate o Arrears_rate
    'procesando'=> 50, // Registro de Pago Estatus procesando solicitud
    'confirmado'=> 51, // Registro de Pago confirmado
    'rechazado'=> 52, // Registro de Pago rechazado   
    'commission_activa'=>60,    // Ronda Comisiones que estan Activas (Pendientes)
    'commission_pagada'=>62,    // Ronda Comisiones que estan Pagadas (Canceladas)
    'commission_bloqueada'=>63,  // Ronda Comisiones que estan Bloqueadas (Perdidas)
    'commission_riesgo' =>66,   // Ronda Comissiones en riesgo
    'commission_mora' =>65,   // Ronda Comissiones en riesgo
    'commission_puntual'=>67,   // Comisiones que estan dentro de los usuarios quienes seran ahijados
    'commission_finalizada'=>68,   // Comisiones que estan dentro de los usuarios quienes seran ahijados
    'img_profile'=>'/resources/users/default/user.jpg',
    'tipo_cuota' => 'cuota', // Tipos de pago Seccion Cobranza
    'tipo_mora'=>    'mora', // Tipos de pago Seccion Cobranza
    'tipo_ambas' => 'ambas', // Tipos de pago Seccion Cobranza
    'cuota_generada' => 30, // cuotas de  prestamo
    'cuota_noGenerada' => 31, // cuotas de  prestamo
    'cuota_pagada' => 32, // cuotas de  prestamo
    'cuota_vencida' => 33, // cuotas de  prestamo
    'cuota_mora' => 34, // cuotas de  prestamo
    'contactos_todos' => 9, // Todos los clientes (Generacion de Mensajes)
    'msg_enviado' => 70, // estatus del mensaje "enviado" (Generacion de Mensajes)
    'msg_eliminado' => 71, // estatus del mensaje  "eliminado" (Generacion de Mensajes)
    'cAuto' => 'option1', // Opciones para mostrar y hacer conciliaciones en cobranza de manera Automatica
    'cManual' => 'option2', // Opciones para mostrar y hacer conciliaciones en cobranza de manera Manual    
    'expired_days'=>5, //dias que tiene una cuota para expirar es decir de la fecha de pagoaestar en vencimiento
    'consulAhijados' => 40,   // Opcion del DropdownList de seccion Consulta (Prestamos Activos)
    'consulActivos' => 41,   // Opcion del DropdownList de seccion Consulta (Prestamos Activos)
    'consulAlDia' => 42,   // Opcion del DropdownList de seccion Consulta (Prestamos Activos)
    'consulEnMora' => 43,   // Opcion del DropdownList de seccion Consulta (Prestamos Activos)
    'mora_impagada' => 45, // mora status Impagada
    'mora_pagada' => 46 // mora Status Pagada

];

